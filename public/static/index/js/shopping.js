$(function(){
	
	//每次进入此页面都检查下被选中价格及被选中产品个数
	TotalPrice();
	btncolor();
	function btncolor(){
		if( $(".goodsCheck:checked").length==0 ){ //是否没有产品被选中
			$(".jui_gwc_btn").addClass('jui_bg_ccc');  //没有选中按钮为灰色
		}else{
			$(".jui_gwc_btn").removeClass('jui_bg_ccc'); //有产品被选中按钮为绿色
		}
	}
	/******------------分割线-----------------******/
  // 点击商品按钮
  $(".goodsCheck").click(function() {
    var goods = $(this).closest(".jui_gwc_bar").find(".goodsCheck"); //获取所有商品
    var goodsC = $(this).closest(".jui_gwc_bar").find(".goodsCheck:checked"); //获取所有被选中的商品
	if (goods.length == goodsC.length) { //如果选中的商品等于所有商品
	    $("#AllCheck").prop('checked', true); //全选按钮被选中
        TotalPrice();
	}else{
		$("#AllCheck").prop('checked', false); //else全选按钮不被选中 
        TotalPrice();
		}
    //按钮颜色控制
	if( goodsC.length==0 ){
		$(".jui_gwc_btn").addClass('jui_bg_ccc');
		}else{
		$(".jui_gwc_btn").removeClass('jui_bg_ccc');
			}
  });
    
  // 点击全选按钮
  $("#AllCheck").click(function() {
    if ($(this).prop("checked") == true) { //如果全选按钮被选中
      $(".goodsCheck").prop('checked', true); //所有按钮都被选中
	  $(".jui_gwc_btn").removeClass('jui_bg_ccc');
      TotalPrice();
    } else {
      $(".goodsCheck").prop('checked', false); //else所有按钮不全选
	  $(".jui_gwc_btn").addClass('jui_bg_ccc');
      TotalPrice();
    }
  });
  
	//计算
	function TotalPrice(){
         var allprice = 0; //总价
		 var all_jifen = 0;
		 $(".goodsCheck").each(function() { //循环店铺里面的商品
				 if ( $(this).is(":checked") ) { //如果该商品被选中
				  var num = parseInt($(this).parents(".jui_gwc_list").find(".jui_gwc_pro_num").val()); //得到商品的数量
				  var price_num = $(this).parents(".jui_gwc_list").find(".price").length;
				  var jifen_num = $(this).parents(".jui_gwc_list").find(".pro_jifen").length;
				  if( price_num == 0 ){
					  var jifen = parseFloat($(this).parents(".jui_gwc_list").find(".pro_jifen").text()); //得到商品的积分
					  var total_jifen = jifen * num; //计算单个商品的总价
					  var total = 0;
				  }else if( jifen_num == 0 ){
				      var price = parseFloat($(this).parents(".jui_gwc_list").find(".price").text()); //得到商品的单价
					  var total = price * num;
					  var total_jifen =0;
					  }	 
				  allprice += total; //计算该店铺的总价
				  all_jifen += total_jifen;
				}
				//console.log(allprice);
				$(".ShopTotal").text(allprice.toFixed(2));
				$(".total_jifen").text(all_jifen.toFixed(2));
         });
	}
  
  
	 
	//加减按钮 
	$(".jui_add").click(function(){
		var t=$(this).parent().find('.jui_gwc_pro_num');
		$(this).siblings('.jui_min').removeClass('jui_bgc_dadada');
		t.val(parseInt(t.val())+1);
		TotalPrice();
	})
	$(".jui_min").click(function(){
		var t=$(this).parent().find('.jui_gwc_pro_num');
		if( parseInt(t.val())>1 ){
			t.val(parseInt(t.val())-1);
		 }else{
			t.val(1);
			box_timer('产品不能小于1哦');//自动消失弹出框 调用public.js
			//timebox();
	     }
		 TotalPrice();
	})

	
	
});
