(function(win) {
        var docEl = win.document.documentElement;
        var time;

        function refreshRem() {
                var width = docEl.getBoundingClientRect().width;
                //console.log(width)
				//750效果图尺寸，直接除以100得到rem单位
                /* 最大750字体太大了，下面最大540目前还不知道有没有问题 */
				/*if (width > 540) {
                        width = 540;
                }*/
				if (width > 540) {
                        width = 540;
                }
                var rem = width / 10;
                docEl.style.fontSize = rem + 'px';
        }

        win.addEventListener('resize', function() {
                clearTimeout(time);
                time = setTimeout(refreshRem, 1);
        }, false);
        win.addEventListener('pageshow', function(e) {
                if (e.persisted) {
                        clearTimeout(time);
                        time = setTimeout(refreshRem, 1);
                }
        }, false);
        refreshRem();
})(window);

