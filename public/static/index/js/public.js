//判断页面主体高度
$(document).ready(function(){
    //获取主体高度
	//window.screen.availHeight获取屏幕工作区域的高度和宽度（去掉状态栏）
	//document.body.clientHeight网页可见区域的高度和宽度（不加边线）
	//document.body.offsetHeight网页可见区域的高度和宽度（加边线）
	//document.body.scrollHeight网页全文的高度和宽度
	//console.log($('div.jui_footer').length);
	//console.log($('div.jui_top_bar').length);
	if($('.jui_footer').length>0){
	   if($('.jui_top_bar').length>0){
	      /*头部底部都存在*/
		  var zt_height=document.body.clientHeight-$(".jui_footer").outerHeight()-$(".jui_top_bar").outerHeight();
	   }else{
		   /*底部存在，头部不存在*/
		   var zt_height=document.body.clientHeight-$(".jui_footer").outerHeight();
		   }
	}else{
	  if($('div.jui_top_bar').length>0){
	      /*头部存在底部不存在*/
		  var zt_height=document.body.clientHeight-$(".jui_top_bar").outerHeight();
	   }else{
		   /*头部底部都不存在*/
		   var zt_height=document.body.clientHeight;
		   }	  
	}
	$(".jui_main").css("min-height",zt_height);
	$(".jui_main").css("height",zt_height);
	
	
	

	//控制图片为正方形且居中不变形显示
	$('.pro_img').height($('.pro_img').width());
	$('.pro_img').each(function(){
		var img_w = $(this).find('img').width();
		var img_h = $(this).find('img').height();
		if( img_w>img_h ){
			$(this).find('img').css('height','100%');
		}else{
			$(this).find('img').css('width','100%');
		}
	});
	
	
	
	
	/*如果富文本上传的图片，去掉一些行内样式*/
	$(".procon_ztcon img").removeAttr("width");	
	$(".procon_ztcon img").removeAttr("height");	
	$(".procon_ztcon img").removeAttr("alt");	
	$(".procon_ztcon img").removeAttr("width");	
	$(".procon_ztcon img").removeAttr("style");	
	$(".procon_ztcon img").removeAttr("title");	
		
	
	/*如果富文本上传的图片，去掉一些行内样式*/
	$(".newscon_zt img").removeAttr("width");	
	$(".newscon_zt img").removeAttr("height");	
	$(".newscon_zt img").removeAttr("alt");	
	$(".newscon_zt img").removeAttr("width");	
	$(".newscon_zt img").removeAttr("style");	
	$(".newscon_zt img").removeAttr("title");	
		
});




/*提示类弹出框 1.5S后自动消失*/
function box_timer(text) {
	var oDiv = document.createElement('div');
	oDiv.className = 'jui_box_point_bg';
	oDiv.innerHTML += "<div class='jui_box_point_bar'>"+text+"</div>";
	document.body.appendChild(oDiv);
	setTimeout(function(){
		oDiv.remove();
		},1500)
	}

/* 单图上传封装成函数，可以用于多个单图上传，每一个调用一下函数即可 */
var input = document.createElement('input');
input.type = 'file';
input.accept = 'image/*';
function upimg(barid,imgid,valid){
    $(barid).on('click', function () {
        var reader = new FileReader();
        input.onchange = function() {
            var file = this.files[0];
            if(!/(jpg|png|jpeg)$/.test(file.name)) {
                 //box_timer('仅支持png,jpg格式图片');
                return;
            }
            reader.readAsDataURL(file);
            reader.onload = function() {
                $(imgid).attr('src', this.result);
                $(valid).val(this.result);
            };
            input = document.createElement('input');
            input.type = 'file';
            input.accept = 'image/*';
        };
        input.click();
    });
}


