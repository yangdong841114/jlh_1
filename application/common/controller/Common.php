<?php

namespace app\common\controller;

use think\Controller;
use think\Db;

/**
 * 项目公共控制器
 * @package app\common\controller
 */
class Common extends Controller
{
    /**
     * 初始化
      
     */
    protected function initialize()
    {
        // 后台公共模板
        $this->assign('_admin_base_layout', config('admin_base_layout'));
        // 当前配色方案
        $this->assign('system_color', config('system_color'));
        // 输出弹出层参数
        $this->assign('_pop', $this->request->param('_pop'));
    }

    /**
     * 获取筛选条件
      
     * @alter 小乌 <82950492@qq.com>
     * @return array
     */
    final protected function getMap()
    {
        $search_field     = input('param.search_field/s', '', 'trim');
        $keyword          = input('param.keyword/s', '', 'trim');
        $filter           = input('param._filter/s', '', 'trim');
        $filter_content   = input('param._filter_content/s', '', 'trim');
        $filter_time      = input('param._filter_time/s', '', 'trim');
        $filter_time_from = input('param._filter_time_from/s', '', 'trim');
        $filter_time_to   = input('param._filter_time_to/s', '', 'trim');
        $select_field     = input('param._select_field/s', '', 'trim');
        $select_value     = input('param._select_value/s', '', 'trim');
        $search_area      = input('param._s', '', 'trim');
        $search_area_op   = input('param._o', '', 'trim');

        $map = [];

        // 搜索框搜索
        if ($search_field != '' && $keyword !== '') {
            $map[] = [$search_field, 'like', "%$keyword%"];
        }

        // 下拉筛选
        if ($select_field != '') {
            $select_field = array_filter(explode('|', $select_field), 'strlen');
            $select_value = array_filter(explode('|', $select_value), 'strlen');
            foreach ($select_field as $key => $item) {
                if ($select_value[$key] != '_all') {
                    $map[] = [$item, '=', $select_value[$key]];
                }
            }
        }

        // 时间段搜索
        if ($filter_time != '' && $filter_time_from != '' && $filter_time_to != '') {
            $map[] = [$filter_time, 'between time', [$filter_time_from . ' 00:00:00', $filter_time_to . ' 23:59:59']];
        }

        // 表头筛选
        if ($filter != '') {
            $filter         = array_filter(explode('|', $filter), 'strlen');
            $filter_content = array_filter(explode('|', $filter_content), 'strlen');
            foreach ($filter as $key => $item) {
                if (isset($filter_content[$key])) {
                    $map[] = [$item, 'in', $filter_content[$key]];
                }
            }
        }

        // 搜索区域
        if ($search_area != '') {
            $search_area = explode('|', $search_area);
            $search_area_op = explode('|', $search_area_op);
            foreach ($search_area as $key => $item) {
                list($field, $value) = explode('=', $item);
                $value = trim($value);
                $op    = explode('=', $search_area_op[$key]);
                if ($value != '') {
                    switch ($op[1]) {
                        case 'like':
                            $map[] = [$field, 'like', "%$value%"];
                            break;
                        case 'between time':
                        case 'not between time':
                            $value = explode(' - ', $value);
                            if ($value[0] == $value[1]) {
                                $value[0] = date('Y-m-d', strtotime($value[0])) . ' 00:00:00';
                                $value[1] = date('Y-m-d', strtotime($value[1])) . ' 23:59:59';
                            }
                        default:
                            $map[] = [$field, $op[1], $value];
                    }
                }
            }
        }
        return $map;
    }

    /**
     * 获取字段排序
     * @param string $extra_order 额外的排序字段
     * @param bool $before 额外排序字段是否前置
      
     * @return string
     */
    final protected function getOrder($extra_order = '', $before = false)
    {
        $order = input('param._order/s', '');
        $by    = input('param._by/s', '');
        if ($order == '' || $by == '') {
            return $extra_order;
        }
        if ($extra_order == '') {
            return $order . ' ' . $by;
        }
        if ($before) {
            return $extra_order . ',' . $order . ' ' . $by;
        } else {
            return $order . ' ' . $by . ',' . $extra_order;
        }
    }

    /**
     * 渲染插件模板
     * @param string $template 模板名称
     * @param string $suffix 模板后缀
      
     * @return mixed
     */
    /**
     * 渲染插件模板
     * @param string $template 模板文件名
     * @param string $suffix 模板后缀
     * @param array $vars 模板输出变量
     * @param array $config 模板参数
      
     * @return mixed
     */
    final protected function pluginView($template = '', $suffix = '', $vars = [], $config = [])
    {
        $plugin_name = input('param.plugin_name');

        if ($plugin_name != '') {
            $plugin = $plugin_name;
            $action = 'index';
        } else {
            $plugin = input('param._plugin');
            $action = input('param._action');
        }
        $suffix = $suffix == '' ? 'html' : $suffix;
        $template = $template == '' ? $action : $template;
        $template_path = config('plugin_path') . "{$plugin}/view/{$template}.{$suffix}";
        return parent::fetch($template_path, $vars, $config);
    }

    final  protected  function set_config()
    {
        $config = Db::name('Config')->where(array('id' => 1))->find();
        if (!empty($config)) {
            return $config;
        }
        return 0;
    }

    final protected function get_maxNum($userId = '')
    {
        return Db::name('deal')->where(array('uid' => $userId, 'd_type' => 2, 'd_date' => date('Y-m-d')))->where('d_credit_1 > 0')->sum('d_total');
    }

    final  protected function check_user()
    {
        $url_login          = url('Login/login');
        $config             = Db::name('Config')->where(array('id' => 1))->find();
        $url_account_list   = url('Login/account_list');
        $z_uid              = cookie('z_uid');
        if (session('uid') && session('uid') != '') {
            $uid            = intval(session('uid'));
            $user            = Db::name('User')->where(array('id' => $uid, 'm_del' => 0))->find();
            if (!empty($user)) {
                if ($user['m_lock'] == 1) {
                    if ($z_uid > 0) {
                        echo "<script>
                                setTimeout(function() {
                                    window.location.href = '$url_account_list';
                                },500);
                    </script>";
                        exit;
                    } else {
                        echo "<script>
                                setTimeout(function() {
                                    window.location.href = '$url_login';
                                },500);
                    </script>";
                        exit;
                    }
                }
                if ($user['m_avatar']) {
                    $m_avatar   = explode('.', $user['m_avatar']);
                    if (count($m_avatar) > 2) {
                        $user['m_avatar'] = $m_avatar[1] . '.' . $m_avatar[2];
                    }
                } else {
                    $user['m_avatar']   = $config['w_logo'];
                }
                if ($user['m_car_img']) {
                    $m_car_img  = explode(',', $user['m_car_img']);
                    $img        = array();
                    if (!empty($m_car_img) && count($m_car_img) >= 2) {
                        foreach ($m_car_img as $k => $v) {
                            $car_img   = explode('.', $v);
                            if (count($car_img) > 2) {
                                $img[]   = $car_img[1] . '.' . $car_img[2];
                            } else {
                                $img[]   = $car_img[0] . '.' . $car_img[1];
                            }
                        }
                    } else {
                        $car_img   = explode('.', $m_car_img[0]);
                        $img[0]    = $car_img[0] . '.' . $car_img[1];
                        $img[1]    = $config['w_logo'];
                    }
                    $user['m_car_img']  = $img;
                } else {
                    $user['m_car_img']  = array($config['w_logo'], $config['w_logo']);
                }

                session('user_session', serialize($user));
                return $user;
            }
        }
        if ($z_uid > 0) {
            echo "<script>
                    setTimeout(function() {
                        window.location.href = '$url_account_list';
                    },500)
        </script>";
            exit;
        } else {
            echo "<script>
                    setTimeout(function() {
                        window.location.href = '$url_login';
                    },500)
        </script>";
            exit;
        }
    }

    //获取用户信息
    final protected function getUser($uid)
    {
        $user = Db::name('user')->where('id', $uid)->field('id,m_nickname,m_avatar,m_phone,m_account')->find();
        if (empty($user)) {
            return '无';
        }
        $user['m_avatar'] = $user['m_avatar'] == '' ? get_file_path($this->getConfig('w_logo')) : $user['m_avatar'];
        return '昵称：' . $user['m_nickname'] . '<br/>账号：' . $user['m_account'] . '<br/>手机：' . $user['m_phone'];
    }

    final protected function getIdentity($uid)
    {
        $user = Db::name('user')->where(array('id' => $uid))->where("m_name<>'' and m_car_id<>'' and m_car_img<>''")->find();
        if (empty($user)) {
            return '无';
        }
        return '姓名：' . $user['m_name'] . '<br/>身份证号：' . $user['m_car_id'];
    }


    // 获取系统设置内容
    final protected function getConfig($name = null)
    {
        $config = Db::name('Config')->where('id', 1)->find();
        if ($name == null) {
            return $config;
        } else {
            return $config[$name];
        }
    }

    final protected function level_name()
    {
        $data = Db::name('level')->order('id asc')->column('sort,name');
        return $data;
    }

    final protected function bankCode()
    {
        $bank = array(
            40000   =>    '中国银行',
            40001   =>    '南京银行',
            40002   =>    '招商银行',
            40003   =>    '工商银行',
            40004   =>    '广发银行',
            40005   =>    '杭州银行',
            40006   =>    '中国农业银行',
            40007   =>    '中国邮政储蓄银行',
            40008   =>    '浙商银行',
            40009   =>    '渤海银行',
            40011   =>    '东亚银行（中国）有限公司',
            40026   =>    '上海银行',
            40031   =>    '江苏银行',
            40032   =>    '宁波银行',
            40033   =>    '平安银行',
            40035   =>    '温州银行',
            40059   =>    '徽商银行',
            40060   =>    '重庆银行',
            40061   =>    '哈尔滨银行',
            40088   =>    '包商银行',
            40162   =>    '上海农商银行',
            40180   =>    '北京农村商业银行',
            40188   =>    '苏州银行',
            40217   =>    '中国建设银行',
            40218   =>    '交通银行',
            40219   =>    '中信银行',
            40221   =>    '上海商业银行',
            40222   =>    '永隆银行',
            40223   =>    '中国光大银行',
            40226   =>    '华夏银行',
            40228   =>    '中国民生银行',
            40229   =>    '兴业银行',
            40230   =>    '上海浦东发展银行',
            40240   =>    '江西银行',
        );
        return $bank;
    }
    final protected function bankOpenCode()
    {
        $OpenCode = array(
            1000    =>    '北京市',
            1100    =>    '天津市',
            1210    =>    '石家庄市',
            1240    =>    '唐山市',
            1260    =>    '秦皇岛市',
            1270    =>    '邯郸市',
            1310    =>    '邢台市',
            1340    =>  '保定市',
            1380    =>  '张家口市',
            1410    =>  '承德市',
            1430    =>  '沧州市',
            1460    =>  '廊坊市',
            1480    =>  '衡水市',
            1610    =>  '太原市',
            1620    =>  '大同市',
            1630    =>  '阳泉市',
            1640    =>  '长治市',
            1680    =>  '晋城市',
            1690    =>  '朔州市',
            1710    =>  '忻州市',
            1730    =>  '吕梁',
            1750    =>  '晋中市',
            1770    =>  '临汾市',
            1810    =>  '运城市',
            1910    =>  '呼和浩特市',
            1920    =>  '包头市',
            1930    =>  '乌海市',
            1940    =>  '赤峰市',
            1960    =>  '呼伦贝尔市',
            1980    =>  '兴安盟',
            1990    =>  '通辽市',
            2010    =>  '锡林郭勒盟',
            2030    =>  '乌兰察布市',
            2050    =>  '鄂尔多斯市',
            2070    =>  '巴彦淖尔市',
            2080    =>  '阿拉善盟',
            2210    =>  '沈阳市',
            2220    =>  '大连市',
            2230    =>  '鞍山市',
            2240    =>  '抚顺市',
            2250    =>  '本溪市',
            2260    =>  '丹东市',
            2270    =>  '锦州市',
            2276    =>  '葫芦岛市',
            2280    =>  '营口市',
            2290    =>  '阜新市',
            2310    =>  '辽阳市',
            2320    =>  '盘锦市',
            2330    =>  '铁岭市',
            2340    =>  '朝阳市',
            2410    =>  '长春市',
            2420    =>  '吉林市',
            2430    =>  '四平市',
            2440    =>  '辽源市',
            2450    =>  '通化市',
            2460    =>  '白山市',
            2470    =>  '白城市',
            2490    =>  '延边自治州',
            2520    =>  '松原市',
            2610    =>  '哈尔滨市',
            2640    =>  '齐齐哈尔市',
            2650    =>  '大庆市',
            2660    =>  '鸡西市',
            2670    =>  '鹤岗市',
            2680    =>  '双鸭山市',
            2690    =>  '佳木斯市',
            2710    =>  '伊春市',
            2720    =>  '牡丹江市',
            2740    =>  '七台河市',
            2760    =>  '绥化市',
            2780    =>  '黑河市',
            2790    =>  '大兴安岭市',
            2900    =>  '上海市',
            3010    =>  '南京市',
            3020    =>  '无锡市',
            3030    =>  '徐州市',
            3040    =>  '常州市',
            3050    =>  '苏州市',
            3060    =>  '南通市',
            3070    =>  '连云港市',
            3080    =>  '淮安市',
            3090    =>  '宿迁市',
            3110    =>  '盐城市',
            3120    =>  '扬州市',
            3128    =>  '泰州市',
            3140    =>  '镇江市',
            3310    =>  '杭州市',
            3320    =>  '宁波市',
            3330    =>  '温州市',
            3350    =>  '嘉兴市',
            3360    =>  '湖州市',
            3370    =>  '绍兴市',
            3380    =>  '金华市',
            3410    =>  '衢州市',
            3420    =>  '舟山市',
            3430    =>  '丽水市',
            3450    =>  '台州市',
            3610    =>  '合肥市',
            3620    =>  '芜湖市',
            3630    =>  '蚌埠市',
            3640    =>  '淮南市',
            3650    =>  '马鞍山市',
            3660    =>  '淮北市',
            3670    =>  '铜陵市',
            3680    =>  '安庆市',
            3710    =>  '黄山市',
            3720    =>  '阜阳市',
            3722    =>  '亳州市',
            3740    =>  '宿州市',
            3750    =>  '滁州市',
            3760    =>  '六安市',
            3771    =>  '宣城市',
            3781    =>  '巢湖市',
            3790    =>  '池州市',
            3910    =>  '福州市',
            3930    =>  '厦门市',
            3940    =>  '莆田市',
            3950    =>  '三明市',
            3970    =>  '泉州市',
            3990    =>  '漳州市',
            4010    =>  '南平市',
            4030    =>  '宁德市',
            4050    =>  '龙岩市',
            4210    =>  '南昌市',
            4220    =>  '景德镇市',
            4230    =>  '萍乡市',
            4240    =>  '九江市',
            4260    =>  '新余市',
            4270    =>  '鹰谭市',
            4280    =>  '赣州市',
            4310    =>  '宜春市',
            4330    =>  '上饶市',
            4350    =>  '吉安市',
            4370    =>  '抚州市',
            4510    =>  '济南市',
            4520    =>  '青岛市',
            4530    =>  '淄博市',
            4540    =>  '枣庄市',
            4550    =>  '东营市',
            4560    =>  '烟台市',
            4580    =>  '潍坊市',
            4610    =>  '济宁市',
            4630    =>  '泰安市',
            4634    =>  '莱芜市',
            4650    =>  '威海市',
            4660    =>  '滨州市',
            4680    =>  '德州市',
            4710    =>  '聊城市',
            4730    =>  '临沂市',
            4732    =>  '日照市',
            4750    =>  '菏泽市',
            4910    =>  '郑州市',
            4920    =>  '开封市',
            4930    =>  '洛阳市',
            4950    =>  '平顶山市',
            4960    =>  '安阳市',
            4970    =>  '鹤壁市',
            4980    =>  '新乡市',
            5010    =>  '焦作市',
            5020    =>  '濮阳市',
            5030    =>  '许昌市',
            5040    =>  '漯河市',
            5050    =>  '三门峡市',
            5060    =>  '商丘市',
            5080    =>  '周口市',
            5110    =>  '驻马店市',
            5130    =>  '南阳市',
            5150    =>  '信阳市',
            5210    =>  '武汉市',
            5220    =>  '黄石市',
            5230    =>  '十堰市',
            5260    =>  '宜昌市',
            5280    =>  '襄樊市',
            5286    =>  '随州市',
            5310    =>  '鄂州市',
            5320    =>  '荆门市',
            5330    =>  '黄冈市',
            5350    =>  '孝感市',
            5360    =>  '咸宁市',
            5370    =>  '荆州市',
            5410    =>  '恩施州',
            5510    =>  '长沙市',
            5520    =>  '株州市',
            5530    =>  '湘潭市',
            5540    =>  '衡阳市',
            5550    =>  '邵阳市',
            5570    =>  '岳阳市',
            5580    =>  '常德市',
            5590    =>  '张家界市',
            5610    =>  '益阳市',
            5620    =>  '娄底市',
            5630    =>  '郴州市',
            5650    =>  '永州市',
            5670    =>  '怀化市',
            5690    =>  '湘西',
            5810    =>  '广州市',
            5820    =>  '韶关市',
            5840    =>  '深圳市',
            5850    =>  '珠海市',
            5860    =>  '汕头市',
            5865    =>  '揭阳市',
            5869    =>  '潮州市',
            5880    =>  '佛山市',
            5890    =>  '江门市',
            5910    =>  '湛江市',
            5920    =>  '茂名市',
            5930    =>  '肇庆市',
            5937    =>  '云浮市',
            5950    =>  '惠州市',
            5960    =>  '梅州市',
            5970    =>  '汕尾市',
            5980    =>  '河源市',
            5990    =>  '阳江市',
            6010    =>  '清远市',
            6020    =>  '东莞市',
            6030    =>  '中山市',
            6110    =>  '南宁市',
            6128    =>  '崇左市',
            6140    =>  '柳州市',
            6155    =>  '来宾市',
            6170    =>  '桂林市',
            6210    =>  '梧州市',
            6225    =>  '贺州市',
            6230    =>  '北海市',
            6240    =>  '玉林市',
            6242    =>  '贵港市',
            6261    =>  '百色市',
            6281    =>  '河池市',
            6311    =>  '钦州市',
            6330    =>  '防城港市',
            6410    =>  '海口市',
            6420    =>  '三亚市',
            6510    =>  '成都市',
            6530    =>  '重庆市',
            6550    =>  '自贡市',
            6560    =>  '攀枝花市',
            6570    =>  '泸州市',
            6580    =>  '德阳市',
            6590    =>  '绵阳市',
            6610    =>  '广元市',
            6620    =>  '遂宁市',
            6630    =>  '内江市',
            6636    =>  '资阳市',
            6650    =>  '乐山市',
            6652    =>  '眉山市',
            6710    =>  '宜宾市',
            6730    =>  '南充市',
            6737    =>  '广安市',
            6750    =>  '达州市',
            6758    =>  '巴中市',
            6770    =>  '雅安市',
            6790    =>  '阿坝州',
            6810    =>  '甘孜州',
            6840    =>  '凉山州',
            7010    =>  '贵阳市',
            7020    =>  '六盘水市',
            7030    =>  '遵义市',
            7050    =>  '铜仁市',
            7070    =>  '兴义市',
            7090    =>  '毕节市',
            7110    =>  '安顺市',
            7130    =>  '黔东南州',
            7150    =>  '都匀市',
            7310    =>  '昆明市',
            7340    =>  '昭通市',
            7360    =>  '曲靖市',
            7380    =>  '楚雄市',
            7410    =>  '玉溪市',
            7430    =>  '红河州',
            7450    =>  '文山州',
            7470    =>  '普洱市',
            7490    =>  '西双版纳州',
            7510    =>  '大理市',
            7530    =>  '保山市',
            7540    =>  '德宏州',
            7550    =>  '丽江市',
            7560    =>  '怒江州',
            7570    =>  '迪庆州',
            7580    =>  '临沧市',
            7700    =>  '拉萨市',
            7720    =>  '昌都市',
            7740    =>  '山南地区',
            7760    =>  '日喀则市',
            7790    =>  '那曲地区',
            7810    =>  '阿里地区',
            7830    =>  '林芝地区',
            7910    =>  '西安市',
            7920    =>  '铜川市',
            7930    =>  '宝鸡市',
            7950    =>  '咸阳市',
            7970    =>  '渭南市',
            7990    =>  '汉中市',
            8010    =>  '安康市',
            8030    =>  '商洛市',
            8040    =>  '延安市',
            8060    =>  '榆林市',
            8210    =>  '兰州市',
            8220    =>  '嘉峪关市',
            8230    =>  '金昌市',
            8240    =>  '白银市',
            8250    =>  '天水市',
            8260    =>  '酒泉市',
            8270    =>  '张掖市',
            8280    =>  '武威市',
            8290    =>  '定西市',
            8310    =>  '陇南地区',
            8330    =>  '平凉市',
            8340    =>  '庆阳市',
            8360    =>  '临夏州',
            8380    =>  '甘南州',
            8510    =>  '西宁市',
            8540    =>  '海北州',
            8550    =>  '黄南州',
            8560    =>  '海南州',
            8570    =>  '果洛州',
            8580    =>  '玉树州',
            8590    =>  '海西州',
            8710    =>  '银川市',
            8720    =>  '石嘴山市',
            8731    =>  '吴忠市',
            8733    =>  '中卫市',
            8741    =>  '固原市',
            8810    =>  '乌鲁木齐市',
            8820    =>  '克拉玛依市',
            8830    =>  '吐鲁番市',
            8840    =>  '哈密市',
            8850    =>  '昌吉市',
            8870    =>  '博乐市',
            8880    =>  '库尔勒市',
            8910    =>  '阿克苏市',
            8930    =>  '阿图什市',
            8940    =>  '喀什市',
            8960    =>  '和田市',
            8980    =>  '伊宁市',
            9010    =>  '塔城市',
            9020    =>  '阿勒泰市',
            9028    =>  '石河子市',
        );
        return $OpenCode;
    }
}
