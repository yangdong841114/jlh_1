<?php
namespace app\index\controller;
use think\Db;
use think\facade\Env;
use think\Session;
/**
 * 前台首页控制器
 * @package app\index\controller
 */
class Order extends Home
{

    public function shop(){
        $config                 = $this->set_config();
        $user                   = $this->check_user();
        $banner                 = Db::name('Banner')->order('b_index asc')->select();
        $cate                   = Db::name('Cate')->where(array('c_del'=>0))->order('c_index asc')->select();
        $integral_goods         = Db::name('Goods')->where(array('g_del'=>0,'g_lock'=>1,'g_tui'=>1,'g_type'=>2))->order('last_time desc')->select();
        foreach($integral_goods as &$row){
            $row['g_img'] = explode(',',$row['g_pic'])[0];
        }
        $goods                  = Db::name('Goods')->where(array('g_del'=>0,'g_lock'=>1,'g_tui'=>1,'g_type'=>1))->order('last_time desc')->select();
        foreach($goods as &$item){
            $item['g_img'] = explode(',',$item['g_pic'])[0];
        }
        $this->assign(array('config'=>$config,'user'=>$user,'banner'=>$banner,'cate'=>$cate,'integral_goods'=>$integral_goods,'goods'=>$goods));
        return $this->fetch();
    }

    public function pro_list(){
        $config                 = $this->set_config();
        $user                   = $this->check_user();
        $get_type               = input('type');
        $get_id                 = input('id');
        $get_key                = input('key');
        $type                   = isset($get_type)?intval($get_type):0;
        $id                     = isset($get_id)?intval($get_id):0;
        $key                    = isset($get_key)?$get_key:0;
        $goods                  = goods_list($type,$key,$id);
        $this->assign(array('config'=>$config,'user'=>$user,'goods'=>$goods,'type'=>$type));
        return $this->fetch();
    }

    public function pro_con(){
        $config                 = $this->set_config();
        $user                   = $this->check_user();
        $get_id                 = input('id');
        $id                     = isset($get_id)?intval($get_id):0;
        $goods                  = Db::name('Goods')->where(array('g_del'=>0,'id'=>$id))->find();
        $goods['g_pic']         = explode(',',$goods['g_pic']);
        $this->assign(array('config'=>$config,'user'=>$user,'goods'=>$goods));
        return $this->fetch();
    }

    //确认订单
    public function qrdd(){
        $config                 = $this->set_config();
        $user                   = $this->check_user();
        $get_num                = input('num');
        $get_id                 = input('g_id');
        $get_aid                = input('a_id');
        $gid                    = isset($get_id)?$get_id:0;
        $num                    = isset($get_num)?intval($get_num):0;
        $aid                    = isset($get_aid)?intval($get_aid):0;
        return json(array('code' => 0, 'msg' => '暂未开放兑换购买'));
        if(!$user['m_pay_pwd']){
           $url=url('Center/jymm_set');
           echo "<script>
                alert('请先设置交易密码');
                setTimeout(function() {
                    window.location.href=$url;
                },500);
            </script>";
        }
         if($this->request->post()){
             $fields        =   input('post.');
             $address       =   Db::name('Address')->where(array('id'=>$fields['a_id'],'uid'=>$user['id']))->find();
             if(empty($address)){
                 return json(array('code' => 0, 'msg' => '收货地址有误'));
             }
             $goods         =   Db::name('Goods')->where(array('id'=>$fields['g_id'],'g_del'=>0))->find();
             if(empty($goods)){
                 return json(array('code' => 0, 'msg' => '商品信息有误'));
             }
             if($goods['g_lock']==0){
                 return json(array('code' => 0, 'msg' => '当前商品已下架'));
             }
             if($goods['g_ku']<$fields['g_num']){
                 return json(array('code' => 0, 'msg' => '商品库存不足'));
             }
             $g_num         =   $fields['g_num'];
             $money         =   $user['m_balance'];   //可用余额
             $integral      =   $user['m_integral'];  //可用积分
             $g_price       =   0;
             if($goods['g_type']==1){  //购物专区
                 $g_price   =   $goods['g_price']*$g_num;
                 if($money < $g_price){
                     return json(array('code' => 0, 'msg' => '您的可用余额不足'));
                 }
             }
             $g_credit      = $goods['g_credit']*$g_num;
             if($integral<$g_credit){
                 return json(array('code' => 0, 'msg' => '您的可用积分不足'));
             }
             //4.插入order表
             $order =  array(
                 'gid'          =>  $fields['g_id'],
                 'o_buy_num'    =>  $fields['g_num'],
                 'o_type'       =>  $goods['g_type'],
                 'o_price'      =>  $goods['g_price'],
                 'o_credit'     =>  $goods['g_credit'],
                 'o_credit_1'   =>  $g_price,
                 'o_credit_2'   =>  $g_credit,
                 'uid'          =>  $user['id'],
                 'o_code'       =>  createNO(3, 18,'Order','o_code'),
                 'o_info'       =>  $fields['o_mes'],
                 'aid'          =>  $fields['a_id'],
                 'o_status'     =>  0,
                 'o_addtime'    =>  time(),
                 'last_time'    =>  time(),
             );
             $res_id = Db::name('Order')->insertGetId($order);
             if($res_id){
                 $res_1  = Db::name('Goods')->where(array('id'=>$fields['g_id']))->update(array('g_ku'=>$goods['g_ku']-$fields['g_num'],'g_sale'=>$goods['g_sale']+$fields['g_num'],'last_time'=>time()));
                 $res_2  = 0;
                if($g_price>0){
                    $res_2 = do_logs($user['id'], 1,'m_balance', -$g_price, '商城消费,购买'.$goods['g_title'].'消耗余额');
                }
                if($g_credit>0){
                    $res_2 = do_logs($user['id'], 2,'m_integral', -$g_credit, '商城消费,购买'.$goods['g_title'].'消耗积分');
                }
                 $res_3 = Db::name('Order')->where(array('id'=>$res_id))->update(array('o_status'=>1,'o_pay_time'=>time(),'last_time'    =>  time()));
                 if($res_1 && $res_2 && $res_3){
                     return json(array('code' => 1, 'msg' => '下单成功'));
                 }else{
                     return json(array('code' => 0, 'msg' => '下单失败'));
                 }
             }else{
                 return json(array('code' => 0, 'msg' => '下单失败'));
             }
         }
        $goods                  = Db::name('Goods')->where(array('g_del'=>0,'id'=>$gid))->find();
        $goods['g_img']         = explode(',',$goods['g_pic'])[0];
        if($aid){
            $address            = Db::name('Address')->where(array('id'=>$aid))->find();;
        }else{
            $address            = Db::name('Address')->where(array('uid'=>$user['id']))->order('a_is_default desc')->find();
        }
        if(empty($address)){
            $a_status           = 0;
        }else{
            $a_status           = 1;
        }
        $this->assign(array('config'=>$config,'user'=>$user,'goods'=>$goods,'num'=>$num,'status'=>$a_status,'address'=>$address));
        return $this->fetch();
    }

    //订单列表
    public function order_list(){
        $config                 = $this->set_config();
        $user                   = $this->check_user();
        $get_type               = input('type');
        $type                   = isset($get_type)?intval($get_type):0;
        $o_status               = 0;
        $where                  = '';
        if($type>0){
            $where              = "o_status ='$type'";
        }
        $order                  = Db::name('Order a')
            ->join('w_goods g','g.id=a.gid','LEFT')
            ->where(array('uid'=>$user['id']))
            ->where($where)
            ->field('a.*,g.g_title,g.g_pic,g.g_price,g.g_credit,g.g_type')
            ->order('a.last_time desc')
            ->select();
        if(!empty($order)){
            foreach ($order as &$row){
                $row['g_img']           = explode(',',$row['g_pic'])[0];
                $row['o_addtime']       = !$row['o_addtime']?'':date("Y-m-d H:i",$row['o_addtime']);
                $row['o_send_time']     = !$row['o_send_time']?'':date("Y-m-d H:i",$row['o_send_time']);
                $row['o_take_time']     = !$row['o_take_time']?'':date("Y-m-d H:i",$row['o_take_time']);
                $row['o_pay_time']      = !$row['o_pay_time']?'':date("Y-m-d H:i",$row['o_pay_time']);
            }
            $o_status            = 1;
        }
        $this->assign(array('config'=>$config,'user'=>$user,'order'=>$order,'status'=>$o_status,'type'=>$type));
        return $this->fetch();

    }

    //订单详情
    public function order_con(){
        $config                 = $this->set_config();
        $user                   = $this->check_user();
        $get_id                 = input('id');
        $id                     = isset($get_id)?intval($get_id):0;
        $order                  = Db::name('Order')->where(array('id'=>$id))->find();
        if(empty($order)){
           $this->error('数据有误');
        }
        $address                    = Db::name('Address')->where(array('id'=>$order['aid']))->find();
        $goods                      = Db::name('Goods')->where(array('id'=>$order['gid']))->find();
        $goods['g_img']             = explode(',',$goods['g_pic'])[0];
        $order['o_addtime']         = !$order['o_addtime']?'':date("Y-m-d H:i",$order['o_addtime']);
        $order['o_send_time']       = !$order['o_send_time']?'':date("Y-m-d H:i",$order['o_send_time']);
        $order['o_take_time']       = !$order['o_take_time']?'':date("Y-m-d H:i",$order['o_take_time']);
        $order['o_pay_time']        = !$order['o_pay_time']?'':date("Y-m-d H:i",$order['o_pay_time']);
        $this->assign(array('config'=>$config,'user'=>$user,'order'=>$order,'goods'=>$goods,'address'=>$address));
        return $this->fetch();
    }

    //确认收获
    public function taken(){
        $config                 = $this->set_config();
        $user                   = $this->check_user();
        $uid                    = $user['id'];
        if($this->request->post()){
            $fields        =    input('post.');
            $order_id      =    $fields['order_id'];
            $order         =    Db::name('Order')->where(array('id'=>$order_id,'o_status'=>2))->find();
            if(empty($order)){
                return json(array('code' => 0, 'msg' => '订单信息不存在'));
            }
            $time          =   time();
            $res           =   Db::name('Order')->where(array('id'=>$order_id,'o_status'=>2))->update(array('o_take_time'=>$time,'last_time'=>$time,'o_status'=>3));
            if($res){
                return json(array('code' => 1, 'msg' => '确认收货成功'));
            }else{
                return json(array('code' => 0, 'msg' => '确认收货失败，请与客服联系'));
            }
        }
    }

    //删除订单
    public function del_order(){
        $config                 = $this->set_config();
        $user                   = $this->check_user();
        if($this->request->post()){
            $fields        =   input('post.');
            $order_id      =    $fields['order_id'];
            $order         =    Db::name('Order')->where(array('id'=>$order_id,'o_status'=>3))->find();
            if(empty($order)){
                return json(array('code' => 0, 'msg' => '订单信息不存在'));
            }
            $res           =   Db::name('Order')->where(array('id'=>$order_id,'o_status'=>3))->delete();
            if($res){
                return json(array('code' => 1, 'msg' => '删除成功'));
            }else{
                return json(array('code' => 0, 'msg' => '删除失败，请稍后重试'));
            }
        }
    }

    // 确认订单页面
    public function confirm_order()
    {

        $config = $this->set_config();
        $user = $this->check_user();

        $param = $this->request->param();
        $get_id = $param['gid']; // 商品id
        $etype = $param['etype']; // 兑换类型 1积分兑换 2持仓兑换
        $exchange_id = $param['exchange_id'];
        if($etype == 1){
            $get_num = $param['num']; // 数量
            
        }elseif($etype == 2){
            $get_num = Db::name('deal')->where(['id' => $param['exchange_id'], 'uid' => $user['id']])->value('d_sell_num');
        }

        $gid = isset($get_id)?$get_id:0;
        $number = isset($get_num)?intval($get_num):0;

        if(!$user['m_pay_pwd']){
            $url=url('Center/jymm_set');
            echo "<script>
                 alert('请先设置交易密码');
                 setTimeout(function() {
                     window.location.href=$url;
                 },500);
            </script>";
        }

        // 商品信息
        $good_info = Db::name('Goods')->where(array('g_del'=>0,'id'=>$gid))->find();

        if($this->request->post()){
            if(empty($param['real_name'])) $this->error('请填写姓名');
            if(empty($param['phone'])) $this->error('请填写手机号');
            if(empty($param['a_city'])) $this->error('请选择地区');
            if(empty($param['a_detail'])) $this->error('请填写详细地址');
            if(empty($param['etype'])) $this->error('缺少参数，请返回重试');
            if(empty($param['num'])) $this->error('缺少参数，请返回重试');
            if(empty($param['gid'])) $this->error('缺少参数，请返回重试');
            $number = $param['num']; // 兑换的数量

            $_match = '/^0?1[3|4|5|6|7|8|9][0-9]\d{8}$/';
            if(!preg_match($_match, $param['phone'])){
                $this->error('手机号格式不正确');
            }

            // 判断用户地区 并且获取邮费
            $_postage_money = 7; // 邮费
            if(is_int(mb_strpos($param['a_city'], '新疆')) || is_int(mb_strpos($param['a_city'], '西藏')) || is_int(mb_strpos($param['a_city'], '内蒙')) || is_int(mb_strpos($param['a_city'], '宁夏')) || is_int(mb_strpos($param['a_city'], '青海'))){
                $_postage_money = 10;
            }

            $postage_money = ceil($param['num']/6)*$_postage_money;
            
            // 判断用户余额是否足够
            if($user['m_balance'] < $postage_money){
                $this->error('账户余额不足支付运费');
            }

            $time = time(); // 当前时间戳
            $g_credit = 0; // 积分总价
            $g_price = 0; // 订单总价
            // deal 表的查询条件
            $deal_where = [
                'id' => $param['exchange_id'],
                'uid' => $user['id']
            ];

            // 如果是积分兑换
            if($param['etype'] == 1){
                $_integral = $param['num']*$good_info['g_credit']; // 需要支付的积分
                if($user['m_integral'] < $_integral){
                    $this->error('您的积分不足');
                }
                $g_credit = $good_info['g_credit']*$number;
                
            }elseif($param['etype'] == 2){
                $deal_info = Db::name('deal')->where($deal_where)->find();
                if(!$deal_info){
                    $this->error('当前持仓数量不足');
                }
                if($deal_info['d_status'] !== 2){
                    $this->error('当前持仓数量不足');
                }
                $g_price = 0;
                $number = $deal_info['d_sell_num'];
            }

            // 判断商品信息
            if(empty($good_info)){
                $this->error('商品信息有误');
            }
            if($good_info['g_lock'] == 0){
                $this->error('当前商品已下架');
            }
            if($good_info['g_type'] != 2){
                $this->error('商品未开放购买兑换');
            }
            if($good_info['g_ku'] < $number){
                $this->error('商品库存不足');
            }

            // 添加地址信息
            $address_data = [
                'a_name' => $param['real_name'],
                'a_phone' => $param['phone'],
                'a_city' => $param['a_city'],
                'a_detail' => $param['a_detail'],
                'uid' => $user['id'],
                'a_time' => $time
            ];
            // $address_id = 0;
            $address_id = Db::name('address')->insertGetId($address_data);
            
            // 添加订单信息
            $order_data = [
                'gid' => $good_info['id'],
                'o_buy_num' => $number,
                'o_type' => $good_info['g_type'],
                'o_price' => $good_info['g_price'],
                'o_credit' => $good_info['g_credit'],
                'o_credit_1' => $g_price,
                'o_credit_2' => $g_credit,
                'uid' => $user['id'],
                'o_code' => createNO(3, 18,'Order','o_code'),
                'o_info' => '',
                'aid' => $address_id,
                'o_status' => 0,
                'o_addtime' => $time,
                'last_time' => $time,
            ];
            $order_id = Db::name('order')->insertGetId($order_data);

            if($param['etype'] == 2){
                Db::name('deal')->where($deal_where)->update(['d_status' => 5, 'last_time' => $time]);
                Db::name('fine')->where(['did' => $param['exchange_id']])->update(['f_status' => 4, 'f_finish_time' => $time]);
                // 如果是 持仓兑换 添加日志 操作持仓的日志
                $deal_log_data = [
                    'uid' => $user['id'],
                    'info' => '商城消费，兑换'.$good_info['g_title'].'，更改持仓状态',
                    'deal_id' => $param['exchange_id'],
                    'add_time' => time(),
                ];
                Db::name('exchange_deal_log')->insert($deal_log_data);
            }else{
                // 积分兑换 减少积分 添加日志
                $res_2 = do_logs($user['id'], 2, 'm_integral', -$g_credit, '商城消费，购买'.$good_info['g_title'].'消耗积分');
            }

            // 支付运费
            do_logs($user['id'], 1, 'm_balance', -$postage_money, '商城消费，购买'.$good_info['g_title'].'支付运费');

            // 存储运费 以及 日志 
            $_user_account = '1800100000020';
            $_user = Db::name('user')->where('m_account', $_user_account)->find();
            if($_user){
                do_logs($_user['id'], 1, 'm_balance', $postage_money, '用户'.$user['m_account'].'商城消费，购买'.$good_info['g_title'].'支付运费');
            }
            $exchange_code = '02000016';
            $postage_log = [
                'l_date' => date('Ymd'),
                'add_time' => time(),
                'l_time' => date('YmdHis'),
                'from_user' => $user['m_account'],
                'to_user' => !empty($_user['m_account'])?$_user['m_account']:'',
                'postage_money' => $postage_money,
                'exchange_code' => $exchange_code,
                'field_1' => 1,
                'field_2' => 1,
                'field_3' => 3,
            ];
            Db::name('postage_log')->insert($postage_log);

            // 更新商品库存
            $new_goods_data = [
                'g_ku' => $good_info['g_ku']-$number,
                'g_sale' => $good_info['g_sale']+$number,
                'last_time' => $time
            ];
            Db::name('goods')->where('id', $gid)->update($new_goods_data);

            // 支付完成修改订单状态
            Db::name('order')->where('id', $order_id)->update(['o_status'=>1,'o_pay_time'=>time(),'last_time' => time()]);

            $this->success('兑换成功');
        }

        $this->assign(['goods' => $good_info, 'num' => $number, 'config' => $config, 'gid' => $gid, 'etype' => $etype, 'exchange_id' => $exchange_id]);

        return $this->fetch();

    }

    // 获取持仓的数量
    public function get_deal_list()
    {
        if($this->request->post()){
            $param = $this->request->post();
            $user = $this->check_user();
            if(empty($param['type'])) $this->error('请选择持仓类型');

            if(!in_array($param['type'], [1,2,3])){
                $this->error('持仓类型不正确');
            }

            $where = [
                'uid' => $user['id'],
                'd_status' => 2,
            ];

            if($param['type'] == 1){
                $where['d_credit_1'] = ['>', 0];
            }elseif($param['type'] == 2){
                $where['d_credit_2'] = ['>', 0];
            }elseif($param['type'] == 3){
                $where['d_credit_3'] = ['>', 0];
            }

            $field = [
                'id',
                Db::raw('SUM(`d_sell_num`) AS `number`'),
            ];

            $_list = Db::name('deal')->where($where)->field($field)->group('id')->select();

            $this->success('获取成功', '', $_list);

        }
    }


}