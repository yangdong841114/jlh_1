<?php

namespace app\index\controller;

use think\Db;
use think\facade\Env;
use think\facade\Log;
use think\Session;

use think\Validate;
use think\validate\ValidateRule;

/**
 * 前台首页控制器
 * @package app\index\controller
 */
class Index extends Home
{
    //首页
    public function index()
    {
        $user               = $this->check_user();
        timing_release($user['id']);
        $grant              = grant_reward($user['id']);
        $m_level            = get_level()[$user['m_level']];
        $uid                = $user['id'];
        $config             = $this->set_config();
        $setUserLevel       = new UserLevel();
        $setUserLevel->userLevel($uid);
        $beginToday         = mktime(0,0,0,date('m'),date('d'),date('Y'));
        $set_signing        = Db::name('User')->whereLike('m_line', '%,' . $uid . ',%')->where("m_level>=1 and m_del=0 and m_lock=0")->count();
        $wallet             = Db::name('wallet')->where("uid = $uid")->find();
        $keling             = Db::name('deal')->where("uid = $uid")->where("d_addtime < $beginToday")->where('d_status = 0 and d_type = 1 and d_credit_3 > 0 and d_token > 0 and d_num > 0')->sum('d_num');
        $pifa               = Db::name('deal')->where("uid = $uid")->where("d_addtime < $beginToday")->where('d_status = 0 and d_type = 1 and d_credit_2 > 0 and d_token > 0 and d_num > 0')->sum('d_num');
        $pifaM              = Db::name('deal')->where("uid = $uid")->where("d_addtime > $beginToday")->where('d_status = 0 and d_type = 1 and d_credit_2 > 0 and d_token > 0 and d_num > 0')->sum('d_num');
        $this->assign(array('config' => $config, 'user' => $user, 'uid' => $uid, 'm_level' => $m_level, 'grant' => $grant, 'set_signing' => $set_signing,'wallet' => $wallet,'keling' => $keling,'pifa' => $pifa,'pifaM' => $pifaM));
        return $this->fetch();
    }

    public function demo()
    {
        $config = $this->set_config();
        $data = Db::name('about')->where('id', 5)->find();
        $value = $data['n_content'];
        $this->assign('value', $value);
        $this->assign('config', $config);
        return $this->fetch();
    }


    //领取奖励或者批发票
    public function u_grant_reward()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        if ($this->request->post()) {
            $fields = input('post.');
            $config_return_data = getSystemConfig($uid, '');
            if (is_array($config_return_data)) {
                return json($config_return_data);
            }
            $d_id   = $fields['d_id'];
            $grant  = grant_reward($uid);
            if ($d_id != $grant) {
                return json(array('code' => 0, 'msg' => '您的奖励信息有误'));
            }
            $res = set_grant_reward($uid, $d_id);
            if (is_array($res)) {
                return json($res);
            } else {
                $deal = Db::name('Deal')->where(array('id' => $d_id))->find();
                twoReward($uid, $deal['pid'], $deal['d_total']);
                return json(array('code' => 1, 'msg' => '恭喜您领取成功'));
            }
        }
    }

    //行情
    public function quotations()
    {
        $user = $this->check_user();
        $config = $this->set_config();
        $product = Db::name('Product')->where(array('p_setup_status' => 1))->order('last_time desc')->select();
        foreach ($product as &$row) {
            $row['p_img'] = explode(',', $row['p_image'])[0];
            $row['p_addtime'] = date('Y-m-d', $row['p_addtime']);
        }
        $this->assign(array('config' => $config, 'user' => $user, 'product' => $product));
        return $this->fetch();
    }

    public function quotations_con()
    {
        $user = $this->check_user();
        $config = $this->set_config();
        $id = input('id');
        $product = Db::name('Product')->where(array('p_setup_status' => 1, 'id' => $id))->find();
        if (empty($product)) {
            $this->error('产品信息不存在', url('Index/quotations'));
        }
        $pro_img = explode(',', $product['p_image']);
        $product['p_img'] = $pro_img[0];
        $this->assign(array('config' => $config, 'user' => $user, 'product' => $product, 'pro_img' => $pro_img));
        return $this->fetch();
    }

    public function open_account()
    {
        $user = $this->check_user();
        $uid = $user['id'];
        $config = $this->set_config();
        $this->assign(array('config' => $config, 'user' => $user, 'uid' => $uid));
        return $this->fetch();
    }


    //开户
    public function set_open_account()
    {
        $user = $this->check_user();
        $uid = $user['id'];
        if ($this->request->post()) {

            // 后台审核 增加
            // if($user['m_is_pass'] == 1){
            //     return json(array('code' => 400, 'msg' => '审核已通过，请勿提交'));
            // }

            $config = $this->set_config();

            $param = $this->request->param();

            if(!isset($param['m_name'])){
                return json(array('code' => 400, 'msg' => '请输入真实姓名'));
            }

            if(!$param['m_car_img_1'] || !$param['m_car_img_2'] ){
                return json(array('code' => 400, 'msg' => '请按要求上传身份证照片'));
            }

            $has_card   = Db::name('User')->where(array('m_car_id'=>$param['m_cardnum'],'m_del'=>0,'m_reg_status'=>1))->find();

            if(!empty($has_card) && $has_card['id'] !== $user['id']){
                return json(array('code' => 400, 'msg' => '身份证号码已被注册'));
            }

            if(strpos($param['m_car_img_1'], 'data:image') > -1){
                $m_car_img_1 = Base64GetImage($param['m_car_img_1']);

            }else{
                $m_car_img_1 = $param['m_car_img_1'];
            }

            if(strpos($param['m_car_img_2'], 'data:image') > -1){
                $m_car_img_2 = Base64GetImage($param['m_car_img_2']);

            }else{
                $m_car_img_2 = $param['m_car_img_2'];
            }

            // 判断是不是机构
            $is_mechanism = 0;
            if(isset($param['is_mechanism']) && $param['is_mechanism'] == 1){
                $is_mechanism = 1;
                if(empty($param['legal_person_name'])){
                    return json(array('code' => 400, 'msg' => '请填写法人姓名'));
                }
            }
            // 后台审核 增加
            $time = time();
            $user_data = [
                'm_nickname' => $param['m_name'],
                'm_name' => $param['m_name'],
                'm_car_id' => $param['m_cardnum'],
                'm_car_img' => $m_car_img_1.','.$m_car_img_2,
                'm_is_pass' => 0,
                // 'm_bank_code' => '',
                // 'm_bank_carid' => '',
                // 'm_bank_open_code' => '',
                // 'm_bank_open' => '',
                // 'm_bank_signing' => 0,
                'last_time' => $time,
                'is_mechanism' => $is_mechanism,
                'legal_person_name' => ($is_mechanism==1)?$param['legal_person_name']:''
            ];

            // $res = Db::name('user')->where('id', $user['id'])->update($user_data);
            Db::name('user')->where('id', $user['id'])->update($user_data);

            // if($res){
            //     return ['code' => 200, 'msg' => '提交成功，等待审核'];
            // }else{
            //     return ['code' => 400, 'msg' => '提交失败'];
            // }

            $user_info = Db::name('user')->where('id', $user['id'])->find();

            $data = array(
                'mem_code' => $user_info['m_account'],
                'exchange_fund_account' => $user_info['m_account'],
                'trade_account' => $user_info['m_account'],
                'full_name' => $user_info['m_name'],
                'tel' => $user_info['m_phone'],
                'id_no' => $user_info['m_car_id'],
                'gender' => $user_info['m_sex'],
            );
            $api = new Api();
            $res = $api->mem_user_register($data, $user['id']);
            $res = json_decode($res, true);
            if ($res['error_no'] == 0) {
                Db::name('User')->where(array('id' => $user['id']))->update(array('m_bank_signing' => 2, 'last_time' => time()));
                return json(array('code' => 200, 'msg' => '开户成功,请前往签约'));
            }
            return json(array('code' => 0, 'msg' => $res['error_info']));

            // $data = array(
            //     'mem_code' => $user['m_account'],
            //     'exchange_fund_account' => $user['m_account'],
            //     'trade_account' => $user['m_account'],
            //     'full_name' => $user['m_name'],
            //     'tel' => $user['m_phone'],
            //     'id_no' => $user['m_car_id'],
            //     'gender' => $user['m_sex'],
            // );
            // $api = new  Api();
            // $res = $api->mem_user_register($data, $uid);
            // $res = json_decode($res, true);
            // if ($res['error_no'] == 0) {
            //     Db::name('User')->where(array('id' => $uid))->update(array('m_bank_signing' => 2, 'last_time' => time()));
            //     return json(array('code' => 1, 'msg' => '开户成功,请前往签约'));
            // }
            // return json(array('code' => 0, 'msg' => $res['error_info']));
        }
    }

    //入金
    public function recharge()
    {
        $user = $this->check_user();
        $config = $this->set_config();
        $uid = $user['id'];
        if ($this->request->post()) {
            $fields = input('post.');
            if ($user['m_bank_code'] != 40026) {
                return json(array('code' => 0, 'msg' => '其他银行请登陆网银进行转账'));
            }
            if (!isset($fields['cz_money']) || $fields['cz_money'] <= 0) {
                return json(array('code' => 0, 'msg' => '请输入充值金额'));
            }
            $cz_money = floatval($fields['cz_money']);
            $data['occur_balance'] = $cz_money;
            if ($fields['cz_desc']) {
                $data['remark'] = $fields['cz_desc'];
            }
            $api = new  Api();
            $res = $api->ext_in_golden($data);
            $res = json_decode($res, true);
            if ($res['error_no'] == 0) {
                return json(array('code' => 1, 'msg' => '充值发起成功,请稍等片刻'));
            }
            return json(array('code' => 0, 'msg' => $res['error_info']));
        }
        $this->assign(array('config' => $config, 'user' => $user, 'uid' => $uid));
        return $this->fetch();
    }


    //出金
    public function withdrawal()
    {
        $user = $this->check_user();
        $config = $this->set_config();
        $uid = $user['id'];
        if ($this->request->post()) {
            $fields = input('post.');
            if ($user['m_set_pay'] == 1) {
                return json(array('code' => 0, 'msg' => '账号出金已被限制'));
            }
            if ($user['m_set_pay_num'] != 0 && $user['m_set_pay_num'] != $fields['t_money']) {
                return json(array('code' => 0, 'msg' => '账号出金已到上限'));
            }
            //获取用户等级
            $user_level = Db::name('user')->where('id', $uid)->value('m_level');
            if ($user_level > 5) {
                //线下发放
                $userInfo = getUserInfo($uid);
                $gold_data = [
                    'uid'           => $uid,
                    'account'       => $userInfo['m_account'],
                    'nickname'      => $userInfo['m_nickname'],
                    'phone'         => $userInfo['m_phone'],
                    'money'         => $fields['t_money'],
                    'create_time'   => time()
                ];
                $res = Db::name('gold')->insert($gold_data);
                if ($res) {
                    do_logs($uid, 1, 'm_balance', -$fields['t_money'], '发起提现,扣除对应余额');
                    return json(['code' => '申请成功,请等待审核']);
                }
                return json(['code' => '申请失败']);
            } else {
                if (!isset($fields['t_money']) || $fields['t_money'] <= 0) {
                    return json(array('code' => 0, 'msg' => '请输入提现金额'));
                }
                $cz_money = floatval($fields['t_money']);
                $data['occur_balance'] = $cz_money;
                if ($fields['t_desc']) {
                    $data['remark'] = $fields['t_desc'];
                }
                $api = new Api();
                $res = $api->ext_out_golden($data);
                $res = json_decode($res, true);
                if ($res['error_no'] == 0) {
                    $occur_balance = $res['occur_balance'];
                    // $occur_balance = $data['occur_balance'];
                    do_logs($uid, 1, 'm_balance', -$occur_balance, '发起提现,扣除对应余额');
                    return json(array('code' => 1, 'msg' => '提现发起成功,请稍等片刻'));
                }
                return json(array('code' => 0, 'msg' => $res['error_info']));
            }
        }
        $this->assign(array('config' => $config, 'user' => $user, 'uid' => $uid));
        return $this->fetch();
    }


    public function mingxi()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $this->assign(array('config' => $config, 'user' => $user));
        return $this->fetch();
    }

    public function buy()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $order_limit = $user['m_order_limit'];
        $uid = $user['id'];
        $bill_id = intval(input('bill_id'));                     //input('post.')
        if (!$bill_id) {
            $bill_id = Db::name('Product')->where(array('p_setup_status' => 1))->order('id asc')->value('id');
        }
        $deal_list = Db::name('Deal')->where(array('pid' => $bill_id, 'd_type' => 2))->where("d_num>0 and uid<>$uid and d_status in (1,4)")->order('id asc')->select();
        foreach ($deal_list as &$row) {
            $row['d_addtime'] = date('Y-m-d H:i', $row['d_addtime']);
        }
        //settlement_profit($uid, $bill_id);                                                   //收益完成判断
        $pro_data = Db::name('Product')->where(array('id' => $bill_id))->find();
        $profit_json = get_product($bill_id);
        $profit_data = json_decode($profit_json, true);
        $deal_ratio = explode(':', $pro_data['p_deal_ratio']);        //比例
        $retail_ratio = $deal_ratio[0];
        if (!$order_limit) {
            $order_limit = $retail_ratio;
        }
        $pro_list = Db::name('Product')->where(array('p_del' => 0, 'p_setup_status' => 1))->order('id asc')->select();
        $this->assign(array('config' => $config, 'user' => $user, 'deal_list' => $deal_list, 'pro_list' => $pro_list, 'pro_data' => $pro_data, 'profit_data' => $profit_data, 'order_limit' => $order_limit));
        return $this->fetch();
    }

    //自动匹配
    public function auto_buy_old()
    {
        $config = $this->set_config();
        $user   = $this->check_user();
        $uid    = $user['id'];
        if ($this->request->post()) {
            $fields     = input('post.');
            $pid        = $fields['pid'];
            $buy_num    = $fields['buy_num'];
            $product    = Db::name('Product')->where(array('id' => $pid))->find();
            //检测系统参数
            //          $config_return_data = getSystemConfig($uid, $buy_num);
            //          if(is_array($config_return_data)){
            //                return json($config_return_data);
            //           }
            //1.检查买方是否被限制交易和是否被交易过
            $buy_return_data = getUserIdentity($uid, $pid, $buy_num, 1);
            if (is_array($buy_return_data)) {
                return json($buy_return_data);
            }
            //2.检查买方是否有钱包
            add_wallet($uid, $pid);
            //3.检查买方余额是否充足
            $deal_price  = $product['p_retail_price'] * $buy_num;
            $buy_balance = getUserBalanceIdentity($uid, $deal_price);
            if (is_array($buy_balance)) {
                return json($buy_balance);
            }
            //检查有没有符合条件的单子
            $deal_info = Db::name('deal')
                ->where('d_num', $buy_num)
                ->where('d_type', 2)
                ->where('d_status', 1)
                ->find();
            $buy_uid = $uid;
            if (!empty($deal_info)) {//有合适的进行交易 代码不走这里 直接进else
                #####票的种类####
                $credit_type = 1;
                if ($deal_info['d_credit_2'] > 0) {
                    $credit_type = 2;
                }
                if ($deal_info['d_credit_3'] > 0) {
                    $credit_type = 3;
                }
                #####票的种类####
                $sell_num   = $deal_info['d_num'];
                $pid        = $deal_info['pid'];
                $did        = $deal_info['id'];
                $product    = getProduct($pid);
                $time = time();
                //检查是否触发奖批发票奖励信息 p_deal_ratio
                $deal_ratio = explode(':', $product['p_deal_ratio']);
                if (empty($deal_ratio) || count($deal_ratio) != 2) {
                    return json(array(
                        'msg'   => '产品配售比例有误',
                        'code'  => 0,
                    ));
                }
                //5.检测卖方订单是否完成
                $new_num = $sell_num - $buy_num;
                $set_sell_data['d_num'] = $new_num;
                $set_sell_data['last_time'] = $time;
                if ($new_num > 0) {
                    $set_sell_data['d_sell_num'] = $deal_info['d_sell_num'] + $buy_num;
                    $set_sell_data['d_status'] = 1;
                } elseif ($new_num == 0) {
                    $set_sell_data['d_sell_num'] = $deal_info['d_total'];
                    $set_sell_data['d_status'] = 6;
                    $set_sell_data['d_finish_time'] = $time;
                }
                $fine_list = Db::name('Fine')->where(array('did' => $deal_info['id']))->order('id asc')->limit($buy_num)->select();
                if (empty($fine_list) || count($fine_list) < $buy_num) {
                    return json(array(
                        'msg' => '产品数量不足,购买操作有误',
                        'code' => 0,
                    ));
                }
                $fine_list_id = array();
                foreach ($fine_list as $k => $v) {
                    $fine_list_id[] = $v['id'];
                }
                #####修改卖方用户钱包产品票的数据#####
                $sell_uid = $deal_info['uid'];
                $wallet_res = inspect_wallet($sell_uid, $pid, $buy_num, $credit_type);
                if (is_array($wallet_res)) {
                    return json($wallet_res);
                }
                #####修改卖方用户钱包产品票的数据#####
                #####修改买方用户钱包产品票的数据#####  (收益完成后写入)
                /*$wallet_res          =  set_wallet($buy_uid,$pid,$buy_num,1);
                if(!$wallet_res){
                    return json(array(
                        'msg' => '写入买方钱包失败,请联系管理员',
                        'code' => 0,
                    ));
                }*/
                #####修改买方用户钱包产品票的数据#####

                #####修改卖方订单状态#####
                $fine_ids           = implode(',', $fine_list_id);
                $fine_res_sell      = Db::name('Fine')->where("id in ($fine_ids)")->update(array('f_finish_time' => $time, 'f_end' => 1, 'f_status' => 3));
                $deal_res_sell      = Db::name('Deal')->where(array('id' => $did))->update($set_sell_data);
                #####修改卖方订单状态#####

                #####修改买方用户余额#####
                $res_balance_buy = set_balance($buy_uid, $deal_price);
                if (is_array($res_balance_buy)) {
                    return json($res_balance_buy);
                }
                #####修改买方用户余额#####

                #####修改卖方用户余额#####
                $res_balance_sell   = do_logs($sell_uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                #####修改卖方用户余额#####
                #####发放奖励信息#####
                $res_reward         = inspect_reward($buy_uid, $pid, $deal_info, $buy_num);
                #####发放奖励信息#####
                if ($fine_res_sell && $deal_res_sell && $res_reward && $res_balance_sell && $res_balance_buy) {
                    return json(array(
                        'msg' => '交易成功',
                        'code' => 1,
                    ));
                } else {
                    return json(array(
                        'msg' => '交易过程发生错误,请稍后重试',
                        'code' => 0,
                    ));
                }
            } else {        //没有 进行排队
                #####修改买方用户余额#####
                $fee_ratio = Db::name('config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
                //手续费金额
                $all_fee    = bcmul($deal_price, $fee_ratio, 2);
                $deal_price = bcadd($all_fee, $deal_price, 2);//总金额+(手续费比例*总金额)
                //检查该用户是否是指定账号
                $is_appoint = Db::name('wallet')->where('uid', $uid)->where('pid', $pid)->value('m_identity');
                if ($is_appoint != 0) {
                    $deal_price = $product['p_retail_price'] * $buy_num;
                } else {
                    ####新增####
                    if ($all_fee > 0) {
                        do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
                    }
                    ####新增####
                }
                $res_balance_buy = set_balance($buy_uid, $deal_price);
                if (is_array($res_balance_buy)) {
                    return json($res_balance_buy);
                }
                $deal_price = $product['p_retail_price'];
                $time = time();
                #####修改买方用户余额#####
                $data = [
                    'uid' => $uid,
                    'pid' => $pid,
                    'sid' => $pid,
                    'd_code' => randomkeys(8),
                    'd_credit_1' => $buy_num,
                    'd_total' => $buy_num,
                    'd_num' => $buy_num,
                    'd_type' => 1,
                    'd_addtime' => $time,
                    'd_price' => $deal_price,
                    'd_fee' => $all_fee,
                    'd_date' => date('Y-m-d', time()),
                    'd_status' => 10
                ];
                $res = Db::name('deal')->insertGetId($data);//插入主表
                $fine_data = [
                    'bid'       => $uid,
                    'did'       => $res,
                    'pid'       => $pid,
                    'sid'       => 1,
                    'f_type'    => 1,
                    'f_status'  => 7,
                    'f_price'   => $product['p_retail_price'],
                    'f_profit_start_time' => 0,
                    'f_profit_end_time' => 0,
                    'f_addtime' => $time,
                ];
                for ($i = 1; $i <= $buy_num; $i++) {
                    $fine_data['f_code'] = randomkeys(8);
                    $fine_data['f_date'] = date('Y-m-d', time());
                    Db::name('fine')->insert($fine_data);//插入子表
                }
                if ($res) {
                    return json(array(
                        'msg' => '交易成功,排单中',
                        'code' => 1,
                    ));
                }
                return json(array(
                    'msg' => '排单失败',
                    'code' => 0,
                ));
            }
        }
    }

    //自动卖出
    public function auto_sell_old()
    {
        //接受参数
        $param      = request()->param();
        $user       = $this->check_user();
        $uid        = $user['id'];
        $pid        = $param['pid'];
        $sell_num   = $param['sell_num'];
        $time       = time();
        $date       = date('Y-m-d', $time);
        ####检测用户钱包是否存在####
        add_wallet($uid, $pid);
        //检测系统参数
        //        $config_return_data = getSystemConfig($uid, $sell_num,2);
        //        if (is_array($config_return_data)) {
        //            return json($config_return_data);
        //        }
        //检查有没有合适的买家
        $res = Db::name('Deal')
            ->where('d_type', 1)
            ->where('uid', '<>', $uid)
            ->where('d_status', 10)
            ->where('d_total', $sell_num)
            ->find();
        #####修改卖方用户钱包产品票的数据#####
        $wallet_res = inspect_wallet($uid, $pid, $sell_num, $param['credit_type']);
        if (is_array($wallet_res)) {
            return json($wallet_res);
        }
        #####修改卖方用户钱包产品票的数据#####
        ####写入主表####
        $deal_data = array(
            'uid'           => $uid,
            'pid'           => $pid,
            'sid'           => $pid,
            'd_code'        => randomkeys(8),
            'd_type'        => 2,
            'd_total'       => $sell_num,
            'd_num'         => $sell_num,
            'd_sell_num'    => 0,
            'd_addtime'     => $time,
            'd_status'      => 4,
            'd_date'        => $date,
            'last_time'     => $time,
            'd_grant'       => -1,
        );
        $product_data               = Db::name('Product')->where(array('id' => $pid))->find();
        $deal_data['d_price']       = $product_data['p_retail_price'];
        $deal_data['d_credit_1']    = $param['sell_num'];
        $deal_price                 = $product_data['p_retail_price'] * $sell_num;
        $end_time                   = set_deal_end_time($time, $product_data['p_income_days']);
        //如果卖的是批发票那么应该有截留
        if ($param['credit_type'] == 2) {
            //取出截留比例
            $p_detail       = getProduct($pid);
            if ($p_detail['p_back'] != 0) {     //需要截留 截留金额等于利润*比例
                //计算利润 利润=（零售-批发）*数量
                $money              = ($p_detail['p_retail_price'] - $p_detail['p_whole_price']) * $sell_num;
                $integral           = bcmul($money, $p_detail['p_back'], 2);                            //入积分的数量
                $deal_price         = bcsub($money, $integral, 2);                                    //入余额的数量
                if ($deal_price < 0) {
                    return json(['code' => 0, 'msg' => '截留比例计算有误']);
                }
                do_logs($uid, 2, 'm_integral', $integral, '出售成功,积分增加');
                do_logs(2261, 1, 'm_balance', $integral, '批发票出售成功,资金转入主力账户');  //资金给到主力截留账号
            }
        }
        //手续费
        $fee_ratio          = Db::name('config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
        $all_fee            = bcmul($deal_price, $fee_ratio, 2);      //总手续费
        ####新增####
        if ($all_fee > 0) {
            do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
        }
        ####新增####
        $deal_price         = bcsub($deal_price, $all_fee, 2);        //总金额-手续费
        $deal_data['d_fee'] = $all_fee;
        //检查该用户是否是指定账号
        $is_appoint         = Db::name('wallet')->where('uid', $uid)->where('pid', $pid)->value('m_identity');
        if ($is_appoint != 0) {
            $deal_price         = $product_data['p_retail_price'] * $sell_num;
            $deal_data['d_fee'] = 0;
        }
        $deal_id = Db::name('Deal')->insertGetId($deal_data);
        if (!$deal_id) {
            return json(['code' => 0, 'msg' => '写入失败']);
        }
        $deal_info              = Db::name('Deal')->where('id', $deal_id)->find();
        if (!empty($res)) { //交易
            Db::startTrans();//开启事务
            try {
                Db::name('deal')->where('id', $res['id'])->update(['d_status' => 2, 'd_num' => 0, 'd_sell_num' => $res['d_num'], 'last_time' => $time]); //买家主表更新
                Db::name('deal')->where('id', $deal_id)->update(['d_status' => 4, 'd_num' => 0, 'd_sell_num' => $sell_num, 'last_time' => $time, 'd_finish_time' => $time]); //卖家主表更新
                Db::name('fine')->where('did', $res['id'])->where('f_status', 7)->where('pid', $pid)->update(['f_status' => 1, 'sdid' => $deal_id, 'sid' => $uid, 'f_profit_start_time' => $time, 'f_profit_end_time' => $end_time]);//买家子表更新
                #####修改卖方用户余额#####
                $res_balance_sell = do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                #####修改卖方用户余额#####
                //todo::给买方发放奖励
                #####发放奖励信息#####
                $res_reward = auto_inspect_reward($res['uid'], $pid, $deal_info, $sell_num);
                //发放团队奖
                $team_reward = teamReward($res['id'], $sell_num, $pid);
                //升级
                $userLevel = (new UserLevel())->userLevel($res['uid']);
                if ($res_balance_sell && $res_reward && $team_reward && $userLevel) {
                    Db::commit();
                    return json(['code' => 1, 'msg' => '交易成功']);
                }
            } catch (\Exception $e) {
                Db::rollback();
                //                Log::write(date().'||'.request()->action().'||'.$e->getMessage(),'info');
                return json(['code' => 0, 'msg' => '交易失败']);
            }
        } else {            //交易给系统
            //查看该产品由谁来进行收购
            $appoint_user   = Db::name('Wallet')->where('pid', $pid)->where('m_identity', 1)->value('uid');
            Db::startTrans();
            try {
                #####修改卖方用户余额#####
                $res_balance_sell = do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                #####修改买方用户余额#####
                Db::name('Deal')->where('id', $deal_id)->update(['d_status' => 4, 'd_num' => 0, 'd_sell_num' => $sell_num, 'last_time' => $time, 'd_finish_time' => $time]); //卖家主表更新
                $res_balance_buy = set_balance($appoint_user, $deal_price);
                if (is_array($res_balance_buy)) {
                    return json($res_balance_buy);
                }
                #####修改买方用户余额#####
                //todo::给买方发放奖励
                #####发放奖励信息#####
                $res_reward = system_inspect_reward($appoint_user, $pid, $deal_info, $sell_num);
                $user_level = (new UserLevel())->userLevel($appoint_user);
                //发放团队奖
                $team_reward = teamReward($res['id'], $sell_num, $pid);
                //向用户上级发放奖励
                if ($res_balance_sell && $res_reward && $user_level && $team_reward) {
                    Db::commit();
                    return json(['code' => 0, 'msg' => '交易成功']);
                }
                //           #####发放奖励信息#####
            } catch (\Exception $e) {
                Db::rollback();
                return json(['code' => 0, 'msg' => '交易失败']);
            }
        }
    }


    public function auto_sell()
    {
        //接受参数
        $param      = request()->param();
        $user       = $this->check_user();
        $uid        = $user['id'];
        $pid        = $param['pid'];
        $sell_num   = $param['sell_num'];
        $time       = time();
        $date       = date('Y-m-d', $time);
        $config     = $this->set_config();
        $get_maxNum = $this->get_maxNum($uid);
        ####检测用户钱包是否存在####
        add_wallet($uid, $pid);
        //检测系统参数
        $config_return_data = getSystemConfig($uid, $sell_num,2);
        if (is_array($config_return_data)) {
            return json($config_return_data);
        }
        //检查卖方是否被限制交易和是否被交易过
        $sell_return_data = getUserIdentity($uid, $pid, $sell_num,0);
        if (is_array($sell_return_data)) {
            return json($sell_return_data);
        }
        //检测今日售票额度余额
        if(($uid != 1309 && $uid != 2261 && $uid != 904 && $uid != 1129) && $param['credit_type'] == 1){
            $lastNum = $config['w_max_sell_num_day'] - $get_maxNum;
            if($lastNum <= 0){
                return json(['code' => 0, 'msg' => '今日零售票售票额度已满']);
            }
            if ($lastNum > 0 && $lastNum < $sell_num) {
                return json(['code' => 0, 'msg' => '今日零售票售票只剩下'.$lastNum.'张额度']);
            }
            //检测够不够十票
            if ($param['credit_type'] == 1 && $sell_num < 10) {
                return json(['code' => 0, 'msg' => '零售票出售不能少于十张']);
            }
        }
        //检查有没有合适的买家
        /*$res = Db::name('deal')
            ->where('d_type', 1)
            ->where('uid', '<>', $uid)
            ->where('d_status', 10)
            ->order('id', 'asc')
            ->select();*/
        #####修改卖方用户钱包产品票的数据#####
        $wallet_res = inspect_wallet($uid, $pid, $sell_num, $param['credit_type']);
        if (is_array($wallet_res)) {
            return json($wallet_res);
        }
        #####修改卖方用户钱包产品票的数据#####
        ####写入主表####
        $deal_data = array(
            'uid'       => $uid,
            'pid'       => $pid,
            'sid'       => $pid,
            'd_code'    => randomkeys(8),
            'd_type'    => 2,
            'd_total'   => $sell_num,
            'd_num'     => $sell_num,
            'd_sell_num'=> 0,
            'd_addtime' => $time,
            'd_status'  => 1,
            'last_time' => $time,
            'd_grant'   => -1,
            'd_date'    => $date,
        );
        $product_data           = getProduct($pid);
        $deal_data['d_price']   = $product_data['p_retail_price'];
        if ($param['credit_type'] == 1) {
            $deal_data['d_credit_1']    = $sell_num;
        } elseif ($param['credit_type'] == 2) {
            $deal_data['d_credit_2']    = $sell_num;
            $deal_data['d_price']       = $product_data['p_whole_price'];
        } else {
            $deal_data['d_credit_3']    = $sell_num;
            $deal_data['d_price']       = $product_data['p_whole_price'];
        }
        $deal_price             = $product_data['p_retail_price'] * $sell_num;
        //$end_time               = set_deal_end_time($time,$product_data['p_income_days']);
        //手续费
        // $fee_ratio              = Db::name('config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
        // $all_fee                = bcmul($deal_price, $fee_ratio, 2); //总
        ####新增####
        /*if($all_fee>0){
            do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
        }*/
        ####新增####
        $all_fee                = 0;
        $deal_data['d_sfee']    = $all_fee;
        //检查该用户是否是指定账号
        /*$appoint_user           = Db::name('Wallet')->where('pid', $pid)->where('m_identity', 1)->value('uid');
        if ($appoint_user == $uid) {
            $deal_data['d_sfee'] = 0;
        }*/
        $deal_id = Db::name('Deal')->insertGetId($deal_data);
        if ($deal_id) {
            return json(['code' => 1, 'msg' => '交易成功']);
        }else{
            return json(['code' => 0, 'msg' => '交易失败']);
        }
        die;
        
        //接受参数
        $param      = request()->param();
        $user       = $this->check_user();
        $uid        = $user['id'];
        $pid        = $param['pid'];
        $sell_num   = $param['sell_num'];
        $time       = time();
        $date       = date('Y-m-d', $time);
        ####检测用户钱包是否存在####
        add_wallet($uid, $pid);
        //检测系统参数
        $config_return_data = getSystemConfig($uid, $sell_num, 2);
        if (is_array($config_return_data)) {
            return json($config_return_data);
        }
        //检查卖方是否被限制交易和是否被交易过
        $sell_return_data = getUserIdentity($uid, $pid, $sell_num, 0);
        if (is_array($sell_return_data)) {
            return json($sell_return_data);
        }
        //检测够不够十票
        if ($param['credit_type'] == 1 && $sell_num < 10) {
            return json(['code' => 0, 'msg' => '零售票出售不能少于十张']);
        }
        if($sell_num == 28){
            return json(['code' => 0, 'msg' => '零售票出售不能少于28张']);
        }
        //检查有没有合适的买家
        $res = Db::name('deal')
            ->where('d_type', 1)
            ->where('uid', '<>', $uid)
            ->where('d_status', 10)
            ->order('id', 'asc')
            ->select();
        #####修改卖方用户钱包产品票的数据#####
        $wallet_res = inspect_wallet($uid, $pid, $sell_num, $param['credit_type']);
        if (is_array($wallet_res)) {
            return json($wallet_res);
        }
        #####修改卖方用户钱包产品票的数据#####
        ####写入主表####
        $deal_data = array(
            'uid'       => $uid,
            'pid'       => $pid,
            'sid'       => $pid,
            'd_code'    => randomkeys(8),
            'd_type'    => 2,
            'd_total'   => $sell_num,
            'd_num'     => $sell_num,
            'd_sell_num' => 0,
            'd_addtime' => $time,
            'd_status'  => 1,
            'last_time' => $time,
            'd_grant'   => -1,
            'd_date'    => $date,
        );
        $product_data           = getProduct($pid);
        $deal_data['d_price']   = $product_data['p_retail_price'];
        if ($param['credit_type'] == 1) {
            $deal_data['d_credit_1']    = $sell_num;
        } elseif ($param['credit_type'] == 2) {
            $deal_data['d_credit_2']    = $sell_num;
            $deal_data['d_price']       = $product_data['p_whole_price'];
        } else {
            $deal_data['d_credit_3']    = $sell_num;
            $deal_data['d_price']       = $product_data['p_whole_price'];
        }
        $deal_price             = $product_data['p_retail_price'] * $sell_num;
        $end_time               = 0;
        //手续费
        $fee_ratio              = Db::name('config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
        $all_fee                = bcmul($deal_price, $fee_ratio, 2); //总
        ####新增####
        if ($all_fee > 0) {
            do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
        }
        ####新增####
        $deal_data['d_sfee']    = $all_fee;
        //检查该用户是否是指定账号
        $appoint_user           = Db::name('Wallet')->where('pid', $pid)->where('m_identity', 1)->value('uid');
        if ($appoint_user == $uid) {
            $deal_data['d_sfee'] = 0;
        }
        $deal_id = Db::name('Deal')->insertGetId($deal_data);
        if (!$deal_id) {
            return json(['code' => 0, 'msg' => '写入失败']);
        }
        //查询该产品的主力账户
        if ($appoint_user == $uid) {    //该卖家是主力账户无需秒卖
            if (!empty($res)) {         //有用户需要交易
                foreach ($res as $h => $l) {
                    $deal_info          = Db::name('Deal')->where('id', $deal_id)->find();
                    $deal_info['sid']   = $deal_info['uid'];
                    if ($deal_info['d_num'] == 0) {
                        continue;
                    }
                    if ($deal_info['d_num'] > $l['d_num']) {        //卖出的数量>买家要买的数量
                        //todo::执行交易 剩下的等待下个买家
                        $surplus_num = $deal_info['d_num'] - $l['d_num']; //计算卖方卖完还剩多少个
                        //更新表数据
                        Db::name('deal')//买家主表更新
                            ->where('id', $l['id'])
                            ->update(['d_num' => 0, 'd_sell_num' => $l['d_total'], 'd_status' => 2, 'd_finish_time' => $end_time]);
                        Db::name('fine')//买家子表更新
                            ->where('did', $l['id'])
                            ->where('f_status', 7)
                            ->where('pid', $pid)
                            ->update(['f_status' => 1, 'sdid' => $deal_id, 'sid' => $uid, 'f_profit_start_time' => $time, 'f_profit_end_time' => $end_time, 'f_finish_time' => $time]);
                        Db::name('deal')//卖家主表更新
                            ->where('id', $deal_id)
                            ->update(['d_num' => $surplus_num, 'd_sell_num' => $deal_info['d_total'] - $surplus_num, 'last_time' => $time]);
                        if ($param['credit_type'] == 2) {                                             //批发票 应有截留
                            $all_price      = bcmul($l['d_num'], $product_data['p_retail_price']);    //总金额 数量*批发价
                            $product_ratio  = $product_data['p_back'];                                //取出该产品的截留比例
                            $integral       = bcmul($all_price, $product_ratio, 2);             //积分=总金额*比例
                            $money          = bcsub($all_price, $integral, 2);                  //余额
                            do_logs($uid, 1, 'm_balance', $money, '出售成功,产品货款入金');
                            do_logs($uid, 2, 'm_integral', $integral, '出售成功,产品货款入积分');
                            do_logs(2261, 1, 'm_balance', $integral, '批发票出售成功,资金转入主力账户');  //资金给到主力截留账号
                        } else {
                            $deal_price = $product_data['p_retail_price'] * $l['d_num'];
                            #####修改卖方用户余额#####
                            do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                            #####修改卖方用户余额#####
                        }
                        #####发放奖励信息#####
                        $res_reward         = auto_inspect_reward($l['uid'], $pid, $deal_info, $l['d_num']);
                        //发放团队奖
                        $team_reward        = teamReward($l['uid'], $l['d_num'], $pid);
                        //升级
                        $userLevel          = (new UserLevel())->userLevel($l['uid']);
                    }
                    if ($deal_info['d_num'] < $l['d_num']) {//卖出的数量<买家要买的数量
                        //todo::卖出所有给买家
                        Db::name('deal')//买家主表更新
                            ->where('id', $l['id'])
                            ->update(['d_num' => $l['d_num'] - $deal_info['d_num'], 'last_time' => $time, 'd_sell_num' => $l['d_sell_num'] + $deal_info['d_num']]);
                        //先获取买家子表数据对应的条数ID后续更新使用
                        $find_ids = Db::name('fine')
                            ->where('did', $l['id'])
                            ->where('f_status', 7)
                            ->where('pid', $pid)
                            ->limit(0, $deal_info['d_num'])
                            ->column('id');
                        //                        $array = json_encode(['deal_info'=>$deal_info,'l_info'=>$l,'ids'=>$find_ids]);
                        //                        $textname = $l['id'].'.txt';
                        //                        file_put_contents($textname,$array);
                        //买家子表更新
                        Db::name('fine')//买家子表更新
                            ->where('id', 'in', $find_ids)
                            ->update(['f_status' => 1, 'sdid' => $deal_id, 'sid' => $uid, 'f_profit_start_time' => $time, 'f_profit_end_time' => $end_time, 'f_finish_time' => $time]);
                        Db::name('deal')//卖家主表更新
                            ->where('id', $deal_id)
                            ->update(['d_num' => 0, 'd_sell_num' => $deal_info['d_total'], 'd_status' => 4, 'd_finish_time' => $time, 'last_time' => $time, 'f_finish_time' => $time]);
                        if ($param['credit_type'] == 2) {//批发票 应有截留
                            $all_price      = bcmul($deal_info['d_num'], $product_data['p_retail_price']);  //总金额 数量*批发价
                            $product_ratio  = $product_data['p_back'];                                      //取出该产品的截留比例
                            $integral       = bcmul($all_price, $product_ratio, 2);                   //积分=总金额*比例
                            $money          = bcsub($all_price, $integral, 2);                        //余额
                            do_logs($uid, 1, 'm_balance', $money, '出售成功,产品货款入金');
                            do_logs($uid, 2, 'm_integral', $integral, '出售成功,产品货款入积分');
                            do_logs(2261, 1, 'm_balance', $integral, '批发票出售成功,资金转入主力账户');  //资金给到主力截留账号
                        } else {
                            $deal_price     = $product_data['p_retail_price'] * $deal_info['d_num'];
                            #####修改卖方用户余额#####
                            do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                            #####修改卖方用户余额#####
                        }
                        //todo::给买方发放奖励
                        #####发放奖励信息#####
                        $res_reward  = auto_inspect_reward($l['uid'], $pid, $deal_info, $deal_info['d_num']);
                        //发放团队奖
                        $team_reward = teamReward($l['uid'], $deal_info['d_num'], $pid);
                        //升级
                        $userLevel   = (new UserLevel())->userLevel($l['uid']);
                    }
                    if ($deal_info['d_num'] == $l['d_num']) {
                        //todo::直接交易即可
                        Db::name('deal')//买家主表更新
                            ->where('id', $l['id'])
                            ->update(['d_num' => 0, 'd_sell_num' => $l['d_total'], 'd_status' => 2, 'last_time' => $time, 'd_finish_time' => $end_time]);
                        Db::name('fine')//买家子表更新
                            ->where('did', $l['id'])
                            ->where('f_status', 7)
                            ->where('pid', $pid)
                            ->update(['f_status' => 1, 'sdid' => $deal_id, 'sid' => $uid, 'f_profit_start_time' => $time, 'f_profit_end_time' => $end_time, 'f_finish_time' => $time]);
                        Db::name('deal')//卖家主表更新
                            ->where('id', $deal_id)
                            ->update(['d_num' => 0, 'd_sell_num' => $deal_info['d_total'], 'last_time' => $time, 'd_status' => 4, 'd_finish_time' => $time]);
                        if ($param['credit_type'] == 2) {//批发票 应有截留
                            $all_price      = bcmul($deal_info['d_num'], $product_data['p_retail_price']); //总金额 数量*批发价
                            $product_ratio  = $product_data['p_back'];//取出该产品的截留比例
                            $integral       = bcmul($all_price, $product_ratio, 2); //积分=总金额*比例
                            $money          = bcsub($all_price, $integral, 2);//余额
                            do_logs($uid, 1, 'm_balance', $money, '出售成功,产品货款入金');
                            do_logs($uid, 2, 'm_integral', $integral, '出售成功,产品货款入积分');
                            do_logs(2261, 1, 'm_balance', $integral, '批发票出售成功,资金转入主力账户');  //资金给到主力截留账号
                        } else {
                            $deal_price = $product_data['p_retail_price'] * $deal_info['d_num'];
                            #####修改卖方用户余额#####
                            do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                            #####修改卖方用户余额#####
                        }
                        #####发放奖励信息#####
                        $res_reward     = auto_inspect_reward($l['uid'], $pid, $deal_info, $deal_info['d_num']);
                        //发放团队奖
                        $team_reward    = teamReward($l['uid'], $deal_info['d_num'], $pid);
                        //升级
                        $userLevel      = (new UserLevel())->userLevel($l['uid']);
                    }
                }
            }
            if ($deal_id) {
                return json(['code' => 1, 'msg' => '发布成功']);
            }
        }
        if ($appoint_user != $uid) {        //该卖家用户不是主力账户 需要秒卖
            //不是主力账户 那么会产生手续费 计算应到账
            //手续费比例
            $fee_ratio = Db::name('Config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
            if (!empty($res)) {//有用户需要交易
                foreach ($res as $h => $l) {
                    $deal_info          = Db::name('Deal')->where('id', $deal_id)->find();
                    $deal_info['sid']   = $deal_info['uid'];
                    if ($deal_info['d_num'] == 0) {
                        continue;
                    }
                    if ($deal_info['d_num'] > $l['d_num']) {//卖出的数量>买家要买的数量
                        //todo::执行交易 剩下的卖给主力账户
                        //更新表数据
                        $surplus_num = $deal_info['d_num'] - $l['d_num']; //计算卖方卖完还剩多少个
                        Db::name('deal')//买家主表更新
                            ->where('id', $l['id'])
                            ->update(['d_num' => 0, 'd_sell_num' => $l['d_total'], 'd_status' => 2, 'd_finish_time' => $end_time]);
                        Db::name('fine')//买家子表更新
                            ->where('did', $l['id'])
                            ->where('f_status', 7)
                            ->where('pid', $pid)
                            ->update(['f_status' => 1, 'sdid' => $deal_id, 'sid' => $uid, 'f_profit_start_time' => $time, 'f_date' => $date, 'f_profit_end_time' => $end_time, 'f_finish_time' => $time]);
                        Db::name('deal')//卖家主表更新
                            ->where('id', $deal_id)
                            ->update(['d_num' => $surplus_num, 'd_sell_num' => $deal_info['d_total'] - $surplus_num, 'last_time' => $time]);
                        $deal_price = bcmul($l['d_num'], $product_data['p_retail_price'], 2);
                        $all_fee    = bcmul($deal_price, $fee_ratio, 2); //总手续费
                        ####新增####
                        if ($all_fee > 0) {
                            do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
                        }
                        ####新增####
                        $deal_price = bcsub($deal_price, $all_fee, 2);//总金额-手续费
                        if ($param['credit_type'] == 2) {//批发票 应有截留
                            //总金额
                            $all_price = bcmul($l['d_num'], $product_data['p_retail_price'], 2);
                            //取出该产品的截留比例
                            $product_ratio  = $product_data['p_back'] / 100;
                            $integral       = bcmul($all_price, $product_ratio, 2); //积分
                            $money          = bcsub($all_price, $integral, 2);//余额
                            do_logs($uid, 1, 'm_balance', bcsub($money, $all_fee, 2), '出售成功,产品货款入金');
                            do_logs($uid, 2, 'm_integral', $integral, '出售成功,产品货款入积分');
                            do_logs(2261, 1, 'm_balance', $integral, '批发票出售成功,资金转入主力账户');  //资金给到主力截留账号
                        } else {
                            #####修改卖方用户余额#####
                            do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                            #####修改卖方用户余额#####
                        }
                        #####发放用户奖励信息#####
                        $res_reward     = auto_inspect_reward($l['uid'], $pid, $deal_info, $l['d_num']);
                        //发放团队奖
                        $team_reward    = teamReward($l['id'], $l['d_num'], $pid);
                        //升级
                        $userLevel      = (new UserLevel())->userLevel($l['uid']);
                    }
                    if ($deal_info['d_num'] < $l['d_num']) {//卖出的数量<买家要买的数量
                        //todo::卖出所有给买家
                        Db::name('deal')//买家主表更新
                            ->where('id', $l['id'])
                            ->update(['d_num' => $l['d_num'] - $deal_info['d_num'], 'last_time' => $time, 'd_sell_num' => $l['d_sell_num'] + $deal_info['d_num']]);
                        //先获取买家子表数据对应的条数ID后续更新使用
                        $find_ids = Db::name('fine')
                            ->where('did', $l['id'])
                            ->where('f_status', 7)
                            ->where('pid', $pid)
                            ->limit(0, $deal_info['d_num'])
                            ->column('id');
                        Db::name('fine')//买家子表更新
                            ->where('did', $l['id'])
                            ->where('id', 'in', $find_ids)
                            ->where('f_status', 7)
                            ->where('pid', $pid)
                            ->update(['f_status' => 1, 'sdid' => $deal_id, 'sid' => $uid, 'f_profit_start_time' => $time, 'f_date' => $date, 'f_profit_end_time' => $end_time, 'f_finish_time' => $time]);
                        Db::name('deal')//卖家主表更新
                            ->where('id', $deal_id)
                            ->update(['d_status' => 4, 'd_date' => date('Y-m-d', time()), 'd_finish_time' => $time, 'd_num' => 0, 'd_sell_num' => $deal_info['d_total'], 'last_time' => $time]);
                        #####修改卖方用户余额#####
                        $deal_price = bcmul($deal_info['d_num'], $product_data['p_retail_price'], 2);
                        $all_fee    = bcmul($deal_price, $fee_ratio, 2); //总手续费
                        ####新增####
                        if ($all_fee > 0) {
                            do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
                        }
                        ####新增####
                        $deal_price = bcsub($deal_price, $all_fee, 2);    //总金额-手续费
                        if ($param['credit_type'] == 2) {                       //批发票 应有截留
                            //总金额
                            $all_price      = bcmul($deal_info['d_num'], $product_data['p_retail_price'], 2);
                            //取出该产品的截留比例
                            $product_ratio  = $product_data['p_back'] / 100;
                            $integral       = bcmul($all_price, $product_ratio, 2); //积分
                            $money          = bcsub($all_price, $integral, 2);//余额
                            do_logs($uid, 1, 'm_balance', bcsub($money, $all_fee, 2), '出售成功,产品货款入金');
                            do_logs($uid, 2, 'm_integral', $integral, '出售成功,产品货款入积分');
                            do_logs(2261, 1, 'm_balance', $integral, '批发票出售成功,资金转入主力账户');  //资金给到主力截留账号
                        } else {
                            do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                            #####修改卖方用户余额#####
                        }
                        #####发放奖励信息#####
                        $res_reward         = auto_inspect_reward($l['uid'], $pid, $deal_info, $deal_info['d_num']);
                        //$bry        = ['id' => $deal_id, 'd_num' => $deal_info['d_num'], 'l_num' => $l['d_num']];
                        //发放团队奖
                        $team_reward        = teamReward($l['uid'], $deal_info['d_num'], $pid);
                        //升级
                        $userLevel          = (new UserLevel())->userLevel($l['uid']);
                    }
                    if ($deal_info['d_num'] == $l['d_num']) {
                        //todo::直接交易即可
                        Db::name('Deal')//买家主表更新
                            ->where('id', $l['id'])
                            ->update(['d_num' => 0, 'd_sell_num' => $l['d_total'], 'd_status' => 2, 'last_time' => $time, 'd_finish_time' => $end_time]);
                        Db::name('Fine')//买家子表更新
                            ->where('did', $l['id'])
                            ->where('f_status', 7)
                            ->where('pid', $pid)
                            ->update(['f_status' => 1, 'sdid' => $deal_id, 'sid' => $uid, 'f_profit_start_time' => $time, 'f_date' => $date, 'f_profit_end_time' => $end_time, 'f_finish_time' => $time]);
                        Db::name('Deal')//卖家主表更新
                            ->where('id', $deal_id)
                            ->update(['d_num' => 0, 'd_sell_num' => $deal_info['d_num'], 'last_time' => $time, 'd_status' => 4, 'd_finish_time' => $time]);
                        #####修改卖方用户余额#####
                        $deal_price = bcmul($l['d_num'], $product_data['p_retail_price'], 2);
                        $all_fee    = bcmul($deal_price, $fee_ratio, 2); //总手续费
                        ####新增####
                        if ($all_fee > 0) {
                            do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
                        }
                        ####新增####
                        $deal_price = bcsub($deal_price, $all_fee, 2);//总金额-手续费
                        if ($param['credit_type'] == 2) {//批发票 应有截留
                            //总金额
                            $all_price      = bcmul($l['d_num'], $product_data['p_retail_price'], 2);
                            //取出该产品的截留比例
                            $product_ratio  = $product_data['p_back'] / 100;
                            $integral       = bcmul($all_price, $product_ratio, 2); //积分
                            $money          = bcsub($all_price, $integral, 2);//余额
                            do_logs($uid, 1, 'm_balance', bcsub($money, $all_fee, 2), '出售成功,产品货款入金');
                            do_logs($uid, 2, 'm_integral', $integral, '出售成功,产品货款入积分');
                            do_logs(2261, 1, 'm_balance', $integral, '批发票出售成功,资金转入主力账户');  //资金给到主力截留账号
                        } else {
                            do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                            #####修改卖方用户余额#####
                        }
                        #####发放奖励信息#####
                        $res_reward     = auto_inspect_reward($l['uid'], $pid, $deal_info, $l['d_num']);
                        //发放团队奖
                        $team_reward    = teamReward($l['uid'], $l['d_num'], $pid);
                        //升级
                        $userLevel      = (new UserLevel())->userLevel($l['uid']);
                    }
                }
                $deal_info = Db::name('Deal')->where('id', $deal_id)->find();
                if ($deal_info['d_num'] != 0) { //没有卖完 卖给主力账户
                    //更新卖家主表
                    Db::name('deal')
                        ->where('id', $deal_info['id'])
                        ->update(['d_num' => 0, 'd_sell_num' => $deal_info['d_total'], 'last_time' => $time, 'd_status' => 4, 'd_finish_time' => $time, 'd_date' => date('Y-m-d', time())]);
                    #####修改卖方用户余额#####
                    $deal_price = bcmul($deal_info['d_num'], $product_data['p_retail_price'], 2);
                    $all_fee    = bcmul($deal_price, $fee_ratio, 2); //总手续费
                    ####新增####
                    if ($all_fee > 0) {
                        do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
                    }
                    ####新增####
                    $deal_price = bcsub($deal_price, $all_fee, 2);//总金额-手续费
                    if ($param['credit_type'] == 2) {//批发票 应有截留
                        //总金额
                        $all_price      = bcmul($deal_info['d_num'], $product_data['p_retail_price'], 2);
                        //取出该产品的截留比例
                        $product_ratio  = $product_data['p_back'] / 100;
                        $integral       = bcmul($all_price, $product_ratio, 2); //积分
                        $money          = bcsub($all_price, $integral, 2);//余额
                        do_logs($uid, 1, 'm_balance', bcsub($money, $all_fee, 2), '出售成功,产品货款入金');
                        do_logs($uid, 2, 'm_integral', $integral, '出售成功,产品货款入积分');
                        do_logs(2261, 1, 'm_balance', $integral, '批发票出售成功,资金转入主力账户');  //资金给到主力截留账号
                    } else {
                        do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                    }
                    //增加主力账户主表
                    $appoint_deal = [
                        'uid'           => $appoint_user,
                        'sid'           => 1,
                        'pid'           => $pid,
                        'd_code'        => randomkeys(8),
                        'd_type'        => 1,
                        'd_total'       => $deal_info['d_num'],
                        'd_num'         => 0,
                        'd_sell_num'    => $deal_info['d_num'],
                        'd_addtime'     => $time,
                        'd_price'       => $product_data['p_retail_price'],
                        'd_credit_1'    => $deal_info['d_num'],
                        'd_status'      => 2,
                        'last_time'     => $time,
                        'd_finish_time' => $end_time,
                        'd_date'        => $date,
                        'd_sfee'        => $deal_data['d_sfee']
                    ];
                    $appoint_deal_id = Db::name('deal')->insertGetId($appoint_deal);
                    //增加主力账户子表
                    $appoint_fine = [
                        'bid'                   => $appoint_user,
                        'sid'                   => $uid,
                        'did'                   => $appoint_deal_id,
                        'sdid'                  => $deal_id,
                        'pid'                   => $pid,
                        'f_code'                => randomkeys(8),
                        'f_status'              => 1,
                        'f_type'                => 0,
                        'f_price'               => $product_data['p_retail_price'],
                        'f_addtime'             => $time,
                        'f_profit_start_time'   => $time,
                        'f_profit_end_time'     => $end_time,
                        'f_date'                => $date,
                        'f_end'                 => 0,
                        'f_finish_time'         => $time
                    ];
                    for ($i = 1; $i <= $deal_info['d_num']; $i++) {
                        Db::name('fine')->insert($appoint_fine);
                    }
                    #####修改主力账户用户余额#####
                    $should_price       = $deal_info['d_num'] * $product_data['p_retail_price']; //主力账户应付的钱为 (出售数量-买家买走数量)*单价
                    $res_balance_buy    = set_balance($appoint_user, $should_price);
                    if (is_array($res_balance_buy)) {
                        return json($res_balance_buy);
                    }
                    #####修改主力账户用户余额#####
                    #####发放主力账户奖励信息#####
                    $res_reward     = auto_inspect_reward($appoint_user, $pid, $deal_info, $deal_info['d_num']);
                    //发放团队奖
                    $team_reward    = teamReward($appoint_user, $deal_info['d_num'], $pid);
                    //升级
                    $userLevel      = (new UserLevel())->userLevel($appoint_user);
                    #####发放主力账户奖励信息#####
                }
                if ($res_reward && $team_reward && $userLevel) {
                    return json(['code' => 1, 'msg' => '交易成功']);
                }
                return json(['code' => 0, 'msg' => '交易失败']);
            } else {//没有用户 全部卖给主力账户
                //需要先检查主力账户余额是否充足
                #####修改主力账户用户余额#####
                $deal_info          = Db::name('Deal')->where('id', $deal_id)->find();
                $should_price       = $deal_info['d_num'] * $product_data['p_retail_price']; //主力账户应付的钱为 (出售数量-买家买走数量)*单价
                $res_balance_buy    = set_balance($appoint_user, $should_price);
                if (is_array($res_balance_buy)) {
                    return json(['code' => 0, 'msg' => '系统账户暂时无法收购']);
                }
                //更新卖家主表
                Db::name('Deal')
                    ->where('id', $deal_info['id'])
                    ->update(['d_num' => 0, 'd_sell_num' => $deal_info['d_total'], 'last_time' => $time, 'd_status' => 4, 'd_finish_time' => $time, 'd_date' => date('Y-m-d', time())]);
                #####修改卖方用户余额#####
                $deal_price = bcmul($deal_info['d_num'], $product_data['p_retail_price'], 2);
                $all_fee    = bcmul($deal_price, $fee_ratio, 2); //总手续费
                ####新增####
                if ($all_fee > 0) {
                    do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
                }
                ####新增####
                $deal_price = bcsub($deal_price, $all_fee, 2);//总金额-手续费
                if ($param['credit_type'] == 2) {//批发票 应有截留
                    //总金额
                    $all_price      = bcmul($deal_info['d_num'], $product_data['p_retail_price'], 2);
                    //取出该产品的截留比例
                    $product_ratio  = $product_data['p_back'] / 100;
                    $integral       = bcmul($all_price, $product_ratio, 2); //积分
                    $money          = bcsub($all_price, $integral, 2);//余额
                    do_logs($uid, 1, 'm_balance', bcsub($money, $all_fee, 2), '出售成功,产品货款入金');
                    do_logs($uid, 2, 'm_integral', $integral, '出售成功,产品货款入积分');
                    do_logs(2261, 1, 'm_balance', $integral, '批发票出售成功,资金转入主力账户');  //资金给到主力截留账号
                } else {
                    do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                }
                //增加主力账户主表
                $appoint_deal = [
                    'uid'           => $appoint_user,
                    'sid'           => 1,
                    'pid'           => $pid,
                    'd_code'        => randomkeys(8),
                    'd_type'        => 1,
                    'd_total'       => $deal_info['d_num'],
                    'd_num'         => 0,
                    'd_sell_num'    => $deal_info['d_num'],
                    'd_addtime'     => $time,
                    'd_price'       => $product_data['p_retail_price'],
                    'd_credit_1'    => $deal_info['d_num'],
                    'd_status'      => 2,
                    'last_time'     => $time,
                    'd_finish_time' => $end_time,
                    'd_date'        => $date,
                    'd_sfee'        => $deal_data['d_sfee']
                ];
                $appoint_deal_id = Db::name('deal')->insertGetId($appoint_deal);
                //增加主力账户子表
                $appoint_fine = [
                    'bid'                   => $appoint_user,
                    'sid'                   => $uid,
                    'did'                   => $appoint_deal_id,
                    'sdid'                  => $deal_id,
                    'pid'                   => $pid,
                    'f_status'              => 1,
                    'f_type'                => 0,
                    'f_price'               => $product_data['p_retail_price'],
                    'f_addtime'             => $time,
                    'f_profit_start_time'   => $time,
                    'f_profit_end_time'     => $end_time,
                    'f_date'                => $date,
                    'f_end'                 => 0,
                    'f_finish_time'         => $time
                ];
                for ($i = 1; $i <= $deal_info['d_num']; $i++) {
                    $appoint_fine['f_code'] = randomkeys(8);
                    Db::name('fine')->insert($appoint_fine);
                }
                #####修改主力账户用户余额#####
                #####发放主力账户奖励信息#####
                $res_reward     = auto_inspect_reward($appoint_user, $pid, $deal_info, $deal_info['d_num']);
                //发放团队奖
                $team_reward    = teamReward($appoint_user, $deal_info['d_num'], $pid);
                //升级
                $userLevel      = (new UserLevel())->userLevel($appoint_user);
                if ($res_reward && $team_reward && $userLevel) {
                    return json(['code' => 1, 'msg' => '交易成功']);
                }
                return json(['code' => 0, 'msg' => '交易失败']);
            }
        }
    }

    public function auto_buy()
    {
        $config = $this->set_config();
        $user   = $this->check_user();
        $uid    = $user['id'];
        if ($this->request->post()) {
            $fields = input('post.');
            $pid    = $fields['pid'];
            if (empty($pid)) {
                return json(['code' => 0, 'msg' => '请选择产品']);
            }
            $buy_num = $fields['buy_num'];
            if($buy_num>10 || $buy_num<=0){
                return json(['code' => 0, 'msg' => '买入数量有误']);
            }
            $time     = time();
            $product  = Db::name('Product')->where(array('id' => $pid))->find();
            
            //$end_time = set_deal_end_time($time,$product['p_income_days']);
            //检测系统参数
            $config_return_data = getSystemConfig($uid, $buy_num);
            if (is_array($config_return_data)) {
                return json($config_return_data);
            }
            //检测购票限制量
            $s = strtotime(date('Y-m-d').'00:00:00');
            $l = strtotime(date('Y-m-d').'23:59:59');
            //查询今日购买几单
            $count  = Db::name('Fine')->where('bid',$uid)->where('f_addtime','>',$s)->where('f_addtime','<',$l)->count();
            //获取限制数量
            $max        = $this->getConfig('w_max_num');
            /*if ($order_limit != 0) {//限制
                if(($count+$buy_num)>$order_limit){
                    return json(['code'=>0,'msg'=>'今日购买量超限']);
                }
            }*/
            /*if(($count+$buy_num)>$max){
                return json(['code'=>0,'msg'=>'今日购买量超限']);
            }*/
            //1.检查买方是否被限制交易和是否被交易过
            $buy_return_data = getUserIdentity($uid, $pid, $buy_num, 1);
            if (is_array($buy_return_data)) {
                return json($buy_return_data);
            }
            //2.检查买方是否有钱包
            add_wallet($uid, $pid);
            //3.检查买方余额是否充足
            $deal_price     = $product['p_retail_price'] * $buy_num;
            $buy_balance    = getUserBalanceIdentity($uid, $deal_price);
            if (is_array($buy_balance)) {
                return json($buy_balance);
            }
            $all_fee = 0;
            //插入数据
            $deal_data = [
                'uid'           => $uid,
                'pid'           => $pid,
                'sid'           => $pid,
                'd_code'        => randomkeys(8),
                'd_credit_1'    => $buy_num,
                'd_total'       => $buy_num,
                'd_num'         => $buy_num,
                'd_type'        => 1,
                'd_fee'         => $all_fee,
                'd_addtime'     => $time,
                'd_price'       => $deal_price,
                'd_date'        => date('Y-m-d', time()),
                'd_status'      => 10
            ];
            $insert_deal = Db::name('deal')->insertGetId($deal_data);
            $fine_data = [
                'bid'                   => $uid,
                'did'                   => $insert_deal,
                'pid'                   => $pid,
                'sid'                   => 0,
                'f_type'                => 0,
                'f_status'              => 7,
                'f_price'               => $product['p_retail_price'],
                'f_profit_start_time'   => 0,
                'f_profit_end_time'     => 0,
                'f_addtime'             => $time,
                'f_date'                => date('Y-m-d', time())
            ];
            for ($i = 1; $i <= $buy_num; $i++) {
                $fine_data['f_code']    = randomkeys(8);
                $insert_fine            = Db::name('fine')->insert($fine_data);             //插入子表
            }
            if($insert_deal && $insert_fine){
                return json(['code' => 1, 'msg' => '交易成功']);
            }else{
                return json(['code' => 0, 'msg' => '交易失败']);
            }
        }
    }


    //提货
    public function carry()
    {
        $did = $this->request->param('d_id');
        $aid = $this->request->param('a_id');
        $deal_info = Db::name('deal')->where('id', $did)->find();
        //获取有多少
        $count = Db::name('fine')->where('did', $did)->where('f_status', 1)->count();
        $time = time();
        //减少持有/改变状态
        $update_deal = Db::name('deal')->where('id', $did)->update(['d_status' => 5]);//主表
        $update_fine = Db::name('fine')->where('did', $did)->where('f_status', 1)->update(['f_status' => 4, 'd_finish_time' => $time]); //子表
        //写入订单表
        $data = [
            'gid' => $deal_info['pid'],
            'o_buy_num' => $count,
            'uid' => $deal_info['uid'],
            'o_price' => $deal_info['d_price'],
            'o_code' => randomkeys(18),
            'o_status' => 1,
            'o_addtime' => $time,
            'o_type' => 7,
            'aid' => $aid,
            'o_info' => '用户提货',
            'o_credit_1' => $count * $deal_info['d_price']
        ];
        $res = Db::name('order')->insertGetId($data);
        if ($update_deal && $update_fine && $res) {
            return json(['code' => 1, 'msg' => '提货成功']);
        }
        return json(['code' => 0, 'msg' => '提货失败']);
    }


    //获取卖家信息
    public function buy_data()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        if ($this->request->post()) {
            $fields = input('post.');
            $bill_id = $fields['bill_id'];
            $deal_list = Db::name('Deal')->where(array('pid' => $bill_id, 'd_type' => 2))->where("d_num>0 and uid<>$uid and d_status in (1,4)")->order('id asc')->select();
            foreach ($deal_list as &$row) {
                $row['d_addtime'] = date('Y-m-d H:i', $row['d_addtime']);
            }
            $pro_data = Db::name('Product')->where(array('id' => $bill_id))->find();
            $profit_json = get_product($bill_id);
            $profit_data = json_decode($profit_json, true);
            return json(array('code' => 1, 'deal_list' => $deal_list, 'config' => $config, 'profit_data' => $profit_data, 'pro_data' => $pro_data));
        }
    }

    //获取卖家用户信息
    public function buy_get_info()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        if ($this->request->post()) {
            $fields = input('post.');
            $did = $fields['did'];
            $deal_info = Db::name('Deal')->where(array('id' => $did, 'd_type' => 2))->where("d_status in (1,4)")->find();
            $sell_uid = $deal_info['uid'];
            $sell_user = getUserInfo($sell_uid);
            if (!empty($deal_info) && !empty($sell_user)) {
                return json(array('deal_info' => $deal_info, 'sell_user' => $sell_user, 'code' => 1, 'msg' => '获取成功'));
            } else {
                return json(array('code' => 0, 'msg' => '获取失败'));
            }
        }
    }


    //手动进行下单
    public function place_order()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $time = time();
        $date = date('Y-m-d', $time);
        if ($this->request->post()) {
            $fields = input('post.');
            $did = $fields['did'];
            $buy_num = $fields['sell_num'];
            $deal_info = Db::name('Deal')->where(array('id' => $did, 'd_type' => 2))->find();
            #####票的种类####
            $credit_type = 1;
            if ($deal_info['d_credit_2'] > 0) {
                $credit_type = 2;
            }
            if ($deal_info['d_credit_3'] > 0) {
                $credit_type = 3;
            }
            #####票的种类####
            $sell_num = $deal_info['d_num'];
            if ($buy_num > $sell_num) {
                return json(array('code' => 0, 'msg' => '购买数量有误'));
            }
            ####卖方用户信息####
            $sell_uid   = $deal_info['uid'];
            $sell_user  = getUserInfo($sell_uid);
            ####卖方用户信息####

            ####买方用户信息####
            $buy_user   = $user;
            $buy_uid    = $buy_user['id'];
            ####买方用户信息####
            if ($sell_uid == $buy_uid) {
                return json(array(
                    'msg' => '不能购买您自己售出的产品',
                    'code' => 0,
                ));
            }
            $pid = $deal_info['pid'];
            $product = getProduct($pid);
            ######执行业务逻辑######
            //检测系统参数
            $config_return_data = getSystemConfig($buy_uid, $buy_num);
            if (is_array($config_return_data)) {
                return json($config_return_data);
            }
            //1.检查买方是否被限制交易和是否被交易过
            $buy_return_data = getUserIdentity($buy_uid, $pid, $buy_num, 1);
            if (is_array($buy_return_data)) {
                return json($buy_return_data);
            }
            //2.检查卖方是否被限制交易和是否被交易过
            $sell_return_data = getUserIdentity($sell_uid, $pid, $buy_num, 0);
            if (is_array($sell_return_data)) {
                return json($sell_return_data);
            }
            //3.检查买方是否有钱包
            add_wallet($buy_uid, $pid);
            //4.检查买方余额是否充足
            $deal_price = $product['p_retail_price'] * $buy_num;
            $buy_balance = getUserBalanceIdentity($buy_uid, $deal_price);
            if (is_array($buy_balance)) {
                return json($buy_balance);
            }
            //5.检测卖方订单是否完成
            $new_num                    = $sell_num - $buy_num;
            $set_sell_data['d_num']     = $new_num;
            $set_sell_data['last_time'] = $time;
            if ($new_num > 0) {
                $set_sell_data['d_sell_num'] = $deal_info['d_sell_num'] + $buy_num;
                $set_sell_data['d_status'] = 1;
            } elseif ($new_num == 0) {
                $set_sell_data['d_sell_num'] = $deal_info['d_total'];
                $set_sell_data['d_status'] = 6;
                $set_sell_data['d_finish_time'] = $time;
            }
            $fine_list = Db::name('Fine')->where(array('did' => $did))->order('id asc')->limit($buy_num)->select();
            if (empty($fine_list) || count($fine_list) < $buy_num) {
                return json(array(
                    'msg' => '产品数量不足,购买操作有误',
                    'code' => 0,
                ));
            }
            //6.检查是否触发奖批发票奖励信息 p_deal_ratio
            $deal_ratio     = explode(':', $product['p_deal_ratio']);
            if (empty($deal_ratio) || count($deal_ratio) != 2) {
                return json(array(
                    'msg' => '产品配售比例有误',
                    'code' => 0,
                ));
            }
            $fine_list_id   = array();
            foreach ($fine_list as $k => $v) {
                $fine_list_id[] = $v['id'];
            }
            #####修改卖方用户钱包产品票的数据#####
            $wallet_res     = inspect_wallet($sell_uid, $pid, $buy_num, $credit_type);
            if (is_array($wallet_res)) {
                return json($wallet_res);
            }
            #####修改卖方用户钱包产品票的数据#####

            #####修改买方用户钱包产品票的数据#####  (收益完成后写入)
            /*$wallet_res          =  set_wallet($buy_uid,$pid,$buy_num,1);
            if(!$wallet_res){
                return json(array(
                    'msg' => '写入买方钱包失败,请联系管理员',
                    'code' => 0,
                ));
            }*/
            #####修改买方用户钱包产品票的数据#####

            #####修改卖方订单状态#####
            $fine_ids       = implode(',', $fine_list_id);
            $fine_res_sell  = Db::name('Fine')->where("id in ($fine_ids)")->update(array('f_finish_time' => $time, 'f_end' => 1, 'f_status' => 3));
            $deal_res_sell  = Db::name('Deal')->where(array('id' => $did))->update($set_sell_data);
            #####修改卖方订单状态#####

            #####修改买方用户余额#####
            //手续费
            $fee_ratio      = Db::name('config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
            ####新增####
            $all_fee        = bcmul($deal_price, $fee_ratio, 2);
            if ($all_fee > 0) {
                do_logs(371, 1, 'm_balance', $all_fee, '交易产生手续费,资金转入主力账户');  //资金给到主力手续费账号
            }
            ####新增####
            $deal_price         = bcadd(bcmul($deal_price, $fee_ratio, 4), $deal_price);//总金额+(手续费比例*总金额)
            $res_balance_buy    = set_balance($buy_uid, $deal_price);
            if (is_array($res_balance_buy)) {
                return json($res_balance_buy);
            }
            #####修改买方用户余额#####

            #####修改卖方用户余额#####
            $res_balance_sell = do_logs($sell_uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
            #####修改卖方用户余额#####
            #####发放奖励信息#####
            $res_reward = inspect_reward($buy_uid, $pid, $deal_info, $buy_num);
            #####发放奖励信息#####
            $user_level = (new UserLevel())->userLevel($buy_uid);
            //发放团队奖
            $team_reward = teamReward($buy_uid, $sell_num, $pid);
            if ($fine_res_sell && $deal_res_sell && $res_reward && $res_balance_sell && $res_balance_buy && $user_level && $team_reward) {
                return json(array(
                    'msg' => '交易成功',
                    'code' => 1,
                ));
            } else {
                return json(array(
                    'msg' => '交易过程发生错误,请稍后重试',
                    'code' => 0,
                ));
            }
            ######执行业务逻辑######
        }
    }

    public function sell_info()
    {
        $param = $this->request->param();
        $user = $this->check_user();
        $uid = $user['id'];
        $product = getProduct($param['pid']);
        $p_deal_ratio = explode(':', $product['p_deal_ratio']);
        $retail_price = $p_deal_ratio[0];              //零售比例
        $who_price = $p_deal_ratio[1];              //零售比例
        if ($param['selcheck'] == 1) {
            $field = 'm_credit_1';
        } elseif ($param['selcheck'] == 2) {
            $field = 'm_credit_2';
        } else {
            $field = 'm_credit_3';
        }
        //查询该产品该种类有多少可以出售
        $count = Db::name('wallet')->where('uid', $uid)->where('pid', $param['pid'])->value($field);
        return json(['code' => 1, 'msg' => '获取成功', 'data' => $count, 'product' => $product, 'retail_price' => $retail_price, 'who_price' => $who_price]);
    }


    public function sell()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        $bill_id = intval(input('bill_id'));                     //input('post.')
        if (!$bill_id) {
            $bill_id = Db::name('Product')->where(array('p_setup_status' => 1))->order('id asc')->value('id');
        }
        //settlement_profit($uid, $bill_id);                                                   //收益完成判断
        $deal_list = Db::name('Deal')->where(array('pid' => $bill_id, 'd_type' => 2))->where("d_num>0 and uid<>$uid and d_status in (1,4)")->order('id asc')->select();
        $pro_data = Db::name('Product')->where(array('id' => $bill_id))->find();
        $profit_json = get_product($bill_id);
        $profit_data = json_decode($profit_json, true);
        foreach ($deal_list as &$row) {
            $row['d_addtime'] = date('Y-m-d H:i', $row['d_addtime']);
        }
        add_wallet($uid, $bill_id);
        $wallet = Db::name('Wallet')->where(array('uid' => $uid, 'pid' => $bill_id))->find();
        $pro_list = Db::name('Product')->where(array('p_del' => 0, 'p_setup_status' => 1))->order('id asc')->select();
        $this->assign(array('config' => $config, 'user' => $user, 'deal_list' => $deal_list, 'pro_list' => $pro_list, 'pro_data' => $pro_data, 'profit_data' => $profit_data, 'wallet' => $wallet));
        return $this->fetch();
    }

    public function chicang()
    {
        $config     = $this->set_config();
        $user       = $this->check_user();
        $uid        = $user['id'];
        $bill_id    = (int) input('bill_id');
        $sql        = array('d_type' => 1);
        if (!$bill_id) {
            $bill_id = Db::name('Product')->order('id asc')->value('id');
            $sql['pid'] = $bill_id;
        }

        $list = Db::name('Deal')
            ->alias('d')
            ->join('product p', 'd.pid=p.id', 'LEFT')
            ->where('uid', $uid)
            ->where($sql)
            ->where('d_status', 'in', [2,3])
            ->field('p_title,d.*,case when d_credit_1 > 0 then "零售产品" when d_credit_2 > 0 then "批发产品" else "奖励产品" end as p_type')
            ->field('d_sell_num as p_num,start_time as p_start_time , end_time as p_end_time , end_datetime as p_end_datetime')
            ->order('id desc')
            ->select();
        $wallet     = Db::name('Wallet')->where(array('uid' => $uid, 'pid' => $bill_id))->find();
        $pro_list   = Db::name('Product')->where(array('p_del' => 0, 'p_setup_status' => 1))->order('id asc')->select();
        $this->assign(array('config' => $config, 'user' => $user, 'pro_list' => $pro_list, 'list' => $list, 'wallet' => $wallet));
        return $this->fetch();
    }


    public function chedan()
    {
        if ($this->request->isPost()) {
            $did        = $this->request->param('d_id');
            if (empty($did)) {
                return json(['code' => 0, 'msg' => '参数异常']);
            }
            $res        = Db::name('deal')->where('id', $did)->find();
            $all_price  = bcadd(bcmul($res['d_price'], $res['d_num'], 2), $res['d_fee'], 2);
            //查看是买方还是卖方
            if ($res['d_type'] == 1) {//买
                //改变主表
                $deal_save = Db::name('deal')->where('id', $did)->update(['d_status' => -1]);
                //改变子表
                $fine_save = Db::name('fine')->where('did', $did)->update(['f_status' => -1]);
                if ($deal_save && $fine_save) {
                    //改变余额
                    $res_balance_sell = do_logs($res['uid'], 1, 'm_balance', $all_price, '撤单,金额返还');
                    if ($res_balance_sell) {
                        return json(['code' => 1, 'msg' => '撤单成功']);
                    }
                    return json(['code' => 0, 'msg' => '撤单失败']);
                }
                return json(['code' => 0, 'msg' => '撤单失败']);
            } else {
                //卖
                return json(['code' => 1, 'msg' => '挂卖不能撤单']);
            }
        }
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        //查看自己匹配中的单子
        $list = Db::name('deal')
            ->alias('d')
            ->join('product p', 'd.pid=p.id', 'LEFT')
            ->where('uid', $uid)
            ->where('d_status', 'in', '10,-1')
            ->order('id', 'desc')
            ->field('p.p_title,d.*')
            ->select();
        foreach ($list as $key => $value) {
            $list[$key]['p_type'] = '零售产品';
            if ($value['d_credit_2'] > 0) {
                $list[$key]['p_type'] = '批发产品';
            }
            if ($value['d_credit_3'] > 0) {
                $list[$key]['p_type'] = '奖励产品';
            }
            $list[$key]['d_addtime'] = date('Y-m-d H:i:s', $value['d_addtime']);
        }
        $this->assign(array('config' => $config, 'user' => $user, 'list' => $list));
        return $this->fetch();
    }


    public function chengjiao()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        $type = $this->request->param('type');
        $condition['d_type'] = $type;
        if (empty($type)) {
            $condition = '';
        }
        $list = Db::name('deal')
            ->alias('d')
            ->join('product p', 'd.pid=p.id', 'LEFT')
            ->where('uid', $uid)
            ->where($condition)
            ->where('d_status', 'in', '-1,2,4,10')
            ->order('id', 'desc')
            ->field('p.p_title,d.*')
            ->select();
        foreach ($list as $key => $value) {
            $list[$key]['p_type'] = '零售产品';
            if ($value['d_credit_2'] > 0) {
                $list[$key]['p_type'] = '批发产品';
            }
            if ($value['d_credit_3'] > 0) {
                $list[$key]['p_type'] = '奖励产品';
            }
            if (!empty($value['d_finish_time'])) {
                $list[$key]['d_finish_time'] = date('Y-m-d H:i:s', $value['d_finish_time']);
            } else {
                $list[$key]['d_finish_time'] = '还未成交';
            }
        }
        //        halt($list);
        $this->assign(array('config' => $config, 'user' => $user, 'list' => $list, 'type' => $type));
        return $this->fetch();
    }

    //入金记录
    public function entryRecord()
    {

        $user = $this->check_user();
        $config = $this->set_config();
        $uid = $user['id'];
        $list = Db::name('zlog')
            ->where('uid', $uid)
            ->where('z_type', 1)
            ->order('z_addtime', 'desc')
            ->select();
        foreach ($list as $key => $value) {
            $list[$key]['z_addtime'] = date('Y-m-d H:i:s', $value['z_addtime']);
            if (empty($value['z_desc'])) {
                $list[$key]['z_desc'] = '暂无备注';
            }
            $list[$key]['z_overtime'] = date('Y-m-d H:i:s', $value['last_time']);
            if ($value['last_time'] == 0) {
                $list[$key]['z_overtime'] = '暂未更新';
            }
        }
        $this->assign('list', $list);
        $this->assign('config', $config);
        return $this->fetch();
    }

    //出金记录
    public function goldRecord()
    {
        $user = $this->check_user();
        $config = $this->set_config();
        $uid = $user['id'];
        $list = Db::name('zlog')
            ->where('uid', $uid)
            ->where('z_type', 2)
            ->order('z_addtime', 'desc')
            ->select();
        foreach ($list as $key => $value) {
            $list[$key]['z_addtime'] = date('Y-m-d H:i:s', $value['z_addtime']);
            if (empty($value['z_desc'])) {
                $list[$key]['z_desc'] = '暂无备注';
            }
            $list[$key]['z_overtime'] = date('Y-m-d H:i:s', $value['last_time']);
            if ($value['last_time'] == 0) {
                $list[$key]['z_overtime'] = '暂未更新';
            }
        }
        $this->assign('list', $list);
        $this->assign('config', $config);
        return $this->fetch();
    }

    //余额记录
    public function balanceRecord()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        $list = Db::name('logs')
            ->where('l_uid', $uid)
            ->where('l_type', 1)
            ->where('l_num <> -45135.40')
            ->where('l_num <> -30090.27')
            ->order('l_time', 'desc')
            ->select();
        foreach ($list as $key => $value) {
            $list[$key]['l_time'] = date('Y-m-d H:i:s', $value['l_time']);
        }
        $this->assign('list', $list);
        $this->assign('config', $config);
        return $this->fetch();
    }

    //积分记录
    public function integralRecord()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        $list = Db::name('Logs')
            ->where('l_uid', $uid)
            ->where('l_type', 2)
            ->order('l_time', 'desc')
            ->select();
        foreach ($list as $key => $value) {
            $list[$key]['l_time'] = date('Y-m-d H:i:s', $value['l_time']);
        }
        $this->assign('list', $list);
        $this->assign('config', $config);
        return $this->fetch();
    }

    //消费积分记录
    public function spendRecord()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        $list = Db::name('logs')
            ->where('l_uid', $uid)
            ->where('l_type', 3)
            ->order('l_time', 'desc')
            ->select();
        foreach ($list as $key => $value) {
            $list[$key]['l_time'] = date('Y-m-d H:i:s', $value['l_time']);
        }
        $this->assign('list', $list);
        $this->assign('config', $config);
        return $this->fetch();
    }

    //静态积分记录
    public function lockRecord()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        $list = Db::name('logs')
            ->where('l_uid', $uid)
            ->where('l_type', 4)
            ->order('l_time', 'desc')
            ->select();
        foreach ($list as $key => $value) {
            $list[$key]['l_time'] = date('Y-m-d H:i:s', $value['l_time']);
        }
        $this->assign('list', $list);
        $this->assign('config', $config);
        return $this->fetch();
    }



    public function test()
    {
         //set_deal();
            //$js = Db::name('Tuser')->where('t_add_price>=-5 and t_add_price<=5')->select();
            //$js = Db::name('Tuser')->select();
            //foreach ($js as $k=>$v){
                //$t_data = explode("：",$v['t_num']);
                //Db::name('Tuser')->where(array('t_id'=>$v['t_id']))->update(array('t_num'=>$t_data[1],'t_text'=>$t_data[2],'t_test'=>$t_data[3]));
                //$t_old_price      = $v['t_old_price']/100;
                //Db::name('User')->where(array('m_account'=>$v['t_account']))->update(array('m_balance'=>$t_old_price));
                //Db::name('Tuser')->where(array('t_id'=>$v['t_id']))->delete();
               /* $t_num          = str_replace('，交易所资金账号','',$v['t_num']);
                $t_account      = str_replace('，交易所余额','',$v['t_text']);
                $t_price        = str_replace('，清算中心余额',',',$v['t_test']);
                $t_data         = explode(',',$t_price);
                $t_user_price   = $t_data[0];
                $t_old_price    = $t_data[1];
                $t_add_price    = $t_data[1]-$t_data[0];
                Db::name('Tuser')->where(array('t_id'=>$v['t_id']))->update(array('t_num'=>$t_num,'t_account'=>$t_account,'t_user_price'=>$t_user_price,'t_old_price'=>$t_old_price,'t_add_price'=>$t_add_price));*/
            //}
            //die;
        //        dump($js);
        //        die;
        /* if(!empty($user)){
             foreach ($user as $k=>$v){
                 //$t_old_price = $v['t_user_price']-$v['t_price'];  //我们的余额减去联交运的余额所得余额
                 $t_add_price = $v['t_price']-$v['t_user_price'];  //联交运的余额减去我们的余额所得余额
                 $t_status = 0;
                 if($v['t_user_price'] != $v['t_price']){
                     $t_status = 1;
                 }
                 Db::name('Tuser')->where(array('t_account'=>$v['t_account']))->update(array('t_add_price'=>$t_add_price,'t_status'=>$t_status));

             }
         }*/
        //$user        = Db::name('User')->where(array('m_bank_signing'=>1))->select();
        //$count       = count($user);
        //print_r($count);die;
        /*if(!empty($user)){
            foreach ($user as $k=>$v){
                $is_user = Db::name('User')->where(array('m_account'=>$v['t_account']))->find();
                if(empty($is_user)){
                    Db::name('Tuser')->where(array('t_account'=>$v['t_account']))->delete();
                }
            }
        }*/
        //        if(!empty($buy_data)){
        //            foreach ($buy_data as $k=>$v){
        //                $sell_data  = Db::name('Js')->where(array('w_saccount'=>$v['w_account']))->select();
        //                if(!empty($sell_data)){
        //                    foreach ($sell_data as $k1 => $v1){
        //                        Db::name('Sjs')->insert(array('ws_code'=>$v1['w_code'],'ws_deal_code'=>$v1['w_deal_code'],'ws_account'=>$v1['w_account'],'ws_saccount'=>$v1['w_saccount'],'ws_price'=>$v1['w_price']));
        //                        Db::name('Js')->where(array('w_id'=> $v1['w_id']))->delete();
        //                    }
        //                }
        //            }
        //        }

        //        $logs = Db::name('Logs')->where(array('l_info'=>'日终对账,根据清算中心文件操作余额3'))->select();
        //        if(!empty($logs)){
        //            foreach ($logs as $k=>$v){
        //                $user  = Db::name('User')->where(array('id'=>$v['l_uid']))->find();
        //                $l_num = 0;
        //                if($v['l_num']<0){
        //                    $l_num = abs($v['l_num']);
        //                }elseif($v['l_num']>0){
        //                    $l_num = -$v['l_num'];
        //                }
        //                if(!empty($user)){
        //                    Db::name('User')->where(array('id'=>$user['id']))->update(array('m_balance'=>$user['m_balance']+$l_num));
        //                }
        //                Db::name('Logs')->where(array('id'=>$v['id']))->update(array('l_info'=>'日终对账,根据清算中心文件操作余额2'));
        //            }
        //        }
        //        Db::name('Zlog')->where(array('z_status'=>2))->where('z_overtime<>0')->update(array('z_status'=>1,'z_overtime'=>time()));
        //        print_r(111);
        //        die;
        //          $deal = Db::name('Deal')->where(array('d_status'=>1,'d_type'=>2,'d_sell_num'=>0))->select();
        //          foreach ($deal as $k=>$v){
        //                $wallet = Db::name('Wallet')->where(array('uid'=>$v['uid'],'pid'=>$v['pid']))->find();
        //                if(empty($wallet)){
        //                    add_wallet($v['uid'], $v['pid']);
        //                }
        //                if($v['d_credit_1']>0){
        //                    $set_wallet = $wallet['m_credit_1']+$v['d_credit_1'];
        //                    Db::name('Wallet')->where(array('id'=>$wallet['id']))->update(array('m_credit_1'=>$set_wallet));
        //                }elseif($v['d_credit_2']>0){
        //                    $set_wallet = $wallet['m_credit_2']+$v['d_credit_2'];
        //                    Db::name('Wallet')->where(array('id'=>$wallet['id']))->update(array('m_credit_2'=>$set_wallet));
        //                }elseif($v['d_credit_3']>0){
        //                    $set_wallet = $wallet['m_credit_3']+$v['d_credit_3'];
        //                    Db::name('Wallet')->where(array('id'=>$wallet['id']))->update(array('m_credit_3'=>$set_wallet));
        //                }
        //                Db::name('Deal')->where(array('id'=>$v['id']))->delete();
        //          }
        //          die;
        //        $time = time();
        //        $day  = 5;
        //        $res = set_deal_end_time(1588751465,$day);
        //        dump($res);die;
        //        $fine = Db::name('Fine')->where(array('f_status'=>1,'f_date'=>'2020-04-30'))->select();
        //        if(!empty($fine)){
        //            foreach ($fine as $k => $v){
        //                    $day         = 5*86400;
        //                    $new_endtime = $v['f_profit_end_time']+$day;
        //                    Db::name('Fine')->where(array('id'=>$v['id']))->update(array('f_profit_end_time'=>$new_endtime));
        //            }
        //        }
        //        print_r(111);die;

        /*$wte    = Db::name('Wte')->order('id desc')->select();
        //Db::name('Wallet')->where("pid=1 and uid not in (1880,1868,1870)")->update(array('m_credit_1'=>0,'m_credit_2'=>0,'m_credit_3'=>0,));
        foreach ($wte as $k => $v){
            if($v['m_credit_1']==false or $v['m_credit_1']==''){
                $v['m_credit_1'] = 0;
            }
            if($v['m_credit_2']==false or $v['m_credit_2']==''){
                $v['m_credit_2'] = 0;
            }
            if($v['m_credit_3']==false or $v['m_credit_3']==''){
                $v['m_credit_3'] = 0;
            }
            $user           = Db::name('User')->where(array('m_account'=>$v['m_account']))->find();
            if(!empty($user)){
                $uid        = $user['id'];
                $wallet     = Db::name('Wallet')->where(array('uid' => $uid, 'pid' => 1))->find();
                $w_id       = $wallet['id'];
                $m_credit_1 = $wallet['m_credit_1']+$v['m_credit_1'];
                $m_credit_2 = $wallet['m_credit_2']+$v['m_credit_2'];
                $m_credit_3 = $wallet['m_credit_3']+$v['m_credit_3'];
                Db::name('Wallet')->where(array('id'=>$w_id))->update(array('m_credit_1'=>$m_credit_1,'m_credit_2'=>$m_credit_2,'m_credit_3'=>$m_credit_3,'last_time'=>time()));
            }
        }*/
        //        Db::name('User')->where("m_name<>'' and m_car_id<>''")->update(array('m_real'=>1));
        //        $account      = '1818049119039,1813238826869,1800100000031,1815709151444,1800100000041,1813832212723,1800100000021,1800100000107,1800100000237,1800100000022';
        //        $test         = Db::name('Test')->where("m_account not in ($account) ")->order('id desc')->select();
        //        foreach ($test as $k1=>$v1){
        //            if($v1['m_bank_code'] == false){
        //                $v1['m_bank_code']=0;
        //            }
        //            if($v1['m_bank_open_code'] == false){
        //                $v1['m_bank_open_code']=0;
        //            }
        //            $time = 0;
        //            if($v1['m_bank_signing'] == 1){
        //                $time = time();
        //            }
        //            $user = Db::name('User')->where(array('m_account'=>$v1['m_account']))->find();
        //            if(!empty($user)){
        //                Db::name('User')->where(array('m_account'=>$v1['m_account']))->update(array(
        //                    'm_name'            => $v1['m_name'],
        //                    'm_car_id'          => $v1['m_car_id'],
        //                    'm_bank_name'       => $v1['m_bank_name'],
        //                    'm_bank_code'       => $v1['m_bank_code'],
        //                    'm_bank_open'       => $v1['m_bank_open'],
        //                    'm_bank_open_code'  => $v1['m_bank_open_code'],
        //                    'm_bank_carid'      => $v1['m_bank_carid'],
        //                    'm_bank_signing'    => $v1['m_bank_signing'],
        //                    'm_virtual_account' => $v1['m_virtual_account'],
        //                    'm_balance'         => $v1['m_balance'],
        //                    'm_checkout_account'=> $v1['m_checkout_account'],
        //                    'm_bank_settime'    => $time,
        //                ));
        //            }
        //        }
        //        die;
        //        $bank           = $this->bankCode();
        //        foreach ($bank as $k1=>$v1){
        //
        //            if(!empty($test)){
        //                Db::name('Sb1')->where("m_bank_open like '%$v1%'")->update(array('m_bank_name'=>$v1,'m_bank_code'=>$k1));
        //            }
        //        }
        //        print_r($test);die;
        //        $test1          = Db::name('Sb1')->where("m_bank_open<>''")->order('id desc')->select();
        //        print_r($test1);die;
        //        foreach ($test1 as $k => $v) {
        ////            $v['m_bank_signing'] = 0;
        ////            if($v['m_bank_signing_text'] == '成功'){
        ////                $v['m_bank_signing'] = 1;
        ////            }
        ////            if($v['m_balance']=='' || $v['m_balance'] == false){
        ////                $v['m_balance'] = 0;
        ////            }
        //            $data = Db::name('Sb1')->where(array('m_account' => $v['m_account']))->find();
        //            if (!empty($data)) {
        //                Db::name('Sb1')->where(array('m_account' => $v['m_account']))->update(array('m_balance'=>$v['m_balance'],'m_checkout_account' => $v['m_checkout_account'],'m_name' => $v['m_name']));
        //            }
        //            /*$bank = Db::name('Bank_database')->where(array('bank_open_code'=>$v['m_bank_open_code']))->find();
        //            if(!empty($bank)){
        //                Db::name('Sb1')->where(array('m_account'=>$v['m_account']))->update(array('m_bank_open'=>$bank['bank_name']));
        //            }*/
        //        }
    }

    /*public function test2(){
        $fee_ratio              = Db::name('config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
        $time                   = time();
        //$date                 = date('Y-m-d',$time);
        $date                   = '2020-05-11';
        //$init_date            = date('Ymd');              //业务时间
        $init_date              = '20200511';               //业务时间
        $exchange_code          = '02000016';               //交易所代码
        $biz_type               = 1;                        //业务类型
        $exchange_market_type   = 1;
        $from_data              =   array();
        $init_date              =   '20200511';               //业务时间
        $exchange_code          =   '02000016';               //交易所代码
        $biz_type               =   1;                        //业务类型
        $f_title                =   $init_date.'_'.$exchange_code.'_fees.txt';
        $fields['f_title']      =   $f_title;
        $fields['f_path']       =   $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/static/open_bank/';
        $path                   =   './static/open_bank/'.$fields['f_title'];
        $data                   =   Db::name('Deal')->where("d_addtime>=1588176000 and d_addtime<=1589212800 and d_type=2 and d_credit_2>0 and d_status=4")->select();
        if(!empty($data)){
            foreach ($data as $k1=>$v1){
                $row['init_date']                   = $init_date;
                $row['serial_no']                   = '3325'.$k1;
                $row['exchange_code']               = $exchange_code;
                $row['exchange_market_type']        = 1;
                $row['biz_type']                    = $biz_type;
                $row['exchange_fees_type']          = 3;                                                //必填
                $product                            = getProduct($v1['pid']);
                $row['fees_balance']                = ($product['p_retail_price']*$v1['d_sell_num']*100)/2;                   //必填
                $row['payer_mem_code']              = getMember($v1['uid'],'m_account');
                $row['payer_fund_account']          = getMember($v1['uid'],'m_account');
                $row['payee_mem_code']              = '1800000000001';
                $row['payee_fund_account']          = '1800000000001';
                $row['deal_id']                     = '';
                $row['remark']                      = '';
                $row['busi_datetime']               = date('YmdHis',$v1['d_addtime']);
                $from_data[]                        = $row;
            }
        }
        $file	                                    = fopen($path, 'w');
        foreach ($from_data as $k=>$v){
            $sta_data = implode('|',$v);
            if(file_exists($path)){
                file_put_contents($path, $sta_data.PHP_EOL, FILE_APPEND);
            }else{
                if($file){
                    fwrite($file, $sta_data);
                }
            }
        }
        fclose($file);
        die;
    }*/



    // 提货页面
    public function tihuo($id = 0)
    {

        $pro_list = Db::name('Product')->where(array('p_del' => 0, 'p_setup_status' => 1))->order('id asc')->select();
        
        // 点击提货进入
        if($id !== 0){

            $info = Db::name('deal')->where('id', $id)->find();
            $pro_list = Db::name('Product')->where(array('p_del' => 0, 'p_setup_status' => 1))->order('id asc')->select();

            $this->assign('deal_id', $info['id']);
            $this->assign('info', $info);

        }else{

            // 消费积分进入
            $where[] = [
                ['p_setup_status', '=', 1],
                ['p_stock', '>', 0]
            ];
               
            $exchange_pro_list = Db::name('exchange_product')->where($where)->field('id, p_title, p_code')->order('id asc')->select();

            // dump($exchange_pro_list); die;

            $this->assign('exchange_pro_list', $exchange_pro_list);
            $this->assign('deal_id', $id);
        }

        $this->assign('pro_list', $pro_list);

        return $this->fetch();

    }


    // 提货操作
    public function isTihuo()
    {

        if(request()->isPost()){
            $config = $this->set_config();
            if($config['w_exchange'] == 0){
                return ['code' => 400, 'msg' => '暂未开启置换功能'];
            }

            // $iME_status = $this->is_min_exchange_num();
            // if($iME_status['code'] !== 200){
            //     return $iME_status;
            // }

            $user = $this->check_user();

            $param = $this->request->param();

             // 判断请求参数
            $rule = [
                'deal_id'   => 'require',
                'bill_id'  => 'require|number',
                'num'   => 'require|number|integer',
                'real_name' => 'require',
                'phone' => 'require|mobile',
                'a_city' => 'require',
                'a_detail' => 'require',
            ];

            $msg = [
                'deal_id.require' => '请刷新后重新填写',
                'bill_id.require' => '请选择产品',
                'bill_id.number' => '请刷新后重新填写',
                'num.require'     => '请填写数量',
                'num.number'     => '数量只能是整数',
                'num.integer'     => '数量只能是整数',
                'real_name.require'   => '请填写姓名',
                'phone.require'   => '请填写手机号',
                'phone.mobile'   => '请填写正确的手机号',
                'a_city.require'  => '请选择地址',
                'a_detail.require'  => '请填写详细的地址',
            ];

            $validate   = Validate::make($rule, $msg);
            $result = $validate->check($param);

            if(!$result) {
                return ['code' => 400, 'msg' => $validate->getError()];
                // dump($validate->getError());
            }

            $time = time();

            if($param['th_type'] == 1){
                // 商品详情
                $product_info = Db::name('product')->where('id', $param['bill_id'])->field('id, p_retail_price, p_whole_price')->find();
                if(empty($product_info)){
                    return ['code' => 400, 'msg' => '操作失败'];
                }
            }elseif($param['th_type'] == 2){
                // 商品详情
                $product_info = Db::name('exchange_product')->where('id', $param['bill_id'])->find();
                if(empty($product_info)){
                    return ['code' => 400, 'msg' => '操作失败'];
                }
            }else{
                return ['code' => 400, 'msg' => '请刷新后重试'];
            }

            // 添加地址
            $address_data = [
                'a_name' => $param['real_name'],
                'a_phone' => $param['phone'],
                'a_city' => $param['a_city'],
                'a_detail' => $param['a_detail'],
                'uid' => $user['id'],
                'a_is_default' => 0,
                'a_time' => $time,
            ];
            $address_id = Db::name('address')->insertGetId($address_data);
            if(!$address_id){
                return ['code' => 400, 'msg' => '操作失败'];
            }

            // 消费积分进入
            if($param['deal_id'] == 0){

                // 判断积分是否足够
                // 需要支付的积分
                $need_pay_integral = bcmul($param['num'] , $product_info['p_retail_price'], 2);
                if(bccomp($user['m_integral'], $need_pay_integral, 2) < 0){
                    return ['code' => 400, 'msg' => '积分不足，操作失败'];
                }

                // 如果提货类型是兑换 修改产品的库存数量等
                if($param['th_type'] == 2){

                    // 判断库存是否足够
                    if(bccomp($param['num'], $product_info['p_stock']) > 0){
                        return ['code' => 400, 'msg' => '产品库存不足'];
                    }

                    $ex_pro_status = 1;
                    if(bccomp($param['num'], $product_info['p_stock']) == 0){
                        $ex_pro_status = 0;
                    }
                    
                    $ex_pro_data = [
                        'last_time' => $time,
                        'p_stock' => bcsub($product_info['p_stock'], $param['num'], 0),
                        'p_setup_status' => $ex_pro_status,
                    ];

                    Db::name('exchange_product')->where('id', $product_info['id'])->update($ex_pro_data);
                }

                try {
                    // 添加 order 信息
                    $order_data = [
                        'gid' => $product_info['id'], // 商品id
                        'o_buy_num' => $param['num'], // 购买数量
                        'o_price' => $product_info['p_retail_price'], // 商品单价
                        'o_code' => randomkeys(18), // 订单编号
                        'o_info' => '用户提货', // 订单留言
                        'o_credit_1' => $need_pay_integral, //订单总价
                        'uid' => $user['id'], // 用户id
                        'o_type' => 1, // 订单类型
                        'aid' => $address_id, // 地址id
                        'o_addtime' => $time,
                        'o_status' => 1, // 0待付款  1待发货 2待收货 3已完成
                        'o_exchange_type' => ($param['th_type'] == 2)?2:1, // 类型1产品表2置换表
                    ];
                    Db::name('order')->insert($order_data);

                    // 扣除用户积分
                    Db::name('user')->where('id', $user['id'])->setDec('m_integral', $need_pay_integral);

                    // 添加积分日志
                    $log_data = [
                        'l_uid' => $user['id'],
                        'l_type' => 2,
                        'l_num' => -$need_pay_integral,
                        'l_info' => '提货兑换，扣除积分',
                        'l_time' => $time,
                    ];
                    Db::name('logs')->insert($log_data);
    
                    return ['code' => 200, 'msg' => '操作成功'];
                } catch (\Throwable $th) {
                    //throw $th;
                    return ['code' => 400, 'msg' => '操作失败'];
                }

            }else{
                // 提货进入
                try {
                    // deal 详情
                    $deal_info = Db::name('deal')->where([
                        ['id', '=', $param['deal_id']],
                        ['d_status', '<>', 5]
                        ])->find();
                    if(empty($deal_info)){
                        return ['code' => 400, 'msg' => '操作失败'];
                    }
    
                    // 添加 order 信息
                    $order_data = [
                        'gid' => $deal_info['pid'], // 商品id
                        'o_buy_num' => $param['num'], // 购买数量
                        'o_price' => $product_info['p_retail_price'], // 商品单价
                        'o_code' => randomkeys(18), // 订单编号
                        'o_info' => '用户提货', // 订单留言
                        'o_credit_1' => bcmul($param['num'], $product_info['p_retail_price'], 2), //订单总价
                        'uid' => $user['id'], // 用户id
                        'o_type' => 2, // 订单类型
                        'aid' => $address_id, // 地址id
                        'o_addtime' => $time,
                        'o_status' => 1, // 0待付款  1待发货 2待收货 3已完成
                    ];
                    Db::name('order')->insert($order_data);
    
                    // 修改 deal 表
                    $dael_data = [
                        'd_status' => 5,
                    ];
                    Db::name('deal')->where('id', $param['deal_id'])->update($dael_data);
    
                    // 修改 fine 表
                    $fine_data = [
                        'f_status' => 4,
                    ];
                    Db::name('fine')->where([
                        ['did', '=', $param['deal_id']]
                    ])->update($fine_data);
    
                    return ['code' => 200, 'msg' => '操作成功'];
                } catch (\Throwable $th) {
                    //throw $th;
                    return ['code' => 400, 'msg' => '操作失败'];
                }
            }
        }
    }


    // 判断置换要求
    public function is_min_exchange_num()
    {
        // return ['code' => 200, 'msg' => '可进行置换'];

        $config = $this->set_config();

        $user = $this->check_user();

        $min_exchange_num = $config['w_min_exchange_num'];

        if($min_exchange_num > 0){

            $where = [
                'uid' => $user['id'],
                'gid' => 1,
            ];

            $user_num = Db::name('order')->where($where)->count('o_buy_num');

            if($user_num < $min_exchange_num){
                
                // return ['code' => 400, 'msg' => '置换需要持有'.$min_exchange_num.'盒宁红一号'];
                return ['code' => 400, 'msg' => '自提'.$min_exchange_num.'盒宁红茶方可置换其他产品'];
            }
            return ['code' => 200, 'msg' => '可进行置换'];

        }


    }

    // 积分互转页面
    public function integral_turn()
    {

        $min_mun = 186;

        $param = $this->request->param(); // 接收的参数

        if(empty($param['account'])){
            return json_encode(['code' => 400, 'msg' => '请填写对方账号']);
        }
        if(empty($param['number'])){
            return json_encode(['code' => 400, 'msg' => '请填写转赠数量']);
        }
        if($param['number'] < $min_mun){
            return json_encode(['code' => 400, 'msg' => '最低转赠数量为' . $min_mun]);
        }

        if($param['number']%$min_mun !== 0){
            return json_encode(['code' => 400, 'msg' => '转赠数量必须是'.$min_mun.'的倍数']);
        }

        $account = trim($param['account']);
        $number = trim($param['number']);

        $fee = bcmul($number, 0.05, 2); // 手续费

        // 当前用户的信息
        $user = $this->check_user();

        if(empty($user)){
            return json_encode(['code' => 400, 'msg' => '登录状态异常']);
        }

        $dec_number = $number + $fee; // 总扣除

        if($user['m_integral'] < $dec_number){
            return json_encode(['code' => 400, 'msg' => '您的积分不足']);
        }

        // 转增用户的信息
        $z_user = Db::name('user')->where('m_account', $account)->find();

        if(empty($z_user)){
            return json_encode(['code' => 400, 'msg' => '对方账号不存在']);
        }

        // 扣除当前用户的积分
        Db::name('user')->where('id', $user['id'])->setDec('m_integral', $dec_number);

        // 增加对方用户的积分
        Db::name('user')->where('id', $z_user['id'])->setInc('m_integral', $number);

        $time = time();

        // 扣除当前用户积分日志
        // 扣除档期啊男用户积分手续费日志
        // 增加对方用户积分日志
        $log_data = [
            [
                'l_uid' => $user['id'],
                'l_type' => 2,
                'l_num' => -$number,
                'l_info' => '积分转赠'.$account.'扣除积分',
                'l_time' => $time,
            ],
            [
                'l_uid' => $user['id'],
                'l_type' => 2,
                'l_num' => -$fee,
                'l_info' => '积分转赠'.$account.'扣除手续费',
                'l_time' => $time,
            ],
            [
                'l_uid' => $z_user['id'],
                'l_type' => 2,
                'l_num' => $number,
                'l_info' => $user['m_account'].'转赠积分增加',
                'l_time' => $time,
            ],
        ];

        Db::name('logs')->insertAll($log_data);

        return json_encode(['code' => 200, 'msg' => '转赠成功']);

    }




}
