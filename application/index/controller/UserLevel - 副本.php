<?php

namespace app\index\controller;

use think\Db;

class UserLevel extends Home
{
    //用户升级
    public function userLevel($user_id)
    {
        //获取该用户所有上级
        $upUser = Db::name('User')
            ->where('id', $user_id)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->value('m_line');
        if (empty($upUser)) {
            return ['code' => 0, 'msg' => '无须升级'];
        }
        $upUser = str_replace('0,', '', $upUser);
        $array  = explode(',', $upUser);         //转成数组
        $array  = array_reverse($array);                  //反转数组 从低级用户向上级用户升级
        foreach ($array as $value) {
            $userInfo = getUserInfo($value);
            if (is_array($userInfo) && $userInfo['m_lock'] == 1) { //删除或锁定跳过
                continue;
            }
            if (empty($value)) {
                continue;
            }
            //todo::开始升级 升降级实时 从低等级向高等级一级一级判断
            $total = $this->getLow($value);            //获取直推以及团队人数
            $this->up1($value);               //有效
            $this->up2($value, $total);       //初级
            $this->up3($value, $total);       //中级
            $this->up4($value, $total);       //高级
            $this->up5($value, $total);       //区域
        }
        return 1;
    }

    /**
     * 升有效
     * @param $user_id
     * @param $total
     */
    public function up1($user_id)
    {
        $is_have = Db::name('Deal')->where('uid', $user_id)->where('d_status', 2)->find();
        if (!empty($is_have)) {
            return Db::name('User')->where('id', $user_id)->update(['m_level' => 1]);
        }
    }


    //初级
    public function up2($user_id, $total)
    {
        if ($total['low'] >= 5 && $total['total'] >= 30) {//直推和团队满足
            return Db::name('user')->where('id', $user_id)->update(['m_level' => 2]);
        }
    }

    //中级
    public function up3($user_id, $total)
    {
        $lowCount = Db::name('User') //初级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 1)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        if ($total['low'] >= 5 && $lowCount >= 2) {
            return Db::name('user')->where('id', $user_id)->update(['m_level' => 3]);
        }
    }

    //高级
    public function up4($user_id, $total)
    {
        $lowCount = Db::name('User') //初级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 1)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        $inCount = Db::name('User') //中级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 2)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        if ($total['low'] >= 5 && $inCount >= 2 && $lowCount >= 1) {
            return Db::name('user')->where('id', $user_id)->update(['m_level' => 4]);
        }
    }


    //区域
    public function up5($user_id, $total)
    {
        $inCount = Db::name('User') //中级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 2)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        $upCount = Db::name('User') //高级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 3)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        if ($total['low'] >= 5 && $inCount >= 1 && $upCount >= 2) {
            return Db::name('user')->where('id', $user_id)->update(['m_level' => 5]);
        }
    }

    /**
     * 获取直推人数与团队人数
     * @param $user_id
     * @return mixed
     */
    public function getLow($user_id)
    {
        $low = Db::name('user')->where('id', $user_id)->field('m_line,m_level,m_push_num')->find();
        $total['low'] = $low['m_push_num'];                                                                                                                               //直推人数
        $total['total'] = Db::name('user')->whereLike('m_line', '%,' . $user_id . ',%')->where('m_level >= 1 and m_del=0 and m_lock=0')->count();     //团队人数
        $total['user_level'] = $low['m_level'];
        return $total;
    }

    public function auto_sell()
    {
        //接受参数
        $param = request()->param();
        $user = $this->check_user();
        $uid = $user['id'];
        $pid = $param['pid'];
        $sell_num = $param['sell_num'];
        $time = time();
        ####检测用户钱包是否存在####
        add_wallet($uid, $pid);
        //检测系统参数
//        $config_return_data = getSystemConfig($uid, $sell_num);
//        if (is_array($config_return_data)) {
//            return json($config_return_data);
//        }
        //检查有没有合适的买家
        $res = Db::name('deal')
            ->where('d_type', 1)
            ->where('uid', '<>', $uid)
            ->where('d_status', 10)
            ->where('d_total', $sell_num)
            ->find();
        #####修改卖方用户钱包产品票的数据#####
        $wallet_res = inspect_wallet($uid, $pid, $sell_num, $param['credit_type']);
        if (is_array($wallet_res)) {
            return json($wallet_res);
        }
        #####修改卖方用户钱包产品票的数据#####
        ####写入主表####
        $deal_data = array(
            'uid' => $uid,
            'pid' => $pid,
            'sid' => $pid,
            'd_code' => randomkeys(8),
            'd_type' => 2,
            'd_total' => $sell_num,
            'd_num' => $sell_num,
            'd_sell_num' => 0,
            'd_addtime' => $time,
            'd_status' => 4,
            'last_time' => $time,
            'd_grant' => -1,
        );
        $product_data = Db::name('Product')->where(array('id' => $pid))->find();
        if ($param['credit_type'] == 1) {
            $deal_data['d_price'] = $product_data['p_retail_price'];
            $deal_data['d_credit_1'] = $param['sell_num'];
            $fine_data['f_type'] = 1;
            $fine_data['f_price'] = $product_data['p_retail_price'];
        } elseif ($param['credit_type'] == 2) {
            $deal_data['d_price'] = $product_data['p_whole_price'];
            $deal_data['d_credit_2'] = $param['sell_num'];
            $fine_data['f_type'] = 2;
            $fine_data['f_price'] = $product_data['p_whole_price'];
        } elseif ($param['credit_type'] == 3) {
            $deal_data['d_price'] = $product_data['p_whole_price'];
            $deal_data['d_credit_3'] = $param['sell_num'];
            $fine_data['f_type'] = 3;
            $fine_data['f_price'] = $product_data['p_whole_price'];
        }
        $deal_price = $product_data['p_retail_price'] * $sell_num;
        //如果卖的是批发票那么应该有截留
        if ($param['credit_type'] == 2) {
            //取出截留比例
            $p_detail = getProduct($pid);
            if ($p_detail['p_back'] != 0) { //需要截留 截留金额等于利润*比例
                //计算利润 利润=（零售-批发）*数量
                $money = ($p_detail['p_retail_price'] - $p_detail['p_whole_price']) * $sell_num;
                $integral = bcmul($money, $p_detail['p_back'], 2); //入积分的数量
                $deal_price = bcsub($money, $integral, 2);//入余额的数量
                $add_integral = do_logs($uid, 2, 'm_integral', $integral, '出售成功,积分增加');
            }
        }

        //手续费
        $fee_ratio = Db::name('config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
        $all_fee = bcmul($deal_price, $fee_ratio, 2); //总手续费
        $deal_price = bcsub($deal_price, $all_fee, 2);//总金额-手续费
        $deal_data['d_fee'] = $all_fee;
        //检查该用户是否是指定账号
        $is_appoint = Db::name('wallet')->where('uid', $uid)->where('pid', $pid)->value('m_identity');
        if ($is_appoint != 0) {
            $deal_price = $product_data['p_retail_price'] * $sell_num;
            $deal_data['d_fee'] = 0;
        }
        $deal_id = Db::name('Deal')->insertGetId($deal_data);
        if (!$deal_id) {
            return json(['code' => 0, 'msg' => '写入失败']);
        }
        $deal_info = Db::name('Deal')->where('id', $deal_id)->find();
        //查询该产品的主力账户
        $appoint_user = Db::name('wallet')->where('pid', $pid)->where('m_identity', 1)->value('uid');
        if ($appoint_user == $uid) {//该卖家是主力账户无需秒卖
            if ($res) {//有用户需要交易
                if ($sell_num > $res['d_num']) {//卖出的数量>买家要买的数量
                    //todo::执行交易 剩下的等待下个买家
                    $surplus_num = $sell_num - $res['d_num']; //计算卖方卖完还剩多少个
                    //更新表数据
                    Db::name('deal')//买家主表更新
                        ->where('id', $res['id'])
                        ->update(['d_status' => 2, 'd_finish_time' => time()]);
                    Db::name('fine')//买家子表更新
                        ->where('did', $res['id'])
                        ->where('f_status', 7)
                        ->where('pid', $pid)
                        ->update(['f_status' => 1, 'sdid' => $deal_id]);
                    Db::name('deal')//卖家主表更新
                        ->where('id', $deal_id)
                        ->update(['d_num' => $surplus_num, 'd_sell_num' => $sell_num, 'last_time' => $time]);
                    $deal_price = $product_data['p_retail_price'] * $sell_num;
                    #####修改卖方用户余额#####
                    $res_balance_sell = do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                    #####修改卖方用户余额#####
                    #####发放奖励信息#####
                    $res_reward = auto_inspect_reward($res['uid'], $pid, $deal_info, $sell_num);
                    //发放团队奖
                    $team_reward = teamReward($res['uid'], $sell_num, $pid);
                    //升级
                    $userLevel = (new UserLevel())->userLevel($res['uid']);
                    if($res_balance_sell && $res_reward && $team_reward && $userLevel){
                        return json(['code' => 1, 'msg' => '交易成功']);
                    }
                    return json(['code' => 0, 'msg' => '交易失败']);
                }
                if ($sell_num < $res['d_num']) {//卖出的数量<买家要买的数量
                    //todo::卖出所有给买家
                    Db::name('deal')//买家主表更新
                        ->where('id', $res['id'])
                        ->update(['last_time' => $time, 'd_num' => $res['d_total'] - $res['d_sell_num'] - $sell_num,'d_sell_num'=>$res['d_sell_num']+$sell_num]);
                    //先获取买家子表数据对应的条数ID后续更新使用
                    $find_ids = Db::name('fine')
                        ->where('did', $res['id'])
                        ->where('f_status', 7)
                        ->where('pid', $pid)
                        ->limit(0, $sell_num)
                        ->column('id');
                    //买家子表更新
                    Db::name('fine')//买家子表更新
                        ->where('id', 'in', $find_ids)
                        ->update(['f_status' => 1, 'sdid' => $deal_id, 'f_finish_time' => $time]);
                    Db::name('deal')//卖家主表更新
                        ->where('id', $deal_id)
                        ->update(['d_num' => 0, 'd_sell_num' => $sell_num, 'd_status' => 4, 'd_finish_time' => $time, 'last_time' => $time]);
                    #####修改卖方用户余额#####
                    $res_balance_sell = do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                    #####修改卖方用户余额#####
                    //todo::给买方发放奖励
                    #####发放奖励信息#####
                    $res_reward = auto_inspect_reward($res['uid'], $pid, $deal_info, $sell_num);
                    //发放团队奖
                    $team_reward = teamReward($res['uid'], $sell_num, $pid);
                    //升级
                    $userLevel = (new UserLevel())->userLevel($res['uid']);
                    //todo::应该继续向下匹配 待完善。。。
                    $sell_order = Db::name('deal')
                        ->where('d_type',2)
                        ->where('d_num','<>',0)
                        ->where('d_status',1)
                        ->where('id','<>',$deal_id)
                        ->order('d_addtime','asc')
                        ->select();
                    if(empty($sell_order)){
                        return json(['code' => 1, 'msg' => '交易成功']);
                    }
                    $diff_num = $res['d_num'] - $sell_num;//还差多少
                    $star_num = 0;
                    $star_array = [];
                    foreach ($sell_order as $key =>$value){
                        $star_num+= $value['d_num']; //每个订单的数量进行累加
                        array_unshift($star_array,$value['id']);//将订单id推进数组
                        if($star_num>$diff_num){//够了 那么执行交易
                            //对数组内的订单id相应的订单进行操作
                            foreach ($star_array as $z){
                                //数组最后一个元素超的
                                $last =end($star_array);
                                if($value!=$last){//不是最后一个元素的全部卖出
                                    $order_info = Db::name('deal')->where('id',$z)->find(); //获取这笔订单的详情
                                    Db::name('deal')//买家主表更新
                                    ->where('id', $res['id'])
                                        ->update(['last_time' => $time, 'd_num' => $res['d_total'] - $res['d_sell_num'] - $order_info['d_num'],'d_sell_num'=>$res['d_sell_num']+$order_info['d_num']]);
                                    //取出买家对应数量的子表id更新使用
                                    $ids = Db::name('fine')
                                        ->where('did', $res['id'])
                                        ->where('f_status', 7)
                                        ->where('pid', $pid)
                                        ->order('id','asc')
                                        ->limit(0,$order_info['d_num'])
                                        ->column('id');
                                    Db::name('fine')//买家子表更新
                                        ->where('id', 'in',$ids)
                                        ->update(['status'=>1,'f_finish_time'=>$time]);
                                    Db::name('deal')//卖家主表更新
                                        ->where('id', $z)
                                        ->update(['d_num' => 0, 'd_sell_num' => $order_info['total'], 'd_status' => 6, 'd_finish_time' => $time, 'last_time' => $time]);
                                    $new_sell_num = $order_info['d_num'];
                                }else{ //最后超出部分的主单
                                    $new_sell_num = $diff_num-$star_num; //最后一单卖了多少
                                    $order_info = Db::name('deal')->where('id',$z)->find(); //获取这笔订单的详情
                                    Db::name('deal')//买家主表更新
                                        ->where('id', $res['id'])
                                        ->update(['last_time' => $time, 'd_num' => 0,'d_sell_num'=>$res['d_total'],'d_status'=>2,'d_finish_time'=>$time]);
                                    Db::name('fine')//买家子表更新
                                        ->where('did',$res['id'] )
                                        ->update(['status'=>1,'f_finish_time'=>$time,'f_end'=>1]);
                                    Db::name('deal')//卖家主表更新
                                        ->where('id', $z)
                                        ->update(['d_num' => $order_info['d_num']-$new_sell_num, 'd_sell_num' => $order_info['d_sell_num']+$new_sell_num, 'd_finish_time' => $time, 'last_time' => $time]);
                                    $new_sell_num = $order_info['d_num'];
                                }
                                #####修改卖方用户余额#####
                                $res_balance_sell = do_logs($order_info['uid'], 1, 'm_balance', $new_sell_num*$product_data['p_retail_price'], '出售成功,产品货款入金');
                                #####修改卖方用户余额#####
                                //todo::给买方发放奖励
                                #####发放奖励信息#####
                                $res_reward = auto_inspect_reward($res['uid'], $pid, $order_info, $new_sell_num);
                                //发放团队奖
                                $team_reward = teamReward($res['uid'], $new_sell_num, $pid);
                                //升级
                                $userLevel = (new UserLevel())->userLevel($res['uid']);
                            }
                        }
                    }
                    if($res_balance_sell && $res_reward && $team_reward && $userLevel){
                        return json(['code' => 1, 'msg' => '交易成功']);
                    }
                    return json(['code' => 0, 'msg' => '交易失败']);
                }
                if ($sell_num == $res['d_num']) {
                    //todo::直接交易即可
                    Db::name('deal')//买家主表更新
                        ->where('id', $res['id'])
                        ->update(['d_num' => 0, 'd_sell_num' => $res['d_total'], 'd_status' => 2, 'last_time', 'd_finish_time' => $time]);
                    Db::name('fine')//买家子表更新
                        ->where('did', $res['id'])
                        ->where('f_status', 7)
                        ->where('pid', $pid)
                        ->update(['f_status' => 1, 'sdid' => $deal_id]);
                    Db::name('deal')//卖家主表更新
                        ->where('id', $deal_id)
                        ->update(['d_num' => 0, 'd_sell_num' => $sell_num, 'last_time' => $time, 'd_status' => 4, 'd_finish_time' => $time]);
                    #####修改卖方用户余额#####
                    $res_balance_sell = do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                    #####修改卖方用户余额#####
                    #####发放奖励信息#####
                    $res_reward = auto_inspect_reward($res['uid'], $pid, $deal_info, $sell_num);
                    //发放团队奖
                    $team_reward = teamReward($res['uid'], $sell_num, $pid);
                    //升级
                    $userLevel = (new UserLevel())->userLevel($res['uid']);
                    if($res_balance_sell && $res_reward && $team_reward && $userLevel){
                        return json(['code' => 1, 'msg' => '交易成功']);
                    }
                    return json(['code' => 0, 'msg' => '交易失败']);
                }
            }
        }
        if ($appoint_user != $uid) {//该卖家用户不是主力账户 需要秒卖
            //不是主力账户 那么会产生手续费 计算应到账
            //手续费
            $fee_ratio = Db::name('config')->where('id', 1)->value('w_deal_fee') / 100; //获取系统设定手续费
            $all_fee = bcmul($deal_price, $fee_ratio, 2); //总手续费
            $deal_price = bcsub($deal_price, $all_fee, 2);//总金额-手续费
            $deal_data['d_fee'] = $all_fee;
            if ($res) {//有用户需要交易
                if ($sell_num > $res['d_num']) {//卖出的数量>买家要买的数量
                    //todo::执行交易 剩下的卖给主力账户
                    //更新表数据
                    $surplus_num = $sell_num - $res['d_num']; //计算卖方卖完还剩多少个
                    Db::name('deal')//买家主表更新
                        ->where('id', $res['id'])
                        ->update(['d_status' => 2, 'd_finish_time' => time()]);
                    Db::name('fine')//买家子表更新
                        ->where('did', $res['id'])
                        ->where('f_status', 7)
                        ->where('pid', $pid)
                        ->update(['f_status' => 1, 'sdid' => $deal_id]);
                    Db::name('deal')//卖家主表更新
                        ->where('id', $deal_id)
                        ->update(['d_num' => $surplus_num, 'd_sell_num' => $sell_num, 'last_time' => $time]);
                    #####修改卖方用户余额#####
                    $res_balance_sell = do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                    #####修改卖方用户余额#####
                    #####发放用户奖励信息#####
                    $res_reward = auto_inspect_reward($res['uid'], $pid, $deal_info, $sell_num);
                    //发放团队奖
                    $team_reward = teamReward($res['id'], $sell_num, $pid);
                    //升级
                    $userLevel = (new UserLevel())->userLevel($res['uid']);
                    #####发放用户奖励信息#####
                    #####修改主力账户用户余额#####
                    $should_price = ($sell_num-$res['d_num'])*$product_data['p_retail_price']; //主力账户应付的钱为 (出售数量-买家买走数量)*单价
                    $res_balance_buy = set_balance($appoint_user, $should_price);
                    if (is_array($res_balance_buy)) {
                        return json($res_balance_buy);
                    }
                    #####修改主力账户用户余额#####
                    #####发放主力账户奖励信息#####
                    $res_reward = auto_inspect_reward($appoint_user, $pid, $deal_info, $sell_num);
                    //发放团队奖
                    $team_reward = teamReward($appoint_user, $sell_num, $pid);
                    //升级
                    $userLevel = (new UserLevel())->userLevel($res['uid']);
                    #####发放主力账户奖励信息#####
                    if($res_balance_sell && $res_reward && $team_reward && $userLevel){
                        return json(['code' => 1, 'msg' => '交易成功']);
                    }
                    return json(['code' => 0, 'msg' => '交易失败']);
                }
                if ($sell_num < $res['d_num']) {//卖出的数量<买家要买的数量
                    //todo::卖出所有给买家
                    Db::name('deal')//买家主表更新
                        ->where('id', $res['id'])
                        ->update(['d_num'=>$res['d_total']- $res['d_sell_num'] - $sell_num,'last_time'=>$time,'d_sell_num'=>$res['d_sell_num']+$sell_num]);
                    //先获取买家子表数据对应的条数ID后续更新使用
                    $find_ids = Db::name('fine')
                        ->where('did', $res['id'])
                        ->where('f_status', 7)
                        ->where('pid', $pid)
                        ->limit(0, $sell_num)
                        ->column('id');
                    Db::name('fine')//买家子表更新
                        ->where('did', $res['id'])
                        ->where('id','in',$find_ids)
                        ->where('f_status', 7)
                        ->where('pid', $pid)
                        ->update(['f_status' => 1, 'sdid' => $deal_id]);
                    Db::name('deal')//卖家主表更新
                        ->where('id', $deal_id)
                        ->update(['d_num' => 0, 'd_sell_num' => $sell_num, 'last_time' => $time]);
                    #####修改卖方用户余额#####
                    $res_balance_sell = do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                    #####修改卖方用户余额#####
                    #####发放奖励信息#####
                    $res_reward = auto_inspect_reward($res['uid'], $pid, $deal_info, $sell_num);
                    //发放团队奖
                    $team_reward = teamReward($res['uid'], $sell_num, $pid);
                    //升级
                    $userLevel = (new UserLevel())->userLevel($res['uid']);
                    //todo::应该继续向下匹配 待完善。。。
                    $sell_order = Db::name('deal')
                        ->where('d_type',2)
                        ->where('d_num','<>',0)
                        ->where('d_status',1)
                        ->where('id','<>',$deal_id)
                        ->order('d_addtime','asc')
                        ->select();
                    if(empty($sell_order)){
                        return json(['code' => 1, 'msg' => '交易成功']);
                    }
                    $diff_num = $res['d_num'] - $sell_num;//还差多少
                    $star_num = 0;
                    $star_array = [];
                    foreach ($sell_order as $key =>$value){
                        $star_num+= $value['d_num']; //每个订单的数量进行累加
                        array_unshift($star_array,$value['id']);//将订单id推进数组
                        if($star_num>$diff_num){//够了 那么执行交易
                            //对数组内的订单id相应的订单进行操作
                            foreach ($star_array as $z){
                                //数组最后一个元素超的
                                $last =end($star_array);
                                if($value!=$last){//不是最后一个元素的全部卖出
                                    $order_info = Db::name('deal')->where('id',$z)->find(); //获取这笔订单的详情
                                    Db::name('deal')//买家主表更新
                                    ->where('id', $res['id'])
                                        ->update(['last_time' => $time, 'd_num' => $res['d_total'] - $res['d_sell_num'] - $order_info['d_num'],'d_sell_num'=>$res['d_sell_num']+$order_info['d_num']]);
                                    //取出买家对应数量的子表id更新使用
                                    $ids = Db::name('fine')
                                        ->where('did', $res['id'])
                                        ->where('f_status', 7)
                                        ->where('pid', $pid)
                                        ->order('id','asc')
                                        ->limit(0,$order_info['d_num'])
                                        ->column('id');
                                    Db::name('fine')//买家子表更新
                                    ->where('id', 'in',$ids)
                                        ->update(['status'=>1,'f_finish_time'=>$time]);
                                    Db::name('deal')//卖家主表更新
                                    ->where('id', $z)
                                        ->update(['d_num' => 0, 'd_sell_num' => $order_info['total'], 'd_status' => 6, 'd_finish_time' => $time, 'last_time' => $time]);
                                    $new_sell_num = $order_info['d_num'];
                                }else{ //最后超出部分的主单
                                    $new_sell_num = $diff_num-$star_num; //最后一单卖了多少
                                    $order_info = Db::name('deal')->where('id',$z)->find(); //获取这笔订单的详情
                                    Db::name('deal')//买家主表更新
                                    ->where('id', $res['id'])
                                        ->update(['last_time' => $time, 'd_num' => 0,'d_sell_num'=>$res['d_total'],'d_status'=>2,'d_finish_time'=>$time]);
                                    Db::name('fine')//买家子表更新
                                    ->where('did',$res['id'] )
                                        ->update(['f_status'=>1,'f_finish_time'=>$time,'f_end'=>1]);
                                    Db::name('deal')//卖家主表更新
                                    ->where('id', $z)
                                        ->update(['d_num' => $order_info['d_num']-$new_sell_num, 'd_sell_num' => $order_info['d_sell_num']+$new_sell_num, 'd_finish_time' => $time, 'last_time' => $time]);
                                    $new_sell_num = $order_info['d_num'];
                                }
                                #####修改卖方用户余额#####
                                $res_balance_sell = do_logs($order_info['uid'], 1, 'm_balance', $new_sell_num*$product_data['p_retail_price'], '出售成功,产品货款入金');
                                #####修改卖方用户余额#####
                                //todo::给买方发放奖励
                                #####发放奖励信息#####
                                $res_reward = auto_inspect_reward($res['uid'], $pid, $order_info, $new_sell_num);
                                //发放团队奖
                                $team_reward = teamReward($res['uid'], $new_sell_num, $pid);
                                //升级
                                $userLevel = (new UserLevel())->userLevel($res['uid']);
                            }

                        }
                    }
                    if($res_balance_sell && $res_reward && $team_reward && $userLevel){
                        return json(['code' => 1, 'msg' => '交易成功']);
                    }
                    return json(['code' => 0, 'msg' => '交易失败']);
                }
                if ($sell_num == $res['d_num']) {
                    //todo::直接交易即可
                    Db::name('deal')//买家主表更新
                        ->where('id', $res['id'])
                        ->update(['d_num' => 0, 'd_sell_num' => $res['d_total'], 'd_status' => 2, 'last_time'=>$time, 'd_finish_time' => $time]);
                    Db::name('fine')//买家子表更新
                        ->where('did', $res['id'])
                        ->where('f_status', 7)
                        ->where('pid', $pid)
                        ->update(['f_status' => 1, 'sdid' => $deal_id]);
                    Db::name('deal')//卖家主表更新
                        ->where('id', $deal_id)
                        ->update(['d_num' => 0, 'd_sell_num' => $sell_num, 'last_time' => $time, 'd_status' => 4, 'd_finish_time' => $time]);
                    #####修改卖方用户余额#####
                    $res_balance_sell = do_logs($uid, 1, 'm_balance', $deal_price, '出售成功,产品货款入金');
                    #####修改卖方用户余额#####
                    #####发放奖励信息#####
                    $res_reward = auto_inspect_reward($res['uid'], $pid, $deal_info, $sell_num);
                    //发放团队奖
                    $team_reward = teamReward($res['uid'], $sell_num, $pid);
                    //升级
                    $userLevel = (new UserLevel())->userLevel($res['uid']);
                    if($res_balance_sell && $res_reward && $team_reward && $userLevel){
                        return json(['code' => 1, 'msg' => '交易成功']);
                    }
                    return json(['code' => 0, 'msg' => '交易失败']);
                }
            }
        }
    }

    public function auto_buy()
    {
        $config = $this->set_config();
        $user = $this->check_user();
        $uid = $user['id'];
        if ($this->request->post()) {
            $fields = input('post.');
            $pid = $fields['pid'];
            $buy_num = $fields['buy_num'];
            $time = time();
            $next_time = $time+(86400*5);
            $product = Db::name('Product')->where(array('id' => $pid))->find();
            //检测系统参数
//            $config_return_data = getSystemConfig($uid, $buy_num);
//            if (is_array($config_return_data)) {
//                return json($config_return_data);
//            }
            //1.检查买方是否被限制交易和是否被交易过
            $buy_return_data = getUserIdentity($uid, $pid, $buy_num, 1);
            if (is_array($buy_return_data)) {
                return json($buy_return_data);
            }
            //2.检查买方是否有钱包
            add_wallet($uid, $pid);
            //3.检查买方余额是否充足
            $deal_price = $product['p_retail_price'] * $buy_num;
            $buy_balance = getUserBalanceIdentity($uid, $deal_price);
            if (is_array($buy_balance)) {
                return json($buy_balance);
            }
            //获取当前产品的主力账户
            $appoint_user = Db::name('wallet')->where('pid',$pid)->where('m_identity',1)->value('uid');
            //插入数据
            $deal_data = [
                'uid' => $uid,
                'pid' => $pid,
                'd_code' => randomkeys(8),
                'd_credit_1' => $buy_num,
                'd_total' => $buy_num,
                'd_num'=>$buy_num,
                'd_type' => 1,
                'd_addtime' => $time,
                'd_price' => $deal_price,
                'd_date'=>date('Y-m-d',time()),
                'd_status' => 10
            ];
            $insert_deal = Db::name('deal')->insertGetId($deal_data);
            $fine_data = [
                'bid' => $uid,
                'did' => $insert_deal,
                'pid' => $pid,
                'sid' => 1,
                'f_type'=>1,
                'f_status' => 7,
                'f_price' => $product['p_retail_price'],
                'f_profit_start_time'=>$time,
                'f_profit_end_time'=>$next_time,
                'f_date' => date('Y-m-d',time()),
                'f_addtime' => $time,
            ];
            for ($i = 1; $i <= $buy_num; $i++) {
                $fine_data['f_code'] = randomkeys(8);
                Db::name('fine')->insert($fine_data);//插入子表
            }
            //检查有没有符合条件的单子
            $deal_info = Db::name('deal')
                ->where('uid',$appoint_user)
                ->where('d_type', 2)
                ->where('d_status', 1)
                ->select();
            $buy_uid = $uid;
            if ($deal_info) {//有合适的进行交易
                $sum = Db::name('deal')
                    ->where('uid',$appoint_user)
                    ->where('d_type', 2)
                    ->where('d_status', 1)
                    ->sum('d_num');
                $buy_order_info= Db::name('deal')->where('id',$insert_deal)->find();
                if($sum>=$buy_num){ //卖方队列数量足够
                    $number = 0;
                    foreach ($deal_info as $key =>$value){
                        $number +=$value['d_num'];
                        array_unshift($star_array,$value['id']);//将订单id推进数组
                        if($number>$buy_num){//超出
                            foreach ($star_array as $z){
                                //数组最后一个元素超的
                                $last =end($star_array);
                                $order_info = Db::name('deal')->where('id',$z)->find(); //获取这笔卖家订单的详情
                                if($value!=$last){//不是最后一个元素的全部卖出
                                    Db::name('deal')//买家主表更新
                                        ->where('id', $insert_deal)
                                        ->update(['last_time' => $time, 'd_num' => $buy_order_info['d_num']-$z['d_num'],'d_sell_num'=>$buy_order_info['d_sell_num']+$order_info['d_num']]);
                                    //取出买家对应数量的子表id更新使用
                                    $ids = Db::name('fine')
                                        ->where('did', $insert_deal)
                                        ->where('f_status', 7)
                                        ->where('pid', $pid)
                                        ->order('id','asc')
                                        ->limit(0,$order_info['d_num'])
                                        ->column('id');
                                    Db::name('fine')//买家子表更新
                                        ->where('id', 'in',$ids)
                                        ->update(['f_status'=>1,'f_finish_time'=>$time]);
                                    Db::name('deal')//卖家主表更新
                                    ->where('id', $z)
                                        ->update(['d_num' => 0, 'd_sell_num' => $order_info['total'], 'd_status' => 4, 'd_finish_time' => $time, 'last_time' => $time]);
                                    $new_sell_num = $order_info['d_num'];
                                }else{ //最后超出部分的主单
                                    $new_sell_num = $buy_num-$number; //最后一单卖了多少
                                    Db::name('deal')//买家主表更新
                                    ->where('id', $insert_deal)
                                        ->update(['last_time' => $time, 'd_num' => 0,'d_sell_num'=>$buy_order_info['d_total'],'d_status'=>2,'d_finish_time'=>$time]);
                                    Db::name('fine')//买家子表更新
                                    ->where('did',$insert_deal)
                                        ->update(['status'=>1,'f_finish_time'=>$time,'f_end'=>1]);
                                    Db::name('deal')//卖家主表更新
                                        ->where('id', $z)
                                        ->update(['d_num' => $order_info['d_num']-$new_sell_num, 'd_sell_num' => $order_info['d_sell_num']+$new_sell_num,'last_time' => $time]);
                                }
                                #####修改卖方用户余额#####
                                $res_balance_sell = do_logs($order_info['uid'], 1, 'm_balance', $new_sell_num*$product['p_retail_price'], '出售成功,产品货款入金');
                                #####修改卖方用户余额#####
                                //todo::给买方发放奖励
                                #####发放奖励信息#####
                                $res_reward = auto_inspect_reward($insert_deal['uid'], $pid, $order_info, $new_sell_num);
                                //发放团队奖
                                $team_reward = teamReward($insert_deal['uid'], $new_sell_num, $pid);
                                //升级
                                $userLevel = (new UserLevel())->userLevel($insert_deal['uid']);
                            }
                            if($res_balance_sell && $res_reward && $team_reward && $userLevel){
                                return json(['code'=>1,'msg'=>'交易成功']);
                            }else{
                                return json(['code'=>0,'msg'=>'交易失败']);
                            }

                        }
                        if($number==$buy_num){
                            foreach ($star_array as $z){
                                $order_info = Db::name('deal')->where('id',$z)->find(); //获取这笔卖家订单的详情
                                Db::name('deal')//买家主表更新
                                    ->where('id', $insert_deal)
                                    ->update(['last_time' => $time, 'd_num' => 0,'d_sell_num'=>$buy_order_info['d_total'],'d_finish_time'=>$time,'d_status'=>2]);
                                Db::name('fine')//买家子表更新
                                    ->where('did',$insert_deal)
                                    ->update(['f_status'=>1,'f_finish_time'=>$time,'f_end'=>1]);
                                Db::name('deal')//卖家主表更新
                                    ->where('id', $z)
                                    ->update(['d_num' => 0, 'd_sell_num' => $order_info['d_total'],'last_time' => $time,'d_status'=>4,'d_finish_time'=>$time]);
                                #####修改卖方用户余额#####
                                $res_balance_sell = do_logs($order_info['uid'], 1, 'm_balance', $order_info['d_num']*$product['p_retail_price'], '出售成功,产品货款入金');
                                #####修改卖方用户余额#####
                                //todo::给买方发放奖励
                                #####发放奖励信息#####
                                $res_reward = auto_inspect_reward($insert_deal['uid'], $pid, $order_info, $order_info['d_num']);
                                //发放团队奖
                                $team_reward = teamReward($insert_deal['uid'], $order_info['d_num'], $pid);
                                //升级
                                $userLevel = (new UserLevel())->userLevel($insert_deal['uid']);
                            }
                            if($res_balance_sell && $res_reward && $team_reward && $userLevel){
                                return json(['code'=>1,'msg'=>'交易成功']);
                            }else{
                                return json(['code'=>0,'msg'=>'交易失败']);
                            }
                        }
                    }
                }else{ //卖方队列数量不够
                    //修改买方单子信息
                    Db::name('deal')//买家主表更新
                    ->where('id', $insert_deal)
                        ->update(['last_time' => $time, 'd_num' => $buy_order_info['d_num']-$sum,'d_sell_num'=>$sum]);
                    //取出买方子单对应数量的id
                    $ids = Db::name('find')
                        ->where('did',$insert_deal)
                        ->where('f_status',7)
                        ->where('f_end',0)
                        ->limit(0,$sum)
                        ->column('id');
                    Db::name('fine')//买家子表更新
                        ->where('did', 'in',$ids)
                        ->update(['f_status'=>1,'f_finish_time'=>$time,'f_end'=>1]);
                    foreach ($deal_info as $key =>$value){ //将全部单子卖给买方
                        //修改卖方单子
                        Db::name('deal')//卖家主表更新
                            ->where('id', $value)
                            ->update(['d_num' => 0, 'd_sell_num' => $value['d_total'],'last_time' => $time,'d_status'=>4,'d_finish_time'=>$time]);
                        #####修改卖方用户余额#####
                        $res_balance_sell = do_logs($value['uid'], 1, 'm_balance', $value['d_num']*$product['p_retail_price'], '出售成功,产品货款入金');
                        #####修改卖方用户余额#####
                        //todo::给买方发放奖励
                        #####发放奖励信息#####
                        $res_reward = auto_inspect_reward($insert_deal['uid'], $pid, $value, $value['d_num']);
                        //发放团队奖
                        $team_reward = teamReward($insert_deal['uid'], $value['d_num'], $pid);
                        //升级
                        $userLevel = (new UserLevel())->userLevel($insert_deal['uid']);
                    }
                    if($res_balance_sell && $res_reward && $team_reward && $userLevel){
                        return json(['code'=>1,'msg'=>'交易成功']);
                    }else{
                        return json(['code'=>0,'msg'=>'交易失败']);
                    }
                }
            }

        }
    }



}