<?php


namespace app\index\service;


use app\index\controller\Home;
use think\Db;

class User extends Home
{
    //用户升级
    public function userLevel($user_id)
    {
        //获取该用户所有上级
        $upUser = Db::name('user')
            ->where('id', $user_id)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->value('m_line');
        if (empty($upUser)) {
            return ['code' => 0, 'msg' => '无须升级'];
        }
        $array = explode(',', $upUser); //转成数组
        $array = array_reverse($array);  //反转数组 从低级用户向上级用户升级
        foreach ($array as $value) {
            if ($value == 0) { //最高级跳过
                continue;
            }
            $userInfo = getMember($value, 'm_del,m_lock');
            if ($userInfo['m_del'] || $userInfo['m_lock']) { //删除或锁定跳过
                continue;
            }
            //todo::开始升级 升降级实时 从低等级向高等级一级一级判断
            $total = $this->getLow($value); //获取直推以及团队人数
            $res = $this->up1($value); //有效
            $res = $this->up2($value, $total); //初级
            $res = $this->up3($value, $total); //中级
            $res = $this->up4($value, $total); //高级
            $res = $this->up5($value, $total); //区域
        }
        if($res){
            return ['code'=>1,'msg'=>'升级成功'];
        }
    }


    /**
     * 升有效
     * @param $user_id
     * @param $total
     */
    public function up1($user_id)
    {
        $is_have = Db::name('deal')->where('uid',$user_id)->where('d_status',2)->find();
        if($is_have){
           return Db::name('user')->where('id', $user_id)->update(['m_level' => 1]);
        }
    }

    //初级
    public function up2($user_id, $total)
    {
        if ($total['low'] >= 5 && $total['total'] >= 30) {//直推和团队满足
            return Db::name('user')->where('id', $user_id)->update(['m_level' => 2]);
        }
    }

    //中级
    public function up3($user_id, $total)
    {
        $lowCount = Db::name('user') //初级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 1)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        if ($total['low'] >= 5 && $lowCount >= 2) {
           return Db::name('user')->where('id', $user_id)->update(['m_level' => 3]);
        }
    }

    //高级
    public function up4($user_id, $total)
    {
        $lowCount = Db::name('user') //初级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 1)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        $inCount = Db::name('user') //中级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 2)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        if ($total['low'] >= 5 && $inCount >= 2 && $lowCount >= 1) {
           return Db::name('user')->where('id', $user_id)->update(['m_level' => 4]);
        }
    }

    //区域
    public function up5($user_id, $total)
    {
        $inCount = Db::name('user') //中级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 2)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        $upCount = Db::name('user') //高级下级
        ->where('m_tid', $user_id)
            ->where('m_level', '>', 3)
            ->where('m_del', 0)
            ->where('m_lock', 0)
            ->count();
        if ($total['low'] >= 5 && $inCount >= 1 && $upCount >= 2) {
           return Db::name('user')->where('id', $user_id)->update(['m_level' => 5]);
        }
    }

    /**
     * 获取直推人数与团队人数
     * @param $user_id
     * @return mixed
     */
    public function getLow($user_id)
    {
        $low = Db::name('user')
            ->where('id', $user_id)
            ->field('m_line,m_level,m_push_num')
            ->find();
        $total['low'] = $low['m_push_num'];//直推人数
        $total['total'] = Db::name('user')//团队人数
            ->whereLike('m_line','%,'.$user_id.',%')
            ->count();
        $total['user_level'] = $low['m_level'];
        return $total;
    }

    //自动匹配买入
    public function auto_buy(){

        $config         = $this->set_config();
        $user           = $this->check_user();
        $uid            = $user['id'];
        if($this->request->post()){
            $fields     = input('post.');
            $pid        = $fields['pid'];
            $buy_num    = $fields['buy_num'];
            $product    = Db::name('Product')->where(array('id'=>$pid))->find();
            //检测系统参数
            $config_return_data  = getSystemConfig($uid,$buy_num);
            if(is_array($config_return_data)){
                return json($config_return_data);
            }
            //1.检查买方是否被限制交易和是否被交易过
            $buy_return_data     = getUserIdentity($uid,$pid,$buy_num,1);
            if(is_array($buy_return_data)){
                return json($buy_return_data);
            }
            //2.检查买方是否有钱包
            add_wallet($uid,$pid);
            //3.检查买方余额是否充足
            $deal_price          = $product['p_retail_price']*$buy_num;
            $buy_balance         = getUserBalanceIdentity($uid,$deal_price);
            if(is_array($buy_balance)){
                return json($buy_balance);
            }
            //检查有没有符合条件的单子
            $deal_info = Db::name('deal')
                ->where('d_num',$buy_num)
                ->where('d_type',2)
                ->where('d_status',1)
                ->find();
//            halt($deal_info);
            $buy_uid = $uid;
            if($deal_info){//有合适的进行交易 代码不走这里 直接进else
                #####票的种类####
                $credit_type         =  1;
                if($deal_info['d_credit_2']>0){
                    $credit_type     = 2;
                }
                if($deal_info['d_credit_3']>0){
                    $credit_type     = 3;
                }
                #####票的种类####
                $sell_num            =   $deal_info['d_num'];
                $pid                 =   $deal_info['pid'];
                $did = $deal_info['id'];
                $product             =   getProduct($pid);
                $time = time();
                //检查是否触发奖批发票奖励信息 p_deal_ratio
                $deal_ratio         = explode(':',$product['p_deal_ratio']);
                if(empty($deal_ratio) || count($deal_ratio)!=2){
                    return json(array(
                        'msg' => '产品配售比例有误',
                        'code' => 0,
                    ));
                }
                //5.检测卖方订单是否完成
                $new_num                            = $sell_num-$buy_num;
                $set_sell_data['d_num']             = $new_num;
                $set_sell_data['last_time']         = $time;
                if($new_num > 0){
                    $set_sell_data['d_sell_num']    = $deal_info['d_sell_num']+$buy_num;
                    $set_sell_data['d_status']      = 1;
                }elseif($new_num == 0){
                    $set_sell_data['d_sell_num']    = $deal_info['d_total'];
                    $set_sell_data['d_status']      = 6;
                    $set_sell_data['d_finish_time'] = $time;
                }
                $fine_list      = Db::name('Fine')->where(array('did'=>$deal_info['id']))->order('id asc')->limit($buy_num)->select();
                if(empty($fine_list) || count($fine_list)<$buy_num){
                    return json(array(
                        'msg' => '产品数量不足,购买操作有误',
                        'code' => 0,
                    ));
                }
                $fine_list_id = array();
                foreach ($fine_list as $k=>$v){
                    $fine_list_id[]  = $v['id'];
                }
                #####修改卖方用户钱包产品票的数据#####
                $sell_uid = $deal_info['uid'];
                $wallet_res          =   inspect_wallet($sell_uid,$pid,$buy_num,$credit_type);
                if(is_array($wallet_res)){
                    return json($wallet_res);
                }
                #####修改卖方用户钱包产品票的数据#####

                #####修改买方用户钱包产品票的数据#####  (收益完成后写入)
                /*$wallet_res          =  set_wallet($buy_uid,$pid,$buy_num,1);
                if(!$wallet_res){
                    return json(array(
                        'msg' => '写入买方钱包失败,请联系管理员',
                        'code' => 0,
                    ));
                }*/
                #####修改买方用户钱包产品票的数据#####

                #####修改卖方订单状态#####
                $fine_ids            =   implode(',',$fine_list_id);
                $fine_res_sell       =   Db::name('Fine')->where("id in ($fine_ids)")->update(array('f_finish_time'=>$time,'f_end'=>1,'f_status'=>3));
                $deal_res_sell       =   Db::name('Deal')->where(array('id'=>$did))->update($set_sell_data);
                #####修改卖方订单状态#####

                #####修改买方用户余额#####
                $res_balance_buy      =   set_balance($buy_uid,$deal_price);
                if(is_array($res_balance_buy)){
                    return json($res_balance_buy);
                }
                #####修改买方用户余额#####

                #####修改卖方用户余额#####
                $res_balance_sell     =   do_logs($sell_uid,1,'m_balance',$deal_price,'出售成功,产品货款入金');
                #####修改卖方用户余额#####
                #####发放奖励信息#####
                $res_reward           =   inspect_reward($buy_uid,$pid,$deal_info,$buy_num);
                #####发放奖励信息#####
                if($fine_res_sell && $deal_res_sell && $res_reward && $res_balance_sell && $res_balance_buy){
                    return json(array(
                        'msg' => '交易成功',
                        'code' => 1,
                    ));
                }else{
                    return json(array(
                        'msg' => '交易过程发生错误,请稍后重试',
                        'code' => 0,
                    ));
                }
            }else{//没有进行排队
                #####修改买方用户余额#####
                $res_balance_buy      =   set_balance($buy_uid,$deal_price);
                if(is_array($res_balance_buy)){
                    return json($res_balance_buy);
                }
                #####修改买方用户余额#####
                $data = [
                    'uid'=>$uid,
                    'pid'=>$pid,
                    'd_code'=>randomkeys(8),
                    'd_credit_1'=>$buy_num,
                    'd_total'=>$buy_num,
                    'd_type'=>1,
                    'd_addtime'=>time(),
                    'd_price'=>$deal_price,
                    'd_status'=>10
                ];
                $res = Db::name('deal')->insertGetId($data);//插入主表
//            $fine_data = [
//                'bid'=>$uid,
//                'did'=>$res,
//                'pid'=>$pid,
//                'sid'=>1,
//                'f_status'=>7,
//                'f_price'=>$product['p_retail_price'],
//                'f_addtime'=>time(),
//            ];
//            for ($i=1;$i<=$buy_num;$i++){
//                Db::name('fine')->insert($fine_data);
//            }  //插入子表
                if($res){
                    return json(array(
                        'msg' => '交易成功,排单中',
                        'code' => 1,
                    ));
                }
                return json(array(
                    'msg' => '排单失败',
                    'code' => 0,
                ));
            }

        }
    }


    //卖出
    public function sell()
    {
        //接受参数
        $param = request()->param();
        $user           = $this->check_user();
        $uid            = $user['id'];
        $pid = $param['pid'];
        $sell_num = $param['sell_num'];
        $time = time();
        ####检测用户钱包是否存在####
        add_wallet($uid,$pid);
        //检测系统参数
        $config_return_data  = getSystemConfig($uid,$sell_num);
        if(is_array($config_return_data)){
            return json($config_return_data);
        }
        //检查有没有合适的买家
        $res = Db::name('deal')
            ->where('d_type',1)
            ->where('uid','<>',$uid)
            ->where('d_status',10)
            ->where('d_total',$sell_num)
            ->find();
        if($res){ //交易
            //检查有没有票
            $is_have = Db::name('deal')->where('uid',$uid)->where('d_status',2)->count();
            if($is_have<$sell_num){
                return json(['code'=>0,'msg'=>'数量不足']);
            }
            ####写入主表####
            $deal_data = array(
                'uid'            => $uid,
                'pid'            => $pid,
                'sid'            => $pid,
                'd_code'         => randomkeys(8),
                'd_type'         => 2,
                'd_total'        => $sell_num,
                'd_num'          => $sell_num,
                'd_sell_num'     => 0,
                'd_addtime'      => $time,
                'd_status'       => 4,
                'last_time'      => $time,
                'd_grant'        => -1,
            );
            $product_data       = Db::name('Product')->where(array('id'=>$pid))->find();
            if($param['credit_type'] == 1){
                $deal_data['d_price']       = $product_data['p_retail_price'];
                $deal_data['d_credit_1']    = $param['sell_num'];
                $fine_data['f_type']        = 1;
                $fine_data['f_price']       = $product_data['p_retail_price'];
            }elseif($param['credit_type'] == 2){
                $deal_data['d_price'] = $product_data['p_whole_price'];
                $deal_data['d_credit_2']    = $param['sell_num'];
                $fine_data['f_type']        = 2;
                $fine_data['f_price']       = $product_data['p_whole_price'];
            }elseif($param['credit_type'] == 3){
                $deal_data['d_price']       = $product_data['p_whole_price'];
                $deal_data['d_credit_3']    = $param['sell_num'];
                $fine_data['f_type']        = 3;
                $fine_data['f_price']       = $product_data['p_whole_price'];
            }
            $deal_price          = $product_data['p_retail_price']*$sell_num;
            $deal_data['d_price'] = $deal_price;
            $deal_id = Db::name('Deal')->insertGetId($deal_data);
            if(!$deal_id){
                return json(['code'=>0,'msg'=>'写入失败']);
            }
            $deal_info = Db::name('Deal')->where('id',$deal_id)->find();
            Db::name('deal')->where('id',$res['id'])->update(['d_status'=>4,'last_time'=>$time,'d_finish_time'=>$time]); //买家主表更新
            Db::name('deal')->where('id',$deal_id)->update(['d_status'=>2]); //卖家子表更新
//            Db::name('fine')->where('did',$res['id'])->where('pid',$pid)->update(['f_status'=>1]);//买家子表更新
            #####修改卖方用户余额#####
            $res_balance_sell     =   do_logs($uid,1,'m_balance',$deal_price,'出售成功,产品货款入金');
            #####修改卖方用户余额#####
            //发放奖励
            //todo::给买方发放奖励
            #####发放奖励信息#####
            $res_reward           =   inspect_reward($res['uid'],$pid,$deal_info,$sell_num);
            if($res_reward){
                $this->userLevel($res['uid']);
                return json(['code'=>0,'msg'=>'交易成功']);
            }
            #####发放奖励信息#####
            return json(['code'=>0,'msg'=>'交易失败']);
        }else{//交易给系统
            halt('交易给系统');
        }
    }



//    /**
//     * 自动匹配交易
//     */
//    public function transfer()
//    {
//        $param = request()->param();
//        $pid = $param['pid'];
//        $buy_num = $param['buy_num'];
//        $user = $this->check_user();
//        $uid = $user['id'];
//        $config_return_data = getSystemConfig($uid, $param['buy_num']); //校验系统参数
//        if (is_array($config_return_data)) {
//            return json($config_return_data);
//        }
//        if ($param['type'] == 1) { //1买
//            /**
//             * todo::1.判断买还是卖
//             * todo::2.如果是买 看有没有未交易完成的卖单 如果有发生交易事件 如果没有或者购买没有完成插入sql进行排队 等待计划任务执行
//             * todo::3.如果是卖 看有没有在排队的买家 如果有发生交易事件 如果没有 系统进行收购
//             */
//            //2.检查买方是否有钱包
//            add_wallet($uid, $pid);
//            //3.检查买方余额是否充足
//            $product = Db::name('Product')->where(array('id' => $pid))->find();
//            $deal_price = $product['p_retail_price'] * $buy_num;
//            $buy_balance = getUserBalanceIdentity($uid, $deal_price);
//            if (is_array($buy_balance)) {
//                return json($buy_balance);
//            }
//
//            //查看是否有足够数量未交易完成的子卖单
//            $son_order_count = Db::name('fine')
//                ->where('sid', '<>', $uid)
//                ->where('f_status', 0)
//                ->where('f_end', 0)
//                ->count();
//            //根据时间排序 交易单子
//            $son_order = Db::name('fine')
//                ->where('sid', '<>', $uid)
//                ->where('f_status', 0)
//                ->where('f_end', 0)
//                ->order('f_addtime', 'asc')
//                ->limit(0, $buy_num)
//                ->column('id');
//            //获取子订单的父单
//            $up_order = Db::name('deal')
//                ->where('id', 'in', $son_order)
//                ->order('d_addtime', 'asc')
//                ->field('id,d_num')
//                ->select();
//            if ($buy_num <= $son_order_count) {//可以直接进行交易
//                //todo::执行交易。。。
//                $num = 0;
//                foreach ($up_order as $key => $value) {
//                    $num += $value['d_num'];//每个单子的剩余和
//                    if ($num < $buy_num) { //和不够说明还要向下匹配
//                        //TODO::更新数据 待完善
//                        Db::name('deal')->where('id',$value['id'])->update(['d_num'=>0,'d_status'=>6,'last_time'=>time(),'d_finish_time'=>time()]);
//                        Db::name('fine')->where('did',$value['id'])->update(['f_finish_time'=>time(),'f_end'=>1,'f_status'=>3]);
//                    } else {
//                        $su_num = $num - $buy_num; //这笔单应该剩多少
//                        //TODO::这笔单减少数量 待完善
//                        Db::name('deal')->where('id',$value['id'])->update(['d_num'=>0,'d_status'=>6,'last_time'=>time(),'d_finish_time'=>time()]);
//                        Db::name('fine')->where('did',$value['id'])->update(['f_finish_time'=>time(),'f_end'=>1,'f_status'=>3]);
//                    }
//                }
//
//            } else { //买的数量大于子单数量 先交易能交易的
//                $calcNum = $buy_num - $son_order_count; //计算得出还差多少个子单子
//                //todo::把能交易的单子进行交易 待完善
//                $update = Db::name('fine')->where('id','in',$son_order)->update();//子单子更新
//                $update = Db::name('fine')->where('id','in',$son_order)->update();//父单子更新
//                //todo::然后存入sql 等待定时任务扫描执行 待完善
//                Db::name('test')->insert();
//            }
//        } else { //2卖
//            //查看排单表有没有等待的买家
//            $data = Db::name('test')->where('status', 0)->where('uid', '<>', $uid)->select();
//            if ($data) { //有买家
//                //检查数量是否足够被消费
//                $count = Db::name('test')->where('status', 0)->where('uid', '<>', $uid)->sum('calc_num');
//                if ($count >= $buy_num) {//够了 准备执行交易
//
//                } else {//不够 交易可以交易的 然后剩下的系统收购
//
//                }
//            } else { //无买家 系统收购
//
//            }
//        }
//
//
//    }
}