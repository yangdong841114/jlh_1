<?php

use think\Db;
use think\Session;

// 为方便系统核心升级，二次开发中需要用到的公共函数请写在这个文件，不要去修改common.php文件

//格式化数据
function SafeFilter($arr)
{
    $ra = Array('/([\x00-\x08,\x0b-\x0c,\x0e-\x19])/', '/script/', '/javascript/', '/vbscript/', '/expression/', '/applet/', '/meta/', '/xml/', '/blink/', '/link/', '/style/', '/embed/', '/object/', '/frame/', '/layer/', '/title/', '/bgsound/', '/base/', '/onload/', '/onunload/', '/onchange/', '/onsubmit/', '/onreset/', '/onselect/', '/onblur/', '/onfocus/', '/onabort/', '/onkeydown/', '/onkeypress/', '/onkeyup/', '/onclick/', '/ondblclick/', '/onmousedown/', '/onmousemove/', '/onmouseout/', '/onmouseover/', '/onmouseup/', '/onunload/');
    if (is_array($arr)) {
        foreach ($arr as $key => $value) {
            if (!is_array($value)) {
                if (!get_magic_quotes_gpc()) {
                    $value = addslashes($value);                                //给单引号（'）、双引号（"）、反斜线（\）与NUL（NULL字符）加上反斜线转义
                }
                $value = preg_replace($ra, '', $value);             //删除非打印字符，粗暴式过滤xss可疑字符串
                $arr[$key] = $value;                                            //去除 HTML 和 PHP 标记并转换为HTML实体
            } else {
                $this->SafeFilter($arr[$key]);
            }
        }
    }
    return $arr;
}

//聚合获取验证码
function jh_send_sms($mobile, $type)
{
    $url = "http://v.juhe.cn/sms/send";
    $sms = Db::name('Sms')->where(array('id' => 1))->find();
    if ($type == 'reg') {
        $tpl = $sms['w_user_reg_sms'];
    } elseif ($type == 'edt') {
        $tpl = $sms['w_user_log_sms'];
    } elseif ($type == 'fot') {
        $tpl = $sms['w_user_rep_sms'];
    }
    $M_code = str_replace('4', '6', mt_rand(10000, 99999));
    $M_code = 123456;
    $arr = array(
        'key' => $sms['w_secret'],
        'mobile' => $mobile,
        'tpl_id' => $tpl,                                     //您申请的短信模板ID,根据实际情况修改
        'tpl_value' => "#code#=" . $M_code      //您设置的模板变量,根据实际情况修改
    );
    $paramstring = http_build_query($arr);
    $content = juheCurl($url, $paramstring);
    $result = json_decode($content, true);
    if (!empty($result)) {
        return 1;
    } else {
        return 0;
    }
}

//聚合短信
function juheCurl($url, $params = false, $ispost = 0)
{
    $httpInfo = array();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'JuheData');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    if ($ispost) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, $url);
    } else {
        if ($params) {
            curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);
        } else {
            curl_setopt($ch, CURLOPT_URL, $url);
        }
    }
    $response = curl_exec($ch);
    if ($response === FALSE) {
        return 0;
    }
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
    curl_close($ch);
    return $response;
}

//短信宝
function send_code($phone, $type)
{
    $sms_key = 'w_user_' . $type . '_sms';
    $sms = Db::name('Sms')->where(array('id' => 1))->find();
    $sms_txt = $sms[$sms_key];
    $sms_name = '用户';
    $member = Db::name('User')->where(array('m_phone' => $phone))->find();
    if (!empty($member) && isset($member['m_name'])) {
        $sms_name = $member['m_name'];
    }
    $verifycode = generate_code(6);
    $sms_txt = str_replace("{name}", $sms_name, $sms_txt);
    $sms_txt = str_replace("{code}", $verifycode, $sms_txt);
    //$verifycode             =   123456;
    session('verycode', $verifycode);
    session('phone', $phone);
    return send_sms($phone, $sms_txt);
}

//生成验证码
function generate_code($length)
{
    return rand(pow(10, ($length - 1)), pow(10, $length) - 1);
}

//调用短信接口
function send_sms($pnum, $psms)
{
    $config = Db::name('Config')->where(array('id' => 1))->find();
    $w_sms_account = $config['w_sms_account'];
    $w_sms_token = $config['w_sms_token'];
    $sendurl = 'http://api.smsbao.com/sms?u=' . $w_sms_account . '&p=' . $w_sms_token . '&m=' . $pnum . '&c=' . urlencode($psms);
    return file_get_contents($sendurl);
}


//获取用户某个字段信息
function getMember($uid, $keys)
{
    $members = Db::name('User')->where(array('id' => $uid))->field($keys)->find();
    if (empty($members)) {
        return '暂无';
    }
    return $members[$keys];
}


//获取用户信息
function getUserInfo($uid)
{
    if ($uid) {
        $members = Db::name('User')->where(array('id' => $uid, 'm_del' => 0))->find();
        if (empty($members)) {
            return 0;
        }
        return $members;
    } else {
        return 0;
    }
}

/**
 * @param $uid
 * @param $pid
 * @param $buy_num
 * @param $type
 * @return array|int
 * @throws \think\db\exception\DataNotFoundException
 * @throws \think\db\exception\ModelNotFoundException
 * @throws \think\exception\DbException
 * 判断用户是否达到购买条件
 */
function getUserIdentity($uid, $pid, $buy_num, $type=1)
{
    $time = time();
    $date = date('Y-m-d', $time);
    $start_time = strtotime(date("Y-m-d", $time));
    $end_time   = $start_time + 60 * 60 * 24;
    $user = Db::name('User')->where(array('id' => $uid, 'm_del' => 0))->find();
    $product = Db::name('Product')->where(array('id' => $pid))->find();
    if ($user['m_lock'] == 1) {
        return array(
            'msg' => '用户已被锁定,不能交易',
            'code' => 0,
        );
    }
    $bank_signing = $user['m_bank_signing'];
    if ($bank_signing != 1) {
        return array(
            'msg' => '用户未完成签约,不能进行交易',
            'code' => 0,
        );
    }
    $help_array = array(904,1309,371,2261);
    if(in_array($uid,$help_array)){
        return 1;
    }
    if ($type) {
        $set_deal = $user['m_set_deal'];
        if ($set_deal == 1) {
            return array(
                'msg' => '您的买入交易已被关闭',
                'code' => 0,
            );
        }
        $order_limit = $user['m_order_limit'];        //最大交易量限制
        $deal_data   = Db::name('Deal')->where(array('d_type' =>1, 'uid' => $uid, 'pid' => $pid, 'd_price'=>$product['p_retail_price']))->where("d_status in (1,2,3,4,5,6,7,10) and d_addtime>$start_time and d_addtime<$end_time")->sum('d_total');
        $new_num     = $buy_num + $deal_data;
        if ($order_limit) {
            if ($order_limit < $new_num) {
                return array(
                    'msg' => '交易数量已超过上限',
                    'code' => 0,
                );
            }
        }
    }else{
        $set_sell_deal = $user['m_set_sell_deal'];
        if ($set_sell_deal == 1) {
            return array(
                'msg' => '您的卖出交易已被关闭',
                'code' => 0,
            );
        }
        $order_limit = $user['m_order_sell_limit'];        //最大交易量限制
        $deal_data   = Db::name('Deal')->where(array('d_type' =>2, 'uid' => $uid, 'pid' => $pid, 'd_price'=>$product['p_retail_price']))->where("d_status in (1,2,3,4,5,6,7) and d_addtime>$start_time and d_addtime<$end_time")->sum('d_total');
        $new_num     = $buy_num + $deal_data;
        if ($order_limit) {
            if ($order_limit < $new_num) {
                return array(
                    'msg' => '交易数量已超过上限',
                    'code' => 0,
                );
            }
        }
    }
//    $deal_info = Db::name('Deal')->where(array('d_type' => $type, 'uid' => $uid, 'pid' => $pid, 'd_date' => $date))->where("d_status in (1,2,4)")->find();
//    if (!empty($deal_info)) {
//        return array(
//            'msg' => '今天你已经交易过了',
//            'code' => 0,
//        );
//    }
    return 1;
}

//检测余额是否充足
function getUserBalanceIdentity($uid, $price)
{
    $user = Db::name('User')->where(array('id' => $uid, 'm_del' => 0))->find();
    $m_balance = $user['m_balance'];
    if ($price > $m_balance) {
        return array('code' => 0, 'msg' => '您的钱包余额不足');
    }
    $new_balance = $m_balance - $price;
    if ($new_balance < 0) {
        return array('code' => 0, 'msg' => '您的钱包余额不足');
    }
    return 1;
}



//检测系统参数
function getSystemConfig($uid=0,$num='',$type=1)
{
    $time           = time();
    $date           = date('Y-m-d',$time);
    $week           = week();
    $start_time     = $week[0];
    $end_time       = $week[1];
    $config = Db::name('Config')->where(array('id' => 1))->find();
    $deal_date = $config['w_deal_date'];       //交易日期
    if ($deal_date === 1) {
        $is_day = isHoliday();
        if($is_day!=0){
            return array('code' => 0, 'msg' => '非交易日不能进行交易');
        }
    }else{
        $deal_date_arr = explode(',',$deal_date);
        if(empty($deal_date_arr)){
            return array('code' => 0, 'msg' => '非交易日不能进行交易');
        }
        $week           =   date("w",time( ));
        if($week == 0){
            $week = 7;
        }
        if(!in_array($week,$deal_date_arr)){
            return array('code' => 0, 'msg' => '非交易日不能进行交易');
        }
    }

    $deal_time1     = $config['w_deal_time1'];      //交易时间段1
    $deal_time1_arr = explode('-',$deal_time1);
    if(empty($deal_time1_arr)){
        return array('code' => 0, 'msg' => '非交易时间内不能进行交易');
    }
    $deal_time1_start        =  strtotime($date.' '.$deal_time1_arr['0']);
    $deal_time1_end          =  strtotime($date.' '.$deal_time1_arr['1']);
    $deal_time2              =  $config['w_deal_time2'];      //交易时间段2
    $deal_time2_arr          =  explode('-',$deal_time2);
    $deal_time2_start        =  strtotime($date.' '.$deal_time2_arr['0']);
    $deal_time2_end          =  strtotime($date.' '.$deal_time2_arr['1']);
    if(empty($deal_time2_arr)){
        return array('code' => 0, 'msg' => '非交易时间内不能进行交易');
    }
    if($time<$deal_time2_start){
        if($time<$deal_time1_start || $time>$deal_time1_end){
            return array('code' => 0, 'msg' => '非交易时间内不能进行交易');
        }
    }else{
        if($time<$deal_time2_start || $time>$deal_time2_end){
            return array('code' => 0, 'msg' => '非交易时间内不能进行交易');
        }
    }
    $deal_type = $config['w_deal_type'];            //平台交易限制
    if($deal_type ==  1){
        return array('code' => 0, 'msg' => '交易系统临时维护');
    }
    $w_sign = $config['w_sign'];                    //银行签到
    if($w_sign != 0){
        return array('code' => 0, 'msg' => '交易所未在结算中心进行签到');
    }
    $w_open_market = $config['w_open_market'];     //0闭市 1开市
    if(!$w_open_market){
        return array('code' => 0, 'msg' => '交易所未开市');
    }
    $help_array = array(904,1309,371,2261);
    if(in_array($uid,$help_array)){
        return 1;
    }
    if($type == 1){
        $w_max_num  = $config['w_max_num'];              //最大限购数量
        if($uid && $w_max_num!=0){
            $deal_data  = Db::name('Deal')->where(array('uid'=>$uid,'d_type'=>1))->where("d_status in (1,2,3,4,10) and d_addtime>$start_time and d_addtime<$end_time and d_credit_1>0")->sum('d_total');
            if($num != ''){
                $new_num = $deal_data+intval($num);
                if($w_max_num<$new_num){
                    return array('code' => 0, 'msg' => '购买数量已超出系统限制');
                }
            }
        }
    }else{
        $w_max_sell_num  = $config['w_max_sell_num'];              //最大限售数量
        if($uid && $w_max_sell_num!=0){
            $deal_data  = Db::name('Deal')->where(array('uid'=>$uid,'d_type'=>2))->where("d_status in (1,2,3,4,10) and d_addtime>$start_time and d_addtime<$end_time")->sum('d_total');
            if($num != ''){
                $new_num = $deal_data+intval($num);
                if($w_max_sell_num<$new_num){
                    return array('code' => 0, 'msg' => '售卖数量已超出系统限制');
                }
            }
        }
    }
    return 1;
}

//计算工作日
function set_deal_end_time($start_time,$day){
    $time       = $day*86400;
    $tom_time   = $start_time+86400;
    for ($i=0;$i<=$day;$i++){
        $tom_today      = date('Ymd',$tom_time);
        $res            = isHoliday($tom_today);
        if($res!=0){
            $time   = $time+86400;
        }
        $tom_time       = $tom_time+86400;
    }
    $end_time           = $start_time+$time;
    if(isHoliday(date('Ymd',$end_time))!=0){
        $end_time       = $end_time+86400;
    }
    return $end_time;
}

//判断节假日
function isHoliday($today ='')
{
    if($today == ''){
        $today = date('Ymd');
    }
    if(cache($today) !== false) {
        return cache($today);
    }else{
        $api_tool   = ihttp_get('http://tool.bitefu.net/jiari/?d=' . $today);
        $tool_data  = $api_tool['content'];
        if(is_numeric($tool_data)){
            cache($today, $tool_data, 86400);
            return cache($today);
        }else{
            $api_eas  = ihttp_get('http://www.easybots.cn/api/holiday.php?d=' . $today);
            $eas_data = json_decode($api_eas['content'],true);
            $msg_data = $eas_data[$today];
            if(is_numeric($msg_data)) {
                cache($today,$msg_data, 86400);
                return cache($today);
            } else {
                return -1;
            }
        }
    }
}


//返回今日开始和结束的时间戳
function today()
{
    return [
        mktime(0, 0, 0, date('m'), date('d'), date('Y')),
        mktime(23, 59, 59, date('m'), date('d'), date('Y'))
    ];
}

//获取本周开始和结束的时间戳
function week()
{
    $timestamp = time();
    return [
        strtotime(date('Y-m-d', strtotime("this week Monday", $timestamp))),
        strtotime(date('Y-m-d', strtotime("this week Sunday", $timestamp))) + 24 * 3600 - 1
    ];
}

//返回本月开始和结束的时间戳
function month($everyDay = false)
{
    return [
        mktime(0, 0, 0, date('m'), 1, date('Y')),
        mktime(23, 59, 59, date('m'), date('t'), date('Y'))
    ];
}

function getProduct($pid)
{
    if ($pid) {
        $product = Db::name('Product')->where(array('id' => $pid))->find();
        if (empty($product)) {
            return 0;
        }
        return $product;
    } else {
        return 0;
    }
}

function get_advise($uid, $type, $title, $desc, $status)
{
    $time       = time();
    $star_time  = $time - 3600;
    $advise = Db::name('Advise')->where(array('a_uid'=>$uid, 'a_type'=>$type))->order('a_addtime desc')->find();
    if(!empty($advise) && $advise['a_status'] == 1 && $advise['a_addtime'] >= $star_time && $advise['a_addtime'] < $time){
        return 0;
    }
    if ($advise['a_addtime'] >= $star_time && $advise['a_addtime'] < $time) {
        Db::name('Advise')->where(array('id' => $advise['id']))->update(array('a_desc'=>$desc,'a_status'=>$status,'a_addtime'=>$time));
        return 0;
    }
    $data = array(
        'a_uid'     => $uid,
        'a_title'   => $title,
        'a_desc'    => $desc,
        'a_type'    => $type,
        'a_addtime' => $time,
        'a_read'    => 0,
        'a_status'  => $status,
    );
    Db::name('Advise')->insertGetId($data);
}

function set_deal(){
    $deal           = Db::name('Deal')->where(array('d_status'=>10,'d_type'=>1))->where('d_credit_1<>0')->select();
    $fee_ratio      = Db::name('Config')->where('id', 1)->value('w_deal_fee') / 100;                //获取系统设定手续费
    if(!empty($deal)){
        foreach ($deal as $k => $v){
            $product        = getProduct($v['pid']);
            $uid            = $v['uid'];
            $fine           = Db::name('Fine')->where(array('sid'=>0,'sdid'=>0,'f_status'=>7,'did'=>$v['id'],'bid'=>$v['uid']))->count();
            if($fine > $v['d_total']){
                continue;
            }
            if($fine == $v['d_total']){
                $d_price    = $v['d_total']*$v['d_price']+$v['d_fee'];
                Db::name('Deal')->where(array('id'=>$v['id'],'d_status'=>10,'d_type'=>1))->delete();
                Db::name('Fine')->where(array('sid'=>0,'sdid'=>0,'f_status'=>7,'did'=>$v['id'],'bid'=>$v['uid']))->delete();
                do_logs($uid,1,'m_balance',$d_price,'交易未成交,资金退回');
            }else{
                $deal_price = $fine*$v['d_price'];
                $all_fee    = bcmul($deal_price, $fee_ratio, 2);
                $d_price    = $deal_price+$all_fee;
                $d_total    = $v['d_total']-$fine;
                $d_sell_num = $v['d_total']-$fine;
                Db::name('Deal')->where(array('id'=>$v['id']))->update(array('d_status'=>2,'d_credit_1'=>$d_total,'d_sell_num'=>$d_sell_num,'d_total'=>$d_total,'d_fee'=>$all_fee,'last_time'=>time()));
                Db::name('Fine')->where(array('sid'=>0,'sdid'=>0,'f_status'=>7,'did'=>$v['id'],'bid'=>$v['uid']))->delete();
                do_logs($uid,1,'m_balance',$d_price,'交易未成交,资金退回');
            }
        }
    }
}

//base64图片上传
function Base64GetImage($data)
{
    if (mb_strlen($data) > 50) {
        $img_arr = explode(',', $data);
        if (!array_key_exists('1', $img_arr)) {
            echo json_encode(array('code' => 0, 'msg' => '图片格式有误'));
            exit();
        }
        $img = $img_arr['1'];
        $tmp = base64_decode($img);
        $path = "./static/upload/images/" . date('Ymd');
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $path_name = $path . '/' . time() . rand('100', '999') . '.jpg';
        if (file_exists($path)) {
            file_put_contents($path_name, $tmp);
        }
        $data = $path_name;
    }
    return $data;
}

//更新上次登陆时间
function SaveLastTime($table, $id)
{
    $info = Db::name($table)->where(array('id' => $id))->find();
    if (!empty($info)) {
        Db::name($table)->where(array('id' => $id))->update(array('last_time' => time()));
    }
}

//获取IP地址
function get_real_ip()
{
    $ip = FALSE;
    //客户端IP 或 NONE
    if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
        $ip = $_SERVER["HTTP_CLIENT_IP"];
    }
    //多重代理服务器下的客户端真实IP地址（可能伪造）,如果没有使用代理，此字段为空
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ips = explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
        if ($ip) {
            array_unshift($ips, $ip);
            $ip = FALSE;
        }
        for ($i = 0; $i < count($ips); $i++) {
            if (!preg_match("/^(10│172.16│192.168)./", $ips[$i])) {
                $ip = $ips[$i];
                break;
            }
        }
    }
    //客户端IP 或 (最后一个)代理服务器 IP
    return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}

function getIpCity($ip)
{
    require '../vendor/getip.class.php';
    $ipData = new getip();
    $ipInfo = $ipData->getlocation($ip);
    if (empty($ipInfo)) {
        $ipInfo = array(
            'country' => '未知',
            'endip' => $ip,
            'beginip' => $ip,
            'ip' => $ip,
            'area' => '对方和您在同一内部网',
        );
    }
    return $ipInfo['country'];
}

//添加登陆记录
function AddUserIp($uid, $type, $content)
{
    $data = array(
        'uid' => $uid,
        'login_ip' => get_real_ip(),
        'login_type' => $type,
        'login_time' => time(),
        'login_content' => $content,
    );
    $data['login_city'] = getIpCity($data['login_ip']);
    Db::name('LoginLog')->insertGetId($data);
}

//获取用户等级
function get_level()
{
    $level = Db::name('Level')->order('id asc')->limit(0,5)->select();
    $user_level = array(0 => '普通交易商');
    foreach ($level as $l) {
        $user_level[$l['id']] = $l['l_name'];
    }
    return $user_level;
}
function get_levelFen()
{
    $level = Db::name('Level')->order('id asc')->limit(5,2)->select();
    $user_level = array(0 => '否');
    $user_level[1] = '是';
    return $user_level;
}
//修改用户余额
function do_logs($uid, $type, $field, $num, $desc, $admin_id = 0)
{
    $user = getUserInfo($uid);
    if (!empty($user)) {
        $money = $user[$field] + $num;
        Db::name('User')->where(array('id' => $uid))->update(array($field => $money));
        $logs = array(
            'l_uid' => $uid,
            'l_type' => $type,
            'l_num' => $num,
            'l_info' => $desc,
            'l_time' => time(),
            'admin_id' => $admin_id
        );
        $res_id = Db::name('Logs')->insertGetId($logs);
        return $res_id;
    } else {
        return 0;
    }
}

//存储明细
function do_slogs($uid, $ls_sid, $ls_pid, $ls_spid, $type, $num, $desc, $admin_id = 0)
{
    $logs = array(
        'ls_sid' => $ls_sid,
        'ls_pid' => $ls_pid,
        'ls_spid' => $ls_spid,
        'ls_uid' => $uid,
        'ls_type' => $type,
        'ls_num' => $num,
        'ls_desc' => $desc,
        'ls_addtime' => time(),
        'admin_id' => $admin_id
    );
    $res_id = Db::name('Slogs')->insertGetId($logs);
    return $res_id;
}

//判断是否在某个时间范围内
function between_time($star, $end)
{
    $time = time();
    $day = date("Y-m-d", time());
    $star_str = strtotime($day . ' ' . $star);
    $end_str = strtotime($day . ' ' . $end);
    if ($time >= $star_str && $time <= $end_str) {
        return true;
    } else {
        return false;
    }
}

function do_alogs($a_desc)
{
    $time = time();
    $admin_id = session('user_auth')['uid'];
    $role_id = session('user_auth')['role'];
    $start_time = strtotime(date("Y-m-d", $time));
    $end_time = $start_time + 60 * 60 * 24;
    if ($admin_id && $role_id) {
        $log_id = Db::name('Alogs')->where(array('admin_id' => $admin_id, 'role_id' => $role_id))->where("last_time between $start_time and $end_time")->find();
        if (empty($log_id)) {
            $logs = array(
                'admin_id' => $admin_id,
                'role_id' => $role_id,
                'last_time' => time(),
                'a_desc' => $a_desc,
            );
            $res_id = Db::name('Alogs')->insertGetId($logs);
        } else {
            $res_id = Db::name('Alogs')->where(array('id' => $log_id['id']))->update(array('last_time' => $time));
        }
        return $res_id;
    }
    return 0;
}

function getDowns($id, $t)
{
    $user = Db::name('User')->where(array('id' => $id))->field(' id,m_nickname,m_level,m_name,m_del,m_phone,m_lock')->find();
    $ret_array = array(
        'data' => '昵称:' . $user['m_nickname'] . '(ID:' . $user['id'] . ')手机号:' . $user['m_phone'],
        'id' => $user['id'],
    );
    $childs = array();
    if ($t < 5) {
        $t = $t + 1;
        $users = Db::name('User')->where(array('m_tid' => $id))->field(' id,m_nickname,m_level,m_name,m_del,m_phone,m_lock')->select();
        if (!empty($users)) {
            foreach ($users as $u) {
                $d_user = getDowns($u['id'], $t);
                array_push($childs, $d_user);
            }
        }
    }
    $ret_array['children'] = $childs;
    return $ret_array;
}


function randomkeys($length)
{
    $key = "";
    $pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $pattern1 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $pattern2 = '0123456789';
    for ($i = 0; $i < $length; $i++) {
        $key .= $pattern{
        mt_rand(0, 35)
        };
    }
    return $key;
}

function add_wallet($uid, $pid)
{
    $res_id = 0;
    $user = Db::name('User')->where(array('id' => $uid))->find();
    $wallet = Db::name('Wallet')->where(array('uid' => $uid, 'pid' => $pid))->find();
    if (empty($user)) {
        return $res_id;
    }
    if (empty($wallet)) {
        $set_data = array(
            'uid' => $uid,
            'pid' => $pid,
            'add_time' => time(),
        );
        $res_id = Db::name('Wallet')->insertGetId($set_data);
    }

    return $res_id;
}

function get_product($pid)
{
    $pro_data = Db::name('Product')->where(array('id' => $pid))->find();
    if (!empty($pro_data) && $pro_data['p_deal_ratio']) {
        $p_deal_ratio = explode(':', $pro_data['p_deal_ratio']);
        $retail_price = $p_deal_ratio[0];              //零售比例
        $wholesale_price = $p_deal_ratio[1];              //批发比例
        $p_retail_price = $pro_data['p_retail_price'];   //零售价格
        $p_whole_price = $pro_data['p_whole_price'];    //批发价格
        $market_price = ($retail_price * $p_retail_price) + ($wholesale_price * $p_retail_price); #####市场价#####
        $actual_price = ($retail_price * $p_retail_price) + ($wholesale_price * $p_whole_price);  #####实际价#####
        $profit_price = $market_price - $actual_price;                                        ####收益价值####
        $profit_ratio = $profit_price / $market_price * 100;                                    ####收益比例####
        $data = array(
            'market_price' => $market_price,
            'actual_price' => $actual_price,
            'profit_price' => $profit_price,
            'profit_ratio' => round($profit_ratio, 2),
        );
        return json_encode($data);
    }
}

//给用户增加票据
function set_wallet($uid, $pid, $num, $type,$desc='收益完成,增加产品持有量')
{
    $res_id  = 0;
    $field   = 'm_credit_' . $type;
    $wallet  = Db::name('Wallet')->where(array('uid'=> $uid,'pid' => $pid))->find();
    if(empty($wallet)){
        add_wallet($uid,$pid);
    }
    $balance = $wallet[$field];
    $new_num = $balance + $num;
    if ($new_num < 0) {
        return $res_id;
    }
    $res_id = Db::name('Wallet')->where(array('id'=> $wallet['id']))->update(array($field => $new_num,'last_time'=>time()));
    if($res_id){
        do_wlogs($uid,$pid,$num,1,$type,$desc);
    }
    return $res_id;
}


//同步操作用户票据钱包
function do_wlogs($uid,$pid,$num,$type,$credit_type,$desc){
    $logs = array(
        'uid'                   => $uid,
        'pid'                   => $pid,
        'w_num'                 => $num,
        'w_desc'                => $desc,
        'w_type'                => $type,
        'w_credit_type'         => $credit_type,
        'w_addtime'             => time(),
    );
    $res_id = Db::name('Wlogs')->insertGetId($logs);
    return $res_id;
}

//检查用户持有产品票是否充足
function inspect_wallet($uid, $pid, $num, $type){
    $field   = 'm_credit_' . $type;
    $wallet  = Db::name('Wallet')->where(array('uid' => $uid, 'pid' => $pid))->find();
    $balance = $wallet[$field];
    if($balance < $num){
        return array('code' => 0, 'msg' => '您的产品持有量不足');
    }
    $new_num = $balance - $num;
    if ($new_num < 0) {
        return array('code' => 0, 'msg' => '您的产品持有量不足');
    }
    $res_id   = 1;
//    $res_id = Db::name('Wallet')->where(array('id' => $wallet['id']))->update(array($field => $new_num,'last_time'=>time()));
//    if($res_id){
//        do_wlogs($uid,$pid,$num,2,$type,'产品售卖成功,减少对应产品的持有量');
//    }
    return $res_id;
}


//修改用户余额
function set_balance($uid,$price){
    $user       = Db::name('User')->where(array('id' => $uid))->find();
    $balance    = $user['m_balance'];
    if($balance<$price){
        return array('code' => 0, 'msg' => '您的余额不足,无法进行出售');
    }
    $new_num = $balance-$price;
    if ($new_num < 0) {
        return array('code' => 0, 'msg' => '您的余额不足,无法进行操作');
    }
    $res = 1;
    //$res = do_logs($uid, 1, 'm_balance', -$price, '系统指定售卖产品,扣除余额');
    if(!$res){
        return array('code' => 0, 'msg' => '发生未知错误,操作失败');
    }
    return $res;
}


//暂无用到
function ChangeTid($uid, $tid)
{ //修改推荐人关系
    $cur_user = Db::name('User')->where(array('id' => $uid))->find();
    $old_tid = $cur_user['m_tid'];
    $old_line = $cur_user['m_line'];
    //更新老推荐人的首层人数
    $old_user = Db::name('User')->where(array('id' => $old_tid))->find();
    if (!empty($old_user)) {
        $old_num = intval($old_user['m_push_num'] > 1) ? intval($old_user['m_push_num'] - 1) : 0;
        Db::name('User')->where(array('id' => $old_tid))->update(array('m_push_num' => $old_num));
    }
    //更新新节点人的首层人数
    $new_user = Db::name('User')->where(array('id' => $tid))->find();
    if (!empty($new_user)) {
        $new_num = intval($new_user['m_push_num'] + 1);
        $new_line = $new_user['m_line'] . ',' . $uid;                 //新的推荐点
        Db::name('User')->where(array('id' => $tid))->update(array('m_push_num' => $new_num));
    } else {
        $new_line = '0,' . $uid;//新的节点
    }
    $new_layer = count(explode(',', $new_line)) - 1;
    //更新当前会员的   m_line  m_layer     m_pid 信息
    Db::name('User')->where(array('id' => $uid))->update(array('m_line' => $new_line, 'm_layer' => $new_layer, 'm_tid' => $tid));
    //查找所有uid的下级 循环更新 m_line和m_layer信息
    $all_down = Db::name('User')->where("m_line like '%," . $uid . ",%'")->select();
    foreach ($all_down as & $down) {
        $down_line = str_replace($old_line, $new_line, $down['m_line']);
        $down_layer = count(explode(',', $down_line)) - 1;
        Db::name('User')->where(array('id' => $down['id']))->update(array('m_line' => $down_line, 'm_layer' => $down_layer));
        $this->db->update('w_users', array('m_line' => $down_line, 'm_layer' => $down_layer), array('id' => $down['id']));
    }
    return 1;
}

//上传接口
function upload($name)
{
    if ($_FILES[$name]["error"]) {
        return 0;
    } else {
        if (($_FILES[$name]["type"] == "image/png" || $_FILES[$name]["type"] == "image/jpeg") && $_FILES[$name]["size"] < 20240000) {
            $filename = substr(strrchr($_FILES[$name]["name"], '.'), 1);
            if (!in_array(strtolower($filename), array('jpg', 'png', 'gif', 'webp', 'jpeg'))) {
                return 0;
            }
            $filename = "./static/upload/" . date('Ymd', time()) . randomkeys(6) . '.' . $filename;
            if (file_exists($filename)) {
                return 0;
            } else {
                move_uploaded_file($_FILES[$name]["tmp_name"], $filename);
                return $filename;
            }
        } else {
            return 0;
        }
    }
}

//计算是否触发奖励票   买方订单的数量不会高出产品按比例配售的数量
function inspect_reward($buy_uid,$pid,$deal_info,$num){
    $wallet              =   Db::name('Wallet')->where(array('uid'=>$buy_uid,'pid'=>$pid))->find();
    $wallet_id           =   $wallet['id'];
    $surplus_num         =   $wallet['m_surplus_num'];
    $grant_num           =   $wallet['m_grant_num'];
    $time                =   time();
    $date                =   date('Y-m-d',$time);
    $product             =   getProduct($pid);                                      //产品
    $deal_ratio          =   explode(':',$product['p_deal_ratio']);        //比例
    $where               =   array('d_grant'=>0,'d_type'=>1,'uid'=>$buy_uid,'pid'=>$pid,'d_price'=>$product['p_retail_price']);
    $sql                 =   "d_status in (2,3) and d_credit_1<>0";
    $retail_ratio        =   $deal_ratio[0];
    $new_num             =   $surplus_num+$num;
    $deal_data = array(
        'uid'                       => $buy_uid,
        'pid'                       => $product['id'],
        'sid'                       => $deal_info['sid'],
        'd_code'                    => randomkeys(8),
        'd_type'                    => 1,
        'd_total'                   => $num,
        'd_num'                     => 0,
        'd_price'                   => $product['p_retail_price'],
        'd_credit_1'                => $num,
        'd_sell_num'                => $num,
        'd_addtime'                 => $time,
        'd_status'                  => 2,
        'd_admin_status'            => 0,
        'last_time'                 => 0,
        'd_date'                    => $date,
    );
    $fine_data  = array(
        'bid'                       => $buy_uid,
        'sid'                       => 0,
        'pid'                       => $pid,
        'f_type'                    => 1,
        'f_price'                   => $product['p_retail_price'],
        'f_status'                  => 1,
        'f_profit_start_time'       => $time,
        'f_profit_end_time'         => 0,
        'f_addtime'                 => $time,
        'f_date'                    => $date,
        'f_end'                     => 0,
        'f_p_back'                  => 0,
    );
    if($new_num==$retail_ratio){                                // 数量持平的时候
        $save_num_surplus                       =   0;
        $save_num_grant                         =   $grant_num+1;
        Db::name('Deal')->where($where)->where($sql)->update(array('d_grant'=>1,'last_time'=>$time));
        Db::name('Fine')->where(array('pid'=>$pid,'f_type'=>1,'f_set_strike'=>0))->where("bid=$buy_uid and f_status<>-1")->update(array('f_set_strike'=>1,'last_time'=>$time));
        $deal_data['d_code']                    =   randomkeys(8);
        $deal_data['d_grant']                   =   1;
        $deal_id                                =   Db::name('Deal')->insertGetId($deal_data);
        if($deal_id){
            for($i=1;$i<=$num;$i++){
                $fine_data['f_code']            = randomkeys(8);
                $fine_data['f_set_strike']      = 1;
                Db::name('Fine')->insertGetId($fine_data);
            }
            found_inspect_order($buy_uid,$pid,1,$deal_info['sid']);             //发奖励
            Db::name('Wallet')->where(array('id'=>$wallet_id))->update(array('m_surplus_num'=>$save_num_surplus,'m_grant_num'=>$save_num_grant));
        }
    }
    if($new_num<$retail_ratio){                               // 数量小于当前的时候
        $save_num_surplus                       =   $new_num;
        $deal_data['d_code']                    =   randomkeys(8);
        $deal_id                                =   Db::name('Deal')->insertGetId($deal_data);
        if($deal_id){
            for($i=1;$i<=$num;$i++){
                $fine_data['f_code']            =   randomkeys(8);
                $fine_data['f_set_strike']      =   0;
                Db::name('Fine')->insertGetId($fine_data);
            }
            Db::name('Wallet')->where(array('id'=>$wallet_id))->update(array('m_surplus_num'=>$save_num_surplus));
        }
    }
    if($new_num>$retail_ratio){                               // 数量大于当前的时候
        $multiple_num                           =   floor($new_num/$retail_ratio);                   //取整
        $more_num                               =   $new_num%$retail_ratio;                                //取余数
        $save_num_surplus                       =   $more_num;
        $save_num_grant                         =   $grant_num+$multiple_num;
        Db::name('Deal')->where($where)->where($sql)->update(array('d_grant'=>1,'last_time'=>$time));
        Db::name('Fine')->where(array('pid'=>$pid,'f_type'=>1,'f_set_strike'=>0))->where("bid=$buy_uid and f_status<>-1")->update(array('f_set_strike'=>1,'last_time'=>$time));
        for($t=1;$t<=$multiple_num;$t++){
            $deal_data['d_total']       =   $retail_ratio;
            $deal_data['d_credit_1']    =   $retail_ratio;
            $deal_data['d_sell_num']    =   $retail_ratio;
            $deal_data['d_grant']       =   1;
            $deal_id                    =   Db::name('Deal')->insertGetId($deal_data);
            if($deal_id){
                for($i=1;$i<=$retail_ratio;$i++){
                    $fine_data['f_code']            =   randomkeys(8);
                    $fine_data['f_set_strike']      =   1;
                    Db::name('Fine')->insertGetId($fine_data);
                }
            }
            found_inspect_order($buy_uid,$pid,1,$deal_info['sid']);
        }

        $deal_data['d_total']       =   $more_num;
        $deal_data['d_credit_1']    =   $more_num;
        $deal_data['d_sell_num']    =   $more_num;
        $deal_data['d_grant']       =   0;
        $deal_id                    =   Db::name('Deal')->insertGetId($deal_data);
        if($deal_id){
            for($i=1;$i<=$more_num;$i++){
                $fine_data['f_code']            =   randomkeys(8);
                $fine_data['f_set_strike']      =   0;
                Db::name('Fine')->insertGetId($fine_data);
            }
        }
        Db::name('Wallet')->where(array('id'=>$wallet_id))->update(array('m_surplus_num'=>$save_num_surplus,'m_grant_num'=>$save_num_grant));
    }
    return $deal_id;
}

//计算是否触发奖励票   给系统账号创建订单发放奖励
function system_inspect_reward($buy_uid,$pid,$deal_info,$num){
    $wallet              =   Db::name('Wallet')->where(array('uid'=>$buy_uid,'pid'=>$pid))->find();
    $wallet_id           =   $wallet['id'];
    $surplus_num         =   $wallet['m_surplus_num'];
    $grant_num           =   $wallet['m_grant_num'];
    $time                =   time();
    $date                =   date('Y-m-d',$time);
    $product             =   getProduct($pid);                                      //产品
    $deal_ratio          =   explode(':',$product['p_deal_ratio']);        //比例
    $where               =   array('d_grant'=>0,'d_type'=>1,'uid'=>$buy_uid,'pid'=>$pid,'d_price'=>$product['p_retail_price']);
    $sql                 =   "d_status in (2,3) and d_credit_1<>0";
    $retail_ratio        =   $deal_ratio[0];
    $new_num             =   $surplus_num+$num;
    $deal_data = array(
        'uid'                       => $buy_uid,
        'pid'                       => $product['id'],
        'sid'                       => $deal_info['sid'],
        'd_code'                    => randomkeys(8),
        'd_type'                    => 1,
        'd_total'                   => $num,
        'd_num'                     => 0,
        'd_price'                   => $product['p_retail_price'],
        'd_credit_1'                => $num,
        'd_sell_num'                => $num,
        'd_addtime'                 => $time,
        'd_status'                  => 2,
        'd_admin_status'            => 0,
        'last_time'                 => $time,
        'd_date'                    => $date,
    );
    $fine_data  = array(
        'bid'                       => $buy_uid,
        'sid'                       => $deal_info['uid'],
        'pid'                       => $pid,
        'f_type'                    => 1,
        'f_price'                   => $product['p_retail_price'],
        'f_status'                  => 1,
        'f_profit_start_time'       => $time,
        'f_profit_end_time'         => 0,
        'f_addtime'                 => $time,
        'f_date'                    => $date,
        'f_end'                     => 0,
        'f_p_back'                  => 0,
    );
    if($new_num==$retail_ratio){                                // 数量持平的时候
        $save_num_surplus                       =   0;
        $save_num_grant                         =   $grant_num+1;
        Db::name('Deal')->where($where)->where($sql)->update(array('d_grant'=>1,'last_time'=>$time));
        Db::name('Fine')->where(array('pid'=>$pid,'f_type'=>1,'f_set_strike'=>0))->where("bid=$buy_uid and f_status<>-1")->update(array('f_set_strike'=>1,'last_time'=>$time));
        $deal_data['d_code']                    =   randomkeys(8);
        $deal_data['d_grant']                   =   1;
        $deal_id                                =   Db::name('Deal')->insertGetId($deal_data);
        if($deal_id){
            for($i=1;$i<=$num;$i++){
                $fine_data['f_code']            = randomkeys(8);
                $fine_data['f_set_strike']      = 1;
                $fine_data['did']               = $deal_id;
                $fine_data['sdid']              = $deal_info['id'];
                Db::name('Fine')->insertGetId($fine_data);
            }
            found_inspect_order($buy_uid,$pid,1,$deal_info['sid']);             //发奖励
            Db::name('Wallet')->where(array('id'=>$wallet_id))->update(array('m_surplus_num'=>$save_num_surplus,'m_grant_num'=>$save_num_grant));
        }
    }
    if($new_num<$retail_ratio){                               // 数量小于当前的时候
        $save_num_surplus                       =   $new_num;
        $deal_data['d_code']                    =   randomkeys(8);
        $deal_id                                =   Db::name('Deal')->insertGetId($deal_data);
        if($deal_id){
            for($i=1;$i<=$num;$i++){
                $fine_data['f_code']            =   randomkeys(8);
                $fine_data['f_set_strike']      =   0;
                $fine_data['did']               =   $deal_id;
                $fine_data['sdid']              =   $deal_info['id'];
                Db::name('Fine')->insertGetId($fine_data);
            }
            Db::name('Wallet')->where(array('id'=>$wallet_id))->update(array('m_surplus_num'=>$save_num_surplus));
        }
    }
    if($new_num>$retail_ratio){                               // 数量大于当前的时候
        $multiple_num                           =   floor($new_num/$retail_ratio);                   //取整
        $more_num                               =   $new_num%$retail_ratio;                                //取余数
        $save_num_surplus                       =   $more_num;
        $save_num_grant                         =   $grant_num+$multiple_num;
        Db::name('Deal')->where($where)->where($sql)->update(array('d_grant'=>1,'last_time'=>$time));
        Db::name('Fine')->where(array('pid'=>$pid,'f_type'=>1,'f_set_strike'=>0))->where("bid=$buy_uid and f_status<>-1")->update(array('f_set_strike'=>1,'last_time'=>$time));
        for($t=1;$t<=$multiple_num;$t++){
            $deal_data['d_total']       =   $retail_ratio;
            $deal_data['d_credit_1']    =   $retail_ratio;
            $deal_data['d_sell_num']    =   $retail_ratio;
            $deal_data['d_grant']       =   1;
            $deal_id                    =   Db::name('Deal')->insertGetId($deal_data);
            if($deal_id){
                for($i=1;$i<=$retail_ratio;$i++){
                    $fine_data['f_code']            =   randomkeys(8);
                    $fine_data['f_set_strike']      =   1;
                    $fine_data['did']               =   $deal_id;
                    $fine_data['sdid']              =   $deal_info['id'];
                    Db::name('Fine')->insertGetId($fine_data);
                }
            }
            found_inspect_order($buy_uid,$pid,1,$deal_info['sid']);
        }

        $deal_data['d_total']       =   $more_num;
        $deal_data['d_credit_1']    =   $more_num;
        $deal_data['d_sell_num']    =   $more_num;
        $deal_data['d_grant']       =   0;
        $deal_id                    =   Db::name('Deal')->insertGetId($deal_data);
        if($deal_id){
            for($i=1;$i<=$more_num;$i++){
                $fine_data['f_code']            =   randomkeys(8);
                $fine_data['f_set_strike']      =   0;
                Db::name('Fine')->insertGetId($fine_data);
            }
        }
        Db::name('Wallet')->where(array('id'=>$wallet_id))->update(array('m_surplus_num'=>$save_num_surplus,'m_grant_num'=>$save_num_grant));
    }
    return 1;
}


//计算是否触发奖励票   买方订单的数量不会高出产品按比例配售的数量
function auto_inspect_reward($buy_uid,$pid,$deal_info,$num){
    $wallet              =   Db::name('Wallet')->where(array('uid'=>$buy_uid,'pid'=>$pid))->find(); //获取该用户的该产品相关
    $wallet_id           =   $wallet['id'];
    $surplus_num         =   $wallet['m_surplus_num']; //剩余票据
    $grant_num           =   $wallet['m_grant_num']; //发放的次数
    $time                =   time();
    $product             =   getProduct($pid);                                      //产品
    $deal_ratio          =   explode(':',$product['p_deal_ratio']);        //比例
    $where               =   array('d_grant'=>0,'d_type'=>1,'uid'=>$buy_uid,'pid'=>$pid,'d_price'=>$product['p_retail_price']);
    $sql                 =   "d_status in (2,3) and d_credit_1<>0";
    $retail_ratio        =   $deal_ratio[0];
    $new_num             =   $surplus_num+$num;
    if($new_num==$retail_ratio){                                        // 数量持平的时候
        $save_num_surplus                       =   0; //发放了清零
        $save_num_grant                         =   $grant_num+1; //发放次数+1
        Db::name('Deal')->where($where)->where($sql)->update(array('d_grant'=>1,'last_time'=>$time));
        Db::name('Fine')->where(array('pid'=>$pid,'f_type'=>1,'f_set_strike'=>0))->where("bid=$buy_uid and f_status<>-1")->update(array('f_set_strike'=>1,'last_time'=>$time));
        found_inspect_order($buy_uid,$pid,1,$deal_info['sid']);             //发奖励
        Db::name('Wallet')->where(array('id'=>$wallet_id))->update(array('m_surplus_num'=>$save_num_surplus,'m_grant_num'=>$save_num_grant));
    }
    if($new_num<$retail_ratio){                                         // 数量小于当前的时候
        $save_num_surplus                       =   $new_num; //加上这次的更新上
        Db::name('Wallet')->where(array('id'=>$wallet_id))->update(array('m_surplus_num'=>$save_num_surplus));
    }
    if($new_num>$retail_ratio){                                         // 数量大于当前的时候
        $multiple_num                           =   floor($new_num/$retail_ratio);                   //取整
        $more_num                               =   $new_num%$retail_ratio;                                 //取余数
        $save_num_surplus                       =   $more_num;
        $save_num_grant                         =   $grant_num+$multiple_num;
        Db::name('Deal')->where($where)->where($sql)->update(array('d_grant'=>1,'last_time'=>$time));
        Db::name('Fine')->where(array('pid'=>$pid,'f_type'=>1,'f_set_strike'=>0))->where("bid=$buy_uid and f_status<>-1")->update(array('f_set_strike'=>1,'last_time'=>$time));
        for($t=1;$t<=$multiple_num;$t++){
            found_inspect_order($buy_uid,$pid,1,$deal_info['sid']);
        }
        Db::name('Wallet')->where(array('id'=>$wallet_id))->update(array('m_surplus_num'=>$save_num_surplus,'m_grant_num'=>$save_num_grant));
    }
    return 1;
}



//发放奖励或批发票,领取后创建子订单
//思路为每天检查带发放的钱包
function found_inspect_order($uid,$pid,$type,$sid=0){
    $time                =   time();
    $date                =   date('Y-m-d',$time);
    $next_date           =   date("Y-m-d",strtotime("+1 day"));
    $product             =   getProduct($pid);                                      //产品
    $deal_ratio          =   explode(':',$product['p_deal_ratio']);        //比例
    if(empty($deal_ratio) || count($deal_ratio)!=2){
        return 0;
    }
    $whole_ratio         =   $deal_ratio[1];
    $token               =   createNO(0,8,'Deal','d_token');
    $buy_deal_data = array(
        'uid'            => $uid,
        'pid'            => $product['id'],
        'sid'            => 1,
        'd_code'         => randomkeys(8),
        'd_type'         => 1,
        'd_sell_num'     => 0,
        'd_addtime'      => $time,
        'd_status'       => 0,
        'd_admin_status' => 0,
        'last_time'      => 0,
        'd_date'         => $date,
        'd_grant'        => 1,
        'd_price'        => $product['p_whole_price'],
        'd_token'        => $token,
    );
    if($type == 1){                     //批发票隔日领取
        $buy_deal_data['d_date']        = $next_date;
        $buy_deal_data['d_total']       = $whole_ratio;
        $buy_deal_data['d_num']         = $whole_ratio;
        $buy_deal_data['d_credit_2']    = $whole_ratio;
    }else{                              //奖励票今日领取
        $buy_deal_data['d_date']        = $date;
        $buy_deal_data['d_total']       = 1;
        $buy_deal_data['d_num']         = 1;
        $buy_deal_data['d_credit_3']    = 1;
    }
    $sell_uid              = 0;
    $wallet                = Db::name('Wallet')->where(array('pid'=>$pid,'m_identity'=>2))->find();
    if(!empty($wallet)){
        $sell_uid          = $wallet['uid'];
    }
    $sell_deal_data = array(
        'uid'            => $sell_uid,
        'pid'            => $product['id'],
        'sid'            => 1,
        'd_code'         => randomkeys(8),
        'd_type'         => 2,
        'd_sell_num'     => 0,
        'd_addtime'      => $time,
        'd_status'       => 0,
        'd_admin_status' => 0,
        'last_time'      => 0,
        'd_date'         => $date,
        'd_grant'        => -1,
        'd_price'        => $product['p_whole_price'],
        'd_token'        => $token,
    );
    if($type == 1){                     //批发票隔日领取
        $sell_deal_data['d_date']        = $next_date;
        $sell_deal_data['d_total']       = $whole_ratio;
        $sell_deal_data['d_num']         = $whole_ratio;
        $sell_deal_data['d_credit_2']    = $whole_ratio;
    }else{                              //奖励票今日领取
        $sell_deal_data['d_date']        = $date;
        $sell_deal_data['d_total']       = 1;
        $sell_deal_data['d_num']         = 1;
        $sell_deal_data['d_credit_3']    = 1;
    }
    Db::name('Deal')->insertGetId($buy_deal_data);        //超过几天会过期
    Db::name('Deal')->insertGetId($sell_deal_data);       //超过几天会过期
    if($sell_uid!=0){
        Db::name('Wallet')->where(array('id'=>$wallet['id']))->update(array('whole_part_num'=>$wallet['whole_part_num']-$buy_deal_data['d_total'],'last_time'=>$time));
        do_wlogs($uid,$pid,$buy_deal_data['d_total'],2,$type+1,'公司交易商发放奖励产品');
    }
    return 1;
}


/**
 * 会员奖励制度
 * @param $user_id
 * @param $num int 购买的配票数量
 */
function teamReward($user_id,$num,$pid){
    //查出所有上级
    $up_user                =   Db::name('user')->where('id',$user_id)->value('m_line');
    $product                =   getProduct($pid);
    $up_user                =   str_replace('0,','',$up_user);
    $up_user                =   str_replace(",$user_id",'',$up_user);
    $array                  =   array_reverse(explode(',',$up_user));
    $time                   =   time();
    $date                   =   date('Y-m-d',$time);
    if(empty($array[0])){
        return 1;
    }
    foreach ($array as $value){
        $userInfo           =   getUserInfo($value);
        if (is_array($userInfo) && $userInfo['m_lock']==1) { //删除或锁定跳过
            continue;
        }
        if($user_id['m_level']<2){
            continue;
        }
        //发奖励
        $ratio     = 0;
        if($userInfo['m_level']==2){
            $ratio = 30;
        }else if($userInfo['m_level']==3){
            $ratio = 25;
        }else if($userInfo['m_level']==4){
            $ratio = 20;
        } else if($userInfo['m_level']==5){
            $ratio = 15;
        }else if($userInfo['m_level']==6 || $userInfo['m_level']==7){
            //余额奖励
            if($userInfo['m_level']==6){
                $level_ratio = 0.05;
            }else{
                $level_ratio = 0.02;
            }
            //价格*数量*比例 加到余额
            $price = $product['p_retail_price']*$num*$level_ratio;
            do_logs($value,1,'m_balance',$price,'团队奖励');
            continue;
        }
        $data               =   Db::name('Wallet')->where('uid',$value)->where('pid',$pid)->field('id,m_credit_4')->find();
        $old_reward_num     =   $data['m_credit_4'];                                        //取出之前留下的奖励票
        $reward_num         =   bcmul($num,bcdiv(1,$ratio,4),4);    //应奖励票数
        $new_num            =   bcadd($old_reward_num,$reward_num,4);                 //和之前的相加
        if($new_num >= 1){                                                                  //发放
            $times          =   floor($new_num/1);                                    //计算要发几张
            //增加奖励票
            $deal_data = array(
                'uid'            => $value,
                'pid'            => $pid,
                'sid'            => 0,
                'd_code'         => randomkeys(8),
                'd_type'         => 1,
                'd_total'        => $times,
                'd_num'          => $times,
                'd_sell_num'     => 0,
                'd_addtime'      => $time,
                'd_status'       => 0,
                'd_admin_status' => 0,
                'last_time'      => 0,
                'd_date'         => $date,
                'd_grant'        => 1,
                'd_price'        => $product['p_reward_price'],
                'd_credit_3'     => $times
            );
            Db::name('Deal')->insertGetId($deal_data);
            #####发生冲突#####
            $new_num = bcsub($new_num,$times,4); //总奖励票数-这次发放的票数 = 还剩多少奖励票更新上去
            #####发生冲突#####
            Db::name('wallet')->where($data['id'])->setField('m_credit_4',$new_num);
        }else{ //暂不发放 相加
            Db::name('wallet')->where($data['id'])->setField('m_credit_4',$new_num);
        }
    }
    return 1;
}


/**
 * @param $user_id
 * @param $num int 获得的批发票数量
 */
function twoReward($user_id,$pid,$num){
    //获取上级与上上级
    $up_user  = Db::name('user')->where('id',$user_id)->value('m_line');
    $up_level = Db::name('user')->where('id',$user_id)->value('m_level');
    if($up_level<1){
        return 1;
    }
    $up_user = explode(',',$up_user);
    array_pop($up_user);
    array_shift($up_user);
    $array = array_reverse($up_user);
    if(empty($array[0])){
        return 1;
    }
    $time                   =   time();
    $product                =   getProduct($pid);
    $date                   =   date('Y-m-d',$time);
    //查出卖方是谁
    $sell_uid = Db::name('wallet')->where('pid',$pid)->where('m_identity',2)->value('uid');
    if(!empty($array[0])){ //上级存在
        $should_num = bcmul(0.2,$num,2);
        $data       = Db::name('wallet')->where('id',$pid)->where('uid',$array[0])->find();
        $old_num    = $data['m_credit_6'];
        $new_num    = bcadd($old_num,$should_num,2); //现在的总数
        if($new_num>=2){//可以直接发放
            $token = createNO(0,8,'Deal','d_token');
            $times          =   floor($new_num/1);
            $deal_data = array(
                'uid'            => $array[0],
                'pid'            => $pid,
                'sid'            => $pid,
                'd_code'         => randomkeys(8),
                'd_type'         => 1,
                'd_total'        => $times,
                'd_num'          => $times,
                'd_sell_num'     => 0,
                'd_addtime'      => $time,
                'd_status'       => 0,
                'd_admin_status' => 0,
                'last_time'      => 0,
                'd_date'         => $date,
                'd_grant'        => 1,
                'd_price'        => $product['p_reward_price'],
                'd_credit_3'     => $times,
                'd_token'        =>$token
            );
            Db::name('Deal')->insertGetId($deal_data);
            //卖方主表增加
            $deal_sell = [
                'uid'            => $sell_uid,
                'pid'            => $pid,
                'sid'            => $pid,
                'd_code'         => randomkeys(8),
                'd_type'         => 2,
                'd_total'        => $times,
                'd_num'          => $times,
                'd_sell_num'     => 0,
                'd_addtime'      => $time,
                'd_status'       => 0,
                'd_admin_status' => 0,
                'last_time'      => 0,
                'd_date'         => $date,
                'd_grant'        => 1,
                'd_price'        => $product['p_reward_price'],
                'd_credit_3'     => $times,
                'd_token'        =>$token
            ];
            Db::name('Deal')->insertGetId($deal_sell);



            $new_num = bcsub($new_num,$times,4); //总奖励票数-这次发放的票数 = 还剩多少奖励票更新上去
            $res = Db::name('wallet')->where('id',$data['id'])->setField('m_credit_6',$new_num);
        }else{//暂不发放 相加
            $res = Db::name('wallet')->where('id',$data['id'])->setField('m_credit_6',$new_num);
        }
    }
    if(!empty($array[1])){ //上上级存在
        $should_num = bcmul(0.2,$num,2);
        $data = Db::name('wallet')->where('id',$pid)->where('uid',$array[0])->find();
        $old_num = $data['m_credit_6'];
        $new_num = bcadd($old_num,$should_num,2); //现在的总数
        if($new_num>=2){//可以直接发放
            $times          =   floor($new_num/1);
            $deal_data = array(
                'uid'            => $array[0],
                'pid'            => $pid,
                'sid'            => 0,
                'd_code'         => randomkeys(8),
                'd_type'         => 1,
                'd_total'        => $times,
                'd_num'          => $times,
                'd_sell_num'     => 0,
                'd_addtime'      => $time,
                'd_status'       => 0,
                'd_admin_status' => 0,
                'last_time'      => 0,
                'd_date'         => $date,
                'd_grant'        => 1,
                'd_price'        => $product['p_reward_price'],
                'd_credit_3'     => $times
            );
            Db::name('Deal')->insertGetId($deal_data);

            $deal_sell = [
                'uid'            => $sell_uid,
                'pid'            => $pid,
                'sid'            => $pid,
                'd_code'         => randomkeys(8),
                'd_type'         => 2,
                'd_total'        => $times,
                'd_num'          => $times,
                'd_sell_num'     => 0,
                'd_addtime'      => $time,
                'd_status'       => 0,
                'd_admin_status' => 0,
                'last_time'      => 0,
                'd_date'         => $date,
                'd_grant'        => 1,
                'd_price'        => $product['p_reward_price'],
                'd_credit_3'     => $times,
                'd_token'        =>$token
            ];
            Db::name('Deal')->insertGetId($deal_sell);
            $new_num = bcsub($new_num,$times,4); //总奖励票数-这次发放的票数 = 还剩多少奖励票更新上去
            Db::name('wallet')->where('id',$data['id'])->setField('m_credit_6',$new_num);
    }else{
            Db::name('wallet')->where('id',$data['id'])->setField('m_credit_6',$new_num);
        }
    }
    return 1;
}



//执行每天的静态释放
function timing_release($uid){
    $user               = getUserInfo($uid);
    $time               = time();
    $date               = date('Y-m-d',$time);
    $ulogs_data         = Db::name('Ulogs')->where(array('uid'=>$uid,'u_status'=>1,'u_date'=>$date))->find();
    if(!empty($ulogs_data)){
        return 0;
    }
    $yesterday_start    = strtotime('-1 day');
    $yesterday_end      = $yesterday_start+86399;
    $config             = Db::name('Config')->where(array('id'=>1))->find();
    $wot_jt_scale       = $config['w_wot_jt_scale'];        //静态释放比例
    $wot_dt__scale      = $config['w_wot_dt__scale'];       //动态释放比例
    $wot_push_ask       = $config['w_wot_push_ask'];        //直推人数比例要求
    $wot_team_ask       = $config['w_wot_team_ask'];        //团队比例人数要求
    if(empty($user) || $user['m_lock']==1){
        return 0;
    }
    $tics_wot       = $user['m_tics_wot'];              //静态积分
    if($tics_wot == 0){
        return 0;
    }
    $push_num_count     = Db::name('User')->where(array('m_del'=>0,'m_lock'=>0,'m_bank_signing'=>1,'m_real'=>1,'m_tid'=>$uid))->where("m_level>0 and m_reg_time>=$yesterday_start and m_reg_time<=$yesterday_end")->count(); //昨日的直推人数
    $push_num           = Db::name('User')->where(array('m_del'=>0,'m_lock'=>0,'m_bank_signing'=>1,'m_real'=>1,'m_tid'=>$uid))->where("m_level",">","0")->count(); //一共的直推人数
    if($push_num == 0){
        $push_reta      = 0;
    }else{
        $push_reta      = $push_num_count/$push_num*100;
    }
    $team_num_count     = Db::name('User')->where(array('m_del'=>0,'m_lock'=>0,'m_bank_signing'=>1,'m_real'=>1))->where("m_level>0 and m_line like '%,$uid,%' and m_reg_time>=$yesterday_start and m_reg_time<=$yesterday_end")->count(); //昨日的团队人数
    $team_num           = Db::name('User')->where(array('m_del'=>0,'m_lock'=>0,'m_bank_signing'=>1,'m_real'=>1))->where("m_level>0 and m_line like '%,$uid,%'")->count(); //一共的团队人数
    if($team_num == 0){
        $team_reta      = 0;
    }else{
        $team_reta      = $team_num_count/$team_num*100;
    }
    $set_data_jt        = 0;
    if($wot_jt_scale>0){
        $set_data_jt    = $tics_wot*($wot_jt_scale/100);   //静态释放
    }
    if($wot_dt__scale<=0 || ($wot_push_ask<=0 && $wot_team_ask<=0) || $push_reta<$wot_push_ask || $team_reta<$wot_team_ask){
        $set_data_dt    = 0;
    }else{
        $p1 = $push_reta-$wot_push_ask;
        $t1 = $team_reta-$wot_team_ask;
        if($p1<=0){
            $p1 = 0;
        }
        if($t1<=0){
            $t1 = 0;
        }
        if($wot_push_ask && $p1){
            $set_data_dt    = $tics_wot*($wot_dt__scale/100);   //静态释放
        }
        if($team_reta && $t1){
            $set_data_dt    = $tics_wot*($wot_dt__scale/100);   //静态释放
        }
        if($wot_push_ask && $team_reta){
            if($p1 && $t1){
                $set_data_dt    = $tics_wot*($wot_dt__scale/100);   //静态释放
            }
        }
    }
    $m_wot              = $set_data_jt+$set_data_dt;
    do_logs($uid,3,'m_wot',$m_wot,'消费积分到账');
    do_logs($uid,4,'m_tics_wot',-$m_wot,'累计积分释放');
    $u_data = array(
        'uid'           =>$uid,
        'u_date'        =>$date,
        'u_push_num'    =>$push_num_count,
        'u_team_num'    =>$team_num_count,
        'u_jt_num'      =>$set_data_jt,
        'u_dt_num'      =>$set_data_dt,
        'u_total'       =>$m_wot,
        'u_addtime'     =>$time,
        'u_status'      =>1,
    );
    $res = Db::name('Ulogs')->insertGetId($u_data);
    return $res;
}
//自动触发票据回收信息
/*function whole_settlement_profit(){
    $time       = time();
    $date       = date('Y-m-d',$time);
    $product    = Db::name('Product')->where(array('p_setup_status' => 1))->order('last_time desc')->select();
    if(!empty($product)){
        $deal       = Db::name('Deal')->where(array('d_type'=>1,'d_status'=>2,'pid'=>$pid))->select();
        $fine       = Db::name('Fine')->where(array('did'=>$v['id'],'bid'=>$uid,'f_status'=>1,'f_end'=>0,'pid'=>$pid))->where("sdid<>0 and sid<>0 and f_profit_end_time<=$time")->select();
    }
}*/

//收益完成发放收益票据到钱包
function settlement_profit($uid,$pid){
    $time       = time();
    $date       = date('Y-m-d',$time);
    $user       = Db::name('User')->where(array('id'=>$uid))->find();
    if(empty($user)){
        return 0;
    }
    $deal       = Db::name('Deal')->where(array('d_type'=>1,'d_status'=>2,'uid'=>$uid,'pid'=>$pid))->select();
    if(empty($deal)){
        return 0;
    }

    $deal_data  = array(
        'd_status'          => 3,
        'last_time'         => $time,
        'd_finish_time'     => $time,
    );
    $set_deal   = array(
        'f_status'          => 3,
        'f_finish_time'     => $time,
        'last_time'         => $time,
        'f_end'             => 1,
    );
    $fine_sum1              = 0;
    $fine_sum2              = 0;
    $fine_sum3              = 0;
    foreach ($deal as $k=>$v){
        $fine   = Db::name('Fine')->where(array('did'=>$v['id'],'bid'=>$uid,'f_status'=>1,'f_end'=>0,'pid'=>$pid))->where("sdid<>0 and sid<>0 and f_profit_end_time<=$time")->select();
        if(empty($fine)){
            continue;
        }
        $fine_num = count($fine);
        if($v['d_credit_1'] > 0){
            $fine_sum1 += $fine_num;
        }
        if($v['d_credit_2'] > 0) {
            $fine_sum2 += $fine_num;
        }
        if($v['d_credit_3'] > 0) {
            $fine_sum3 += $fine_num;
        }
        Db::name('Fine')->where(array('did'=>$v['id'],'bid'=>$uid,'f_status'=>1,'f_end'=>0,'pid'=>$pid))->where("sdid<>0 and sid<>0 and f_profit_end_time<=$time")->update($set_deal);
        $fine_list = Db::name('Fine')->where(array('did'=>$v['id'],'f_status'=>3,'f_end'=>1))->where("f_finish_time <> 0 and sdid<>0 and sid<>0")->count();
        if($fine_num>0 && $fine_list==$v['d_total']){
            Db::name('Deal')->where(array('id'=>$v['id'],'uid'=>$uid,'pid'=>$pid))->update($deal_data);
        }
    }
    if($fine_sum1 > 0){
        set_wallet($uid,$pid,$fine_sum1,1,'收益完成,增加零售产品持有量');
    }
    if($fine_sum2 > 0){
        set_wallet($uid,$pid,$fine_sum2,2,'收益完成,增加批发产品持有量');
    }
    if($fine_sum3 > 0){
        set_wallet($uid,$pid,$fine_sum3,3,'收益完成,增加奖励产品持有量');
    }
    return 1;
}

//检查系统奖励是否存在
function grant_reward($uid){
    $time       = time();
    $date       = date('Y-m-d',$time);
    $user       = Db::name('User')->where(array('id'=>$uid))->find();
    if(empty($user)){
        return 0;
    }
    $deal       = Db::name('Deal')->where(array('d_type'=>1,'d_status'=>0,'uid'=>$uid,'d_token_isBuy'=>0))->where("d_date<='$date' and d_startDate>'$date'")->order('d_addtime asc')->find();
    if(empty($deal)){
        return 0;
    }
    return      $deal['id'];
}

function set_grant_reward($uid,$did){
    $time                = time();
    $date                = date('Y-m-d',$time);
    $user                = Db::name('User')->where(array('id'=>$uid))->find();
    if(empty($user)){
        return array('code' => 0, 'msg' => '用户信息有误');
    }
    $deal                = Db::name('Deal')->where(array('id'=>$did))->find();
    if(empty($deal)){
        return array('code' => 0, 'msg' => '系统未检测到当前订单');
    }
    $product             =   getProduct($deal['pid']);
    $deal_ratio          =   explode(':',$product['p_deal_ratio']);        //比例
    if(empty($deal_ratio) || count($deal_ratio)!=2){
        return array('code' => 0, 'msg' => '产品配售比例有误');
    }
    $d_total             =   $deal['d_total'];
    $d_price             =   $deal['d_price'];
    $retail_ratio        =   $deal_ratio[0];
    $whole_ratio         =   $deal_ratio[1];
    $deal_price          =   $d_price * $d_total;
    $buy_balance         =   getUserBalanceIdentity($uid, $deal_price);
    if(is_array($buy_balance)){
        return array('code' => 0, 'msg' => '您的钱包余额不足');
    }
    $sell_deal           =   Db::name('Deal')->where(array('d_token'=>$deal['d_token'],'d_type'=>2,'d_num'=>$deal['d_num'],'d_total'=>$deal['d_total']))->where("id <> $did")->find();
    if(empty($sell_deal)){
        return array('code' => 0, 'msg' => '系统未检测到当前订单');
    }
    $res_balance_buy = set_balance($uid,$deal_price);
    if(is_array($res_balance_buy)) {
        return json($res_balance_buy);
    }
    $credit_type = 1;
    if($deal['d_credit_2'] > 0) {
        $credit_type = 2;
    }
    if($deal['d_credit_3'] > 0) {
        $credit_type = 3;
    }
    Db::name('Deal')->where(array('id'=>$deal['id']))->update(array('d_status'=>2,'d_num'=>0,'d_sell_num'=>$deal['d_num'],'last_time'=>$time,'d_token_isBuy'=>1));
    Db::name('Deal')->where(array('id'=>$sell_deal['id']))->update(array('d_status'=>6,'d_finish_time'=>$time,'d_num'=>0,'d_sell_num'=>$sell_deal['d_num'],'last_time'=>$time));
    $fine_data  = array(
        'bid'                       => $uid,
        'sid'                       => $sell_deal['uid'],
        'pid'                       => $product['id'],
        'did'                       => $deal['id'],
        'sdid'                      => $sell_deal['id'],
        'f_type'                    => $credit_type,
        'f_price'                   => $d_price,
        'f_status'                  => 1,
        'f_profit_start_time'       => $time,
        'f_profit_end_time'         => 0,
        'f_addtime'                 => $time,
        'f_date'                    => $date,
        'f_end'                     => 0,
        'f_p_back'                  => 0,
    );
    for($i=1;$i<=$d_total;$i++){
        $fine_data['f_code']            =   randomkeys(8);
        $fine_data['f_set_strike']      =   0;
        Db::name('Fine')->insertGetId($fine_data);
    }
    $res_balance_sell   = 1;
    //$res_balance_sell = do_logs($sell_deal['uid'], 1, 'm_balance', $deal_price, '出售产品成功,货款入金');
    return $res_balance_sell;
}



function delFileByDir($dir)
{
    $dh = opendir($dir);
    while ($file = readdir($dh)) {
        if ($file != "." && $file != "..") {
            $full_path = $dir . "/" . $file;
            if (is_dir($full_path)) {
                delFileByDir($full_path);
            } else {
                unlink($full_path);
            }
        }
    }
    closedir($dh);
}

//上传图片
function setImagePath($img)
{
    $img_arr = explode(',', $img);
    if (count($img_arr) <= 1) {
        if (strlen($img_arr[0]) <= 10) {
            $img_path = get_file_path($img_arr[0]);
        } else {
            $img_path = $img_arr[0];
        }
    } else {
        $images = array();
        foreach ($img_arr as $k => $v) {
            if (strlen($v) <= 10) {
                $images[] = get_file_path($v);
            } else {
                $images[] = $v;
            }
        }
        $img_path = implode(',', $images);
    }
    return $img_path;
}

function sendGet($url)
{
    $http = $url;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $http);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//这个是重点。
    $data = curl_exec($curl);
    curl_close($curl);
    $str = array_iconv($data);
    if (strpos($str, 'pv_none_match') === false) {
        $shares = explode('~', $str);
        return $shares['1'];
    } else {
        $http = $url;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $http);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);//这个是重点。
        $data = curl_exec($curl);
        curl_close($curl);
        $str = array_iconv($data);
        if (strpos($str, 'pv_none_match') === false) {
            $shares = explode('~', $str);
            return $shares['1'];
        } else {
            return false;
        }
    }
}

//设置字符编码
function array_iconv($data, $output = 'utf-8')
{
    $encode_arr = array('UTF-8', 'ASCII', 'GBK', 'GB2312', 'BIG5', 'JIS', 'eucjp-win', 'sjis-win', 'EUC-JP');
    $encoded = mb_detect_encoding($data, $encode_arr);
    if (!is_array($data)) {
        return mb_convert_encoding($data, $output, $encoded);
    } else {
        foreach ($data as $key => $val) {
            $key = array_iconv($key, $output);
            if (is_array($val)) {
                $data[$key] = array_iconv($val, $output);
            } else {
                $data[$key] = mb_convert_encoding($data, $output, $encoded);
            }
        }
        return $data;
    }
}


function send_post($url, $header, $data)
{
    $ch = curl_init();
    if (substr($url, 0, 5) == 'https') {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);              // 对认证证书来源的检查
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);              // 从证书中检查SSL加密算法是否存在
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);              // 使用自动跳转
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $response = curl_exec($ch);
    if ($error = curl_error($ch)) {
        return 0;
    }
    curl_close($ch);
    return $response;
}

function sendPost($url, $header, $data)
{
    $data = http_build_query($data);
    $options = array(
        'http' => array(
            'method' => 'POST',
            'header' => $header,
            'content' => $data,
            'timeout' => 15 * 60 // 超时时间（单位:s）
        )
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    $res_data = json_decode($result, true);
    if (isset($res_data['error_no']) && isset($res_data['error'])) {
        return 0;
    }
    return $result;
}


//file_post 提交
function filePost($url, $post)
{
    $options = array(
        'http' => array(
            'method' => 'POST',
            'content' => http_build_query($post),
            'header' => 'Content-type: application/x-www-form-urlencoded',
        ),
    );
    $result = file_get_contents($url, false, stream_context_create($options));
    $data = json_decode($result, true); //将json数据转成数组
    return $data;
}

function goods_list($type, $key, $cate_id)
{
    $where = '1';
    if ($type) {
        $where .= " and g_type=$type";
    }
    if ($cate_id) {
        $where .= " and g_cid=$cate_id";
    }
    if ($key) {
        $where .= " and g_title like '%" . $key . "%'";
    }
    $goods_list = Db::name('Goods')->where($where)->order('g_tui desc,last_time desc')->select();
    foreach ($goods_list as &$v) {
        $v['g_img'] = explode(',', $v['g_pic'])[0];
    }
    return $goods_list;
}

function createNO($type = 3, $length, $table, $field)
{
    if ($type == 0) {      //纯数字
        $str = '1234567890';
    } elseif ($type == 1) {      //纯字母
        $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    } elseif ($type == 3) {
        $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
    }
    $randStr = str_shuffle($str) . time() . rand(0, 999);//打乱字符串
    $rands = substr($randStr, 0, $length);
    $count = Db::name($table)->where(array($field => $rands))->find();
    if ($count['num'] <= 0) {
        return $rands;
    } else {
        return createNO($type, $length, $table, $field);
    }
}

function ticketCount($type)
{
    $user = $this->check_user();
    $l_hold = $this->db->query("select count(*) as num from w_hold where h_type='" . $type . "' and h_uid='" . $user['id'] . "' and h_status>0", 2);
    $l_num = $l_hold['num'];
    $deal = $this->db->query("select count(*) num from w_deal where d_mid='" . $user['id'] . "' and d_ticket_type in (1,2,3) and d_status=1 group by d_sn", 2);
    $d_num = $deal['num'];
    return $l_num + $d_num;
}

//手续费设置
function setup_fee($uid,$num,$reta,$desc,$type){
    $fee_data = array(
        'f_uid'             => $uid,
        'f_num'             => $num,
        'f_rate'            => $reta,
        'f_addtime'         => time(),
        'f_type'            => $type,
        'f_funds_status'    => 1,
        'f_desc'            => $desc,
    );
    $res = Db::name('Fee')->insertGetId($fee_data);
    return $res;
}


######摘自微擎######
function ihttp_request($url, $post = '', $extra = array(), $timeout = 60)
{
    if (function_exists('curl_init') && function_exists('curl_exec') && $timeout > 0) {
        $ch = ihttp_build_curl($url, $post, $extra, $timeout);
        if (is_error($ch)) {
            return $ch;
        }
        $data = curl_exec($ch);
        $status = curl_getinfo($ch);
        $errno = curl_errno($ch);
        $error = curl_error($ch);
        curl_close($ch);
        if ($errno || empty($data)) {
            return error($errno, $error);
        } else {
            return ihttp_response_parse($data);
        }
    }
    $urlset = ihttp_parse_url($url, true);
    if (!empty($urlset['ip'])) {
        $urlset['host'] = $urlset['ip'];
    }
    $body = ihttp_build_httpbody($url, $post, $extra);
    if ('https' == $urlset['scheme']) {
        $fp = ihttp_socketopen('ssl://' . $urlset['host'], $urlset['port'], $errno, $error);
    } else {
        $fp = ihttp_socketopen($urlset['host'], $urlset['port'], $errno, $error);
    }
    stream_set_blocking($fp, $timeout > 0 ? true : false);
    stream_set_timeout($fp, ini_get('default_socket_timeout'));
    if (!$fp) {
        return error(1, $error);
    } else {
        fwrite($fp, $body);
        $content = '';
        if ($timeout > 0) {
            while (!feof($fp)) {
                $content .= fgets($fp, 512);
            }
        }
        fclose($fp);
        return ihttp_response_parse($content, true);
    }
}


function ihttp_get($url)
{
    return ihttp_request($url);
}


function ihttp_post($url, $data)
{
    $headers = array('Content-Type' => 'application/x-www-form-urlencoded');

    return ihttp_request($url, $data, $headers);
}


function ihttp_multi_request($urls, $posts = array(), $extra = array(), $timeout = 60)
{
    if (!is_array($urls)) {
        return error(1, '请使用ihttp_request函数');
    }
    $curl_multi = curl_multi_init();
    $curl_client = $response = array();

    foreach ($urls as $i => $url) {
        if (isset($posts[$i]) && is_array($posts[$i])) {
            $post = $posts[$i];
        } else {
            $post = $posts;
        }
        if (!empty($url)) {
            $curl = ihttp_build_curl($url, $post, $extra, $timeout);
            if (is_error($curl)) {
                continue;
            }
            if (CURLM_OK === curl_multi_add_handle($curl_multi, $curl)) {
                $curl_client[] = $curl;
            }
        }
    }
    if (!empty($curl_client)) {
        $active = null;
        do {
            $mrc = curl_multi_exec($curl_multi, $active);
        } while (CURLM_CALL_MULTI_PERFORM == $mrc);

        while ($active && CURLM_OK == $mrc) {
            do {
                $mrc = curl_multi_exec($curl_multi, $active);
            } while (CURLM_CALL_MULTI_PERFORM == $mrc);
        }
    }

    foreach ($curl_client as $i => $curl) {
        $response[$i] = curl_multi_getcontent($curl);
        curl_multi_remove_handle($curl_multi, $curl);
    }
    curl_multi_close($curl_multi);

    return $response;
}

function ihttp_socketopen($hostname, $port = 80, &$errno, &$errstr, $timeout = 15)
{
    $fp = '';
    if (function_exists('fsockopen')) {
        $fp = @fsockopen($hostname, $port, $errno, $errstr, $timeout);
    } elseif (function_exists('pfsockopen')) {
        $fp = @pfsockopen($hostname, $port, $errno, $errstr, $timeout);
    } elseif (function_exists('stream_socket_client')) {
        $fp = @stream_socket_client($hostname . ':' . $port, $errno, $errstr, $timeout);
    }

    return $fp;
}


function ihttp_response_parse($data, $chunked = false)
{
    $rlt = array();
    $pos = strpos($data, "\r\n\r\n");
    $split1[0] = substr($data, 0, $pos);
    $split1[1] = substr($data, $pos + 4, strlen($data));
    $split2 = explode("\r\n", $split1[0], 2);
    preg_match('/^(\S+) (\S+) (.*)$/', $split2[0], $matches);
    $rlt['code'] = !empty($matches[2]) ? $matches[2] : 200;
    $rlt['status'] = !empty($matches[3]) ? $matches[3] : 'OK';
    $rlt['responseline'] = !empty($split2[0]) ? $split2[0] : '';
    $isgzip = false;
    $ischunk = false;
    if (isset($split2[1])) {
        $header = explode("\r\n", $split2[1]);
        foreach ($header as $v) {
            $pos = strpos($v, ':');
            $key = substr($v, 0, $pos);
            $value = trim(substr($v, $pos + 1));
            if (isset($rlt['headers'][$key]) && is_array($rlt['headers'][$key])) {
                $rlt['headers'][$key][] = $value;
            } elseif (!empty($rlt['headers'][$key])) {
                $temp = $rlt['headers'][$key];
                unset($rlt['headers'][$key]);
                $rlt['headers'][$key][] = $temp;
                $rlt['headers'][$key][] = $value;
            } else {
                $rlt['headers'][$key] = $value;
            }
            if (!$isgzip && 'content-encoding' == strtolower($key) && 'gzip' == strtolower($value)) {
                $isgzip = true;
            }
            if (!$ischunk && 'transfer-encoding' == strtolower($key) && 'chunked' == strtolower($value)) {
                $ischunk = true;
            }
        }
    }
    if ($chunked && $ischunk) {
        $rlt['content'] = ihttp_response_parse_unchunk($split1[1]);
    } else {
        $rlt['content'] = $split1[1];
    }
    if ($isgzip && function_exists('gzdecode')) {
        $rlt['content'] = gzdecode($rlt['content']);
    }

    $rlt['meta'] = $data;
    if ('100' == $rlt['code']) {
        return ihttp_response_parse($rlt['content']);
    }
    return $rlt;
}

function ihttp_response_parse_unchunk($str = null)
{
    if (!is_string($str) or strlen($str) < 1) {
        return false;
    }
    $eol = "\r\n";
    $add = strlen($eol);
    $tmp = $str;
    $str = '';
    do {
        $tmp = ltrim($tmp);
        $pos = strpos($tmp, $eol);
        if (false === $pos) {
            return false;
        }
        $len = hexdec(substr($tmp, 0, $pos));
        if (!is_numeric($len) or $len < 0) {
            return false;
        }
        $str .= substr($tmp, ($pos + $add), $len);
        $tmp = substr($tmp, ($len + $pos + $add));
        $check = trim($tmp);
    } while (!empty($check));
    unset($tmp);

    return $str;
}

function error($errno, $message = '')
{
    return array(
        'errno' => $errno,
        'message' => $message,
    );
}

function is_error($data)
{
    if (empty($data) || !is_array($data) || !array_key_exists('errno', $data) || (array_key_exists('errno', $data) && 0 == $data['errno'])) {
        return false;
    } else {
        return true;
    }
}

function strexists($string, $find)
{
    return !(false === strpos($string, $find));
}

function ihttp_allow_host($host)
{
    if (strexists($host, '@')) {
        return false;
    }
    return true;
}

function ihttp_parse_url($url, $set_default_port = false)
{
    if (empty($url)) {
        return error(1);
    }
    $urlset = parse_url($url);
    if (!empty($urlset['scheme']) && !in_array($urlset['scheme'], array('http', 'https'))) {
        return error(1, '只能使用 http 及 https 协议');
    }
    if (empty($urlset['path'])) {
        $urlset['path'] = '/';
    }
    if (!empty($urlset['query'])) {
        $urlset['query'] = "?{$urlset['query']}";
    }
    if (strexists($url, 'https://') && !extension_loaded('openssl')) {
        if (!extension_loaded('openssl')) {
            return error(1, '请开启您PHP环境的openssl', '');
        }
    }
    if (empty($urlset['host'])) {
        $current_url = parse_url($GLOBALS['_W']['siteroot']);
        $urlset['host'] = $current_url['host'];
        $urlset['scheme'] = $current_url['scheme'];
        $urlset['path'] = $current_url['path'] . 'web/' . str_replace('./', '', $urlset['path']);
        $urlset['ip'] = '127.0.0.1';
    } elseif (!ihttp_allow_host($urlset['host'])) {
        return error(1, 'host 非法');
    }

    if ($set_default_port && empty($urlset['port'])) {
        $urlset['port'] = 'https' == $urlset['scheme'] ? '443' : '80';
    }

    return $urlset;
}


function ihttp_build_curl($url, $post, $extra, $timeout)
{
    if (!function_exists('curl_init') || !function_exists('curl_exec')) {
        return error(1, 'curl扩展未开启');
    }

    $urlset = ihttp_parse_url($url);
    if (is_error($urlset)) {
        return $urlset;
    }

    if (!empty($urlset['ip'])) {
        $extra['ip'] = $urlset['ip'];
    }

    $ch = curl_init();
    if (!empty($extra['ip'])) {
        $extra['Host'] = $urlset['host'];
        $urlset['host'] = $extra['ip'];
        unset($extra['ip']);
    }
    curl_setopt($ch, CURLOPT_URL, $urlset['scheme'] . '://' . $urlset['host'] . (empty($urlset['port']) || '80' == $urlset['port'] ? '' : ':' . $urlset['port']) . $urlset['path'] . (!empty($urlset['query']) ? $urlset['query'] : ''));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    @curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    @curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    if ($post) {
        if (is_array($post)) {
            $filepost = false;
            foreach ($post as $name => &$value) {
                if (version_compare(phpversion(), '5.5') >= 0 && is_string($value) && '@' == substr($value, 0, 1)) {
                    $post[$name] = new CURLFile(ltrim($value, '@'));
                }
                if ((is_string($value) && '@' == substr($value, 0, 1)) || (class_exists('CURLFile') && $value instanceof CURLFile)) {
                    $filepost = true;
                }
            }
            if (!$filepost) {
                $post = http_build_query($post);
            }
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    if (!empty($GLOBALS['_W']['config']['setting']['proxy'])) {
        $urls = parse_url($GLOBALS['_W']['config']['setting']['proxy']['host']);
        if (!empty($urls['host'])) {
            curl_setopt($ch, CURLOPT_PROXY, "{$urls['host']}:{$urls['port']}");
            $proxytype = 'CURLPROXY_' . strtoupper($urls['scheme']);
            if (!empty($urls['scheme']) && defined($proxytype)) {
                curl_setopt($ch, CURLOPT_PROXYTYPE, constant($proxytype));
            } else {
                curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
                curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
            }
            if (!empty($GLOBALS['_W']['config']['setting']['proxy']['auth'])) {
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, $GLOBALS['_W']['config']['setting']['proxy']['auth']);
            }
        }
    }
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSLVERSION, 1);
    if (defined('CURL_SSLVERSION_TLSv1')) {
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
    }
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1');
    if (!empty($extra) && is_array($extra)) {
        $headers = array();
        foreach ($extra as $opt => $value) {
            if (strexists($opt, 'CURLOPT_')) {
                curl_setopt($ch, constant($opt), $value);
            } elseif (is_numeric($opt) && $opt != 0) {
                curl_setopt($ch, $opt, $value);
            } else {
                $headers[] = "{$opt}: {$value}";
            }
        }
        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
    }

    return $ch;
}

function ihttp_build_httpbody($url, $post, $extra)
{
    $urlset = ihttp_parse_url($url, true);
    if (is_error($urlset)) {
        return $urlset;
    }

    if (!empty($urlset['ip'])) {
        $extra['ip'] = $urlset['ip'];
    }

    $body = '';
    if (!empty($post) && is_array($post)) {
        $filepost = false;
        $boundary = random(40);
        foreach ($post as $name => &$value) {
            if ((is_string($value) && '@' == substr($value, 0, 1)) && file_exists(ltrim($value, '@'))) {
                $filepost = true;
                $file = ltrim($value, '@');

                $body .= "--$boundary\r\n";
                $body .= 'Content-Disposition: form-data; name="' . $name . '"; filename="' . basename($file) . '"; Content-Type: application/octet-stream' . "\r\n\r\n";
                $body .= file_get_contents($file) . "\r\n";
            } else {
                $body .= "--$boundary\r\n";
                $body .= 'Content-Disposition: form-data; name="' . $name . '"' . "\r\n\r\n";
                $body .= $value . "\r\n";
            }
        }
        if (!$filepost) {
            $body = http_build_query($post, '', '&');
        } else {
            $body .= "--$boundary\r\n";
        }
    }

    $method = empty($post) ? 'GET' : 'POST';
    $fdata = "{$method} {$urlset['path']}{$urlset['query']} HTTP/1.1\r\n";
    $fdata .= "Accept: */*\r\n";
    $fdata .= "Accept-Language: zh-cn\r\n";
    if ('POST' == $method) {
        $fdata .= empty($filepost) ? "Content-Type: application/x-www-form-urlencoded\r\n" : "Content-Type: multipart/form-data; boundary=$boundary\r\n";
    }
    $fdata .= "Host: {$urlset['host']}\r\n";
    $fdata .= "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1\r\n";
    if (function_exists('gzdecode')) {
        $fdata .= "Accept-Encoding: gzip, deflate\r\n";
    }
    $fdata .= "Connection: close\r\n";
    if (!empty($extra) && is_array($extra)) {
        foreach ($extra as $opt => $value) {
            if (!strexists($opt, 'CURLOPT_')) {
                $fdata .= "{$opt}: {$value}\r\n";
            }
        }
    }
    if ($body) {
        $fdata .= 'Content-Length: ' . strlen($body) . "\r\n\r\n{$body}";
    } else {
        $fdata .= "\r\n";
    }
    return $fdata;
}
######摘自微擎######
