<?php
namespace app\admin\controller;
use app\common\builder\ZBuilder;
use think\Db;

class Goods extends Admin
{
    //分类列表
    public function cate_list(){
        do_alogs('查看分类列表');
        $map        = $this->getMap();
        $order      = $this->getOrder('c_index asc');
        $data_list  = Db::name('Cate')->where(array('c_del'=>0))->where($map)->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('cate')
            ->setSearch(['id' => 'ID', 'c_name' => '分类名称']) // 设置搜索参数
            ->addColumns([                                                                // 批量添加数据列
                ['id', 'ID'],
                ['c_name', '分类名称', 'text'],
                ['c_img','分类图标','img_url'],
                ['c_index', '排序','text'],
                ['c_display', '展示','status','',['否','是']],
                ['c_addtime','创建时间','datetime'],
                ['last_time','更新时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time'=>150,'id'=>60])
            ->setExtraCss($css)
            ->hideCheckbox()
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,c_addtime')                                                               //添加排序
            ->addTopButton('add',['href' => url('cate_add')])
            ->addRightButtons(
                ['edit'  => ['title' => '编辑','href'=>url('cate_edit', ['id'=>'__id__'])],
                 'delete'=> ['title' => '删除','href'=>url('cate_del', ['id'=>'__id__'])],
                ])
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }
    //添加分类
    public function cate_add(){
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        if($this->request->post()){
            do_alogs('添加分类信息');
            $fields     = input('post.');
            if(!$fields['c_name']){
                $this->error('分类名称 不能为空');
            }
            if(!$fields['c_img']){
                $this->error('分类图标 不能为空');
            }
            $fields['c_img'] = get_file_path($fields['c_img']);
            $fields['c_addtime']    = time();
            $fields['admin_id']     = $admin_id;
            $res_id = Db::name('Cate')->insertGetId($fields);
            if($res_id){
                $this->success('分类添加成功',url('Goods/cate_list'));
            }else{
                $this->error('分类添加失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('新增')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['text', 'c_name', '分类名称','(请输入分类名称)'],
                ['image', 'c_img', '分类图标','(请上传分类图标)'],
                ['number', 'c_index', '排序','(请输入排序)'],
                ['radio', 'c_display','展示','请选择分类是否展示',['否','是'],1],
            ])
            ->layout(['c_name' => 2, 'c_index' => 1])
            ->fetch();
    }
    //编辑分类
    public function cate_edit($id=''){
        $data           = Db::name('Cate')->where(array('id'=>$id))->find();
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        if($this->request->post()){
            do_alogs('编辑分类信息');
            $fields     = input('post.');
            if(!$fields['c_name']){
                $this->error('分类名称 不能为空');
            }
            if($fields['c_img']){
                $fields['c_img'] = setImagePath($fields['c_img']);
            }else{
                if($fields['c_img']==''){
                    $this->error('分类图标 不能为空');
                }
            }
            $fields['last_time']    = time();
            $res_id = Db::name('Cate')->where(array('id'=>$id))->update($fields);
            if($res_id){
                $this->success('分类编辑成功',url('Goods/cate_list'));
            }else{
                $this->error('分类编辑失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('编辑')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden','id'],
                ['text', 'c_name', '分类名称','(请输入分类名称)'],
                ['image', 'c_img', '分类图标','(请上传分类图标)'],
                ['number', 'c_index', '排序','(请输入排序)'],
                ['radio', 'c_display','展示','请选择分类是否展示',['否','是'],1],
            ])
            ->setFormData($data)
            ->layout(['c_name' => 2, 'c_index' => 1])
            ->fetch();
    }
    //删除分类
    public function cate_del($id=''){
        $data           = Db::name('Cate')->where(array('id'=>$id))->find();
        if(!empty($data)){
            $res =Db::name('Cate')->where('id',$id)->update(array('c_del'=>1));
            if($res == 1){
                do_alogs('删除分类信息');
                $this->success('删除成功');
            }else{
                $this->error('删除失败');
            }
        }
    }

    //轮播图列表
    public function banner_list(){
        do_alogs('查看轮播图列表');
        $map        = $this->getMap();
        $order      = $this->getOrder('b_index asc');
        $data_list  = Db::name('Banner')->where($map)->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('banner')
            ->setSearch(['id' => 'ID', 'b_name' => '轮播图名称']) // 设置搜索参数
            ->addColumns([                                                                // 批量添加数据列
                ['id', 'ID'],
                ['b_name', '轮播图名称', 'text'],
                ['b_img','图片','img_url'],
                ['b_index', '排序','text'],
                ['b_time','创建时间','datetime'],
                ['last_time','更新时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time'=>150,'id'=>60])
            ->setExtraCss($css)
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,b_time')                                                               //添加排序
            ->addTopButton('add',['href' => url('banner_add')])
            ->addTopButton('delete',['href' => url('banner_del')])
            ->addRightButtons(
                ['edit'  => ['title' => '编辑','href'=>url('banner_edit', ['id'=>'__id__'])],
                    'delete'=> ['title' => '删除','href'=>url('banner_del', ['id'=>'__id__'])],
                ])
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }

    //轮播图添加
    public function banner_add(){
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        if($this->request->post()){
            do_alogs('添加轮播图信息');
            $fields     = input('post.');
            if(!$fields['b_name']){
                $this->error('轮播图名称 不能为空');
            }
            if(!$fields['b_img']){
                $this->error('轮播图图片 不能为空');
            }
            $fields['b_img']        = get_file_path($fields['b_img']);
            $fields['b_time']       = time();
            $fields['admin_id']     = $admin_id;
            $res_id = Db::name('Banner')->insertGetId($fields);
            if($res_id){
                $this->success('轮播图添加成功',url('Goods/banner_list'));
            }else{
                $this->error('轮播图添加失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('新增')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['text', 'b_name', '名称','(请输入名称)'],
                ['image', 'b_img', '图片','(请上传图片)'],
                ['number', 'b_index', '排序','(请输入排序)'],
                ['text', 'b_link','跳转链接','请输入跳转链接'],
            ])
            ->layout(['b_name' => 2, 'b_index' => 1])
            ->fetch();
    }

    //轮播图编辑
    public function banner_edit($id=''){
        $data           = Db::name('Banner')->where(array('id'=>$id))->find();
        if($this->request->post()){
            do_alogs('添加轮播图信息');
            $fields     = input('post.');
            if(!$fields['b_name']){
                $this->error('轮播图名称 不能为空');
            }
            if($fields['b_img']){
                $fields['b_img'] = setImagePath($fields['b_img']);
            }else{
                if($fields['b_img']==''){
                    $this->error('轮播图图片 不能为空');
                }
            }
            $fields['last_time']        = time();
            $res_id = Db::name('Banner')->where(array('id'=>$id))->update($fields);
            if($res_id){
                $this->success('轮播图编辑成功',url('Goods/banner_list'));
            }else{
                $this->error('轮播图编辑失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('编辑')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden','id'],
                ['text', 'b_name', '名称','(请输入名称)'],
                ['image', 'b_img', '图片','(请上传图片)'],
                ['number', 'b_index', '排序','(请输入排序)'],
                ['text', 'b_link','跳转链接','请输入跳转链接'],
            ])
            ->setFormData($data)
            ->layout(['b_name' => 2, 'b_index' => 1])
            ->fetch();
    }

    //轮播图删除
    public function banner_del(){
        do_alogs('删除轮播图信息');
        if($this->request->isPost()){
            $data   = input('post.');
            $ids    = implode($data['ids'],',');
            $is_del = Db::name('Banner')->where(array('id'=>array('in',$ids)))->delete();

            if($is_del){
                $this->success('删除成功',url('Goods/banner_list'));
            }else{
                $this->error('删除失败');
            }
        }
        $banner_id = input('id');
        $is_del = Db::name('Banner')->where(array('id'=>$banner_id))->delete();
        if($is_del){
            $this->success('删除成功',url('Goods/banner_list'));
        }else{
            $this->error('删除失败');
        }
    }


    public function goods_list(){
        do_alogs('查看商品列表');
        $map        = $this->getMap();
        $order      = $this->getOrder('g_time desc');
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        $cate       = Db::name('Cate')->where(array('c_del'=>0))->select();
        $arr        = array();
        foreach ($cate as $k=>$v){
            $arr[$v['id']]=$v['c_name'];
        }
        $sql        = 1;
        if($admin_id>1 and $role==2){
            $sql    = "admin_id=$admin_id";
        }
        $data_list  = Db::name('Goods')->where(array('g_del'=>0))->where($map)->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('goods')
            ->setSearch(['id' => 'ID', 'g_title' => '商品标题', 'g_price' => '商品价格']) // 设置搜索参数
            ->addTimeFilter('last_time','','开始时间,结束时间')           // 添加时间段筛选
            ->addTopSelect('g_lock', '',['下架','上架'])
            ->addColumns([                                                                // 批量添加数据列
                ['id', 'ID'],
                ['g_title', '商品标题', 'text'],
                ['g_price','商品价格', 'text'],
                ['g_cid', '商品分类','status','',$arr],
                ['g_type', '商品类别','status','',[1=>'购物专区',2=>'积分专区']],
                ['g_pic','商品图片','img_urls'],
                ['g_sale', '销量','text'],
                ['g_credit', '兑换积分','text'],
                ['g_ku', '库存','text'],
                ['g_lock', '上下架','switch'],
                ['g_tui', '推荐','status','',['否','是']],
                ['g_time','创建时间','date'],
                ['last_time','更新时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time'=>150,'id'=>60])
            ->setExtraCss($css)
            ->hideCheckbox()
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,g_time')                                                               //添加排序
            ->addTopButton('add',['href' => url('goods_add')])
            ->addRightButtons(
                ['edit'  => ['title' => '编辑','href'=>url('goods_edit', ['id'=>'__id__'])],
                 'delete'=> ['title' => '删除','href'=>url('goods_del', ['id'=>'__id__'])],
                ])
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }

    public function goods_add(){
        $config         = $this->set_config();
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $cate           = Db::name('Cate')->where(array('c_del'=>0))->select();
        $arr            = array();
        foreach ($cate as $k=>$v){
            $arr[$v['id']]=$v['c_name'];
        }
        if($this->request->post()){
            do_alogs('添加商品列表');
            $fields     = input('post.');
            if(!$fields['g_title']){
                $this->error('商品标题 不能为空');
            }
            if(!$fields['g_cid']){
                $this->error('请选择商品分类');
            }
            if(!$fields['g_pic']){
                $this->error('商品图片不能为空');
            }
            $fields['g_pic'] = get_files_path($fields['g_pic']);
            if($fields['g_type'] == 1){
                if(!$fields['g_price']){
                    $this->error('商品价格 填写有误');
                }
            }else{
                if($fields['g_price']){
                    $this->error('积分专区请勿设置商品价格');
                }
                if(!$fields['g_credit']){
                    $this->error('兑换积分 填写有误');
                }
            }
            if(!$fields['g_ku']){
                $this->error('库存 填写有误');
            }
            if(!$fields['g_detail']){
                $this->error('请认真填写商品详情');
            }

            $fields['g_time']       = time();
            $fields['admin_id']     = $admin_id;
            $res_id = Db::name('Goods')->insertGetId($fields);
            if($res_id){
                $this->success('商品添加成功',url('Goods/goods_list'));
            }else{
                $this->error('商品添加失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('新增')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['text', 'g_title', '商品标题','(请输入商品标题)'],
                ['select', 'g_cid', '商品分类','(请选择商品分类)',$arr],
                ['radio', 'g_type','类别','请选择商品类型',[1=>'购物专区',2=>'积分专区'],1],
                ['images', 'g_pic','商品图片','请上传商品图片'],
                ['number', 'g_price', '商品价格','(请输入商品价格)'],
                ['number', 'g_credit', '积分','(请输入商品积分)'],
                ['number', 'g_sale', '基础销量','(请输入基础销量)'],
                ['number', 'g_ku', '库存','(请输入库存)'],
                ['radio', 'g_tui','是否推荐','',['否','是'],0],
                ['radio', 'g_lock','上下架','',['下架','上架'],0],
                ['ckeditor', 'g_detail', '商品详情','(请输入商品详情)'],
            ])
            ->layout(['g_title' => 4,'g_price' => 1,'g_cid' => 3, 'g_credit' => 1, 'g_sale' => 1, 'g_ku' => 1, 'g_detail' => 6])
            ->fetch();
    }

    public function goods_edit($id=''){
        $data           = Db::name('Goods')->where(array('id'=>$id))->find();
        $cate           = Db::name('Cate')->where(array('c_del'=>0))->select();
        $arr            = array();
        foreach ($cate as $k=>$v){
            $arr[$v['id']]=$v['c_name'];
        }
        if($this->request->post()){
            do_alogs('编辑商品列表');
            $fields     = input('post.');
            if(!$fields['g_title']){
                $this->error('商品标题 不能为空');
            }
            if(!$fields['g_cid']){
                $this->error('请选择商品分类');
            }
            if(!$fields['g_pic']){
                $this->error('商品图片 不能为空');
            }
            $fields['g_pic'] = setImagePath($fields['g_pic']);
            if($fields['g_type'] == 1){
                if(!$fields['g_price']){
                    $this->error('商品价格 填写有误');
                }
            }else{
                if($fields['g_price']>0){
                    $this->error('积分专区请勿设置商品价格');
                }
                if(!$fields['g_credit']){
                    $this->error('兑换积分 填写有误');
                }
            }
            if(!$fields['g_ku']){
                $this->error('库存 填写有误');
            }
            if(!$fields['g_detail']){
                $this->error('请认真填写商品详情');
            }
            $fields['last_time']       = time();
            $res_id = Db::name('Goods')->where(array('id'=>$id))->update($fields);
            if($res_id){
                $this->success('商品编辑成功',url('Goods/goods_list'));
            }else{
                $this->error('商品编辑失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('编辑')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden','id'],
                ['text', 'g_title', '商品标题','(请输入商品标题)'],
                ['select', 'g_cid', '商品分类','(请选择商品分类)',$arr],
                ['radio', 'g_type','类别','请选择商品类型',[1=>'购物专区',2=>'积分专区']],
                ['images', 'g_pic','商品图片','请上传商品图片'],
                ['number', 'g_price', '商品价格','(请输入商品价格)'],
                ['number', 'g_credit', '积分','(请输入商品积分)'],
                ['number', 'g_sale', '基础销量','(请输入基础销量)'],
                ['number', 'g_ku', '库存','(请输入库存)'],
                ['radio', 'g_tui','是否推荐','',['否','是'],0],
                ['radio', 'g_lock','上下架','',['下架','上架'],0],
                ['ckeditor', 'g_detail', '商品详情','(请输入商品详情)'],
            ])
            ->setFormData($data)
            ->layout(['g_title' => 4,'g_price' => 1,'g_cid' => 3, 'g_credit' => 1, 'g_sale' => 1, 'g_ku' => 1, 'g_detail' => 6])
            ->fetch();
    }

    public function goods_del(){
        do_alogs('删除商品列表');
    }

    //商品订单
    public function order_list(){
        do_alogs('查看订单列表');
        $map        = $this->getMap();
        $order      = $this->getOrder('o_addtime desc');
        $express    = Db::name('Express')->select();
        $arr        = array();
        foreach ($express as $k=>$v){
            $arr[$v['id']]=$v['name'];
        }

        $time = time();
        if(isset($_SESSION['tblqry']))
		{
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < $time) unset($qrycache[$_k]);
			}
		}
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => $time + 600];
        $_SESSION['tblqry'] = $qrycache;

        // 所有商品
        $_goods_list = Db::name('goods')->where('g_del', 0)->select();

        $goods_list = [];
        foreach($_goods_list as $k => $v){
            $goods_list[$v['id']] = $v['g_title'];
        }

        $data_list  = Db::name('Order a')->join('w_user b','b.id=a.uid')->join('w_goods c','c.id=a.gid')->join('w_address d','d.id=a.aid')->where($map)->field(
            'a.*,b.m_nickname,b.m_phone,b.m_account,c.g_type,c.g_title,c.g_pic,d.a_name,d.a_phone,d.a_city,d.a_detail')->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        $status     = ['待付款','待发货','待收货','已完成'];
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('order')
            ->setSearch(['id' => 'ID','订单编号','g_title' => '商品名称','m_nickname'=>'用户昵称']) // 设置搜索参数
            ->addTimeFilter('o_addtime','','开始时间,结束时间') // 添加时间段筛选
            ->addTopSelect('o_status', '',$status)
            ->addTopSelect('gid', '', $goods_list)
            ->addColumns([                                                                // 批量添加数据列
                ['id', 'ID'],
                ['o_code', '订单编号','text'],
                ['m_nickname', '昵称', 'text'],
                ['m_phone','手机号','text'],
                ['m_account','交易账号','text'],
                ['o_status','订单状态','status','',$status],
                ['g_title','商品名称','text'],
                ['o_price','单价','text'],
                ['o_credit','积分单价','text'],
                ['o_buy_num','订单数量','text'],
                ['o_credit_1','订单总价','text'],
                ['o_credit_2','积分总价','text'],
                ['g_pic','商品图片','img_urls'],
                ['g_type','商品类型','status','',['1'=>'购物专区','2'=>'积分专区']],
                ['a_name', '收货人','text'],
                ['a_phone', '收货电话','text'],
                ['a_city', '收货城市','text'],
                ['a_detail', '收货城市','text'],
                ['o_info', '订单备注','text'],
                ['o_addtime','创建时间','datetime'],
                ['o_pay_time','付款时间','datetime'],
                ['o_send_time','发货时间','datetime'],
                ['o_take_time','收货时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time'=>150,'id'=>60])
            ->setExtraCss($css)
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,o_addtime')                                                               //添加排序
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('order_list_export', ['uniqkey' => $uniqkey])])
            ->addTopButton('delete',['href' => url('order_del')])
            ->addRightButton('edit',['title'=>'发货','icon'=>'fa fa-fw fa-truck','href' => url('order_set', ['id'=>'__id__'])],['area' => ['500px', '500px']])
            ->addRightButtons(['delete'=>['title' =>'删除','href'=>url('order_del', ['id'=>'__id__'])]])
            ->addTopButton('delete', ['title'=>'待付款', 'href'=>url('send_out_good_status', ['status'=>0]), 'class'=>'btn btn-default ajax-post', 'icon'=>''])
            ->addTopButton('delete', ['title'=>'待发货', 'href'=>url('send_out_good_status', ['status'=>1]), 'class'=>'btn btn-primary ajax-post', 'icon'=>''])
            ->addTopButton('delete', ['title'=>'待收货', 'href'=>url('send_out_good_status', ['status'=>2]), 'class'=>'btn btn-info ajax-post', 'icon'=>''])
            ->addTopButton('delete', ['title'=>'已完成', 'href'=>url('send_out_good_status', ['status'=>3]), 'class'=>'btn btn-success ajax-post', 'icon'=>''])
            
            ->replaceRightButton(['o_status'=>['<>',1]],'','edit')
            ->replaceRightButton(['o_status'=>['<>',3]],'','delete')
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }

    //
    public function order_set($id=''){
        $express    = Db::name('Express')->select();
        $order      = Db::name('Order')->where(array('id'=>$id))->find();
        if(!empty($order) and $order['o_status']!=1){
            $this->error('订单状态有误');
        }
        $arr        = array();
        foreach ($express as $k=>$v){
            $arr[$v['code']]=$v['name'];
        }
        if($this->request->post()){
            do_alogs('设置产品发货');
            $fields         =   input('post.');
            if(!$fields['o_company_code']){
                $this->error('快递公司 不能为空');
            }
            if(!$fields['o_company_num']){
                $this->error('快递编号 不能为空');
            }
            $fields['o_company']        =$arr[$fields['o_company_code']];
            $fields['o_status']         =2;
            $fields['o_send_time']      =time();
            $fields['last_time']        =time();
            $res = Db::name('Order')->where(array('id'=>$id))->update($fields);
            if($res){
                $this->success('设置成功',url('Goods/order_list'),'_parent_reload');
            }else{
                $this->error('设置失败 稍后重试');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('填写物流信息')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['select', 'o_company_code', '快递公司','(请选择快递公司)',$arr],
                ['text', 'o_company_num', '快递编号','(请输入快递编号)'],
                ['number', 'o_company_price','运费','(请输入运费)'],
            ])
            ->fetch();
    }

    public function order_del(){
        do_alogs('删除商城订单信息');
        if($this->request->isPost()){
            $data   = input('post.');
            $ids    = implode($data['ids'],',');
            $is_del = Db::name('Order')->where(array('id'=>array('in',$ids),'o_status'=>3))->delete();

            if($is_del){
                $this->success('删除成功',url('Goods/Order_list'));
            }else{
                $this->error('删除失败');
            }
        }
        $order_id = input('id');
        $is_del = Db::name('Order')->where(array('id'=>$order_id,'o_status'=>3))->delete();
        if($is_del){
            $this->success('删除成功',url('Goods/Order_list'));
        }else{
            $this->error('删除失败');
        }
    }


    // 商品订单导出
    public function order_list_export()
    {

        $uniqkey = input('uniqkey');
    	if(!empty($uniqkey) && isset($_SESSION['tblqry'])){

            $tblqry = $_SESSION['tblqry'];
            
			if(isset($tblqry[$uniqkey])){

                $qrycache = $tblqry[$uniqkey];
                $map = $qrycache['where'];
                $order = $qrycache['order'];

                $data_list  = Db::name('Order a')->join('w_user b','b.id=a.uid')->join('w_goods c','c.id=a.gid')->join('w_address d','d.id=a.aid')->where($map)->field(
                    'a.*,b.m_nickname,b.m_phone,b.m_account,c.g_type,c.g_title,c.g_pic,d.a_name,d.a_phone,d.a_city,d.a_detail')->order($order)->select();

                $titlArr = [
                    'ID', '订单编号', '昵称', '手机号', '交易账号', '订单状态', '商品名称', '单价', '积分单价', '订单数量', '订单总价', '积分总价', '商品类型', '收货人', '收货电话', '收货地址', '订单备注', '创建时间', '付款时间', '发货时间', '收货时间'
                ];
                
                $dataArr = [];

                foreach ($data_list as $k => $v){

                    $status = ['待付款','待发货','待收货','已完成'];

                    $status_txt = $status[$v['o_status']];

                    $dataArr[] = [
                        'ID' => $v['id'],
                        '订单编号' => $v['o_code'],
                        '昵称' => $v['m_nickname'],
                        '手机号' => filter_value($v['m_phone']),
                        '交易账号' => filter_value($v['m_account']),
                        '订单状态' => $status_txt,
                        '商品名称' => $v['g_title'],
                        '单价' => $v['o_price'],
                        '积分单价' => $v['o_credit'],
                        '订单数量' => $v['o_buy_num'],
                        '订单总价' => $v['o_credit_1'],
                        '积分总价' => $v['o_credit_2'],
                        '商品类型' => ($v['g_type']==1)?'购物专区':'积分专区',
                        '收货人' => $v['a_name'],
                        '收货电话' => filter_value($v['a_phone']),
                        '收货地址' => $v['a_city'] . ' ' . $v['a_detail'],
                        '订单备注' => $v['o_info'],
                        '创建时间' => date('Y-m-d H:i:s', $v['o_addtime']),
                        '付款时间' => ($v['o_pay_time']==0)?'未付款':date('Y-m-d H:i:s', $v['o_pay_time']),
                        '发货时间' => ($v['o_send_time']==0)?'未发货':date('Y-m-d H:i:s', $v['o_send_time']),
                        '收货时间' => ($v['o_take_time']==0)?'未收货':date('Y-m-d H:i:s', $v['o_take_time'])
                    ];

                }

                $filename = '商品订单-' . date('YmdHis');
                return export_csv($filename . '.csv', $titlArr, $dataArr);




            }
			
        }

    }

    // 修改订单状态
    public function send_out_good_status()
    {
        $param = $this->request->param();

        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];

        if(!isset($param['status'])) return $this->error('缺少参数');
        if(empty($param['ids'])) return $this->error('请选择需要操作的订单');

        $result = Db::name('order')->where('id', 'in', $param['ids'])->update(['o_status' => $param['status']]);
        if($result){
            // 添加日志
            // var_export($param['ids'], true)
            $_deac = '修改订单，ids为：'.implode(',', $param['ids']).'，状态修改为'.$param['status'];
            $log_info = [
                'admin_id' => $admin_id,
                'role_id' => 0,
                'last_time' => time(),
                'a_desc' => $_deac,
            ];
            Db::name('Alogs')->insert($log_info);
            return $this->success('修改失败', url('order_list'));
        }else{
            return $this->error('操作失败');
        }
    }


}