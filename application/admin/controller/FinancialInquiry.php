<?php
// 财务查询模块
namespace app\admin\controller;

use think\Db;
use app\common\builder\ZBuilder;

class FinancialInquiry extends Admin
{

    // 分类账
    public function ledger()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        // $data_list = Db::name('数据表名')->where($map)->order($order)->limit(20)->paginate();
        $data_list = [];

        return ZBuilder::make('table')
            ->setTableName('数据表名')
            ->addColumns([
                ['id', '序号'],
                ['', '结算日期', 'text'],
                ['', '凭证号', 'text'],
                ['', '凭证摘要', 'text'],
                ['', '对方科目', 'text'],
                ['', '对方科目名称', 'text'],
                ['', '合同号', 'text'],
                ['', '借方发生额', 'text'],
                ['', '贷方发生额', 'text'],
                ['', '期末余额', 'text'],
            ])
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();

    }

    // 分类账合计
    public function ledger_total()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        // $data_list = Db::name('数据表名')->where($map)->order($order)->limit(20)->paginate();
        $data_list = [];

        return ZBuilder::make('table')
            ->setTableName('数据表名')
            ->addColumns([
                ['id', '序号'],
                ['', '凭证摘要号', 'text'],
                ['', '凭证摘要', 'text'],
                ['', '借方发生额', 'text'],
                ['', '贷方发生额', 'text'],
                ['', '余额', 'text'],
            ])
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();

    }

    // 账簿查询
    public function account_book_query()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        // $data_list = Db::name('数据表名')->where($map)->order($order)->limit(20)->paginate();
        $data_list = [];

        return ZBuilder::make('table')
            ->setTableName('数据表名')
            ->addColumns([
                ['id', '序号'],
                ['', '结算日期', 'text'],
                ['', '凭证号', 'text'],
                ['', '凭证摘要号', 'text'],
                ['', '凭证摘要', 'text'],
                ['', '借方科目', 'text'],
                ['', '借方科目名称', 'text'],
                ['', '贷方科目', 'text'],
                ['', '贷方科目名称', 'text'],
                ['', '发生金额', 'text'],
                ['', '合同号', 'text'],
            ])
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();


    }

    // 结算查询
    public function settlement_query()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        // $data_list = Db::name('数据表名')->where($map)->order($order)->limit(20)->paginate();
        $data_list = [];

        return ZBuilder::make('table')
            ->setTableName('数据表名')
            ->addColumns([
                ['id', '序号'],
                ['', '结算日期', 'text'],
                ['', '科目代码', 'text'],
                ['', '科目名称', 'text'],
                ['', '科目级别', 'text'],
                ['', '期初余额', 'text'],
                ['', '借方发生额', 'text'],
                ['', '贷方发生额', 'text'],
                ['', '期末余额', 'text'],
            ])
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();

    }


    // 结算日报表 
    public function settlement_day_report_form()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id desc');

        // $data_list = Db::name('数据表名')->where($map)->order($order)->limit(20)->paginate();
        $data_list = [];

        return ZBuilder::make('table')
            ->setTableName('数据表名')
            ->addColumns([
                ['id', '序号'],
                ['', '科目代码', 'text'],
                ['', '科目名称', 'text'],
                ['', '期初余额', 'text'],
                ['', '借方发生额', 'text'],
                ['', '贷方发生额', 'text'],
                ['', '期末余额', 'text'],
            ])
            ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();
        

    }








}

