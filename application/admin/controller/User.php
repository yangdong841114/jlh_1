<?php
namespace app\admin\controller;
use app\common\builder\ZBuilder;
use app\index\controller\Api;
use think\Db;
use app\admin\model\Users;

class User extends Admin
{
    
    public function user_ReportCont(){
        do_alogs('查看实时报表');
        $time       = time();
        $date       = date('Y-m-d',$time);
        $map        = $this->getMap();
        $order      = $this->getOrder('id desc');
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        
        
        if (!isset($map['0'])) {
            $map['0'] = ['addDate','between time', [''.$date.' 00:00:00', ''.$date.' 23:59:59']];
        }
        
        // print_r($map);
        
        $data_list  = Db::name('UserReportCont')->where($map)->order($order)->limit(20)->paginate();

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
        {
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < NOW) unset($qrycache[$_k]);
            }
        }
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;
        
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('UserReportCont')
            ->addTimeFilter('addDate','','开始时间,结束时间') // 添加时间段筛选
            ->setSearch(['uid' => 'ID', 'c_nickname' => '昵称', 'c_account'=>'交易账号','c_taccount' => '推荐人帐号','c_tname' => '推荐人姓名'])
            ->addColumns([                  // 批量添加数据列
                ['id', 'ID'],
                ['uid', '会员ID'],
                ['c_account', '交易账号'],
                ['c_nickname', '昵称'],
                ['c_zbalance', '昨日余额'],
                ['c_balance', '今日余额'],
                ['c_taccount', '推荐人帐号'],
                ['c_tname', '推荐人姓名'],
                ['c_cc', '持仓'],
                ['c_mj_ls', '买进零售'],
                ['c_mj_jl', '买进特价'],
                ['c_mj_pf', '买进批发'],
                ['c_mc_ls', '卖出零售'],
                ['c_mc_pf', '卖出批发'],
                ['c_mc_jl', '卖出特价'],
                ['c_fee', '手续费'],
                ['c_rj', '今日入金'],
                ['c_cj', '今日出金'],
                ['addDate', '统计时间']
            ])
            ->hideCheckbox()
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('user_reportcont_export', ['uniqkey' => $uniqkey])])
            ->addOrder('c_mj_ls,c_mj_jl,c_mj_pf,c_mc_ls,c_mc_pf,c_mc_jl,c_fee,c_rj,c_cj,c_cc')
            ->setColumnWidth(['id'=>60,'c_account'=>160,'c_nickname'=>120,'c_taccount'=>160,'addDate'=>120])
            ->setPrimaryKey('id')                                                       //添加排序
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }

    public function user_Reportdetails(){
        do_alogs('查看实时报表');
        $map        = $this->getMap();
        $order      = $this->getOrder('id desc');
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        $time       = time();
        $date       = date('Y-m-d',$time);
        
        $data_list  = Db::name('UserReportDetails')->where($map)->order($order)->limit(0,20)->paginate();

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
        {
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < NOW) unset($qrycache[$_k]);
            }
        }
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;
        
        $mjls = 0;
        $mjpf = 0;
        $mjjl = 0;
        $mcls = 0;
        $mcpf = 0;
        $mcjl = 0;
        $sfls = 0;
        $sfpf = 0;
        $sfjl = 0;
        $pppf = 0;
        $ppjl = 0;
        $fee  = 0;
        $rj   = 0;
        $cj   = 0;
                
        
        foreach ($data_list as &$row) {
             $mjls = $mjls + $row['mj_ls'];
             $mjpf = $mjpf + $row['mj_pf'];
             $mjjl = $mjjl + $row['mj_jl'];
             $mcls = $mcls + $row['mc_ls'];
             $mcpf = $mcpf + $row['mc_pf'];
             $mcjl = $mcjl + $row['mc_jl'];
             $sfls = $sfls + $row['sf_ls'];
             $sfpf = $sfpf + $row['sf_pf'];
             $sfjl = $sfjl + $row['sf_jl'];
             $pppf = $pppf + $row['pp_pf'];
             $ppjl = $ppjl + $row['pp_jl'];
             $fee  = $fee  + $row['fee'];
             $rj   = $rj  + $row['rj'];
             $cj   = $cj  + $row['cj'];
        }
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('UserReportDetails')
            ->addTimeFilter('addDate','','开始时间,结束时间') // 添加时间段筛选
            ->addColumns([                  // 批量添加数据列
                ['addDate', '统计时间'],
                ['z_cc','持仓'],
                ['z_dfh','待发货'],
                ['z_yfh','已发货'],
                ['mj_ls', '买进零售'],
                ['mj_pf', '买进批发'],
                ['mj_jl', '买进特价'],
                ['mc_ls', '卖出零售'],
                ['mc_pf', '卖出批发'],
                ['mc_jl', '卖出奖励'],
                ['sf_ls', '释放零售'],
                ['sf_pf', '释放批发'],
                ['sf_jl', '释放特价'],
                ['pp_pf', '配送批发'],
                ['pp_jl', '配送奖励'],
                ['fee', '手续费'],
                ['rj', '入金'],
                ['cj', '出金']
            ])
            ->hideCheckbox()
            ->setColumnWidth(['addDate'=>120,'z_cc'=>120,'z_dfh'=>120,'z_yfh'=>120,'mj_ls'=>200,'mj_pf'=>200,'mj_jl'=>200,'mc_ls'=>200,'mc_pf'=>200,'mc_jl'=>200,'sf_ls'=>200,'sf_pf'=>200,'sf_jl'=>200,'pp_pf'=>200,'pp_jl'=>200,'fee'=>200,'rj'=>200,'cj'=>200])
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('user_reportdetails_export', ['uniqkey' => $uniqkey])])
            ->setPrimaryKey('id')                                                       //添加排序
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }

    public function user_report(){
        do_alogs('查看财务报表');
        $map        = $this->getMap();
        $order      = $this->getOrder('id asc');
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        $time               = time();
        $date       = date('Y-m-d',$time);

        $data_list  = Db::name('UserReport')->where($map)->order($order)->limit(20)->paginate();
        $sumCredit  = Db::name('UserReport')->where($map)->sum('r_credit');
        $sumEpoints  = Db::name('UserReport')->where($map)->sum('r_epoints');
        $sumIntegral  = Db::name('UserReport')->where($map)->sum('r_integral');
        $cj  = Db::name('UserReport')->where($map)->sum('r_cj');
        $rj  = Db::name('UserReport')->where($map)->sum('r_rj');
        $jrcj  = Db::name('UserReport')->where($map)->sum('r_jrcj');
        $jrrj  = Db::name('UserReport')->where($map)->sum('r_jrrj');
        $pageCredit  = 0;
        $epoints = 0;
        $pageCj = 0;
        $pageRj = 0;

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
        {
            $qrycache = $_SESSION['tblqry'];
            foreach($qrycache as $_k => $_v)
            {
                if($_v['expire'] < NOW) unset($qrycache[$_k]);
            }
        }
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;


        foreach ($data_list as &$row) {
            $pageCredit = $pageCredit + $row['r_credit'];
            $epoints = $epoints + $row['r_epoints'];
            $pageRj = $epoints + $row['r_rj'];
            $pageCj = $epoints + $row['r_cj'];
        }
        $sumEpoints = sprintf("%.2f" , $sumEpoints);
        $sumIntegral = sprintf("%.2f" , $sumIntegral);
        $epoints = sprintf("%.2f" , $epoints);
        $cj = sprintf("%.2f" , $cj);
        $rj = sprintf("%.2f" , $rj);
        $jrcj = sprintf("%.2f" , $jrcj);
        $jrrj = sprintf("%.2f" , $jrrj);
        //$sumCredit = $sumCredit >= 52648? $sumCredit- 10000 : $sumCredit;
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('UserReport')
            ->setSearch(['r_uid' => 'ID', 'r_username' => '昵称', 'r_account'=>'交易账号']) // 设置搜索参数
            ->addTimeFilter('addDate','','开始时间,结束时间') // 添加时间段筛选
            //->addTopButton('export1',['title' => '页面持仓'.$pageCredit, 'class' => 'btn btn-success confirm'])
            ->addTopButton('export2',['title' => '持仓汇总'.$sumCredit, 'class' => 'btn btn-success confirm'])
            //->addTopButton('export3',['title' => '页面余额'.$epoints, 'class' => 'btn btn-success confirm'])
            ->addTopButton('export4',['title' => '余额汇总'.$sumEpoints, 'class' => 'btn btn-success confirm'])
            //->addTopButton('export5',['title' => '页面入金'.$pageRj, 'class' => 'btn btn-success confirm'])
            ->addTopButton('export6',['title' => '入金汇总'.$rj, 'class' => 'btn btn-success confirm'])
            //->addTopButton('export7',['title' => '页面出金'.$pageCj, 'class' => 'btn btn-success confirm'])
            ->addTopButton('export8',['title' => '出金汇总'.$cj, 'class' => 'btn btn-success confirm'])
            ->addColumns([                  // 批量添加数据列
                ['r_uid', 'ID'],
                ['r_account', '交易商'],
                ['r_username', '姓名'],
                ['r_zepoints', '昨日余额'],
                ['r_zintegral', '昨日积分'],
                ['r_epoints', '今日余额('.$sumEpoints.')'],
                ['r_integral', '今日积分('.$sumIntegral.')'],
                ['r_rj', '全部入金('.$rj.')'],
                ['r_cj', '全部出金('.$cj.')'],
                ['r_jrrj', '今日入金('.$jrrj.')'],
                ['r_jrcj', '今日出金('.$jrcj.')'],
                ['r_credit', '持仓('.$sumCredit.')'],
                ['r_buy', '买单消费'],
                ['r_pay', '卖单收益'],
                ['r_fee', '手续费'],
                ['r_buy_credit_1', '零售票(买)'],
                ['r_buy_epoints_1', '零售消费(买)'],
                ['r_buy_credit_2', '批发票(买)'],
                ['r_buy_epoints_2', '批发消费(买)'],
                ['r_buy_credit_3', '奖励票(买)'],
                ['r_buy_epoints_3', '奖励消费(买)'],
                ['r_pay_credit_1', '零售票(卖)'],
                ['r_pay_epoints_1', '零售收益(卖)'],
                ['r_pay_credit_2', '批发票(卖)'],
                ['r_pay_epoints_2', '批发收益(卖)'],
                ['r_pay_credit_3', '奖励票(卖)'],
                ['r_pay_epoints_3', '奖励收益(卖)'],
                ['addDate', '统计时间']
            ])
            ->setColumnWidth(['id'=>60,'r_account'=>180,'r_credit'=>140,'r_epoints'=>200,'r_rj'=>200,'r_cj'=>200,'r_jrcj'=>200,'r_jrrj'=>200,'r_integral'=>200,'r_buy_credit_1'=>120,'r_buy_epoints_1'=>120,'r_buy_credit_2'=>120,'r_buy_epoints_2'=>120,'r_buy_credit_3'=>120,'r_buy_epoints_3'=>120,'r_pay_credit_1'=>120,'r_pay_epoints_1'=>120,'r_pay_credit_2'=>120,'r_pay_epoints_2'=>120,'r_pay_credit_3'=>120,'r_pay_epoints_3'=>120,''=>120])
            ->addOrder('r_buy_credit_1,r_buy_epoints_1,r_buy_credit_2,r_buy_epoints_2,r_buy_credit_3,r_buy_epoints_3,r_pay_credit_1,r_pay_epoints_1,r_pay_credit_2,r_pay_epoints_2,r_pay_credit_3,r_pay_epoints_3,r_zepoints,r_zintegral,r_epoints,r_integral,r_rj,r_cj,r_credit,r_buy,r_pay,r_fee')
            ->hideCheckbox()
            ->setPrimaryKey('id')                                                       //添加排序
            //->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('export', ['id' => '__id__'])])
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('user_report_export', ['uniqkey' => $uniqkey])])
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }

    //用户列表
    public function user_index(){
        do_alogs('查看用户列表');
        $map        = $this->getMap();
        $order      = $this->getOrder('m_reg_time desc');
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        $sql        = 1;
        if($admin_id>1 and $role==2){
            $sql    = "admin_id=$admin_id";
        }
        $data_list  = Users::where($map)->where($sql)->where($map)->where('m_del',0)->order($order)->limit(20)->paginate();
        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
		{
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < NOW) unset($qrycache[$_k]);
			}
		}
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where1' => $map, 'where2' => $sql, 'order' => $order, 'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;

        foreach ($data_list as &$row) {
            $row['m_user']          = $this->getUser($row['id']);
            $wallet                 = Db::name('Wallet')->where(array('uid'=>$row['id']))->where('m_identity <> 0')->find();
            if(empty($wallet)){
                $row['m_identity']  = 0;
            }else{
                $row['m_identity']  = $wallet['m_identity'];
            }
            $row['t_user']          = $row['m_tid']=0?'无':$this->getUser($row['m_tid']);
            $row['m_car_name']      = $this->getIdentity($row['id']);;
        }
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        // 授权按钮
        $btn_access = [
            'title' => '设置',
            'icon'  => 'fa fa-fw fa-key',
            'href'  => url('user_set', ['id' => '__id__'])
        ];
        $btn_charge  = [
            'title' => '充值',
            'icon'  => 'fa fa-fw fa-exchange',
            'href'  => url('user_charge', ['id' => '__id__'])
        ];
        $btn_login  = [
            'title' => '登陆',
            'icon'  => 'fa fa-fw fa-bookmark',
            'href'  => url('user_login', ['id' => '__id__'])
        ];
        $btn_tree  = [
            'title' => '图谱',
            'icon'  => 'fa fa-fw fa-tree',
            'href'  => url('user_trees', ['id' => '__id__'])
        ];
        $btn_is_pass  = [
            'title' => '审核开户',
            'icon'  => 'fa fa-fw fa-warning',
            'href'  => url('open_account', ['id' => '__id__'])
        ];
        $btn_client_sign_bind_account = [
            'title' => '审核签约',
            'icon'  => 'fa fa-fw fa-yahoo',
            'href'  => url('client_sign_bind_account', ['id' => '__id__'])
        ];
		// $btn_sell  = [
		// 	'title' => '发售',
		// 	'icon'  => 'fa fa-fw fa-sellsy',
		// 	'href'  => url('user_sell', ['id' => '__id__'])
		// ];
        $btn_rescind  = [
            'title' => '解约',
            'icon'  => 'fa fa-fw fa-resistance',
            'href'  => url('user_rescind', ['id' => '__id__'])
        ];
        $btn_identity  = [
            'title' => '指定账号',
            'icon'  => 'fa fa-fw fa-mouse-pointer',
            'href'  => url('user_identity', ['id' => '__id__'])
        ];
		// $btn_signing  = [
		// 	'title' => '签约修改',
		// 	'icon'  => 'fa fa-fw fa-mouse-pointer',
		// 	'href'  => url('m_bank_signing', ['id' => '__id__'])
		// ];
        $btn_ticket = [
            'title' => '充值票据',
            'icon' => 'fa fa-fw fa-sellsy',
            'href' => url('user_ticket', ['id' => '__id__'])
        ];
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('user')
            ->setSearch(['id' => 'ID', 'm_nickname' => '昵称', 'm_phone' => '手机','m_account'=>'交易账号', 'm_virtual_account' => '附属账号']) // 设置搜索参数
            ->addTimeFilter('last_time','','开始时间,结束时间') // 添加时间段筛选
            ->addTopSelect('m_level', '',get_level())
            ->addTopSelect('m_isFenGongSi', '分公司',get_levelFen())
            ->addTopSelect('m_bank_signing', '是否签约',[ 0 => '否', 1 => '是', 2=> '签约中', 7 => '待审核'])
            ->addTopSelect('m_is_pass', '开户状态',['等待审核','已通过','未通过'])
            ->addColumns([                  // 批量添加数据列
                ['id', 'ID'],
                ['m_user', '交易商', 'text'],
                ['m_sex', '性别','status','',['男','女']],
                ['m_avatar', '头像', 'img_url'],
                ['t_user','推荐人','text'],
                ['m_level', '级别','status','',get_level()],
                ['m_car_name', '实名信息','text'],
                ['m_car_img', '身份证照','img_urls'],
                ['m_real', '实名','switch'],
                ['m_balance','余额','text'],
                ['m_identity','用户身份','status','',['普通用户','产品回收商','奖励发放商','提货商']],
                ['m_virtual_account', '附属账号', 'text'],
                ['m_bank_signing', '是否签约','status','',[0 => '否', 1 => '是', 2 => '签约中', 3 => '签约中', 4 => '签约中', 7 =>'待审核', 8 => '未通过']],
                ['m_is_pass', '开户状态','status','',[0 => '等待审核', 1 => '已通过', 2 => '未通过']],
                ['m_isFenGongSi', '分公司','status','',['否','是']],
                ['m_integral','积分','text'],
                ['m_set_deal', '关闭交易','switch'],
                ['m_lock', '锁定','switch'],
                ['m_wot','消费积分','text'],
                ['m_tics_wot','累计积分','text'],
                //['m_del', '是否删除','status','',['否','是']],
                ['m_reg_time','注册时间','date'],
                ['last_time','上次登陆','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time'=>150,'id'=>60,'m_user'=>180,'t_user'=>180,'m_car_name'=>180])
            ->setExtraCss($css)
            //->hideCheckbox()
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,m_reg_time')                                                               //添加排序
            ->addTopButton('add',['href' => url('user_add')])
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('export', ['uniqkey' => $uniqkey/*'id' => '__id__'*/])])
            ->addRightButton('custom', $btn_access,['area' => ['500px', '500px']])
            ->addRightButton('custom', $btn_ticket, ['area' => ['400px', '750px']])
            ->addRightButton('custom', $btn_charge,['area' => ['500px', '500px']])
            ->addRightButton('custom', $btn_login,['area' => ['400px', '750px']])
			// ->addRightButton('custom', $btn_sell,['area' => ['400px', '750px']])
            ->addRightButton('rescind', $btn_rescind,['area' => ['400px', '750px']])
            ->addRightButton('identity', $btn_identity,['area' => ['400px', '750px']])
            ->addRightButton('m_is_pass', $btn_is_pass,['area' => ['400px', '330px']])
            ->addRightButton('client_sign_bind_account', $btn_client_sign_bind_account,['area' => ['400px', '330px']])
            // ->addRightButton('signing', $btn_signing,['area' => ['400px', '750px']])
            ->addRightButton('custom', $btn_tree) // 添加授权按钮
            ->addRightButtons(
                ['edit'  => ['title' => '编辑','href'=>url('user_edit', ['id'=>'__id__'])],
                 'delete'=> ['title' => '删除','href'=>url('user_del', ['id'=>'__id__'])],
                ])
            ->replaceRightButton(['m_bank_signing'=>['<>',1]],'','rescind')
            ->replaceRightButton(['m_bank_signing'=>['<>',7]],'','client_sign_bind_account')
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }

    public function get_wallet(){
        $admin_id       = session('user_auth')['uid'];
        $product_list   = Db::name('Product')->where(array('p_del' => 0, 'p_setup_status' => 1))->order('id asc')->select();
        $pro_data       = array();
        foreach ($product_list as $k => $v) {
            $pro_data[$v['id']] = $v['p_title'] . '-' . $v['p_code'];
        }
        if ($this->request->post()) {
            $fields = input('post.');
            $pid    = $fields['pid'];
            if($pid != 0){
                $this->success('跳转查询中',url('User/wallet_list',['pid'=>$pid]));
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('查看持仓信息')           // 设置页面标题
            ->addFormItems([                        // 批量添加表单项
                ['select', 'pid', '产品选择', '', $pro_data],
            ])
            ->fetch();
    }

    public function wallet_list(){
        do_alogs('查看持仓列表');
        $pid            = input('pid');
        $product        = getProduct($pid);
        $title          = $product['p_title'].'用户持仓列表';
        $map        = $this->getMap();
        $order      = $this->getOrder('m_reg_time desc');
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        $data_list  = Db::name('Wallet a')
            ->where(array('pid'=>$pid))
            ->join('w_user b', 'b.id=a.uid')
            ->where($map)
            ->order($order)
            ->limit(20)
            ->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        $status = ['普通用户', '产品回收商', '产品批发商', '产品提货商'];
        return ZBuilder::make('table')
            ->setTableName($title)
            ->setSearch(['a.id' => 'ID', 'b.m_nickname' => '昵称', 'b.m_account' => '账号', 'b.m_phone' => '手机号']) // 设置搜索参数
            ->addTopSelect('m_identity', '', $status)
            ->addColumns([                                                                                           // 批量添加数据列
                ['id', 'ID'],
                ['m_nickname', '昵称', 'text'],
                ['m_credit_1', '零售产品', 'text'],
                ['m_credit_2', '批发产品', 'text'],
                ['m_credit_3', '奖励产品', 'text'],
                ['m_identity', '身份', 'status', '', $status],
                ['m_credit_4', '奖励累计点', 'text'],
                //['m_credit_6', '二级分销奖励', 'text'],
                ['m_grant_num', '累计奖励次数', 'text'],
                ['add_time', '创建时间', 'datetime'],
                ['last_time', '更新时间', 'datetime'],
            ])
            ->setColumnWidth(['last_time' => 150,'add_time' => 150, 'id' => 60])
            ->setExtraCss($css)
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,add_time,add_time,m_credit_1,m_credit_2,m_credit_3,m_credit_4')                                                               //添加排序
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板



    }

    public function user_ticket($id = '')
    {
        $admin_id = session('user_auth')['uid'];
        $product_list = Db::name('Product')->where(array('p_del' => 0, 'p_setup_status' => 1))->order('id asc')->select();
        $pro_data = array();
        foreach ($product_list as $k => $v) {
            $pro_data[$v['id']] = $v['p_title'] . '-' . $v['p_code'];
        }
        if ($this->request->post()) {
            $fields = input('post.');
            $pid = $fields['pid'];
            if(empty($pid)){
                $this->error('请选择产品');
            }
            if(empty($fields['num'])|| $fields['num']===0){
                $this->error('充值数量不能为0');
            }
            add_wallet($id, $pid);
            if ($fields['type'] == 1) {
                $field = 'm_credit_1';
            } else if ($fields['type'] == 2){
                $field = 'm_credit_2';
            } else {
                $field = 'm_credit_3';
            }
            $res = Db::name('wallet')->where('uid', $id)->where('pid', $pid)->setInc($field, $fields['num']);
            do_wlogs($id,$pid,$fields['num'],1,$fields['type'],'后台充值');
            //            halt(Db::name('wallet')->getLastSql());
            if ($res){
                $this->success('充值成功');
            }
            $this->error('充值失败');
        }

        return ZBuilder::make('form')
            ->setPageTitle('交易设置')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['select', 'pid', '产品选择', '', $pro_data],
                ['radio', 'type', '充值类型', '', [1 => '零售产品', 2 => '批发产品', 3 => '奖励产品'], 1],
                ['number', 'num', '充值数量', '请输入充值数量'],
            ])
            ->fetch();
    }

    public function old_user_identity($id=''){
        $admin_id           = session('user_auth')['uid'];
        $config             = $this->set_config();
        $product_list       = Db::name('Product')->where(array('p_del'=>0,'p_setup_status'=>1))->order('id asc')->select();
        $pro_data           = array();
        foreach ($product_list as $k=>$v){
            $pro_data[$v['id']] = $v['p_title'].'-'.$v['p_code'];
        }
        $user               = Db::name('User')->where(array('id'=>$id))->find();
        $wallet             = Db::name('Wallet')->where(array('uid'=>$id))->where('m_identity <> 0')->find();
        $identity_type = [1=>'产品回收商',2=>'奖励发放商',3=>'提货商'];
        if($this->request->post()){
            $fields             = input('post.');
            if($user['m_bank_signing'] != 1){
                $this->error('当前账号未签约');
            }
            if(!empty($wallet)){
                $this->error('您已经是公司账号了');
            }
            if(!$fields['pid']){
                $this->error('请选择产品');
            }
            $pid             = $fields['pid'];
            add_wallet($id,$pid);


            $has_wallets     = Db::name('Wallet')->where(array('pid'=>$pid,'m_identity'=>$fields['credit_type']))->where("uid <> $id")->find();
            if(!empty($has_wallets)){
                $this->error('当前公司账号已设置');
            }
            $has_wallet      = Db::name('Wallet')->where(array('uid'=>$id,'pid'=>$pid))->where('m_identity <> 0')->find();
            if(!empty($has_wallet) && $has_wallet['m_identity'] != $fields['credit_type']){
                $this->error('您已经是公司账号了');
            }
            /*if($fields['credit_type'] == 1){
                if($fields['recharge_num'] <= 0){
                    $this->error('产品回收商必须充值余额');
                }
            }*/
            if($fields['product_num'] == 0){
                $this->error('产品分配数量不能为空');
            }
            if($fields['credit_type'] == 1){
                $set_wallet['buy_part_num']     = $fields['product_num'];
            }elseif($fields['credit_type'] == 2){
                $set_wallet['whole_part_num']   = $fields['product_num'];
            }elseif($fields['credit_type'] == 3){
                $set_wallet['carry_part_num']   = $fields['product_num'];
            }
            $set_wallet['m_identity']           = $fields['credit_type'];
            $res = Db::name('Wallet')->where(array('uid'=>$id,'pid'=>$pid))->update($set_wallet);
            if($res){
                if($fields['credit_type'] == 1){
                    do_logs($id,1,'m_balance',$fields['recharge_num'],'设置产品回收商,余额充值成功',$admin_id);
                }
                $this->success('设置成功',url('User/user_index'),'_parent_reload');
            }else{
                $this->error('设置失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('交易设置')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['radio', 'credit_type', '身份设置','',$identity_type,1],
                ['select','pid', '产品选择','',$pro_data],
                ['radio', 'recharge_pay','充值类型','',['余额','票据'],0],
                ['number', 'recharge_num', '充值金额','请输入充值金额'],
                ['number', 'product_num', '分配产品数量','请输入分配产品数量'],
            ])
            ->fetch();
    }

    public function user_identity($id=''){
        $admin_id           = session('user_auth')['uid'];
        $config             = $this->set_config();
        $product_list       = Db::name('Product')->where(array('p_del'=>0,'p_setup_status'=>1))->order('id asc')->select();
        $pro_data           = array();
        foreach ($product_list as $k=>$v){
            $pro_data[$v['id']] = $v['p_title'].'-'.$v['p_code'];
        }
        $user               = Db::name('User')->where(array('id'=>$id))->find();
        $wallet             = Db::name('Wallet')->where(array('uid'=>$id))->where('m_identity <> 0')->find();
        $identity_type = [1=>'产品回收商',2=>'奖励发放商',3=>'提货商'];
        if($this->request->post()){
            $fields             = input('post.');
            if($user['m_bank_signing'] != 1){
                $this->error('当前账号未签约');
            }
        //            if(!empty($wallet)){
        //                $this->error('您已经是公司账号了');
        //            }
            if(!$fields['pid']){
                $this->error('请选择产品');
            }
            $pid             = $fields['pid'];
            add_wallet($id,$pid);


            $has_wallets     = Db::name('Wallet')->where(array('pid'=>$pid,'m_identity'=>$fields['credit_type']))->where("uid <> $id")->find();
            if(!empty($has_wallets)){
                $this->error('当前公司账号已设置');
            }
            $has_wallet      = Db::name('Wallet')->where(array('uid'=>$id,'pid'=>$pid))->where('m_identity <> 0')->find();
            if(!empty($has_wallet) && $has_wallet['m_identity'] != $fields['credit_type']){
                $this->error('您已经是公司账号了');
            }
            /*if($fields['credit_type'] == 1){
                if($fields['recharge_num'] <= 0){
                    $this->error('产品回收商必须充值余额');
                }
            }*/
            if($fields['product_num'] == 0){
                $this->error('产品分配数量不能为空');
            }
            if($fields['credit_type'] == 1){
                $set_wallet['m_credit_1']           = $wallet['m_credit_1']+$fields['product_num'];
                $set_wallet['buy_part_num']         = $fields['product_num'];
            }elseif($fields['credit_type'] == 2){
                $set_wallet['m_credit_2']           = $wallet['m_credit_2']+$fields['product_num'];
                $set_wallet['whole_part_num']       = $fields['product_num'];
            }elseif($fields['credit_type'] == 3){
                $set_wallet['m_credit_1']           = $wallet['m_credit_1']+$fields['product_num'];
                $set_wallet['carry_part_num']        = $fields['product_num'];
            }

            $set_wallet['m_identity']           = $fields['credit_type'];
            $res = Db::name('Wallet')->where(array('uid'=>$id,'pid'=>$pid))->update($set_wallet);
            if($res){
                if($fields['credit_type'] == 1){
                    do_logs($id,1,'m_balance',$fields['recharge_num'],'设置产品回收商,余额充值成功',$admin_id);
                }
                $this->success('设置成功',url('User/user_index'),'_parent_reload');
            }else{
                $this->error('设置失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('交易设置')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['radio', 'credit_type', '身份设置','',$identity_type,1],
                ['select','pid', '产品选择','',$pro_data],
                ['radio', 'recharge_pay','充值类型','',['余额','票据'],0],
                ['number', 'recharge_num', '充值金额','请输入充值金额'],
                ['number', 'product_num', '分配产品数量','请输入分配产品数量'],
            ])
            ->fetch();
    }

    public function m_bank_signing($id=''){
        $admin_id           = session('user_auth')['uid'];
        $config             = $this->set_config();
        $user               = Db::name('User')->where(array('id'=>$id))->find();
        $wallet             = Db::name('Wallet')->where(array('uid'=>$id))->where('m_identity <> 0')->find();
        $m_bank_signing = [0=>'未签约',1=>'已签约'];
        if($this->request->post()){
            $param             = input('post.');
            $res = Db::name('user')->where('id',$id)->setField('m_bank_signing',$param['m_bank_signing']);
            if($res){
                $this->success('设置成功');
            }
            $this->error('设置失败');
        }
        return ZBuilder::make('form')
            ->setPageTitle('签约设置')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['radio', 'm_bank_signing', '签约设置','',$m_bank_signing,$user['m_bank_signing']],
            ])
            ->fetch();
    }

    public function user_rescind($id=''){
        $data       = Db::name('User')->where(array('id'=>$id))->find();
        $uid        = $data['id'];
        $Api        = new Api();
        $msg        = $Api->client_sign_bind_account(1,$uid);
        $msg_arr    = json_decode($msg,true);
        if($msg_arr['error_no'] == 0){
            $this->success($msg_arr['error_info'],url('User/user_index'));
        }else{
            $this->error($msg_arr['error_info']);
        }
    }

    public function user_sell($id=''){
        $admin_id           = session('user_auth')['uid'];
        $config             = $this->set_config();
        $time               = time();
        $date               = date('Y-m-d',$time);
        $product_list       = Db::name('Product')->where(array('p_del'=>0,'p_setup_status'=>1))->order('id asc')->select();
        $pro_data           = array();
        $credit             = array(1=>'零售',2=>'批发',3=>'奖励');
        foreach ($product_list as $k=>$v){
            $pro_data[$v['id']] = $v['p_title'].'-'.$v['p_code'];
        }
        if($this->request->post()){
            $fields             = input('post.');
            do_alogs('设置发售信息信息');
            if(!$fields['pid']){
                $this->error('请选择发售产品');
            }
            $product_data       = Db::name('Product')->where(array('id'=>$fields['pid']))->find();
            if(!$fields['credit_type']){
                $this->error('请选择发售类型');
            }
            if(!$fields['sell_num']){
                $this->error('发售数量有误');
            }
            if(!$fields['sid']){
                $this->error('请选择仓库');
            }
            $store_info     = Db::name('Store_product')->where(array('sid'=>$fields['sid'],'pid'=>$fields['pid']))->find(); //数据
            if(empty($store_info) || $store_info['sp_num']<=0){
                $this->error('当前仓库产品数量为0');
            }
            $store_num      = $store_info['sp_num']-$fields['sell_num'];
            if($store_info['sp_num']<$fields['sell_num'] || $store_num<0){
                $this->error('产品库存不足');
            }
            ####检测用户钱包是否存在####
            add_wallet($id,$fields['pid']);
            #####操作用户余额#####
            if($fields['set_pay'] == 1){                                        //扣除用户对应的余额,增加用户的票据
                if($fields['credit_type'] == 1){
                    $product_price      =   $product_data['p_retail_price'];
                }else{
                    $product_price      =   $product_data['p_whole_price'];
                }
                $price                  =   $fields['sell_num']*$product_price;
                $res_balance            =   set_balance($id,$price);
                if(is_array($res_balance)){
                    $this->error($res_balance['msg']);
                }
            }
            $res = set_wallet($id,$fields['pid'],$fields['sell_num'],$fields['credit_type']);
            if(!$res){
                $this->error('发售失败,产品数量操作有误');
            }
            #####操作用户余额#####

            ####写入主表####
            $deal_data = array(
                'uid'            => $id,
                'pid'            => $fields['pid'],
                'sid'            => $fields['pid'],
                'd_code'         => randomkeys(8),
                'd_type'         => 2,
                'd_total'        => $fields['sell_num'],
                'd_num'          => $fields['sell_num'],
                'd_sell_num'     => 0,
                'd_addtime'      => $time,
                'd_status'       => 4,
                'd_admin_status' => $admin_id,
                'last_time'      => $time,
                'd_date'         => $date,
                'd_grant'        => -1,
            );
            if($fields['credit_type'] == 1){
                $deal_data['d_price']       = $product_data['p_retail_price'];
                $deal_data['d_credit_1']    = $fields['sell_num'];
                $fine_data['f_type']        = 1;
                $fine_data['f_price']       = $product_data['p_retail_price'];
            }elseif($fields['credit_type'] == 2){
                $deal_data['d_price'] = $product_data['p_whole_price'];
                $deal_data['d_credit_2']    = $fields['sell_num'];
                $fine_data['f_type']        = 2;
                $fine_data['f_price']       = $product_data['p_whole_price'];
            }elseif($fields['credit_type'] == 3){
                $deal_data['d_price']       = $product_data['p_whole_price'];
                $deal_data['d_credit_3']    = $fields['sell_num'];
                $fine_data['f_type']        = 3;
                $fine_data['f_price']       = $product_data['p_whole_price'];
            }
            $deal_id = Db::name('Deal')->insertGetId($deal_data);
            if(!$deal_id){
                $this->error('订单创建失败');
            }
            ####写入主表####

            ####操作仓库#####
            Db::name('Store_product')->where(array('id'=>$store_info['id']))->update(array('sp_num'=>$store_num,'last_time'=>$time));
            do_slogs(0,$fields['sid'],$fields['pid'],$store_info['id'],2,$fields['sell_num'],'发售产品,商品出库',$admin_id);
            ####操作仓库#####

            ####写入子表####
            $fine_data['bid']                       = 0;
            $fine_data['sid']                       = $id;
            $fine_data['did']                       = $deal_id;
            $fine_data['pid']                       = $fields['pid'];
            $fine_data['f_status']                  = 0;
            $fine_data['f_profit_start_time']       = strtotime(-$product_data['p_income_days'].' day');
            $fine_data['f_profit_end_time']         = $time;
            $fine_data['f_addtime']                 = $time;
            $fine_data['f_date']                    = $date;
            $fine_data['f_end']                     = 0;
            $fine_data['f_set_strike']              = -1;
            for($i=1;$i<=$fields['sell_num'];$i++){
                $fine_data['f_code']     = randomkeys(8);
                $res_fine                = Db::name('Fine')->insertGetId($fine_data);
            }
            ####写入子表####
            if($res_fine){
                $this->success('设置成功',url('User/user_index'),'_parent_reload');
            }else{
                $this->error('设置失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('交易设置')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden','id'],
                ['radio', 'set_pay','同步操作用户钱包','',['否','是'],0],
                ['radio', 'credit_type', '发售类型','',$credit,1],
                ['linkage', 'pid', '产品','请选择产品',$pro_data,'',url('get_store_info'),'sid'],
                ['select', 'sid', '仓库',''],
                ['number', 'sell_num', '发售数量','请输入发售数量'],
            ])
            ->fetch();
    }

    public function get_store_info($pid=''){
        $arr['code']    = '1';               //判断状态
        $arr['msg']     = '请求成功';         //回传信息
        $store_info     = Db::name('Store a')->join('w_store_product b','a.id=b.sid','left')->where(array('b.pid'=>$pid))->field('a.*,b.pid')->select(); //数据
        $store          = array();
        foreach ($store_info as $k=>$v){
            $store[]    = ['key'=>$v['id'],'value'=>$v['s_title']];
        }
        $arr['list']    = $store;
        return json($arr);
    }

    //添加用户
    public function user_add(){
        // 使用ZBuilder快速创建表单
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        $bank           = $this->bankCode();
        //$open_code      = $this->bankOpenCode();
        $area_info      = Db::name('Area')->where(array('pid'=>0))->order('sid asc')->select();
        $area           = array();
        foreach ($area_info as $k1=>$v1){
            $area[$v1['id']] = $v1['name'];
        }
        $open_bank_info      = Db::name('Bank')->where(array('is_del'=>0))->order('sid asc')->select();
        $open_bank           = array();
        foreach ($open_bank_info as $k2=>$v2){
            $open_bank[$v2['id']] = $v2['name'];
        }
        if($this->request->post()){
            do_alogs('添加用户信息');
            $fields         =   input('post.');
            $m_line         =   '0';
            if($fields['m_tid']){
                $has_push       =   Db::name('User')->where("(m_account =".$fields['m_tid']." or id=".$fields['m_tid']." or m_phone=".$fields['m_tid'].") and m_del=0")->find();
                if(empty($has_push)){
                    $this->error('推荐交易商不存在');
                }
                if($has_push['m_lock']==1){
                    $this->error('推荐交易商账户已被锁定');
                }
                $fields['m_tid'] = $has_push['id'];
                $m_line          = $has_push['m_line'];
            }
            if(!$fields['m_phone']){
                $this->error('手机号不能为空');
            }
            $has_phone  =   Db::name('User')->where(array('m_phone'=>$fields['m_phone'],'m_del'=>0))->find();
            if(!empty($has_phone)){
                $this->error('当前手机号已注册');
            }
            if(!$fields['m_login_pwd']){
                $this->error('请输入登陆密码');
            }
            if(!$fields['m_name']){
                $this->error('请输入真实姓名');
            }
            if(!$fields['m_car_id']){
                $this->error('身份证号码不能为空');
            }
            $has_card   = Db::name('User')->where(array('m_car_id'=>$fields['m_car_id'],'m_del'=>0))->find();
            if(!empty($has_card)){
                $this->error('身份证号码已被注册');
            }
            if($fields['m_bank_code']){
                $fields['m_bank_name'] = $bank[$fields['m_bank_code']];
            }
            if($fields['m_bank_carid']){
                $has_bank   = Db::name('User')->where(array('m_bank_carid'=>$fields['m_bank_carid'],'m_del'=>0))->find();
                if(!empty($has_bank)){
                    $this->error('银行卡号已绑定签约');
                }
            }
            if($fields['m_bank_open']){
                $fields['m_bank_open'] = $open_bank[$fields['m_bank_open']];
            }
            if(!$fields['m_car_img']){
                $this->error('请上传身份证信息');
            }
            $fields['m_car_img'] = get_files_path($fields['m_car_img'],'.');
            if(!$fields['m_nickname']){
                $fields['m_nickname'] = $config['w_name'].'-'.generate_code(8);
            }
            if($fields['m_avatar']){
                $fields['m_avatar'] = get_file_path($fields['m_avatar']);
            }else{
                $fields['m_avatar'] = $config['w_logo'];
            }
            if($fields['m_account']){
                $has_account   = Db::name('User')->where(array('m_account'=>$fields['m_account'],'m_del'=>0))->find();
                if(!empty($has_account)){
                    $this->error('交易账号已存在');
                }
            }else{
                $fields['m_account'] = $config['w_account_start'].$fields['m_phone'];
            }
            $fields['m_login_pwd']   = md5($fields['m_login_pwd']);
            $fields['m_pay_pwd']     = md5($fields['m_pay_pwd']);
            $fields['m_reg_status']  = 1;
            $fields['m_reg_time']    = time();
            if($admin_id>1 and $role==2){
                $fields['admin_id'] = $admin_id;
            }
            unset($fields['__token__']);
            $member_id               = Db::name('User')->insertGetId($fields);
            if($fields['m_tid']){
                $t_user         = getUserInfo($fields['m_tid']);
                $m_push_num     = 1;
                if(!empty($t_user)){
                    $m_push_num = $t_user['m_push_num']+1;
                }
                Db::name('User')->where(array('id'=>$fields['m_tid']))->update(array('m_push_num'=>$m_push_num));
            }
            if ($member_id){               //更新会员节点序列及所在层次
                Db::name('User')->where(array('id'=>$member_id))->update(array('m_line' => $m_line.',' . $member_id, 'm_layer' =>count(explode(',', $m_line))));
                $this->success('添加成功',url('User/user_index'));
            } else {
                $this->error('发生未知错误，请稍后重试');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('新增')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['number', 'm_tid', '推荐人','(推荐人账号或ID或手机号)'],
                ['number', 'm_phone', '手机号','(交易商手机号)'],
                ['text', 'm_nickname', '交易商昵称','(选填)'],
                ['number', 'm_account', '交易账号','(选填,系统可自动生成)'],
                ['text', 'm_cname', '交易商全称','(选填)'],
                ['select', 'm_level','等级','请选择用户等级',get_level()],
                ['password', 'm_login_pwd', '登陆密码'],
                ['password', 'm_pay_pwd', '交易密码'],
                ['image', 'm_avatar', '头像','(选填)'],
                ['text', 'm_name', '真实姓名'],
                ['text', 'm_car_id', '身份证号'],
                ['images', 'm_car_img', '身份证正反面'],
                ['radio', 'm_real','是否实名','',['否','是'],0],
                ['radio', 'm_sex','性别','',['男','女'],0],
                ['select', 'm_bank_code', '银行名称','',$bank],  //$open_code
                ['number', 'm_bank_carid', '银行卡号','(选填)'],
                //['radio', 'm_bank_signing', '签约状态','(选填,非异常状态不建议修改)',['否','是','签约中','签约中','签约中']],
                //['number', 'm_virtual_account', '附属账号','(选填,非异常状态不建议修改)'],
                ['linkage', 'm_bank_open', '开户所属银行','',$open_bank,'',url('get_open_code'),'m_bank_open_code'],
                ['linkage', 'm_area', '开户行省份','',$area,'',url('get_city'),'m_city,m_bank_open_code'],
                ['linkage', 'm_city', '开户行城市','','', '', url('get_open_code'), 'm_bank_open_code'],
                ['select', 'm_bank_open_code', '开户行'],
                ['text', 'm_address', '联系地址','(选填)'],
                ['textarea:6', 'm_info', '简介'],
            ])
            ->layout(['m_tid' => 2, 'm_phone' => 2, 'm_nickname' => 3, 'm_account' => 2, 'm_cname' => 3,
                'm_login_pwd' => 3, 'm_pay_pwd' => 3, 'm_level' => 3, 'm_name' => 2, 'm_car_id' => 3,
                'm_bank_code' => 2,'m_bank_open_code' => 4, 'm_area' => 2,'m_city' => 2,'m_bank_carid' => 3, 'm_bank_open' => 2, 'm_address' => 4, 'm_info' => 4
            ])
            ->fetch();
    }

    //编辑用户
    public function user_edit($id=''){
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        $bank           = $this->bankCode();
        //$open_code      = $this->bankOpenCode();
        $area_info      = Db::name('Area')->where(array('pid'=>0))->order('sid asc')->select();
        $area           = array();
        foreach ($area_info as $k1=>$v1){
            $area[$v1['id']] = $v1['name'];
        }
        $open_bank_info      = Db::name('Bank')->where(array('is_del'=>0))->order('sid asc')->select();
        $open_bank           = array();
        foreach ($open_bank_info as $k2=>$v2){
            $open_bank[$v2['id']] = $v2['name'];
        }
        $data       = Db::name('User')->where(array('id'=>$id))->find();
    
        if($data['m_avatar']){
            $m_avatar   = explode('.',$data['m_avatar']);
            if(count($m_avatar)>2){
                $data['m_avatar'] = $m_avatar[1].'.'.$m_avatar[2];
            }
        }else{
            $data['m_avatar']   = $config['w_logo'];
        }
        if($this->request->post()){
            do_alogs('编辑用户信息');
            $fields         =   input('post.');
            $m_line         =   '0';
            if(!empty($fields['m_tid'])){
                $has_push       =   Db::name('User')->where("(m_account =".$fields['m_tid']." or id=".$fields['m_tid']." or m_phone=".$fields['m_tid'].") and m_del=0 and id<>$id")->find();
                if(empty($has_push)){
                    $this->error('推荐交易商不存在');
                }
                if($has_push['m_lock']==1){
                    $this->error('推荐交易商账户已被锁定');
                }
                $m_tid = $has_push['id'];
                $m_line          = $has_push['m_line'];
                unset($fields['m_tid']);
            }
            if(!$fields['m_phone']){
                $this->error('手机号不能为空');
            }
            $has_phone           =   Db::name('User')->where(array('m_phone'=>$fields['m_phone'],'m_del'=>0))->where("id<>$id")->find();
            if(!empty($has_phone)){
                $this->error('当前手机号已注册');
            }
            if($fields['m_login_pwd']){
                if($fields['m_login_pwd'] != $data['m_login_pwd']){
                    $fields['m_login_pwd'] = md5($fields['m_login_pwd']);
                }
            }
            if($fields['m_pay_pwd']){
                if($fields['m_pay_pwd'] != $data['m_pay_pwd']){
                    $fields['m_pay_pwd']   = md5($fields['m_pay_pwd']);
                }
            }
            if(!$fields['m_name']){
                $this->error('请输入真实姓名');
            }
            if(!$fields['m_car_id']){
                $this->error('身份证号码不能为空');
            }
            $has_card   = Db::name('User')->where(array('m_car_id'=>$fields['m_car_id'],'m_del'=>0))->where("id<>$id")->find();
            if(!empty($has_card)){
                $this->error('身份证号码已被注册');
            }
            if($fields['m_bank_code']){
                $fields['m_bank_name'] = $bank[$fields['m_bank_code']];
            }
            if($fields['m_bank_carid']){
                $has_bank   = Db::name('User')->where(array('m_bank_carid'=>$fields['m_bank_carid'],'m_del'=>0))->where("id<>$id")->find();
                if(!empty($has_bank)){
                    $this->error('银行卡号已绑定签约');
                }
            }
            if($fields['m_bank_open']){
                $fields['m_bank_open'] = $open_bank[$fields['m_bank_open']];
            }
            if(!$fields['m_car_img']){
                $this->error('请上传身份证信息');
            }
            $fields['m_car_img'] =  setImagePath($fields['m_car_img']);
            if(!$fields['m_nickname']){
                $fields['m_nickname'] = $config['w_name'].'-'.generate_code(8);
            }
            if($fields['m_avatar']){
                $fields['m_avatar'] = setImagePath($fields['m_avatar']);
            }else{
                if($fields['m_avatar']==''){
                    $fields['m_avatar'] = $config['w_logo'];
                }
            }
            if($fields['m_account']){
                $has_account   = Db::name('User')->where(array('m_account'=>$fields['m_account'],'m_del'=>0))->where("id<>$id")->find();
                if(!empty($has_account)){
                    $this->error('交易账号已存在');
                }
            }else{
                $fields['m_account'] = $config['w_account_start'].$fields['m_phone'];
            }
            $fields['last_time']     = time();
            // if($data['m_bank_signing']==3 || $data['m_bank_signing']==1){
            //     unset($fields['m_bank_open_code']);
            //     unset($fields['m_bank_open']);
            // }else{
            //     if($data['m_bank_open_code'] and $data['m_bank_open']){
            //         if(!$fields['m_bank_open_code'] && !$fields['m_bank_open']){
            //             unset($fields['m_bank_open_code']);
            //             unset($fields['m_bank_open']);
            //         }
            //     }
            // }

            // if($fields['m_bank_signing'] != $data['m_bank_signing']){
            //     file_put_contents('666888.txt', date('Y-m-d H:i:s').PHP_EOL, FILE_APPEND);
            //     file_put_contents('666888.txt', var_export($fields).PHP_EOL, FILE_APPEND);
            // }

            unset($fields['__token__']);
            $member_id               = Db::name('User')->where(array('id'=>$id))->update($fields);
            if(!empty($m_tid)){
                // 只有admin 才能修改推荐人
                if($admin_id === 1){
                    // 新的推荐人信息
                    $tid_user = Db::name('user')->where('id', $m_tid)->find();
                    // 修改当前用户的 m_tid 和 m_line
                    $user_new_data = [
                        'm_tid' => $m_tid,
                        'm_line' => $tid_user['m_line'] . ',' . $id,
                    ];
                    Db::name('user')->where(['id' => $id])->update($user_new_data);

                    // 修改当前用户的m_line团队
                    $user_arr = Db::name('user')->where('m_line', 'like', '%,'.$id.',%')->field('id, m_line')->select();
                    // dump($user_arr);
                    if(!empty($user_arr)){
                        foreach($user_arr as $k => $v){
                            // 截取字符串，拼接新的 m_line
                            $u_new_info = [
                                'm_line' => $tid_user['m_line'].strstr($v['m_line'], ','.$id.','),
                            ];
                            Db::name('user')->where('id', $v['id'])->update($u_new_info);
                        }
                    }
                    // die;
                }
                $has_t_user          = Db::name('User')->where(array('id'=>$m_tid,'m_del'=>0))->select();
                $m_push_num          = count($has_t_user);
                Db::name('User')->where(array('id'=>$m_tid))->update(array('m_push_num'=>$m_push_num));
            }

            // 判断用户银行卡信息是否变更，如果变更，提交到连交运修改
            if(!empty($fields['m_bank_open']) !== '' && !empty($fields['m_bank_open_code'])){
                $_bank_data = [
                    'open_bank_no' => $fields['m_bank_open_code'],
                    'bank_account' => $fields['m_bank_carid']
                ];
                $Api            = new Api();
                $res            = $Api->update_main_bank_info($_bank_data, $id);
                $res            = json_decode($res, true);
                if($res['error_no'] != 0){
                    $this->error($res['error_info']);
                }
            }else{
            	unset($fields['m_bank_open_code']);
                unset($fields['m_bank_open']);
            }

            if ($member_id){               //更新会员节点序列及所在层次
                Db::name('User')->where(array('id'=>$id))->update(array('m_line' => $m_line.',' . $id, 'm_layer' =>count(explode(',', $m_line))));
                if($fields['set_open_user'] == 1){
                    $Api            = new Api();
                    $res            = $Api->sync_mem_user_modify($id);
                    $res            = json_decode($res,true);
                    if($res['error_no'] == 0){
                        $this->success($res['error_info'],url('User/user_index'));
                    }else{
                        $this->error($res['error_info']);
                    }
                }else{
                    $this->success('编辑成功',url('User/user_index'));
                }
            } else {
                $this->error('发生未知错误，请稍后重试');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('编辑')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden','id'],
                ($admin_id===1)?['number', 'm_tid', '推荐人','(推荐人账号或ID或手机号)']:[],
                // ['number', 'm_tid', '推荐人','(推荐人账号或ID或手机号)'],
                ['number', 'm_phone', '手机号','(交易商手机号)'],
                ['text', 'm_nickname', '交易商昵称','(选填)'],
                ['number', 'm_account', '交易账号','(选填,系统可自动生成)'],
                ['text', 'm_cname', '交易商全称','(选填)'],
                ['select', 'm_level','等级','请选择用户等级',get_level()],
                ['select', 'm_isFenGongSi','分公司','请选择分公司等级',get_levelFen()],
                ['password', 'm_login_pwd', '登陆密码','0','0'],
                ['password', 'm_pay_pwd', '交易密码','0','0'],
                ['image', 'm_avatar', '头像','(选填)'],
                ['text', 'm_name', '真实姓名'],
                ['text', 'm_car_id', '身份证号'],
                ['images', 'm_car_img', '身份证正反面'],
                ['radio', 'm_sex','性别','',['男','女'],0],
                ['radio', 'm_real','是否实名','',['否','是'],0],
                ['select', 'm_bank_code', '银行名称','',$bank],
                ['number', 'm_bank_carid', '银行卡号','(选填)'],
                ['radio', 'm_bank_signing', '签约状态','(选填,非异常状态不建议修改)',['否','是','签约中','签约中','签约中']],
                ['number', 'm_virtual_account', '附属账号','(选填,非异常状态不建议修改)'],
                ['linkage', 'm_bank_open', '开户所属银行','',$open_bank,'',url('get_open_code'),'m_bank_open_code'],
                ['linkage', 'm_area', '开户行省份','',$area,'',url('get_city'),'m_city,m_bank_open_code'],
                ['linkage', 'm_city', '开户行城市','','', '', url('get_open_code'), 'm_bank_open_code'],
                ['select', 'm_bank_open_code', '开户行'],                                        //$open_code
                ['text', 'm_address', '联系地址','(选填)'],
                ['textarea:6', 'm_info', '简介'],
                ['radio', 'set_open_user', '是否同步修改用户开户信息','',[0=>'否',1=>'是'],0],
            ])
            ->setFormData($data)
            ->layout(['m_tid' => 2, 'm_phone' => 2, 'm_nickname' => 3, 'm_account' => 2, 'm_cname' => 3,
                'm_login_pwd' => 3, 'm_pay_pwd' => 3, 'm_level' => 2, 'm_isFenGongSi' => 2, 'm_name' => 2, 'm_car_id' => 3,
                'm_bank_code' => 2,'m_virtual_account' => 3,'m_bank_open_code' => 4, 'm_area' => 2,'m_city' => 2,'m_bank_carid' => 3, 'm_bank_open' => 2, 'm_address' => 4, 'm_info' => 4
            ])
            ->fetch();
    }

    public function get_city($m_area=''){
        $arr['code']    = '1';           //判断状态
        $arr['msg']     = '请求成功';    //回传信息
        $city_info      = Db::name('Area')->where(array('pid'=>$m_area))->order('sid asc')->select(); //数据
        $city           = array();
        foreach ($city_info as $k=>$v){
            $city[]     = ['key'=>$v['id'],'value'=>$v['name']];
        }
        $arr['list']    = $city;
        return json($arr);

    }

    public function get_open_code($m_bank_open='',$m_city=''){
        session('bank_open',$m_bank_open);
        session('city',$m_city);
        $bank_open = session('bank_open');
        $city      = session('city');
        if(!$bank_open){
            return json(array('code'=>0,'msg'=>'请选择开户所属银行'));
        }
        if(!$city){
            return json(array('code'=>0,'msg'=>'请选择开户行城市'));
        }
        $bank_open_data  =  Db::name('Bank_database')->where(array('bank_id'=>$bank_open,'area_id'=>$city))->order('id asc')->select();
        $bank            =  array();
        foreach ($bank_open_data as $k1=>$v1){
            $bank[]     = ['key'=>$v1['bank_open_code'],'value'=>$v1['bank_name']];
        }
        return json(array('code'=>1,'msg'=>'请求成功','list'=>$bank));

    }

    //删除用户
    public function user_del($id=''){
        $user = getUserInfo($id);
        if(!empty($user)){
            $res =Db::name('User')->where('id',$id)->update(array('m_del'=>1));
            if($res == 1){
                do_alogs('删除用户信息');
                $this->success('删除成功');
            }else{
                $this->error('删除失败');
            }
        }
    }

    //用户数据导出
    public function export(){
        // http://127.0.0.1:8211/admin.php/admin/user/export/uniqkey/5f16930a68068.html

        $uniqkey = !empty(input('uniqkey'))?input('uniqkey'):'';

    	if(isset($uniqkey) && isset($_SESSION['tblqry']))
		{
			$tblqry = $_SESSION['tblqry'];
			if(isset($tblqry[$uniqkey]))
			{
				$qrycache = $tblqry[$uniqkey];
				if(isset($qrycache['expire']) && $qrycache['expire'] > time())
				{
                    $data_list = Users::where($qrycache['where1'])->where($qrycache['where2'])->where('m_del', 0)->order($qrycache['order'])->select();

                    // 表格标题
                    $tileArray = [							
                        '交易商ID',
                        '交易商昵称',
                        '交易商账号',
                        '交易商手机号',
                        '交易商性别', 
                        '推荐人ID', 
                        '推荐人昵称', 
                        '推荐人账号', 
                        '推荐人手机号', 
                        '级别', 
                        '真实姓名', 
                        '身份证号', 
                        '实名', 
                        '余额', 
                        '用户身份', 
                        '是否签约', 
                        '分公司', 
                        '积分',
                        // '关闭交易',
                        // '锁定',
                        '消费积分',
                        '累计积分',
                        '注册时间',
                        '上次登陆',
                    ];
                    // 表格内容
                    $dataArray = [];

                    $user_level_name = '';
                    $user_identity = [ '普通用户','产品回收商','奖励发放商','提货商' ];
                    $is_bank_signing = ['否','是','签约中','签约中','签约中'];

                    foreach($data_list as $k => $v){

                        $wallet = Db::name('Wallet')->where(array('uid'=>$v['id']))->where('m_identity <> 0')->find();
                        if(empty($wallet)){
                            $m_identity  = 0;
                        }else{
                            $m_identity  = $wallet['m_identity'];
                        }
                        $pid_user = Db::name('user')->where('id', $v['m_tid'])->field('id, m_nickname, m_account, m_phone')->find();
                        // $row['t_user'] = str_replace('<br/>', PHP_EOL, $row['m_tid']=0?'无':$this->getUser($row['m_tid']));

                        $dataArray[] = [
                            // '注册时间' => $this->filterValue(date("H:i:s", $v['create_time'])),
                            '交易商ID' => $v['id'],
                            '交易商昵称' => $v['m_nickname'],
                            '交易商账号' => filter_value($v['m_account']),
                            '交易商手机号' => filter_value($v['m_phone']),
                            '交易商性别' => ($v['m_sex'] == 0) ? '男' : '女',
                            // '推荐人' => $v['t_user'],
                            '推荐人ID' => !empty($pid_user)?$pid_user['id']:'无', 
                            '推荐人昵称' => !empty($pid_user)?filter_value($pid_user['m_nickname']):'无', 
                            '推荐人账号' => !empty($pid_user)?filter_value($pid_user['m_account']):'无',
                            '推荐人手机号' => !empty($pid_user)?filter_value($pid_user['m_phone']):'无',
                            '级别' => get_level()[$v['m_level']],
                            '真实姓名' => $v['m_name'],
                            '身份证号' => filter_value($v['m_car_id']),
                            '实名' => ($v['m_real'] == 1) ? '已实名' : '未实名',
                            '余额' => $v['m_balance'],
                            '用户身份' => $user_identity[$m_identity],
                            '是否签约' => $is_bank_signing[$m_identity],
                            '分公司' => get_levelFen()[$v['m_isFenGongSi']],
                            '积分' => $v['m_integral'],
                            // '关闭交易' => ($v['m_set_deal'] == 0) ? '关闭' : '开启',
                            // '锁定' => ($v['m_lock'] == 0) ? '锁定' : '未锁定',
                            '消费积分' => $v['m_wot'],
                            '累计积分' => $v['m_tics_wot'],
                            '注册时间' => date('Y-m-d H:i:s', $v['m_reg_time']),
                            '上次登陆' => date('Y-m-d H:i:s', $v['last_time']),
                        ];
                    }
                    
                    // 导出csv文件
                    $filename = 'user-' . date('YmdHis');
                    return export_csv($filename . '.csv', $tileArray, $dataArray);
				}
			}
        }
    	
    }

    public function user_login($id=''){
        $users              = getUserinfo($id);  //前台用户信息
        session('uid',$users['id']);
        $url_login = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/index.php/index/index/index';
        header('Location: ' . $url_login);
        exit;
    }

    //图谱
    public function user_trees($id=''){
        $config         = $this->set_config();
        $set_data[0]    = getDowns($id,0);
        $xxx            = json_encode($set_data);
        //print_r($xxx);die;
        $this->assign(array('xxx'=>$xxx,'config'=>$config));
        return $this->fetch();
    }



    //设置用户参数
    public function user_set($id=''){
        $config         = $this->set_config();
        $data           = Db::name('User')->where(array('id'=>$id))->find();
        if($this->request->post()){
            $fields     = input('post.');
            do_alogs('设置用户交易信息');
            if($fields['m_set_pay_num'] == ''){
                $fields['m_set_pay_num'] = 0;
            }
            if($fields['m_order_limit'] == ''){
                $fields['m_order_limit'] = 0;
            }
            if($fields['m_order_sell_limit'] == ''){
                $fields['m_order_sell_limit'] = 0;
            }
            if($fields['m_set_fee'] == ''){
                $fields['m_set_fee'] = 0;
            }
            $res =Db::name('User')->where('id',$id)->update($fields);
            if($res == 1){
                $this->success('设置成功',url('User/user_index'),'_parent_reload');
            }else{
                $this->error('设置失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('交易设置')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden','id'],
                ['radio', 'm_set_pay','设置出金','',['开','关'],0],
                ['number', 'm_set_pay_num', '设置出金金额'],
                ['number', 'm_set_fee', '设置手续费','(负数少收/0不设置/正数多收,填写百分比)'],
                ['radio', 'm_set_deal', '买入交易开关','',['开','关'],0],
                ['number', 'm_order_limit', '购买量限制','(0为不受每日 填写数字限制订货量 )'],
                ['radio', 'm_set_sell_deal', '卖出交易开关','',['开','关'],0],
                ['number', 'm_order_sell_limit', '卖出量限制','(0为不受每日 填写数字限制卖出量 )'],
                ['select', 'm_type', '交易商类型','请选择交易商类型',[0=>'普通交易商',1=>'经纪商',2=>'挂牌商'],0],
            ])
            ->setFormData($data)
            ->layout(['m_set_pay' => 5, 'm_set_pay_num' => 4, 'm_set_deal' => 5,'m_set_sell_deal' => 5, 'm_set_fee' => 4, 'm_order_limit' => 5, 'm_order_sell_limit' => 5, 'm_type' => 6])
            ->fetch();

    }

    //修改用户
    public function user_fake_del(){

    }

    public function user_charge($id=''){
        $config         = $this->set_config();
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $credit_name    = array('1'=>'余额','2'=>'积分','3'=>'消费积分','4'=>'累计积分');
        $credit         = array('1'=>'m_balance','2'=>'m_integral','3'=>'m_wot','4'=>'m_tics_wot');
        $data           = Db::name('User')->where(array('id'=>$id))->find();
        if($this->request->post()){
            do_alogs('操作用户充值');
            $fields     = input('post.');
            $uid        = $data['id'];
            $type       = $fields['m_set_pay'];
            $cre        = $credit[$type];
            $num        = $fields['m_num'];
            $info       = $fields['m_info'];
            $res        = do_logs($uid,$type,$cre,$num,$info,$admin_id);
            if($res){
                $this->success('充值成功',url('User/user_index'),'_parent_reload');
            }else{
                $this->error('充值失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('充值设置')           // 设置页面标题
            ->addFormItems([                    // 批量添加表单项
                ['radio', 'm_set_pay','充值类型','',$credit_name,1],
                ['number', 'm_num', '充值金额','填写负数即为减扣'],
                ['textarea:6', 'm_info', '备注'],
            ])
            ->fetch();
    }

    public function login_log(){
        do_alogs('查看用户登陆记录');
        $map        = $this->getMap();
        $order      = $this->getOrder('login_time desc');
        $data_list  = Db::name('Login_log a')->join('w_user b','b.id=a.uid')->where($map)->field('a.*,b.m_nickname')->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('login_log')
            ->setSearch(['a.id' => 'ID', 'a.uid' => '用户ID','b.m_nickname'=>'用户昵称'])                        // 设置搜索参数
            ->addTimeFilter('login_time','','开始时间,结束时间')        // 添加时间段筛选
            ->addColumns([ // 批量添加数据列
                ['id', 'ID'],
                ['m_nickname','用户昵称', 'text'],
                ['login_ip', '登陆IP', 'text'],
                ['login_city', '登陆城市', 'text'],
                ['login_type', '登陆方式', 'status','',[1=>'前台登陆',2=>'后台登陆',3=>'其他方式登陆']],
                ['login_time','登录时间','datetime'],
            ])
            ->setExtraCss($css)
            ->setPrimaryKey('id')
            ->hideCheckbox()
            ->addOrder('id,login_time')                                                               //添加排序
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }


    //充值记录
    public function user_logs()
	{
        do_alogs('查看用户充值记录');
        $map        = $this->getMap();
        $order      = $this->getOrder('l_time desc');

        // var_dump($map);
        // var_dump($order);
        // exit;
		defined('NOW') || define('NOW', time());
		if(isset($_SESSION['tblqry']))
		{
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < NOW) unset($qrycache[$_k]);
			}
		}
		else $qrycache = [];

		$uniqkey = uniqid();
		$qrycache[$uniqkey] = ['where' => $map, 'order' => $order, 'expire' => NOW + 600];
		$_SESSION['tblqry'] = $qrycache;

		if(is_array($map)) foreach($map as $whereidx => $where) if(is_array($where) && count($where) === 3 && is_int(strpos($where[0], 'id'))) $map[$whereidx][0] = preg_replace('/(?<![^|])id/', 'a.id', $where[0]);
        $data_list  = Db::name('Logs a')->join('w_user b','b.id=a.l_uid')->where($map)->field('a.*,b.m_nickname')->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('logs')
            ->setSearch(['id' => 'ID', 'l_uid' => '用户ID','m_nickname'=>'用户昵称'])                        // 设置搜索参数
            ->addTimeFilter('l_time','','开始时间,结束时间')        // 添加时间段筛选
            ->addColumns([ // 批量添加数据列
                ['id', 'ID'],
                ['m_nickname','用户昵称', 'text'],
                ['l_num', '金额', 'text'],
                ['l_type', '充值类型', 'status','',[1=>'余额',2=>'积分',3=>'消费积分',4=>'累计积分']],
                ['l_info', '备注', 'text'],
                ['l_time','操作时间','datetime'],
            ])
            ->setExtraCss($css)
            ->setPrimaryKey('id')
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('user_logs_export', ['uniqkey' => $uniqkey])])
            ->hideCheckbox()
            ->addOrder('id,l_time')                                                               //添加排序
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }

    // 用户充值记录导出
	public function user_logs_export()
	{
		$begintime = microtime(true);
       	$uniqkey = input('uniqkey');
    	if(!empty($uniqkey) && isset($_SESSION['tblqry']))
		{
			$tblqry = $_SESSION['tblqry'];
			if(isset($tblqry[$uniqkey]))
			{
				$qrycache = $tblqry[$uniqkey];
				if(isset($qrycache['expire']) && $qrycache['expire'] > time())
				{
					$mysql = new \mysqli(config('database.hostname'), config('database.username'), config('database.password'), config('database.database'));
					if($mysql -> connect_error)
					{
						header('Content-Type: application/json; charset=utf-8');
						exit(json_encode(['type' => 'error', 'message' => 'Connect Error(' . $mysql -> connect_errno . ')' . $mysql -> connect_error]));
					}
					$mysql -> query('SET NAMES UTF8');
					$sql = 'SELECT a.`id`, b.`m_nickname`, a.`l_num`, CASE a.`l_type` WHEN 1 THEN "余额" WHEN 2 THEN "积分" WHEN 3 THEN "消费积分" WHEN 4 THEN "累计积分" ELSE "其它" END AS `l_type`, a.`l_info`, FROM_UNIXTIME(a.`l_time`) AS `l_date` FROM w_logs a INNER JOIN `w_user` b ON b.`id` = a.`l_uid`';
					if(!empty($qrycache['where']) && is_array($qrycache['where']))
					{
						$where = $qrycache['where'];
						$wheres = [];
						foreach($where as $condition)
						{
							if(is_array($condition) && count($condition) === 3)
							{
								switch($condition[1])
								{
									case 'between time':
										$wheres[] = 'a.`' . $condition[0] . '` > UNIX_TIMESTAMP("' . $condition[2][0] . '") AND a.`' . $condition[0] . '` < UNIX_TIMESTAMP("' . $condition[2][1] . '")';
										break;
									case 'like':
										$fields = is_int(strpos($condition[0], '|')) ? explode('|', $condition[0]) : [$condition[0]];
										$likewhere = [];
										foreach($fields as $field)
										{
											if($field === 'm_nickname') $field = 'b.`m_nickname`';
											else $field = 'a.`' . $field . '`';
											$likewhere[] = $field . ' LIKE "' . $condition[2] . '"';
										}
										$wheres[] = count($likewhere) > 1 ? '(' . implode(' OR ', $likewhere) . ')' : $likewhere[0];
										break;
								}
							}
						}
						$sql .= ' WHERE ' . implode(' AND ', $wheres);
					}

					$sql .= empty($qrycache['order']) ? ' ORDER BY ' . str_replace(['l_time', 'asc', 'desc'], ['a.`l_time`', 'ASC', 'DESC'], $qrycache['order']) : ' ORDER BY a.`l_time` DESC';
					// $sql .= ' LIMIT 100';
					// var_dump($sql);
					$res = $mysql -> query($sql);
					if($res)
					{
						$totalrows = $res -> num_rows;
						if($totalrows > 0)
						{
							/*
								$result = $res -> fetch_all(MYSQLI_ASSOC);
								var_dump($result);
								$res -> free();
								$mysql -> close();
								var_dump(microtime(true) - $begintime);
								// var_dump($sql);
								exit;
							*/
							defined('NOW') || define('NOW', time());
							if($totalrows > 1e5)
							{
								$res -> free();
								$totalpg = ceil($totalrows / 1e5);
								$zipfile = new \ZipArchive;
								$_filename = 'user_logs_' . date('YmdHis', NOW);
								if($zipfile -> open($_filename . '.zip', \ZipArchive::CREATE) === TRUE)
								{
									ini_set('memory_limit', '512M');
                                    ini_set('max_execution_time', 0);
                                    $filenames = [];
									for($pg = 0; $pg < $totalpg;)
									{
										$res = $mysql -> query($sql . ' LIMIT ' . $pg++ * 1e5 . ', 100000');
										if($res && $res -> num_rows > 0)
										{
                                            
											$result = $res -> fetch_all(MYSQLI_ASSOC);
											$res -> free();
                                            $filename = $_filename . '_pg' . $pg . '.csv';
                                            $filenames[] = $filename;
											$fp = fopen($filename, 'w');
											fwrite($fp, chr(0xEF) . chr(0xBB) . chr(0xBF));    // 转码 防止乱码(比如微信昵称)
											fputcsv($fp, ['ID', '昵称', '金额', '类型', '备注', '操作时间']);
											array_map(function($item) use ($fp){
												fputcsv($fp, $item);
											}, $result);
											unset($result);
											fclose($fp);
											$zipfile -> addFile($filename);
											// unlink($filename);
										}
									}
									$mysql -> close();
									$zipfile -> close();
									$filename = $_filename . '.zip';
									// ob_end_clean();
									// ob_start();
									header('Content-Type: application/zip');
									header('Content-Disposition: filename=' . $filename);
									$fsize = filesize($filename);
									header('Content-Length: ' . $fsize);
                                    echo(file_get_contents($filename));
                                    foreach($filenames as $_filename) unlink($_filename);
                                    // sleep(3);
                                    unlink($filename);
									exit;
								}
							}
							else
							{
								$result = $res -> fetch_all(MYSQLI_ASSOC);
								$res -> free();
								$mysql -> close();
								// 导出csv文件
								$filename = 'user_logs_' . date('YmdHis');
								return export_csv($filename . '.csv', ['ID', '昵称', '金额', '类型'], $result);
							}
						}
					}
					$mysql -> close();
				}
			}
        }
    }
    
    // 财务报表导出
    public function user_report_export()
    {

        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    ini_set('memory_limit', '512M');
                    ini_set('max_execution_time', 0);

                    $map = $qrycache['where'];
                    $order = $qrycache['order'];

                    $data_list  = Db::name('UserReport')->where($map)->order($order)->select();
                    $sumCredit  = Db::name('UserReport')->where($map)->sum('r_credit');
                    $sumEpoints  = Db::name('UserReport')->where($map)->sum('r_epoints');
                    $sumIntegral  = Db::name('UserReport')->where($map)->sum('r_integral');
                    $cj  = Db::name('UserReport')->where($map)->sum('r_cj');
                    $rj  = Db::name('UserReport')->where($map)->sum('r_rj');
                    $jrcj  = Db::name('UserReport')->where($map)->sum('r_jrcj');
                    $jrrj  = Db::name('UserReport')->where($map)->sum('r_jrrj');
                    $pageCredit  = 0;
                    $epoints = 0;
                    $pageCj = 0;
                    $pageRj = 0;

                    foreach ($data_list as &$row) {
                        $pageCredit = $pageCredit + $row['r_credit'];
                        $epoints = $epoints + $row['r_epoints'];
                        $pageRj = $epoints + $row['r_rj'];
                        $pageCj = $epoints + $row['r_cj'];
                    }
                    $sumEpoints = sprintf("%.2f" , $sumEpoints);
                    $sumIntegral = sprintf("%.2f" , $sumIntegral);
                    $epoints = sprintf("%.2f" , $epoints);
                    $cj = sprintf("%.2f" , $cj);
                    $rj = sprintf("%.2f" , $rj);
                    $jrcj = sprintf("%.2f" , $jrcj);
                    $jrrj = sprintf("%.2f" , $jrrj);


                    $titleArr = ['ID', '交易商', '姓名', '昨日余额', '昨日积分', '今日余额', '今日积分', '全部入金', '全部出金', '今日入金', '今日出金', '持仓', '买单消费', '卖单收益', '手续费', '零售票(买)', '零售消费(买)', '批发票(买)', '批发消费(买)', '奖励票(买)', '奖励消费(买)', '零售票(卖)', '零售收益(卖)', '批发票(卖)', '批发收益(卖)', '奖励票(卖)', '奖励收益(卖)', '统计时间'];

                    $dataArr = [];

                    foreach($data_list as $k => $v){

                        $dataArr[] = [
                            'ID' => $v['r_uid'],
                            '交易商' => filter_value($v['r_account']),
                            '姓名' => $v['r_username'],
                            '昨日余额' => $v['r_zepoints'],
                            '昨日积分' => $v['r_zintegral'],
                            '今日余额' => $v['r_epoints'],
                            '今日积分' => $v['r_integral'],
                            '全部入金' => $v['r_rj'],
                            '全部出金' => $v['r_cj'],
                            '今日入金' => $v['r_jrrj'],
                            '今日出金' => $v['r_jrcj'],
                            '持仓' => $v['r_credit'],
                            '买单消费' => $v['r_buy'],
                            '卖单收益' => $v['r_pay'],
                            '手续费' => $v['r_fee'],
                            '零售票(买)' => $v['r_buy_credit_1'],
                            '零售消费(买)' => $v['r_buy_epoints_1'],
                            '批发票(买)' => $v['r_buy_credit_2'],
                            '批发消费(买)' => $v['r_buy_epoints_2'],
                            '奖励票(买)' => $v['r_buy_credit_3'],
                            '奖励消费(买)' => $v['r_buy_epoints_3'],
                            '零售票(卖)' => $v['r_pay_credit_1'],
                            '零售收益(卖)' => $v['r_pay_epoints_1'],
                            '批发票(卖)' => $v['r_pay_credit_2'],
                            '批发收益(卖)' => $v['r_pay_epoints_2'],
                            '奖励票(卖)' => $v['r_pay_credit_3'],
                            '奖励收益(卖)' => $v['r_pay_epoints_3'],
                            '统计时间' => $v['addDate'],
                        ];

                    }

                    $filename = '财务报表-' . date('YmdHis');
                    return export_csv($filename . '.csv', $titleArr, $dataArr);

                }
            }
        }

    }

    // 交易报表导出
    public function user_reportdetails_export()
    {

        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    ini_set('memory_limit', '512M');
                    ini_set('max_execution_time', 0);

                    $map = $qrycache['where'];
                    $order = $qrycache['order'];

                    $data_list  = Db::name('UserReportDetails')->where($map)->order($order)->select();

                    $titleArr = ['统计时间', '持仓', '待发货', '已发货', '买进零售', '买进批发', '买进特价', '卖出零售', '卖出批发', '卖出奖励', '释放零售', '释放批发', '释放特价', '配送批发', '配送奖励', '手续费', '入金', '出金'];

                    $dataArr = [];
                            
                    foreach ($data_list as $row) {
                        $mjls = 0;
                        $mjpf = 0;
                        $mjjl = 0;
                        $mcls = 0;
                        $mcpf = 0;
                        $mcjl = 0;
                        $sfls = 0;
                        $sfpf = 0;
                        $sfjl = 0;
                        $pppf = 0;
                        $ppjl = 0;
                        $fee  = 0;
                        $rj   = 0;
                        $cj   = 0;

                        $mjls = $mjls + $row['mj_ls'];
                        $mjpf = $mjpf + $row['mj_pf'];
                        $mjjl = $mjjl + $row['mj_jl'];
                        $mcls = $mcls + $row['mc_ls'];
                        $mcpf = $mcpf + $row['mc_pf'];
                        $mcjl = $mcjl + $row['mc_jl'];
                        $sfls = $sfls + $row['sf_ls'];
                        $sfpf = $sfpf + $row['sf_pf'];
                        $sfjl = $sfjl + $row['sf_jl'];
                        $pppf = $pppf + $row['pp_pf'];
                        $ppjl = $ppjl + $row['pp_jl'];
                        $fee  = $fee  + $row['fee'];
                        $rj   = $rj  + $row['rj'];
                        $cj   = $cj  + $row['cj'];

                        $dataArr[] = [
                            '统计时间' => $row['addDate'],
                            '持仓' => !empty($row['z_cc'])?$row['z_cc']:'',
                            '待发货' => !empty($row['z_dfh'])?$row['z_dfh']:'',
                            '已发货' => !empty($row['z_yfh'])?$row['z_yfh']:'',
                            '买进零售' => $mjls,
                            '买进批发' => $mjpf,
                            '买进特价' => $mjjl,
                            '卖出零售' => $mcls,
                            '卖出批发' => $mcpf,
                            '卖出奖励' => $mcjl,
                            '释放零售' => $sfls,
                            '释放批发' => $sfpf,
                            '释放特价' => $sfjl,
                            '配送批发' => $pppf,
                            '配送奖励' => $ppjl,
                            '手续费' => $fee,
                            '入金' => $rj,
                            '出金' => $cj,
                        ];
                    }

                    $filename = '交易报表-' . date('YmdHis');
                    return export_csv($filename . '.csv', $titleArr, $dataArr);

                }
            }
        }

    }

    // 每日交易报表导出
    public function user_reportcont_export()
    {

        $uniqkey = input('uniqkey');
        if(!empty($uniqkey) && isset($_SESSION['tblqry']))
        {
            $tblqry = $_SESSION['tblqry'];
            if(isset($tblqry[$uniqkey]))
            {
                
                $qrycache = $tblqry[$uniqkey];
                if(isset($qrycache['expire']) && $qrycache['expire'] > time())
                {
                    ini_set('memory_limit', '512M');
                    ini_set('max_execution_time', 0);

                    $map = $qrycache['where'];
                    $order = $qrycache['order'];

                    $data_list  = Db::name('UserReportCont')->where($map)->order($order)->select();

                    $titleArr = ['ID', '会员ID', '交易账号', '昵称', '昨日余额', '今日余额', '推荐人帐号', '推荐人姓名', '持仓', '买进零售', '买进特价', '买进批发', '卖出零售', '卖出批发', '卖出特价', '手续费', '今日入金', '今日出金', '统计时间'];

                    $dataArr = [];
                            
                    foreach ($data_list as $rowp) {

                        $dataArr[] = [
                            'ID' => $rowp['id'],
                            '会员ID' => $rowp['uid'],
                            '交易账号' => filter_value($rowp['c_account']),
                            '昵称' => $rowp['c_nickname'],
                            '昨日余额' => $rowp['c_zbalance'],
                            '今日余额' => $rowp['c_balance'],
                            '推荐人帐号' => filter_value($rowp['c_taccount']),
                            '推荐人姓名' => $rowp['c_tname'],
                            '持仓' => $rowp['c_cc'],
                            '买进零售' => $rowp['c_mj_ls'],
                            '买进特价' => $rowp['c_mj_jl'],
                            '买进批发' => $rowp['c_mj_pf'],
                            '卖出零售' => $rowp['c_mc_ls'],
                            '卖出批发' => $rowp['c_mc_pf'],
                            '卖出特价' => $rowp['c_mc_jl'],
                            '手续费' => $rowp['c_fee'],
                            '今日入金' => $rowp['c_rj'],
                            '今日出金' => $rowp['c_cj'],
                            '统计时间' => $rowp['addDate'],
                        ];
                        
                    }

                    $filename = '每日交易报表-' . date('YmdHis');
                    return export_csv($filename . '.csv', $titleArr, $dataArr);

                }
            }
        }

    }

    // 审核开户
    public function open_account($id = '')
    {

        $user = Db::name('user')->where(array('id' => $id))->find();

        if($this->request->post()){

            $param = $this->request->param();

            if($param['m_is_pass'] == 2 || $param['m_is_pass'] == 0){

                if($user['m_bank_signing'] == 1){
                    $this->error('用户已签约完成，不可修改');
                }

                if($user['m_bank_signing'] == 3){
                    $this->error('用户签约中，不可修改');
                }

                $res = Db::name('user')->where('id', $param['id'])->update(['m_is_pass' => $param['m_is_pass'], 'm_bank_signing'=>0, 'last_time' => time()]);

                if($res){
                    $this->success('操作成功',url('User/user_index'),'_parent_reload');
                }else{
                    $this->error('操作失败');
                }

            }else{
                if(intval($user['m_is_pass']) == 1){
                    $this->error('审核已通过，不可修改');
                }
            }

            if(intval($param['m_is_pass']) == 1){

                $uid = $param['id'];

                $data = array(
                    'mem_code' => $user['m_account'],
                    'exchange_fund_account' => $user['m_account'],
                    'trade_account' => $user['m_account'],
                    'full_name' => $user['m_name'],
                    'tel' => $user['m_phone'],
                    'id_no' => $user['m_car_id'],
                    'gender' => $user['m_sex'],
                );
                $api = new Api();
                $res = $api->mem_user_register($data, $uid);
                $res = json_decode($res, true);
                if ($res['error_no'] == 0) {
                    $user_data = [
                        'm_bank_signing' => 2,
                        'm_is_pass' => 1,
                        'last_time' => time(),
                    ];
                    // file_put_contents('888.txt', date('Y-m-d H:i:s'), FILE_APPEND);
                    // file_put_contents('888.txt', var_export($user_data, true).PHP_EOL, FILE_APPEND);
                    Db::name('User')->where(array('id' => $uid))->update($user_data);
                    $this->success('用户开户成功', url('User/user_index'),'_parent_reload');
                }else{

                    $this->error($res['error_info']?$res['error_info']:'用户开户失败');
                }


            }elseif($param['m_is_pass'] == 2){

                $res = Db::name('user')->where('id', $param['id'])->update(['m_is_pass' => 2, 'last_time' => time()]);

                if($res){
                    $this->success('操作成功',url('User/user_index'),'_parent_reload');
                }else{
                    $this->error('操作失败');
                }

            }else{

                $this->success('操作成功', url('User/user_index'), '_parent_reload');
            }

        }

        return ZBuilder::make('form')
            ->setPageTitle('审核开户')
            ->addFormItems([
                ['hidden', 'id'],
                ['radio', 'm_is_pass', '审核开户', '', [0 => '默认', 1 => '通过', 2 => '不通过'], 0],
            ])
            ->setFormData($user)
            ->fetch();
        
    }

    // 审核签约提交接口
    public function client_sign_bind_account()
    {
        $param = $this->request->param();

        $info = Db::name('user')->where('id', $param['id'])->find();

        if($this->request->post()){
            // 通过
            if($param['status'] == 1){

                $Api        = new Api();
                $msg        = $Api->client_sign_bind_account(4, $info['id']);
                $msg_arr    = json_decode($msg,true);
                if($msg_arr['error_no'] == 0){
                    Db::name('User')->where(array('id'=>$info['id']))->update(array('m_bank_signing'=>3,'last_time'=>time()));
                    return json(array('code'=>1,'msg'=>'提交成功,请耐心等待'));
                }else{
                    return json(array('code'=>0,'msg'=>$msg_arr['error_info']));
                }

            }else{
                // 不通过
                $new_data = [
                    'm_bank_signing' => 8, // 审核不通过
                ];
                $res = Db::name('user')->where('id', $param['id'])->update($new_data);

                if($res){
                    $this->success('操作成功', url('User/user_index'),'_parent_reload');
                }else{
                    $this->error('操作失败');
                }
            }
        }

        return ZBuilder::make('form')
            ->setPageTitle('审核签约')
            ->addFormItems([
                ['hidden', 'id'],
                ['radio', 'status', '审核签约', '', [1 => '通过', 0 => '不通过'], 0],
            ])
            ->setFormData($info)
            ->fetch();

    }


}