<?php
 
 
 
 
 
 
 

namespace app\admin\controller;

use app\common\builder\ZBuilder;
use app\admin\model\Log as LogModel;
use think\Db;
/**
 * 系统日志控制器
 * @package app\admin\controller
 */
class Log extends Admin
{
    /**
     * 日志列表
      
     * @return mixed
     * @throws \think\Exception
     */
    public function index()
    {
        // 查询
        $map = $this->getMap();
        // 排序
        $order = $this->getOrder('admin_log.id desc');
        // 数据列表
        $data_list = LogModel::getAll($map, $order);
        // 分页数据
        $page = $data_list->render();

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('系统日志') // 设置页面标题
            ->setSearch(['admin_action.title' => '行为名称', 'admin_user.username' => '执行者', 'admin_module.title' => '所属模块']) // 设置搜索框
            ->hideCheckbox()
            ->addColumns([ // 批量添加数据列
                ['id', '编号'],
                ['title', '行为名称'],
                ['username', '执行者'],
                ['action_ip', '执行IP', 'callback', function($value){
                    return long2ip(intval($value));
                }],
                ['module_title', '所属模块'],
                ['create_time', '执行时间', 'datetime', '', 'Y-m-d H:i:s'],
                ['right_button', '操作', 'btn']
            ])
            ->addOrder(['title' => 'admin_action', 'username' => 'admin_user', 'module_title' => 'admin_module.title'])
            ->addFilter(['admin_action.title', 'admin_user.username', 'module_title' => 'admin_module.title'])
            ->addRightButton('edit', ['icon' => 'fa fa-eye', 'title' => '详情', 'href' => url('details', ['id' => '__id__'])])
            ->setRowList($data_list) // 设置表格数据
            ->setPages($page) // 设置分页数据
            ->fetch(); // 渲染模板
    }

    /**
     * 日志详情
     * @param null $id 日志id
      
     * @return mixed
     * @throws \think\Exception
     */
    public function details($id = null)
    {
        if ($id === null) $this->error('缺少参数');
        $info = LogModel::getAll(['admin_log.id' => $id]);
        $info = $info[0];
        $info['action_ip'] = long2ip(intval($info['action_ip']));

        // 使用ZBuilder快速创建表单
        return ZBuilder::make('form')
            ->setPageTitle('编辑') // 设置页面标题
            ->addFormItems([ // 批量添加表单项
                 ['hidden', 'id'],
                 ['static', 'title', '行为名称'],
                 ['static', 'username', '执行者'],
                 ['static', 'record_id', '目标ID'],
                 ['static', 'action_ip', '执行IP'],
                 ['static', 'module_title', '所属模块'],
                 ['textarea', 'remark', '备注'],
            ])
            ->hideBtn('submit')
            ->setFormData($info) // 设置表单数据
            ->fetch();
    }


    public function test()
    {
        // $path = 'D:\phpstudy_pro\WWW\jiulonghu\public\test\1.txt';
        // $path = 'D:\phpstudy_pro\WWW\jiulonghu\public\test\2.txt';
        // $path = 'D:\phpstudy_pro\WWW\jiulonghu\public\test\3.txt';
        // $path = 'D:\phpstudy_pro\WWW\jiulonghu\public\test\4.txt';
        // $path = 'D:\phpstudy_pro\WWW\jiulonghu\public\test\5.txt';
        // $path = 'D:\phpstudy_pro\WWW\jiulonghu\public\test\6.txt';
        // $path = 'D:\phpstudy_pro\WWW\jiulonghu\public\test\7.txt';
        // $path = 'D:\phpstudy_pro\WWW\jiulonghu\public\test\8.txt';
        // $path = 'D:\phpstudy_pro\WWW\jiulonghu\public\test\9.txt';

        $f = fopen($path, "r");

        $str = fread($f, filesize($path));

        $arr = explode("\r\n", $str);

        // dump($arr);

        $loss = [];

        foreach($arr as $k => $v){

            $time = time();

            $vv = explode('|', $v);

            // dump($vv);

            $user_id = Db::name('user')->where('m_account', $vv[0])->value('id');

            if($user_id){
                // $address_data = [
                //     'a_name' => $vv[6],
                //     'a_phone' => $vv[5],
                //     'a_city' => $vv[1].' '.$vv[2].' '.$vv[3],
                //     'a_detail' => $vv[4],
                //     'uid' => $user_id,
                //     'a_time' => $time,
                //     'a_is_default' => 0
                // ];
                $address_data = [
                    'a_name' => $vv[4],
                    'a_phone' => $vv[3],
                    'a_city' => $vv[1],
                    'a_detail' => $vv[2],
                    'uid' => $user_id,
                    'a_time' => $time,
                    'a_is_default' => 0
                ];
                $address_id = Db::name('address')->insertGetId($address_data);

                // $order_data = [
                //     'gid' => 1,
                //     'o_buy_num' => $vv[8],
                //     'uid' => $user_id,
                //     'o_type' => 1,
                //     'o_price' => $vv[7],
                //     'o_credit_1' => bcmul($vv[7], $vv[8], 2),
                //     'aid' => $address_id,
                //     'o_status' => 3,
                //     'o_company_num' => $vv[9],
                //     'o_addtime' => $time,
                // ];
                $order_data = [
                    'gid' => 1,
                    'o_buy_num' => $vv[6],
                    'uid' => $user_id,
                    'o_type' => 1,
                    'o_price' => $vv[5],
                    'o_credit_1' => bcmul($vv[5], $vv[6], 2),
                    'aid' => $address_id,
                    'o_status' => 3,
                    'o_company_num' => !empty($vv[9])?$vv[9]:'',
                    'o_addtime' => $time,
                ];
                Db::name('order')->insert($order_data);

            }else{
                $loss[] = ['k'=>$k,'m_account'=>$vv[0]];
            }

            // dump($user_id);

            // $user_id =

        }


        dump($loss);

        // echo 11;

    }

    public function importTxt($i = 0)
    {
        dump($i);die;
        $begintime = microtime(true);
        $success_count = 0;

        $tbl_index = 4;
        $tbl_infs = [
        	['_deal', 'INSERT INTO `w_file_transaction_data` (`init_date`, `exchange_code`, `exchange_market_type`, `biz_type`, `deal_id`, `open_mem_code`, `open_fund_account`, `open_trade_account`, `opp_mem_code`, `opp_fund_account`, `opp_trade_account`, `product_category_id`, `product_code`, `entrust_bs`, `deal_type`, `opp_deal_type`, `hpsp_trade_type`, `deal_quantity`, `deal_price`, `hold_price`, `deal_total_price`, `deposit_rate`, `deposit_ratio_type`, `deposit_type`, `deposit_balance`, `receipt_quantity`, `open_poundage`, `opp_poundage`, `deal_time`, `depot_order_no`, `opp_depot_order_no`, `order_id`, `opp_order_id`, `settlement_date`) VALUES '],
			['_memberFund', 'INSERT INTO `w_file_fund_balance` (`init_date`, `exchange_code`, `mem_code`, `fund_account`, `money_type`, `occur_balance`, `current_balance`) VALUES '],
			['_memberPositionDetail', 'INSERT INTO `w_file_position_details` (`init_date`, `exchange_code`, `hold_id`, `mem_code`, `trade_account`, `product_category_id`, `product_code`, `entrust_bs`, `deposit_way`, `open_price`, `hold_price`, `deal_quantity`, `left_quantity`, `present_unit`, `trade_poundage`, `delay_fees`, `perform_balance`, `deposit_rate`, `square_profit_loss`, `settle_profit_loss`, `settle_price`, `deposit_ratio_type`, `deposit_type`, `today_hold_flag`, `deal_time`) VALUES '],
			['_fees', 'INSERT INTO `w_file_external_expenses_details` (`init_date`, `serial_no`, `exchange_code`, `exchange_market_type`, `biz_type`, `exchange_fees_type`, `fees_balance`, `payer_mem_code`, `payer_fund_account`, `payee_mem_code`, `payee_fund_account`, `deal_id`, `remark`, `busi_datetime`) VALUES '],
			['_closeprice', 'INSERT INTO `w_file_settlement_price` (`init_date`, `exchange_code`, `exchange_market_type`, `product_category_id`, `product_code`, `money_type`, `settle_price`) VALUES ']
		];

        $path = './static/importtxt/';

        $temp = scandir($path);

        foreach($temp as $fname)
		{
			if(strpos($fname, $tbl_infs[$tbl_index][0]) !== false)
			{
				$filepath = $path . $fname;

				$fsize = filesize($filepath);
				if($fsize > 0)
				{
					$fp = fopen($filepath, "r");
					$file_content = fread($fp, $fsize);
					fclose($fp);

					$data = explode("\n", trim($file_content));
					unset($file_content);

					$len = count($data);
					$totalpg = ceil($len / 1000);

					$i = 1;
					for($pg = 0; $pg < $totalpg; $totalpg--)
					{
						$_arr = array_map(function($item){
							return '("' . str_replace('|', '", "', $item) . '")';
						}, array_splice($data, 0, 1000));
						$sql = $tbl_infs[$tbl_index][1] . implode(', ', $_arr) . ';';
						unset($_arr);
                        $res = Db::execute($sql);
                        // dump($sql);die;
						unset($sql);
						$success_count += $res;
					}
					unset($data);
				}
			}
		}

		dump('消耗' . (microtime(true) - $begintime) . '秒，共计插入' . $success_count . '条数据');
		exit;
    }

    public function importtxt2($index = 0)
    {

        $data = strtotime(date('Ymd'));

        if($index == 0){
            $num = Db::name('file_bank_check')->where('add_time', $data)->count();
            if($num > 0){
                return;
            }
            $old_info = Db::name('file_bank_check')->order('id desc AND add_time desc')->find();
        }else{
            $num = Db::name('file_bank_check_fee')->where('add_time', $data)->count();
            if($num > 0){
                return;
            }
            $old_info = Db::name('file_bank_check_fee')->order('id desc AND add_time desc')->find();
        }

        $begintime = microtime(true);
        $success_count = 0;

        $tbl_index = $index;
        $tbl_infs = [
            ['_bankCheck.txt', 'INSERT INTO `w_file_bank_check` (`clear_num`, `num`, `occou_date`, `code`, `member_code`, `member_account`, `money_way`, `money_type`, `initiate`, `money`, `currency`, `bank_code`, `bank_product_code`, `bank_id`, `status`, `remarks`, `add_time`, `occou_date_time` ) VALUES '],
			['_bankCheckFee.txt', 'INSERT INTO `w_file_bank_check_fee` (`clear_num`, `num`, `occou_date`, `code`, `member_code`, `member_account`, `money_type`, `money`, `fee_money`, `bank_product_code`, `remarks`, `add_time`, `occou_date_time` ) VALUES ']
		];

        $path = './static/open_bank/';

        $temp = scandir($path);

        foreach($temp as $fname)
		{
            
			if(strpos($fname, $tbl_infs[$tbl_index][0]) !== false)
			{
                $fsize = 0;

                $_str = mb_substr($fname, 0, 8);
                if(!empty($old_info['occou_date'])){

                    if($_str > $old_info['occou_date']){
                        $filepath = $path . $fname;
                        $fsize = filesize($filepath);
                    }
                }else{
                    $filepath = $path . $fname;
                    $fsize = filesize($filepath);
                }

                if($fsize > 0){
                    $fp = fopen($filepath, "r");
                    $file_content = fread($fp, $fsize);
                    fclose($fp);

                    $data = explode("\n", trim($file_content));
                    unset($file_content);
                    unset($data[0]);
                    // dump($data);

                    $len = count($data);
                    $totalpg = ceil($len / 1000);

                    $i = 1;
                    for($pg = 0; $pg < $totalpg; $totalpg--)
                    {
                        $_arr = array_map(function($item) use ($index){
                            
                            $a_arr = explode('&', $item);
                            if($index == 0){
                                return '("'.$a_arr[0].'", "'.$a_arr[1].'", "'.$a_arr[2].'", "'.$a_arr[3].'", "'.$a_arr[4].'", "'.$a_arr[5].'", "'.$a_arr[6].'", "'.$a_arr[7].'", "'.$a_arr[8].'", "'.$a_arr[9].'", "'.$a_arr[10].'", "'.$a_arr[11].'", "'.$a_arr[12].'", "'.$a_arr[13].'", "'.$a_arr[14].'", "'.$a_arr[15].'", "'.strtotime(date('Ymd')).'", "'.strtotime($a_arr[2]).'")';
                            }elseif($index == 1){
                                return '("'.$a_arr[0].'", "'.$a_arr[1].'", "'.$a_arr[2].'", "'.$a_arr[3].'", "'.$a_arr[4].'", "'.$a_arr[5].'", "'.$a_arr[6].'", "'.$a_arr[7].'", "'.$a_arr[8].'", "'.$a_arr[9].'", "'.$a_arr[10].'", "'.strtotime(date('Ymd')).'", "'.strtotime($a_arr[2]).'")';
                            }

                        }, array_splice($data, 0, 1000));

                        $sql = $tbl_infs[$tbl_index][1] . implode(', ', $_arr) . ';';
                        unset($_arr);
                        $res = Db::execute($sql);
                        unset($sql);
                        $success_count += $res;
                    }
                    unset($data);

                }
                
			}
        }
        $success_txt = date('YmdHis').'消耗' . (microtime(true) - $begintime) . '秒，共计插入' . $success_count . '条数据' . PHP_EOL;
        file_put_contents('import' . $tbl_infs[$index][0],  $success_txt, FILE_APPEND);

		// dump('消耗' . (microtime(true) - $begintime) . '秒，共计插入' . $success_count . '条数据');
		// exit;
    }



}