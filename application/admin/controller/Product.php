<?php
namespace app\admin\controller;
use app\common\builder\ZBuilder;
use app\index\controller\Api;
use think\Db;

use think\Validate;
use think\validate\ValidateRule;

class Product extends Admin
{
    //产品列表
    public function product_list(){
        do_alogs('查看产品列表');
        $map        = $this->getMap();
        $order      = $this->getOrder('p_addtime desc');
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        $sql        = 1;
        if($admin_id>1 and $role==2){
            $sql    = "admin_id=$admin_id";
        }
        $data_list  = Db::name('Product')->where(array('p_del'=>0))->where($map)->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        // 授权按钮
        $btn_access = [
            'title' => '报送联交运',
            'icon'  => 'fa fa-fw fa-key',
            'href'  => url('product_set', ['id' => '__id__'])
        ];

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('product')
            ->setSearch(['id' => 'ID', 'p_title' => '产品标题', 'p_retail_price' => '零售价格']) // 设置搜索参数
            ->addTimeFilter('last_time','','开始时间,结束时间') // 添加时间段筛选
            ->addTopSelect('p_setup_status', '',['上架','下架'])
            ->addColumns([ // 批量添加数据列
                ['id', 'ID'],
                ['p_title', '产品标题', 'text'],
                ['p_code','产品编号', 'text'],
                ['p_image','产品图片','img_urls'],
                ['p_retail_price', '零售价','text'],
                ['p_whole_price', '批发价','text'],
                ['p_setup_status', '上下架','switch',''],
                ['p_cate_type', '支持提货','status','',['是','否']],
                ['p_deal_ratio', '配售比率','text'],
                ['p_income_days', '收益天数','text'],
                ['p_status', '联交运报备','status','',['否','是']],
                ['p_back', '截留比例','text'],
                ['p_back_type', '截留类型','text'],
                ['p_min_num', '最小交易量','text'],
                ['p_max_num', '最大交易量','text'],
                ['p_shop_name', '厂家','text'],
                ['p_deal_num', '交易单位','text'],
                ['p_addtime','创建时间','date'],
                ['last_time','更新时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time'=>150,'id'=>60,'p_code'=>110])
            ->setExtraCss($css)
            ->hideCheckbox()
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,p_addtime')                                                               //添加排序
            ->addTopButton('add',['href' => url('product_add')])
            ->addRightButtons(
                ['edit'  => ['title' => '编辑','href'=>url('product_edit', ['id'=>'__id__'])],
                 'delete'=> ['title' => '删除','href'=>url('product_del', ['id'=>'__id__'])],
                ])
            ->addRightButton('custom', $btn_access) // 添加授权按钮
            ->replaceRightButton(['p_status'=>['<>',0]],'','custom')
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板
    }

    public function product_set($id=''){
        $product = getProduct($id);
        if($product['p_status'] ==1){
            $this->error('请勿重复报送');
        }
        if($this->request->post()){
            do_alogs('设置产品报送信息');
            $Api                =   new Api();
            $fields             =   input('post.');
            $p_cate_name        =   $fields['p_cate_name'];
            $p_cate_code        =   randomkeys(8);
            $has_product        =   Db::name('Product')->where(array('p_cate_name'=>$p_cate_name))->find();
            if(!empty($has_product)){
                $p_cate_name = $has_product['p_cate_name'];
                $p_cate_code = $has_product['p_cate_code'];
                $p_cate      = $has_product['p_cate'];
                Db::name('Product')->where(array('id'=>$id))->update(array('p_cate_name'=>$p_cate_name,'p_cate_code'=>$p_cate_code,'p_cate'=>$p_cate));
                $res        = $Api->add_product_info($id);
                $msg_arr    = json_decode($res,true);
                if($msg_arr['error_no'] == 0){
                    $this->success('报送成功');
                }else{
                    $this->error($msg_arr['error_info']);
                }
            }else{
                Db::name('Product')->where(array('id'=>$id))->update(array('p_cate_name'=>$p_cate_name,'p_cate_code'=>$p_cate_code));
                $res        = $Api->insert_category($id,$p_cate_name,$p_cate_code);
                $msg_arr    = json_decode($res,true);
                if($msg_arr['error_no'] == 0){
                    $this->success('报送成功');
                }else{
                    $this->error($msg_arr['error_info']);
                }
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('新增')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['text', 'p_cate_name', '产品分类名称','(请输入产品分类名称)'],
            ])
            ->layout(['p_cate_name'=> 4])
            ->fetch();

    }

    //添加产品
    public function product_add(){
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        if($this->request->post()){
            do_alogs('添加产品信息');
            $fields             =   input('post.');
            $fields['p_code']   =   randomkeys(8);
            $fields['p_addtime']=   time();
            if(!$fields['p_title']){
                $this->error('产品标题 不能为空');
            }
            if(!$fields['p_retail_price']){
                $this->error('产品零售价格 不能为空');
            }
            if(!$fields['p_whole_price']){
                $this->error('产品批发价格 不能为空');
            }
            if(!$fields['p_deal_ratio']){
                $this->error('产品配售比率 不能为空');
            }
            $p_deal_ratio = explode(':',$fields['p_deal_ratio']);
            if(count($p_deal_ratio) != 2){
                $this->error('产品配售比率 请按照规则填写');
            }

            if(!$fields['p_income_days']){
                $this->error('产品收益天数 不能为空');
            }

            if(!$fields['p_min_num']){
                $this->error('最小限额 不能为空');
            }

            if(!$fields['p_max_num']){
                $this->error('最大限额 不能为空');
            }
            if(!$fields['p_image']){
                $this->error('产品图片 不能为空');
            }
            $fields['p_image'] = get_files_path($fields['p_image']);
            if(!$fields['p_shop_name']){
                $this->error('产品厂家 不能为空');
            }
            if(!$fields['p_deal_num']){
                $this->error('交易单位 不能为空');
            }
            if($fields['p_back_type'] == 2){
                if(!$fields['p_deal_num']){
                    $this->error('资金截留比例 不能为空');
                }
            }
            if($fields['p_back_type'] == 3){
                if(!$fields['p_back_time']){
                    $this->error('兑换商品时间 不能为空');
                }
            }
            if(!$fields['p_desc']){
                $this->error('产品描述 不能为空');
            }
            if(!$fields['p_content']){
                $this->error('产品详情 不能为空');
            }

            // 操作 承销商 挂牌商 经纪商
            if(!$fields['underwriter']){
                $this->error('请选择承销商账户');
            }
            if(!$fields['listing_company']){
                $this->error('请选择挂牌商账户');
            }
            if(!$fields['economic_quotient']){
                $this->error('请选择经济商账户');
            }

            $underwriter_info = Db::name('user')->where('id', $fields['underwriter'])->find();
            if($underwriter_info['m_bank_signing'] != 1){
                $this->error('所选承销商账户未签约');
            }
            $listing_company_info = Db::name('user')->where('id', $fields['listing_company'])->find();
            if($listing_company_info['m_bank_signing'] != 1){
                $this->error('所选挂牌商账户未签约');
            }
            $economic_quotient_info = Db::name('user')->where('id', $fields['economic_quotient'])->find();
            if($economic_quotient_info['m_bank_signing'] != 1){
                $this->error('所选经济商账户未签约');
            }

            $fields['admin_id']     = $admin_id;
            $res_id = Db::name('Product')->insertGetId($fields);

            $time = time();
            // 承销商账户信息
            $underwriter_data = [
                'pid' => $res_id,
                'uid' => $underwriter_info['id'],
                'add_time' => $time,
                'm_identity' => 1,
            ];
            // 挂牌商账户信息
            $listing_company_data = [
                'pid' => $res_id,
                'uid' => $listing_company_info['id'],
                'add_time' => $time,
                'm_identity' => 2,
            ];
            // 经济商账户信息
            $economic_quotient_data = [
                'pid' => $res_id,
                'uid' => $economic_quotient_info['id'],
                'add_time' => $time,
                'm_identity' => 3,
            ];

            Db::name('wallet')->insert($underwriter_data);
            Db::name('wallet')->insert($listing_company_data);
            Db::name('wallet')->insert($economic_quotient_data);

            if($res_id){
                $this->success('产品添加成功',url('Product/product_list'));
            }else{
                $this->error('产品添加失败');
            }
        }
        $user_list = Db::name('user')->where('m_del', 0)->select();
        $user = [];
        foreach($user_list as $k => $v){
            $user[$v['id']] = $v['m_name'] . ' - ' . $v['m_account'];
        }
        return ZBuilder::make('form')
            ->setPageTitle('新增')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['text', 'p_title', '产品标题','(请输入产品标题)'],
                ['number', 'p_retail_price', '零售价格','(请输入产品零售价格)'],
                ['number', 'p_whole_price', '批发价格','(请输入产品批发价格)'],
                ['number', 'p_reward_price', '消费积分奖励','(请输入消费积分奖励)'],
                ['text', 'p_deal_ratio','配售比率','请输入产品配售比率'],
                ['number', 'p_income_days','收益天数','请输入收益天数'],
                ['number', 'p_min_num','最小买卖限额','请输入最小买卖限额'],
                ['number', 'p_max_num','最大买卖限额','请输入最大买卖限额'],
                ['select', 'underwriter', '承销商账户', '请选择承销商账户', $user],
                ['select', 'listing_company', '挂牌商账户', '请选择挂牌商账户', $user],
                ['select', 'economic_quotient', '经济商账户', '请选择经济商账户', $user],
                ['images', 'p_image', '产品图片','(请上传产品图片)'],
                ['radio', 'p_type','产品类别','请选择产品类型',['普通产品','特殊产品'],0],
                ['radio', 'p_cate_type','是否支持提货','',['默认','不支持'],0],
                ['text', 'p_shop_name','厂家','请输入产品商厂家'],
                ['text', 'p_deal_num','交易单位','请输入产品交易单位'],
                ['radio', 'p_back_type','截留类型','请选择产品截留类型',['不设置节流','按比例节流','兑换商品'],0],
                ['number', 'p_back','截留比例','请输入产品截留比例'],
                ['number', 'p_back_time','兑换商品时间','请输入兑换商品时间'],
                ['textarea:6', 'p_desc', '产品描述','(请输入产品描述)'],
                ['ckeditor', 'p_content', '产品详情','(请输入产品详情)'],
            ])
            ->layout(['p_title' => 4, 'p_desc' => 5, 'p_retail_price' => 2, 'p_whole_price' => 2, 'p_reward_price' => 2,
                'p_shop_name' => 2, 'p_deal_num' => 2, 'p_deal_ratio' => 2, 'p_income_days' => 2,
                'p_min_num' => 2, 'p_max_num' => 2, 'underwriter' => 2, 'listing_company' => 2, 'economic_quotient' => 2, 'p_back' => 2, 'p_back_time' => 2, 'p_content' => 6
            ])
            ->fetch();
    }

    //编辑产品
    public function product_edit($id=''){
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        $data           = Db::name('Product')->where(array('id'=>$id))->find();
        if($this->request->post()){
            do_alogs('编辑产品信息');
            $fields             =   input('post.');
            $fields['last_time']=   time();
            if(!$fields['p_title']){
                $this->error('产品标题 不能为空');
            }
            if(!$fields['p_retail_price']){
                $this->error('产品零售价格 不能为空');
            }
            if(!$fields['p_whole_price']){
                $this->error('产品批发价格 不能为空');
            }
            if(!$fields['p_deal_ratio']){
                $this->error('产品配售比率 不能为空');
            }
            $p_deal_ratio = explode(':',$fields['p_deal_ratio']);
            if(count($p_deal_ratio) != 2){
                $this->error('产品配售比率 请按照规则填写');
            }
            if(!$fields['p_income_days']){
                $this->error('产品收益天数 不能为空');
            }
            if(!$fields['p_min_num']){
                $this->error('最小限额 不能为空');
            }
            if(!$fields['p_max_num']){
                $this->error('最大限额 不能为空');
            }
            if(!$fields['p_image']){
                $this->error('产品图片 不能为空');
            }
            $fields['p_image'] = setImagePath($fields['p_image']);
            if(!$fields['p_shop_name']){
                $this->error('产品厂家 不能为空');
            }
            if(!$fields['p_deal_num']){
                $this->error('交易单位 不能为空');
            }
            if($fields['p_back_type'] == 2){
                if(!$fields['p_deal_num']){
                    $this->error('资金截留比例 不能为空');
                }
            }
            if($fields['p_back_type'] == 3){
                if(!$fields['p_back_time']){
                    $this->error('兑换商品时间 不能为空');
                }
            }
            if(!$fields['p_desc']){
                $this->error('产品描述 不能为空');
            }
            if(!$fields['p_content']){
                $this->error('产品详情 不能为空');
            }

            // 操作 承销商 挂牌商 经纪商
            if(!$fields['underwriter']){
                $this->error('请选择承销商账户');
            }
            if(!$fields['listing_company']){
                $this->error('请选择挂牌商账户');
            }
            if(!$fields['economic_quotient']){
                $this->error('请选择经济商账户');
            }

            $underwriter_info = Db::name('user')->where('id', $fields['underwriter'])->find();
            if($underwriter_info['m_bank_signing'] != 1){
                $this->error('所选承销商账户未签约');
            }
            $listing_company_info = Db::name('user')->where('id', $fields['listing_company'])->find();
            if($listing_company_info['m_bank_signing'] != 1){
                $this->error('所选挂牌商账户未签约');
            }
            $economic_quotient_info = Db::name('user')->where('id', $fields['economic_quotient'])->find();
            if($economic_quotient_info['m_bank_signing'] != 1){
                $this->error('所选经济商账户未签约');
            }

            $time = time();

            // 承销商账户信息
            $underwriter_data = [
                'uid' => $underwriter_info['id'],
                'last_time' => $time,
            ];
            // 挂牌商账户信息
            $listing_company_data = [
                'uid' => $listing_company_info['id'],
                'last_time' => $time,
            ];
            // 经济商账户信息
            $economic_quotient_data = [
                'uid' => $economic_quotient_info['id'],
                'last_time' => $time,
            ];

            Db::name('wallet')->where(['pid' => $id, 'm_identity' => 1])->update($underwriter_data);
            Db::name('wallet')->where(['pid' => $id, 'm_identity' => 2])->update($listing_company_data);
            Db::name('wallet')->where(['pid' => $id, 'm_identity' => 3])->update($economic_quotient_data);

            $res_id = Db::name('Product')->where(array('id'=>$id))->update($fields);
            if($res_id){
                $this->success('产品编辑成功',url('Product/product_list'));
            }else{
                $this->error('产品编辑失败');
            }
        }

        $user_list = Db::name('user')->where('m_del', 0)->select();
        $user = [];
        foreach($user_list as $k => $v){
            $user[$v['id']] = $v['m_name'] . ' - ' . $v['m_account'];
        }
        $underwriter_id = Db::name('wallet')->where(['pid'=>$id, 'm_identity' => 1])->value('uid');
        $listing_company_id = Db::name('wallet')->where(['pid'=>$id, 'm_identity' => 2])->value('uid');
        $economic_quotient_id = Db::name('wallet')->where(['pid'=>$id, 'm_identity' => 3])->value('uid');
        return ZBuilder::make('form')
            ->setPageTitle('编辑')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden','id'],
                ['text', 'p_title', '产品标题','(请输入产品标题)'],
                ['number', 'p_retail_price', '零售价格','(请输入产品零售价格)'],
                ['number', 'p_whole_price', '批发价格','(请输入产品批发价格)'],
                ['number', 'p_reward_price', '消费积分奖励','(请输入消费积分奖励)'],
                ['text', 'p_deal_ratio','配售比率','请输入产品配售比率'],
                ['number', 'p_income_days','收益天数','请输入收益天数'],
                ['number', 'p_min_num','最小买卖限额','请输入最小买卖限额'],
                ['number', 'p_max_num','最大买卖限额','请输入最大买卖限额'],
                ['select', 'underwriter', '承销商账户', '请选择承销商账户', $user, $underwriter_id],
                ['select', 'listing_company', '挂牌商账户', '请选择挂牌商账户', $user, $listing_company_id],
                ['select', 'economic_quotient', '经济商账户', '请选择经济商账户', $user, $economic_quotient_id],
                ['images', 'p_image', '产品图片','(请上传产品图片)'],
                ['radio', 'p_type','产品类别','请选择产品类型',['普通产品','特殊产品'],0],
                ['radio', 'p_cate_type','是否支持提货','',['默认','不支持'],0],
                ['text', 'p_shop_name','厂家','请输入产品商厂家'],
                ['text', 'p_deal_num','交易单位','请输入产品交易单位'],
                ['radio', 'p_back_type','截留类型','请选择产品截留类型',['不设置节流','按比例节流','兑换商品'],0],
                ['number', 'p_back','截留比例','请输入产品截留比例'],
                ['number', 'p_back_time','兑换商品时间','请输入兑换商品时间'],
                ['textarea:6', 'p_desc', '产品描述','(请输入产品描述)'],
                ['ckeditor', 'p_content', '产品详情','(请输入产品详情)'],
            ])
            ->setFormData($data)
            ->layout(['p_title' => 4, 'p_desc' => 5, 'p_retail_price' => 2, 'p_whole_price' => 2, 'p_reward_price' => 2,
                'p_shop_name' => 2, 'p_deal_num' => 2, 'p_deal_ratio' => 2, 'p_income_days' => 2,
                'p_min_num' => 2, 'p_max_num' => 2, 'underwriter' => 2, 'listing_company' => 2, 'economic_quotient' => 2, 'p_back' => 2, 'p_back_time' => 2, 'p_content' => 6
            ])
            ->fetch();
    }

    //删除产品
    public function product_del($id=''){
        $product = getProduct($id);
        if(!empty($product)){
            $res =Db::name('Product')->where('id',$id)->update(array('p_del'=>1));
            ###########
            ###########
            if($res == 1){
                do_alogs('删除产品信息');
                $this->success('删除成功');
            }else{
                $this->error('删除失败');
            }
        }
    }


    // 置换产品列表
    public function exchange_product_list()
    {
        do_alogs('查看产品列表');
        $map        = $this->getMap();
        $order      = $this->getOrder('id desc');
        
        $data_list  = Db::name('exchange_product')->where($map)->order($order)->limit(20)->paginate();

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('product')
            ->addTimeFilter('last_time','','开始时间,结束时间') // 添加时间段筛选
            ->addTopSelect('p_setup_status', '',['下架','上架'])
            ->addColumns([
                ['id', 'ID'],
                ['p_title', '产品标题', 'text'],
                ['p_code','产品编号', 'text'],
                ['p_retail_price', '零售价','text'],
                ['p_whole_price', '批发价','text'],
                ['p_stock', '库存', 'text'],
                ['p_setup_status', '上下架','status','', ['下架','上架']],
                ['p_addtime','创建时间','date'],
                ['last_time','更新时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time'=>150,'id'=>60,'p_code'=>110])
            ->hideCheckbox()
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,p_addtime')                                                               //添加排序
            ->addTopButton('add',['href' => url('exchange_product_add')])
            ->addRightButtons(
                ['edit'  => ['title' => '编辑','href'=>url('exchange_product_edit', ['id'=>'__id__'])],
                 'delete'=> ['title' => '删除','href'=>url('exchange_product_del', ['id'=>'__id__'])],
                ])
            ->setRowList($data_list) // 设置表格数据
            ->fetch(); // 渲染模板

    }

    // 添加置换产品
    public function exchange_product_add()
    {
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        if($this->request->post()){

            $param = $this->request->param();

            // 判断请求参数
            $rule = [
                'p_title'   => 'require|max:60',
                'p_retail_price'  => 'require|float|egt:0',
                'p_whole_price'   => 'require|float|egt:0',
                'p_stock' => 'require|egt:0',
                'p_setup_status' => 'require|in:0,1',
            ];
            $msg = [
                'p_title.require' => '请填写产品标题',
                'p_title.max' => '产品标题不能超过60个字符',
                'p_retail_price.require' => '请填写零售价格',
                'p_retail_price.number' => '零售价格格式不正确',
                'p_retail_price.egt' => '零售价格格式不正确',
                'p_whole_price.require' => '请填写批发价格',
                'p_whole_price.number' => '批发价格格式不正确',
                'p_whole_price.egt' => '批发价格格式不正确',
                'p_stock.require' => '请填写产品库存',
                'p_stock.egt' => '产品库存必须大于等于0',
                'p_setup_status.require' => '请选择上下架状态',
                'p_setup_status.in' => '上下架状态不正确',
            ];

            $validate   = Validate::make($rule, $msg);
            $result = $validate->check($param);

            if(!$result) {
                $this->error($validate->getError());
            }

            // 判断库存并修改上下架状态
            if($param['p_stock'] <= 0){
                $param['p_stock'] = 0;
                $param['p_setup_status'] = 0;
            }

            $param['p_code']   =   randomkeys(8);
            $param['p_addtime'] =   time();
            $param['last_time'] =   time();

            $param['admin_id']     = $admin_id;

            $res = Db::name('exchange_product')->insertGetId($param);
            if($res){
                do_alogs('添加置换产品信息');

                $this->success('产品添加成功',url('product/exchange_product_list'));
            }else{
                $this->error('产品添加失败');
            }
        }

        return ZBuilder::make('form')
            ->setPageTitle('新增置换产品')
            ->addFormItems([
                ['text', 'p_title', '产品标题','(请输入产品标题)'],
                ['number', 'p_retail_price', '零售价格','(请输入产品零售价格)'],
                ['number', 'p_whole_price', '批发价格','(请输入产品批发价格)'],
                ['number', 'p_stock', '产品库存', '(请输入产品库存)'],
                ['radio', 'p_setup_status', '上下架状态', '请选择产品上下架',['下架', '上架'], 0],
            ])
            ->layout(['p_title' => 8, 'p_retail_price' => 8, 'p_whole_price' => 8, 'p_stock' => 8, 'p_status' => 8])
            ->fetch();
    }

    // 编辑置换产品
    public function exchange_product_edit($id = 0)
    {

        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        $data           = Db::name('exchange_product')->where(array('id'=>$id))->find();
        if($this->request->post()){
            $param = $this->request->param();

            // 判断请求参数
            $rule = [
                'id' => 'require',
                'p_title'   => 'require|max:60',
                'p_retail_price'  => 'require|float|egt:0',
                'p_whole_price'   => 'require|float|egt:0',
                'p_stock' => 'require|egt:0',
                'p_setup_status' => 'require|in:0,1',
            ];
            $msg = [
                'id.require' => '缺少参数,请刷新后重试',
                'p_title.require' => '请填写产品标题',
                'p_title.max' => '产品标题不能超过60个字符',
                'p_retail_price.require' => '请填写零售价格',
                'p_retail_price.number' => '零售价格格式不正确',
                'p_retail_price.egt' => '零售价格格式不正确',
                'p_whole_price.require' => '请填写批发价格',
                'p_whole_price.number' => '批发价格格式不正确',
                'p_whole_price.egt' => '批发价格格式不正确',
                'p_stock.require' => '请填写产品库存',
                'p_stock.egt' => '产品库存必须大于等于0',
                'p_setup_status.require' => '请选择上下架状态',
                'p_setup_status.in' => '上下架状态不正确',
            ];

            $validate = Validate::make($rule, $msg);
            $result = $validate->check($param);

            if(!$result) {
                $this->error($validate->getError());
            }

            // 判断库存并修改上下架状态
            if($param['p_stock'] <= 0){
                $param['p_stock'] = 0;
                $param['p_setup_status'] = 0;
            }

            $param['last_time'] =   time();

            $param['admin_id']     = $admin_id;

            $res = Db::name('exchange_product')->where('id', $param['id'])->update($param);
            if($res){
                do_alogs('编辑置换产品信息');

                $this->success('产品修改成功',url('product/exchange_product_list'));
            }else{
                $this->error('产品修改失败');
            }
        }

        return ZBuilder::make('form')
            ->setPageTitle('编辑置换产品')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden','id'],
                ['text', 'p_title', '产品标题','(请输入产品标题)'],
                ['number', 'p_retail_price', '零售价格','(请输入产品零售价格)'],
                ['number', 'p_whole_price', '批发价格','(请输入产品批发价格)'],
                ['number', 'p_stock', '产品库存', '(请输入产品库存)'],
                ['radio', 'p_setup_status', '上下架状态', '请选择产品上下架',['下架', '上架'], 0],
            ])
            ->setFormData($data)
            ->layout(['p_title' => 8, 'p_retail_price' => 8, 'p_whole_price' => 8, 'p_stock' => 8, 'p_status' => 8])
            ->fetch();
    }

    // 删除置换产品
    public function exchange_product_del($id = 0)
    {

        if(empty($id)){
            $this->error('参数错误');
        }

        $product = Db::name('exchange_product')->where('id', $id)->find();

        if(!empty($product)){
            $res = Db::name('exchange_product')->where('id', $id)->delete();
            if($res == 1){
                do_alogs('删除置换产品信息');
                $this->success('删除成功');
            }else{
                $this->error('删除失败');
            }
        }else{
            $this->error('参数错误');
        }

    }




}