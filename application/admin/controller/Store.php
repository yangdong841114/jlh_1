<?php
namespace app\admin\controller;
use app\common\builder\ZBuilder;
use think\Db;
use app\admin\model\Order;

class Store extends Admin
{
    //仓库列表
    public function store_list(){
        do_alogs('查看仓库列表');
        $map        = $this->getMap();
        $order      = $this->getOrder('s_addtime desc');
        $data_list  = Db::name('Store')->where($map)->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        // 授权按钮
        $btn_access = [
            'title' => '设置',
            'icon'  => 'fa fa-fw fa-key',
            'href'  => url('store_product_list', ['sid' => '__id__'])
        ];
        $s_level    = array('1'=>'初级仓库','2'=>'中级仓库','3'=>'高级仓库');
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('cate')
            ->setSearch(['id' => 'ID', 's_title' => '仓库名称','s_code'=>'仓库编号']) // 设置搜索参数
            ->addTimeFilter('last_time','','开始时间,结束时间') // 添加时间段筛选
            ->addTopSelect('s_level', '',$s_level)
            ->addColumns([                                                                // 批量添加数据列
                ['id', 'ID'],
                ['s_title', '仓库名称', 'text'],
                ['s_code', '仓库编号', 'text'],
                ['s_info', '仓库备注', 'text'],
                ['s_level','等级','status',$s_level],
                ['s_city', '仓库地址','text'],
                ['s_reg_price', '注册资本','text'],
                ['s_invest_price', '投资额度','text'],
                ['s_name', '仓库法人','text'],
                ['s_phone', '联系电话','text'],
                ['s_company', '所属单位','text'],
                ['s_status', '状态','status','',['否','是']],
                ['s_addtime','创建时间','date'],
                ['last_time','更新时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time'=>150,'id'=>60,'s_phone'=>120])
            ->setExtraCss($css)
            ->hideCheckbox()
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,s_addtime')                                                               //添加排序
            ->addTopButton('add',['href' => url('store_add')])
            ->addRightButtons(
                ['edit'  => ['title' => '编辑','href'=>url('store_edit', ['id'=>'__id__'])],
                 'delete'=> ['title' => '删除','href'=>url('store_del', ['id'=>'__id__'])],
                ])
            ->addRightButton('custom', $btn_access) // 添加授权按钮
            ->setRowList($data_list) // 设置表格数据
            ->fetch();               // 渲染模板
    }

    //仓库添加
    public function store_add(){
        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];
        $config         = $this->set_config();
        $s_level        = array('1'=>'初级仓库','2'=>'中级仓库','3'=>'高级仓库');
        if($this->request->post()){
            do_alogs('添加仓库信息');
            $fields     = input('post.');
            if(!$fields['s_title']){
                $this->error('仓库名称 不能为空');
            }
            if(!$fields['s_code']){
                $this->error('仓库编号 不能为空');
            }
            $has_store = Db::name('Store')->where(array('s_code'=>$fields['s_code']))->find();
            if(!empty($has_store)){
                $this->error('仓库编号 已存在');
            }
            if(!$fields['s_city']){
                $this->error('仓库地址 不能为空');
            }
            if(!$fields['s_address']){
                $this->error('详细地址 不能为空');
            }
            $fields['s_addtime']       = time();
            $fields['admin_id']        = $admin_id;
            $res_id = Db::name('Store')->insertGetId($fields);
            if($res_id){
                $this->success('仓库添加成功',url('Store/store_list'));
            }else{
                $this->error('仓库添加失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('新增')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['text', 's_title', '仓库名称','(请输入仓库名称)'],
                ['text', 's_code', '仓库编号','(请输入仓库编号)'],
                ['select', 's_level', '等级','(请选择仓库等级)',$s_level,1],
                ['number', 's_reg_price', '注册资本','(请输入注册资本)'],
                ['number', 's_invest_price', '投资额度','(请输入投资额度)'],
                ['text', 's_name', '法人','(请输入法人姓名)'],
                ['text', 's_phone', '联系电话','(请输入联系电话)'],
                ['text', 's_company', '所属单位','(请输入所属单位)'],
                ['text', 's_city', '仓库地址','(请输入仓库地址)'],
                ['text', 's_address', '详细地址','(请输入仓库详细地址)'],
                ['radio', 's_status','状态','请选择状态',['暂停','启用'],1],
                ['textarea:6', 's_info', '仓库备注','(请输入仓库备注)'],
                ['ckeditor', 's_desc', '仓库详情','(请输入仓库详情)'],
            ])
            ->layout(['s_title' => 3, 's_info' => 6,'s_code'=>2,'s_level'=>2,'s_city'=>3,'s_address'=>4,'s_reg_price'=>2,
                      's_invest_price'=>2,'s_name'=>2,'s_phone'=>2,'s_company'=>2,'s_desc'=>7,])
            ->fetch();
    }


    //仓库设置
    public function store_edit($id=''){
        $data           = Db::name('Store')->where(array('id'=>$id))->find();
        $s_level        = array('1'=>'初级仓库','2'=>'中级仓库','3'=>'高级仓库');
        if($this->request->post()){
            do_alogs('编辑仓库信息');
            $fields     = input('post.');
            if(!$fields['s_title']){
                $this->error('仓库名称 不能为空');
            }
            if(!$fields['s_code']){
                $this->error('仓库编号 不能为空');
            }
            $has_store = Db::name('Store')->where("id<>$id")->where(array('s_code'=>$fields['s_code']))->find();
            if(!empty($has_store)){
                $this->error('仓库编号 已存在');
            }
            if(!$fields['s_city']){
                $this->error('仓库地址 不能为空');
            }
            if(!$fields['s_address']){
                $this->error('详细地址 不能为空');
            }
            $fields['last_time']        = time();
            $res_id = Db::name('Store')->where(array('id'=>$id))->update($fields);
            if($res_id){
                $this->success('仓库编辑成功',url('Store/store_list'));
            }else{
                $this->error('仓库编辑失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('编辑')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden','id'],
                ['text', 's_title', '仓库名称','(请输入仓库名称)'],
                ['text', 's_code', '仓库编号','(请输入仓库编号)'],
                ['select', 's_level', '等级','(请选择仓库等级)',$s_level,1],
                ['number', 's_reg_price', '注册资本','(请输入注册资本)'],
                ['number', 's_invest_price', '投资额度','(请输入投资额度)'],
                ['text', 's_name', '法人','(请输入法人姓名)'],
                ['text', 's_phone', '联系电话','(请输入联系电话)'],
                ['text', 's_company', '所属单位','(请输入所属单位)'],
                ['text', 's_city', '仓库地址','(请输入仓库地址)'],
                ['text', 's_address', '详细地址','(请输入仓库详细地址)'],
                ['radio', 's_status','状态','请选择状态',['暂停','启用'],1],
                ['textarea:6', 's_info', '仓库备注','(请输入仓库备注)'],
                ['ckeditor', 's_desc', '仓库详情','(请输入仓库详情)'],
            ])
            ->setFormData($data)
            ->layout(['s_title' => 3, 's_info' => 6,'s_code'=>2,'s_level'=>2,'s_city'=>3,'s_address'=>4,'s_reg_price'=>2,
                's_invest_price'=>2,'s_name'=>2,'s_phone'=>2,'s_company'=>2,'s_desc'=>7,])
            ->fetch();
    }

    //仓库商品列表
    public function store_product_list($sid=''){
        do_alogs('查看仓库产品信息');
        $map        = $this->getMap();
        $order      = $this->getOrder('a.last_time desc');
        $sql        = 1;
        $s_name     = '全部仓库产品';
        if($sid){
            $sql   .= " and sid=$sid";
            $store  = Db::name('Store')->where(array('id'=>$sid))->find();
            $s_name = $store['s_title'].'产品';
        }
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        if($admin_id>1 and $role==2){
            $sql    .= " and admin_id=$admin_id";
        }
        $data_list  = Db::name('Store_product a')->join('w_product b','b.id=a.pid','left')->join('w_store c','c.id=a.sid','left')->join('w_user d','d.id=a.uid','left')->where($sql)->where($map)->field('a.*,b.p_title,c.s_title,d.m_nickname')->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";
        // 授权按钮
        $btn_up = [
            'title' => '出入库',
            'icon'  => 'fa fa-fw fa-key',
            'href'  => url('store_pro_setup', ['id' => '__id__','sid'=>$sid])
        ];
        $btn_charge  = [
            'title' => '出入库明细',
            'icon'  => 'fa fa-fw fa-exchange',
            'href'  => url('store_slog', ['id' => '__id__'])
        ];
        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle($s_name)
            ->setTableName('store_product')
            ->setSearch(['s_title' => '仓库名称','s_code'=>'仓库编号']) // 设置搜索参数
            ->addTimeFilter('last_time','','开始时间,结束时间') // 添加时间段筛选
            ->addColumns([                                                                // 批量添加数据列
                ['id', 'ID'],
                ['p_title', '产品名称', 'text'],
                ['s_title', '所属仓库名称', 'text'],
                ['m_nickname', '交易商名称', 'text'],
                ['u_account', '所属交易商账号', 'text'],
                ['sp_num','产品数量','text'],
                ['sp_total', '入库总量','text'],
                ['sp_unit', '产品单位','text.edit'],
                ['sp_addtime','创建时间','date'],
                ['last_time','更新时间','datetime'],
                ['right_button', '操作', 'btn']
            ])
            ->setColumnWidth(['last_time'=>150,'id'=>60,'s_phone'=>120])
            ->setExtraCss($css)
            ->hideCheckbox()
            ->setPrimaryKey('id')
            ->addOrder('id,last_time,sp_addtime')                                                               //添加排序
            ->addTopButton('add',['title'=>'入库商品','href' => url('store_pro_add',['sid'=>$sid])])
            ->addRightButton('custom', $btn_up) // 添加出入库按钮
            ->addRightButton('custom', $btn_charge) // 添加出入库明细按钮
            ->setRowList($data_list) // 设置表格数据
            ->fetch();               // 渲染模板
    }

    //商品入库
    public function store_pro_add($sid=''){
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        $store      = Db::name('store')->order('last_time desc')->select();
        if($sid){
            $store      = Db::name('store')->where(array('id'=>$sid))->order('last_time desc')->select();
        }
        $store_arr  = array();
        foreach ($store as $k=>$v){
            $store_arr[$v['id']]=$v['s_title'];
        }
        $pro        = Db::name('Product')->where(array('p_del'=>0))->order('last_time desc')->select();
        $pro_arr    = array();
        foreach ($pro as $k=>$v){
            $pro_arr[$v['id']]=$v['p_title'];
        }
        if($this->request->post()){
            do_alogs('操作仓库产品添加入库');
            $fields     = input('post.');
            if(!$fields['sid']){
                $this->error('仓库 不能为空');
            }
            if(!$fields['pid']){
                $this->error('产品 不能为空');
            }
            if(!$fields['u_account']){
                $this->error('所属交易商 不能为空');
            }
            $u_account  = $fields['u_account'];
            $has_user   = Db::name('User')->where("m_phone=$u_account or m_account=$u_account or id=$u_account")->find();
            if(empty($has_user)){
                $this->error('所属交易商 不存在');
            }
            $fields['uid']  = $has_user['id'];
            $has_spro = Db::name('Store_product')->where(array('sid'=>$fields['sid'],'pid'=>$fields['pid'],'uid'=>$fields['uid']))->find();
            if(!empty($has_spro)){
                $this->error('当前商品已入库,操作库存即可');
            }
            if(!$fields['sp_num']){
                $this->error('产品数量 不能为空');
            }
            if(!$fields['sp_unit']){
                $this->error('产品单位 不能为空');
            }
            $fields['sp_total']          = $fields['sp_num'];
            $fields['sp_addtime']        = time();
            $fields['admin_id']          = $admin_id;
            $res_id = Db::name('Store_product')->insertGetId($fields);
            if($res_id){
                if(!$sid){
                    $this->success('产品添加成功',url('Store/store_product_list'));
                }else{
                    $this->success('产品添加成功',url('Store/store_product_list',['sid'=>$sid]));
                }
            }else{
                $this->error('产品添加失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('入库商品')              // 设置页面标题
            ->addFormItems([                        // 批量添加表单项
                ['select', 'sid', '仓库','(请选择仓库)',$store_arr],
                ['select', 'pid', '产品','(请选择产品)',$pro_arr],
                ['text', 'u_account', '所属交易商','(请输入所属交易商账号或手机号)'],
                ['number', 'sp_num', '产品数量','(请输入仓库产品数量)'],
                ['text', 'sp_unit', '产品单位','(请输入产品单位)'],
                ['textarea:6', 'sp_desc', '备注'],
            ])
            ->layout(['sid' => 3, 'pid' => 4,'u_account'=>2,'sp_num'=>2,'sp_unit'=>2,'sp_desc'=>5])
            ->fetch();
    }

    //仓库商品入库
    public function store_pro_setup($id='',$sid=''){
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        $store_pro  = Db::name('Store_product')->where(array('id'=>$id))->find();
        if($this->request->post()){
            do_alogs('操作仓库产品出入库');
            $fields = input('post.');
            $sp_num = $store_pro['sp_num'];
            if(!$fields['ls_num']){
                $this->error('请输入操作数量');
            }
            if($fields['ls_type'] == 1){
                $ls_desc = '系统入库操作';
                if($fields['ls_num']+$sp_num <= 0){
                    $this->error('入库操作数量设置有误');
                }
                $num     = $sp_num+$fields['ls_num'];
            }elseif ($fields['ls_type'] == 2){
                $ls_desc = '系统出库操作';
                if($sp_num-$fields['ls_num'] < 0){
                    $this->error('出库操作数量设置有误');
                }
                $num     = $sp_num-$fields['ls_num'];
            }
            $set_data = array(
                'sp_num'        => $num,
                'last_time'     => time(),
            );
            if($fields['ls_type']==1){
                $set_data['sp_total'] = $store_pro['sp_total']+$fields['ls_num'];
            }
            if($fields['ls_desc']){
                $ls_desc = $ls_desc.':'.$fields['ls_desc'];
            }
            $res = Db::name('Store_product')->where(array('id'=>$id))->update($set_data);
            if($res){
                do_slogs(0,$store_pro['sid'],$store_pro['pid'],$store_pro['id'],$fields['ls_type'],$fields['ls_num'],$ls_desc,$admin_id);
                if($sid){
                    $this->success('操作成功',url('Store/store_product_list',['sid'=>$sid]));
                }else{
                    $this->success('操作成功',url('Store/store_product_list'));
                }

            }else{
                $this->error('操作失败');
            }
        }
        return ZBuilder::make('form')
            ->setPageTitle('出入库')                  // 设置页面标题
            ->addFormItems([                         // 批量添加表单项
                ['radio', 'ls_type', '类型','(请选择操作类型)',['1'=>'入库','2'=>'出库'],1],
                ['number', 'ls_num', '数量','(请输入出入库数量)'],
                ['textarea:6', 'ls_desc', '备注'],
            ])
            ->layout(['ls_type' => 2, 'ls_num' => 2])
            ->fetch();
    }

    //出入库明细
    public function store_slog($id=''){
        do_alogs('查看仓库产品列表');
        $map        = $this->getMap();
        $order      = $this->getOrder('a.ls_addtime desc');
        $admin_id   = session('user_auth')['uid'];
        $role       = session('user_auth')['role'];
        $sql        = 1;
        if($id){
            $sql    .= " and ls_spid=$id";
        }
        $data_list  = Db::name('Slogs a')->join('w_product b','b.id=a.ls_pid','left')->join('w_store c','c.id=a.ls_sid','left')->where($sql)->where($map)->field('a.*,b.p_title,c.s_title')->order($order)->limit(20)->paginate();
        $css        = "<style>
            tr,th,td{text-align: center;}
        </style>";

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setPageTitle('出入库明细')
            ->setTableName('store_product')
            ->setSearch(['s_title' => '仓库名称','p_title'=>'产品名称']) // 设置搜索参数
            ->addTimeFilter('ls_addtime','','开始时间,结束时间') // 添加时间段筛选
            ->addColumns([                                                                // 批量添加数据列
                ['id', 'ID'],
                ['p_title', '产品名称', 'text'],
                ['s_title', '仓库', 'text'],
                ['ls_type', '类型', 'status','',['1'=>'入库','2'=>'出库','3'=>'提货']],
                ['ls_num','操作数量','text'],
                ['ls_desc', '备注','text'],
                ['ls_addtime','创建时间','datetime'],
            ])
            ->setColumnWidth(['id'=>60,'s_phone'=>120])
            ->setExtraCss($css)
            ->hideCheckbox()
            ->setPrimaryKey('id')
            ->addOrder('id,ls_addtime')                                                               //添加排序
            ->setRowList($data_list) // 设置表格数据
            ->fetch();               // 渲染模板
    }

    // 获取用户地址
    public function getUserAddress($aid)
    {
        $address = Db::name('address')->where('id', $aid)->field('id,a_name,a_phone,a_city,a_detail')->find();
        if (empty($address)) {
            return '无';
        }
        return '姓名：'.$address['a_name'].'<br/>手机号：'.$address['a_phone'].'<br/>地址:'.$address['a_city'].'<br/>详细地址:'.$address['a_detail'];
    }


    // 发货订单列表
    public function send_good_list()
    {

        $map        = $this->getMap();
        $order      = $this->getOrder('id desc');

        $data_list = Order::alias('a')->join('user b', 'a.uid = b.id', 'LEFT')->where($map)->order('a.id desc')->field('a.*')->limit(20)->order($order)->paginate();

        defined('NOW') || define('NOW', time());
        if(isset($_SESSION['tblqry']))
		{
			$qrycache = $_SESSION['tblqry'];
			foreach($qrycache as $_k => $_v)
			{
				if($_v['expire'] < NOW) unset($qrycache[$_k]);
			}
		}
        else $qrycache = [];

        $uniqkey = uniqid();
        $qrycache[$uniqkey] = ['where' => $map, 'order' => $order,'expire' => NOW + 600];
        $_SESSION['tblqry'] = $qrycache;

        foreach ($data_list as &$row) {

            $row['user_info'] = $this->getUser($row['uid']);
            $row['address'] = $this->getUserAddress($row['aid']);
            if($row['o_exchange_type'] == 1){
                $pro_info = Db::name('product')->where('id', $row['gid'])->field('p_title, p_code')->find();
                if(!empty($pro_info)){
                    $row['p_title'] = $pro_info['p_title'].' - '.$pro_info['p_code'];
                }else{
                    $row['p_title'] = '';
                }

            }elseif($row['o_exchange_type'] == 2){
                $pro_info = Db::name('exchange_product')->where('id', $row['gid'])->field('p_title, p_code')->find();
                if(!empty($pro_info)){
                    $row['p_title'] = $pro_info['p_title'].' - '.$pro_info['p_code'];
                }else{
                    $row['p_title'] = '';
                }
            }
            
        }

        $btn_edit_send_good_status  = [
            'title' => '发货',
            'icon'  => 'fa fa-fw fa-mouse-pointer',
            'href'  => url('edit_send_good_status', ['id' => '__id__'])
        ];

        // 使用ZBuilder快速创建数据表格
        return ZBuilder::make('table')
            ->setTableName('order')
            ->setPageTitle('发货订单列表')
            ->setSearch(['b.m_account'=>'交易账号'])
            ->addTopSelect('o_type', '订单类型',[1 => '积分提货', 2 => '持仓提货'])
            ->addTopSelect('o_status', '订单状态', [0 => '待付款', 1 => '待发货', 2 => '待收货', 3 => '已完成'])
            ->addTopButton('export',['title' => '导出', 'class' => 'btn btn-success confirm', 'href' => url('send_good_list_export', ['uniqkey' => $uniqkey])])
            ->addTimeFilter('o_addtime','','开始时间,结束时间') // 添加时间段筛选
            ->addColumns([
                ['id', 'ID'],
                ['p_title', '产品名称', 'text'],
                ['user_info', '用户信息', 'text'],
                ['address', '用户地址', 'text'],
                ['o_type', '订单类型', 'status', '', [1 => '积分提货', 2 => '持仓提货']],
                ['o_buy_num', '购买数量', 'text'],
                ['o_price', '商品单价', 'text'],
                ['o_credit_1', '订单总价', 'text'],
                ['o_company_num', '物流单号', 'text'],
                ['o_status', '订单状态', 'status', '', [0 => '待付款', 1 => '待发货', 2 => '待收货', 3 => '已完成']],
                ['o_addtime', '下单时间', 'datetime'],
                ['right_button', '操作', 'btn']
            ])
            // ->hideCheckbox()
            ->setPrimaryKey('id')
            ->addRightButton('o_status', $btn_edit_send_good_status, ['area' => ['500px', '300px']])
            ->replaceRightButton(['o_status' => ['NEQ', 1]], '', 'o_status')
            ->addTopButton('delete', ['title'=>'待付款', 'href'=>url('send_out_good_status', ['status'=>0]), 'class'=>'btn btn-default ajax-post', 'icon'=>''])
            ->addTopButton('delete', ['title'=>'待发货', 'href'=>url('send_out_good_status', ['status'=>1]), 'class'=>'btn btn-primary ajax-post', 'icon'=>''])
            ->addTopButton('delete', ['title'=>'待收货', 'href'=>url('send_out_good_status', ['status'=>2]), 'class'=>'btn btn-info ajax-post', 'icon'=>''])
            ->addTopButton('delete', ['title'=>'已完成', 'href'=>url('send_out_good_status', ['status'=>3]), 'class'=>'btn btn-success ajax-post', 'icon'=>''])
            ->setRowList($data_list)
            ->fetch();

    }


    // 发货单列表导出
    public function send_good_list_export()
    {

        $uniqkey = input('uniqkey');
		if(!empty($uniqkey) && isset($_SESSION['tblqry']))
		{
			$tblqry = $_SESSION['tblqry'];
			if(isset($tblqry[$uniqkey]))
			{
                
				$qrycache = $tblqry[$uniqkey];
				if(isset($qrycache['expire']) && $qrycache['expire'] > time()){

                    $data_list = Order::alias('a')->join('user b', 'a.uid = b.id', 'LEFT')->where($qrycache['where'])->order('a.id desc')->field('a.*')->order($qrycache['order'])->select();

                    $tileArray = [
                        'ID', '产品名称', '用户ID', '用户账号', '用户手机号', '收货姓名', '收货手机号', '收货地址', '订单类型', '购买数量', '商品单价', '订单总价', '物流单号', '订单状态', '下单时间'
                    ];

                    $dataArray = [];

                    $status_arr = [ 0 => '待付款', 1 => '待发货', 2 => '待收货', 3 => '已完成' ];

                    foreach ($data_list as $v) {
                        $user_info = Db::name('user')->where('id', $v['uid'])->field('id, m_account, m_phone')->find(); // 用户信息
                        $address = Db::name('address')->where(['uid' => $v['uid'], 'id' => $v['aid']])->find(); // 收货地址信息
                        $status_txt = $status_arr[$v['o_status']]; // 状态

                        // 产品名称
                        if($v['o_exchange_type'] == 1){
                            $pro_info = Db::name('product')->where('id', $v['gid'])->field('p_title, p_code')->find();
                            if(!empty($pro_info)){
                                $p_title = $pro_info['p_title'].' - '.$pro_info['p_code'];
                            }else{
                                $p_title = '';
                            }
            
                        }elseif($v['o_exchange_type'] == 2){
                            $pro_info = Db::name('exchange_product')->where('id', $v['gid'])->field('p_title, p_code')->find();
                            if(!empty($pro_info)){
                                $p_title = $pro_info['p_title'].' - '.$pro_info['p_code'];
                            }else{
                                $p_title = '';
                            }
                        }
                        // $p_title = Db::name('product')->where('id', $v['gid'])->value('p_title');
                        
                        $dataArray[] = [
                            'ID' => $v['id'],
                            '产品名称' => isset($p_title)?$p_title:'',
                            '用户ID' => $user_info['id'],
                            '用户账号' => filter_value($user_info['m_account']),
                            '用户手机号' => filter_value($user_info['m_phone']),
                            '收货姓名' => $address['a_name'],
                            '收货手机号' => filter_value($address['a_phone']),
                            '收货地址' => $address['a_city'].' '.$address['a_detail'],
                            '订单类型' => ($v['o_type'] == 1)?'积分提货':'持仓提货',
                            '购买数量' => $v['o_buy_num'],
                            '商品单价' => $v['o_price'],
                            '订单总价' => $v['o_credit_1'],
                            '物流单号' => filter_value($v['o_company_num']),
                            '订单状态' => $status_txt,
                            '下单时间' => date('Y-m-d H:i:s', $v['o_addtime']),
                        ];

                    }

                    // 导出csv文件
                    $filename = '发货单列表-' . date('YmdHis');
                    return export_csv($filename . '.csv', $tileArray, $dataArray);

                }
            }
        }





    }

    // 修改发货状态
    public function edit_send_good_status()
    {
        $param = $this->request->param();

        $info = Order::where('id', $param['id'])->find();

        if($this->request->post())
        {

            $time = time();

            $new_data = [
                'o_company' => $param['o_company'],
                'o_company_num' => $param['o_company_num'],
                'o_status' => 2,
                'o_send_time' => $time,
                'last_time' => $time
            ];

            $result = Order::where('id', $param['id'])->update($new_data);

            if($result){
                $this->success('发货成功', url('store/send_good_list'), '_parent_reload');
            }else{
                $this->error('发生未知错误，请稍后重试');
            }
        }

        return ZBuilder::make('form')
            ->setPageTitle('发货')          // 设置页面标题
            ->addFormItems([                // 批量添加表单项
                ['hidden', 'id'],
                ['text', 'o_company', '快递公司','（发货快递公司名称）'],
                ['text', 'o_company_num', '物流单号','（发货物流单号）'],
            ])
            ->setFormData($info)
            ->fetch();
    }


    // 修改订单状态
    public function send_out_good_status()
    {
        $param = $this->request->param();

        $admin_id       = session('user_auth')['uid'];
        $role           = session('user_auth')['role'];

        if(!isset($param['status'])) return $this->error('缺少参数');
        if(empty($param['ids'])) return $this->error('请选择需要操作的订单');

        $result = Db::name('order')->where('id', 'in', $param['ids'])->update(['o_status' => $param['status']]);
        if($result){
            // 添加日志
            // var_export($param['ids'], true)
            $_deac = '修改订单，ids为：'.implode(',', $param['ids']).'，状态修改为'.$param['status'];
            $log_info = [
                'admin_id' => $admin_id,
                'role_id' => 0,
                'last_time' => time(),
                'a_desc' => $_deac,
            ];
            Db::name('Alogs')->insert($log_info);
            return $this->success('修改失败', url('send_good_list'));
        }else{
            return $this->error('操作失败');
        }
    }

}