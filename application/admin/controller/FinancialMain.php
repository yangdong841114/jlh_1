<?php
/**
 * 财务维护相关
 */
namespace app\admin\controller;

use think\Db;
use app\common\builder\ZBuilder;
use think\Validate;

class FinancialMain extends Admin
{

    // 快速录入模板
    public function mould_list()
    {
        $map = $this->getMap();
        $order = $this->getOrder('id asc');
        $data_list = Db::name('sw_financial_mould')->where($map)->order($order)->limit(20)->paginate();

        return ZBuilder::make('table')
            ->setTableName('sw_financial_mould')
            ->setSearch(['id' => '模板代码', 'mould_name' => '模板名称'])
            ->addColumns([
                ['id', '模板代码'],
                ['mould_name', '模板名称', 'text'],
                ['abstract_num', '摘要号', 'text'],
                ['debit_subject_code', '借方科目代码', 'text'],
                ['credit_subject_code', '贷方科目代码', 'text'],
                ['is_contract', '需要合同号', 'status', '', ['否','是']],
                ['remark', '备注', 'text'],
            ])
            ->addTopButton('add', ['href' => url('add_mould')])
            ->addTopButton('delete', ['href' => url('del_mould')]) // 删除
            // ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();
    }

    // 添加模板
    public function add_mould()
    {

        if($this->request->post()){

            $param = $this->request->post();

            $rule = [
                'mould_name' => 'require|max:25',
                'is_contract' => 'require|in:0,1',
                'debit_subject_code' => 'require|alphaNum',
                'credit_subject_code' => 'require|alphaNum',
                'abstract_num' => 'require|number',
                'remark' => 'max:200',
            ];
            $msg = [
                'mould_name.require' => '请填写模板名称',
                'mould_name.max' => '模板名称不能超过25字符',
                'is_contract.require' => '请选择是否需要合同号',
                'is_contract.in' => '是否需要合同号格式不正确',
                'debit_subject_code.require' => '请填写借方科目代码',
                'debit_subject_code.alphaNum' => '借方科目代码只能输英文或者数字',
                'credit_subject_code.require' => '请填写贷方科目代码',
                'credit_subject_code.alphaNum' => '贷方科目代码只能输英文或者数字',
                'abstract_num.require' => '请填写摘要号',
                'abstract_num.number' => '摘要号只能是数字',
                'remark.max' => '备注不能超过200字符',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($param)){
                $this->error($validate->getError());
            }
            // 判断摘要号是否存在
            $abstract_num = Db::name('sw_financial_abstract')->where('abstract_num', $param['abstract_num'])->find();
            if(!$abstract_num){
                $this->error('摘要号不存在');
            }
            // 判断借方科目代码 和 贷方科目代码 是否存在
            $dsc_where = [
                'subject_code' => $param['debit_subject_code'],
                // 'lending_direction' => 1
            ];
            $debit_subject_code = Db::name('sw_financial_subject')->where($dsc_where)->find();
            if(!$debit_subject_code){
                $this->error('借方科目代码不存在');
            }
            $csc_where = [
                'subject_code' => $param['credit_subject_code'],
                // 'lending_direction' => 2
            ];
            $credit_subject_code = Db::name('sw_financial_subject')->where($csc_where)->find();
            if(!$credit_subject_code){
                $this->error('贷方科目代码不存在');
            }

            $param['create_time'] = time();
            $param['update_time'] = time();

            $reault = Db::name('sw_financial_mould')->insert($param);
            if($reault){
                $this->success('添加成功', url('FinancialMain/mould_list'));
            }else{
                $this->error('添加失败');
            }
        }

        return ZBuilder::make('form')
            ->setPageTitle('新增')
            ->addFormItems([
                ['text', 'mould_name', '模板名称', '(请输入模板名称)'],
                ['text', 'abstract_num', '摘要号', '(请输入摘要号,只能是数字)'],
                ['radio', 'is_contract', '需要合同号', '', ['否','是'], 0],
                ['text', 'debit_subject_code', '借方科目代码', '(请输入借方科目代码)'],
                ['text', 'credit_subject_code', '贷方科目代码', '(请输入贷方科目代码)'],
                ['textarea', 'remark', '备注'],
            ])
            ->layout(['abstract_num' => 3, 'debit_subject_code' => 3, 'credit_subject_code' => 3, 'is_contract' => 3])
            ->fetch();
    }

    // 删除模板
    public function del_mould()
    {
        if($this->request->post()){
            $param = $this->request->post();

            if(empty($param['ids'])) $this->error('请选择要操作的数据');

            $result = Db::name('sw_financial_mould')->delete($param['ids']);

            if($result){
                $this->success('删除成功');
            }else{
                $this->error('删除失败');
            }
        }
    }

    // 获取快速添加模板的详情
    public function get_mould_info()
    {
        if($this->request->post()){
            $param = $this->request->param();
            if(empty($param['id'])) $this->error('请选择要查询的模板id');

            $where[] = ['a.id', '=', $param['id']];
            $join = [
                ['sw_financial_abstract b', 'a.abstract_num = b.abstract_num', 'left'],
                ['sw_financial_subject c', 'a.debit_subject_code = c.subject_code', 'left'],
                ['sw_financial_subject d', 'a.credit_subject_code = d.subject_code', 'left'],
            ];

            $field = 'a.*, b.abstract_name, c.subject_name as debit_subject_name, d.subject_name as credit_subject_name';

            $info = Db::name('sw_financial_mould')->alias('a')->join($join)->where($where)->field($field)->find();

            if($info){
                $this->success('获取成功', '', $info);
            }else{
                $this->error('选择的模板信息不存在');
            }
        }
    }


    // 摘要维护
    public function abstract_main()
    {

        $map = $this->getMap();
        $order = $this->getOrder('id asc');
        $data_list = Db::name('sw_financial_abstract')->where($map)->order($order)->limit(20)->paginate();

        // 归入总账
        $classification = [
            0 => '', 1 => '入金', 2 => '出金', 3 => '银转手续费', 4 => '当日其他项', 5 => '挂牌转让市值变动', 6 => '挂牌销售收入', 7 => '挂牌购货支出', 8 => '挂牌交易手续费', 9 => '挂牌当日其他项', 10 => '挂牌卖出杂费', 11 => '挂牌交货费用', 12 => '商城提现', 13 => '商城充值', 14 => '订单订金变动', 15 => '订单货值变化变动', 16 => '订单交收订金变动', 17 => '订单利润', 18 => '订单交收货值变化', 19 => '订单销售收入', 20 => '订单购货支出', 21 => '订单交易手续费', 22 => '订单交收手续费', 23 => '订单交割补偿费', 24 => '订单当日其他项'
        ];
        // 交易商资金借贷方向
        $transaction_direction = [
            1 => '记借方', 2 => '记贷方', 3 => '不涉及'
        ];
        // 附加帐
        $additional_account = [
            1 => '增值税', 2 => '担保金', 3 => '交收保证金', 4 => '无附加'
        ];

        return ZBuilder::make('table')
            ->setTableName('sw_financial_abstract')
            ->setSearch(['abstract_num' => '摘要号', 'abstract_name' => '摘要名称'])
            ->addTopSelect('classification', '归入总账', $classification)
            ->addColumns([
                ['id', 'ID'],
                ['abstract_num', '摘要号', 'text'],
                ['abstract_name', '摘要名称', 'text'],
                ['subject_code', '双方科目代码', 'text'],
                ['classification', '归入总账', 'status', '', $classification],
                ['transaction_direction', '交易商资金借贷方向', 'status', '', $transaction_direction],
                ['additional_account', '附加帐', 'status', '', $additional_account],
                ['is_init', '是否初始化', 'status', '', ['否', '是']],
            ])
            ->addTopButton('add', ['href' => url('add_abstract_main')]) // 添加
            ->addTopButton('delete', ['href' => url('del_abstract_main')]) // 删除
            // ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();
    }

    // 添加摘要维护
    public function add_abstract_main()
    {

        // 归入总账
        $classification = [
            1 => '入金', 2 => '出金', 3 => '银转手续费', 4 => '当日其他项', 5 => '挂牌转让市值变动', 6 => '挂牌销售收入', 7 => '挂牌购货支出', 8 => '挂牌交易手续费', 9 => '挂牌当日其他项', 10 => '挂牌卖出杂费', 11 => '挂牌交货费用', 12 => '商城提现', 13 => '商城充值', 14 => '订单订金变动', 15 => '订单货值变化变动', 16 => '订单交收订金变动', 17 => '订单利润', 18 => '订单交收货值变化', 19 => '订单销售收入', 20 => '订单购货支出', 21 => '订单交易手续费', 22 => '订单交收手续费', 23 => '订单交割补偿费', 24 => '订单当日其他项'
        ];
        // 交易商资金借贷方向
        $transaction_direction = [
            1 => '记借方', 2 => '记贷方', 3 => '不涉及'
        ];
        // 附加帐
        $additional_account = [
            1 => '增值税', 2 => '担保金', 3 => '交收保证金', 4 => '无附加'
        ];

        if($this->request->post()){

            $param = $this->request->post();

            $rule = [
                'abstract_num' => 'require|number|length:5|unique:sw_financial_abstract',
                'abstract_name' => 'require|max:25',
                'subject_code' => 'max:25',
                'classification' => 'between:1,24',
                'transaction_direction' => 'require|in:1,2,3',
                'additional_account' => 'require|in:1,2,3,4',
            ];
            $msg = [
                'abstract_num.require' => '请填写摘要号',
                'abstract_num.number' => '摘要号只能是数字',
                'abstract_num.length' => '摘要号长度只能是5位',
                'abstract_num' => '摘要号已存在',
                'abstract_name.require' => '请填写摘要名称',
                'abstract_name.max' => '摘要名称不能超过25位',
                'subject_code.max' => '双方科目代码不能超过25位',
                // 'classification.require' => '请选择归入总账',
                'classification.between' => '归入总账格式错误',
                'transaction_direction.require' => '请选择交易商资金借贷方向',
                'transaction_direction.in' => '交易商资金借贷方向格式不正确',
                'additional_account.require' => '请选择附加帐',
                'additional_account.in' => '附加帐格式不正确',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($param)){
                $this->error($validate->getError());
            }

            $param['create_time'] = time();
            $param['update_time'] = time();

            $result = Db::name('sw_financial_abstract')->insert($param);
            if($result){
                $this->success('添加成功', url('abstract_main'));
            }else{
                $this->error('添加失败');
            }

        }

        return ZBuilder::make('form')
            ->setPageTitle('新增')
            ->addFormItems([
                ['text', 'abstract_num', '摘要号', '(请输入摘要号,只能是数字)'],
                ['text', 'abstract_name', '摘要名称', '(请输入摘要名称)'],
                ['text', 'subject_code', '双方科目代码', '(请输入双方科目代码)'],
                ['select', 'classification', '归入总账', '(请选择归入总账)', $classification],
                ['select', 'transaction_direction', '交易商资金借贷方向', '(请选择交易商资金借贷方向)', $transaction_direction],
                ['select', 'additional_account', '附加帐', '(请选择附加帐)', $additional_account],
            ])
            ->layout(['abstract_num' => 6, 'abstract_name' => 6, 'subject_code' => 6, 'classification' => 6, 'transaction_direction' => 6, 'additional_account' => 6])
            ->fetch();
    }

    // 删除摘要维护
    public function del_abstract_main()
    {

        if($this->request->post()){
            $param = $this->request->post();

            if(empty($param['ids'])) $this->error('请选择要操作的数据');

            $result = Db::name('sw_financial_abstract')->delete($param['ids']);

            if($result){
                $this->success('删除成功');
            }else{
                $this->error('删除失败');
            }
        }
    }

    // 获取摘要详情
    public function get_abstract()
    {

        $abstract_num = $this->request->param('abstract_num');

        $info = Db::name('sw_financial_abstract')->where('abstract_num', $abstract_num)->field('id, abstract_num, abstract_name')->find();

        $this->success('成功', '', $info);

    }



    // 科目维护
    public function subject_main()
    {
        $map = $this->getMap();
        $order = $this->getOrder('id asc');
        $data_list = Db::name('sw_financial_subject')->where($map)->order($order)->limit(20)->paginate();

        $lending_direction = [ 1 => '借方', 2 => '贷方'];
        $subject_level = [ 1 => '1', 2 => '2', 3 => '3', 4 => '4'];

        return ZBuilder::make('table')
            ->setTableName('sw_financial_subject')
            ->setSearch(['subject_code' => '科目代码', 'subject_name' => '科目名称'])
            ->addTopSelect('lending_direction', '借贷方向', $lending_direction)
            ->addTopSelect('subject_level', '科目方向', $subject_level)
            ->addColumns([
                ['id', 'ID'],
                ['subject_code', '科目代码', 'text'],
                ['subject_name', '科目名称', 'text'],
                ['lending_direction', '借贷方向', 'status', '', $lending_direction],
                ['subject_level', '科目级别', 'text'],
                ['is_init', '是否初始化', 'status', '', ['否', '是']],
            ])
            ->addTopButton('add', ['href' => url('add_subject_main')]) // 添加
            ->addTopButton('delete', ['href' => url('del_subject_main')]) // 删除
            // ->hideCheckbox()
            ->setRowList($data_list)
            ->fetch();

    }

    // 添加科目维护
    public function add_subject_main()
    {

        $lending_direction = [ 1 => '借方', 2 => '贷方'];
        $subject_level = [ 1 => '1', 2 => '2', 3 => '3', 4 => '4'];
        
        if($this->request->post()){

            $param = $this->request->post();

            $rule = [
                'subject_code' => 'require|max:25|alphaNum|unique:sw_financial_subject',
                'subject_name' => 'require|max:25',
                'lending_direction' => 'require|in:1,2',
                'subject_level' => 'require|in:1,2,3,4',
            ];
            $msg = [
                'subject_code.require' => '请填写科目代码',
                'subject_code.max' => '科目代码不能超过25位',
                'subject_code.alphaNum' => '科目代码格式不正确',
                'subject_code.unique' => '科目代码已存在',
                'subject_name.require' => '科目名称不能为空',
                'subject_name.max' => '科目名称不能超过25位',
                'lending_direction.require' => '请选择借贷方向',
                'lending_direction.in' => '借贷方向格式不正确',
                'subject_level.require' => '请选择科目级别',
                'subject_level.in' => '科目级别格式不正确',
            ];
            $validate = new Validate($rule, $msg);
            if(!$validate->check($param)){
                $this->error($validate->getError());
            }

            $param['create_time'] = time();
            $param['update_time'] = time();

            $result = Db::name('sw_financial_subject')->insert($param);
            if($result){
                $this->success('添加成功', url('subject_main'));
            }else{
                $this->error('添加失败');
            }

        }
        
        return ZBuilder::make('form')
            ->setPageTitle('新增')
            ->addFormItems([
                ['text', 'subject_code', '科目代码', '(只能输入英文或数字)'],
                ['text', 'subject_name', '科目名称', '请输入科目名称'],
                ['select', 'lending_direction', '借贷方向', '', $lending_direction],
                ['select', 'subject_level', '科目级别', '', $subject_level]
            ])
            ->layout(['subject_code' => 6, 'subject_name' => 6, 'lending_direction' => 6, 'subject_level' => 6])
            ->fetch();

    }

    // 删除科目维护
    public function del_subject_main()
    {
        if($this->request->post()){
            $param = $this->request->post();

            if(empty($param['ids'])) $this->error('请选择要操作的数据');

            $result = Db::name('sw_financial_subject')->delete($param['ids']);

            if($result){
                $this->success('删除成功');
            }else{
                $this->error('删除失败');
            }
        }

    }

    // 获取科目详情
    public function get_subject()
    {
        $subject_code = $this->request->param('subject_code');

        $info = Db::name('sw_financial_subject')->where('subject_code', $subject_code)->field('id, subject_code, subject_name')->find();

        $this->success('成功', '', $info);
    }



    // 财务结算
    public function financial_settlement()
    {

        return $this->fetch();
    }







}
